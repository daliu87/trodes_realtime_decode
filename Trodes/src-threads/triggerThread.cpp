/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "triggerThread.h"
#include "globalObjects.h"
#include "trodesSocket.h"



nTrodeStream::nTrodeStream(int numOfChannels) {


    bufferSize = 4096;
    numChannels = numOfChannels;
    numSamplesBelowThresh = 0;
    totalTriggerState = false;
    lastTriggerTime = 0;
    dataBuffer = new int16_t *[numChannels];

    writeBufferPosition.resize(numChannels);
    readBufferPosition.resize(numChannels);
    thresholds.resize(numChannels);
    triggersOn.resize(numChannels);
    triggerStates.resize(numChannels);
    samplesWritten.resize(numChannels);
    samplesRead.resize(numChannels);

    if (REPORTDELAYS) {
        stopWatch.start();
        stopWatchValues.resize(bufferSize);
    }

    timeBuffer = new uint32_t[bufferSize];
    for (int i = 0; i < numChannels; i++) {

        writeBufferPosition[i] = 0;
        readBufferPosition[i] = 0;
        thresholds[i] = 0;
        triggersOn[i] = true;
        triggerStates[i] = false;
        dataBuffer[i] = new int16_t[bufferSize];
        samplesWritten[i] = 0;
        samplesRead[i] = 0;
    }

}

nTrodeStream::~nTrodeStream() {
    for (int i = 0; i < numChannels; i++) {
        delete [] dataBuffer[i];
    }
    delete [] timeBuffer;
    delete [] dataBuffer;
}

void nTrodeStream::writeData(int channel, int value, uint32_t time) {

    if (channel == 0) {
        timeBuffer[writeBufferPosition[channel]] = time;
    }
    dataBuffer[channel][writeBufferPosition[channel]] = value;
    writeBufferPosition[channel] = (writeBufferPosition[channel]+1) % bufferSize;
    samplesWritten[channel]++;

    if (REPORTDELAYS) {
        stopWatchValues[writeBufferPosition[channel]] = stopWatch.nsecsElapsed();
    }
}

bool nTrodeStream::readData(int numSamples, int rewind, QVector<vertex2d>* samplePtr, int* peaks, QVector<qint16>* int16samplePtr) {

    int total = 0;
    int chunk = 0;
    bool success = false;
    int16_t *tmpMax = new int16_t[numChannels]; //stores the current max amplitude
    int16_t *tmpMin = new int16_t[numChannels]; //stores the current min amplitude

    if ((samplesWritten[0] - samplesRead[0] + rewind) > numSamples) {

        //First we rewind
        for (int i = 0; i < numChannels; i++) {
            readBufferPosition[i] = (readBufferPosition[i] - rewind) % bufferSize;
            if (readBufferPosition[i] < 0)
                readBufferPosition[i] += bufferSize;
            samplesRead[i] = samplesRead[i] - rewind;
            tmpMax[i] = 0;
            tmpMin[i] = 0;
        }

        //Then we collect the desired number of points
        while (numSamples - total > 0) {

            chunk = qMin((bufferSize - readBufferPosition[0]), numSamples - total);

            for (int i = 0; i < numChannels; i++) {


                for (int j = 0; j < chunk; j++) {
                    tmpMax[i] = qMax(tmpMax[i], dataBuffer[i][readBufferPosition[i]+j]);
                    tmpMin[i] = qMin(tmpMin[i], dataBuffer[i][readBufferPosition[i]+j]);
                    samplePtr[i][j+total].y = dataBuffer[i][readBufferPosition[i]+j];
                    int16samplePtr[i][j+total] = dataBuffer[i][readBufferPosition[i]+j];
                }

                readBufferPosition[i] = (readBufferPosition[i] + chunk) % bufferSize;
                samplesRead[i] += chunk;
            }

            total += chunk;
        }
        success = true;

        bool tmpTriggerState = false;
        for (int i = 0; i < numChannels; i++) {
            peaks[i] = tmpMax[i];
            peaks[i+numChannels] = tmpMin[i];
            if (triggersOn[i] && dataBuffer[i][readBufferPosition[i]] >= thresholds[i]) {
                tmpTriggerState = true;
            }
        }
        totalTriggerState = tmpTriggerState;
    }

    delete [] tmpMax;
    delete [] tmpMin;

    return success;
}

void nTrodeStream::setThresh(int channel, int newThresh) {

    thresholds[channel] = newThresh;

}

void nTrodeStream::setTriggerMode(int channel, bool triggerOn) {
    triggersOn[channel] = triggerOn;
}

bool nTrodeStream::findEvent() {
    //This function looks for a trigger event on the nTrode.  If a new trigger is found,
    //the function returns true.

    //Define the closest the event finder can get to the current write point in the buffer.
    //This is equal to the number of points in the waveform to collect after the trigger
    int pointsToTrail = POINTSINWAVEFORM-POINTSTOREWIND;

    bool reachedEnd = (samplesRead[0] >= (samplesWritten[0]-pointsToTrail));
    bool tmpTriggerState = false;
    bool newTrigger = false;

    //If one or more channels is already above the threshold,
    //scan foreward until all channels are below their thresholds

    while ((totalTriggerState) && (!reachedEnd)) {
        tmpTriggerState = false;
        for (int i = 0; i < numChannels; i++) {
            if (triggersOn[i] && (dataBuffer[i][readBufferPosition[i]] >= thresholds[i])) {
                tmpTriggerState = true;
            }
            readBufferPosition[i] = (readBufferPosition[i] + 1) % bufferSize;
            samplesRead[i]++;

        }
        totalTriggerState = tmpTriggerState;
        if (samplesRead[0] >= (samplesWritten[0]-pointsToTrail)) {
            reachedEnd = true;
            break;
        }
        if (!totalTriggerState) {
            break;
        } else {
            numSamplesBelowThresh = 0;
        }

    }


    //if all channels are below threshold, look for a new trigger
    //and stop if one is found

    while (!reachedEnd) {
        tmpTriggerState = false;
        for (int i = 0; i < numChannels; i++) {
            if (triggersOn[i] && dataBuffer[i][readBufferPosition[i]] >= thresholds[i]) {
                tmpTriggerState = true;
            }
            readBufferPosition[i] = (readBufferPosition[i] + 1) % bufferSize;
            samplesRead[i]++;

        }
        totalTriggerState = tmpTriggerState;

        if ((totalTriggerState) && (numSamplesBelowThresh > 3)) {
            newTrigger = true;
            numSamplesBelowThresh = 0;
            lastTriggerTime = timeBuffer[readBufferPosition[0]]; //remember the last trigger time
            break;
        } else if (!totalTriggerState){
            numSamplesBelowThresh++;
        }

        if (samplesRead[0] >= (samplesWritten[0]-pointsToTrail)) {
            reachedEnd = true;
            break;
        }

    }

    if (newTrigger && REPORTDELAYS) {
        //print the time it took from receiveing the triggering data point to calculating the entire waveform.
        qint64 delayTime = stopWatch.nsecsElapsed()-stopWatchValues[readBufferPosition[0]];
        if (delayTime > 1500000) {
             qDebug() << "Trigger delay: " << delayTime;
        }
    }
    return newTrigger; // true if all channels were below trigger, and at least one went above
}


Trigger::Trigger(QObject *parent, int trode, int numOfChannels) :
    QObject(parent)
{
    numChannels = numOfChannels;
    dataCounter = 0;
    nTrodeNum = trode;
    logFile = NULL;
    dataHandler = NULL;

    //pointsInWaveform = 40;
    //pointsToRewind = 10;

    //connect(this,SIGNAL(started()),this,SLOT(setUp())); //don't create data on heap until after thread has started

    //setPriority(QThread::HighestPriority);
    //QObject::moveToThread(this);
    //start();

}

void Trigger::setUp() {


    data = new nTrodeStream(numChannels);
    logFile = new QFile;
    waveForms.resize(numChannels);
    waveforms_int16.resize(numChannels);

    peaks.resize(numChannels*2);
    writeSpikesToDisk = false;
    for (int i = 0; i < numChannels; i++) {
        channelsHaveData.push_back(false);

        data->setThresh(i, spikeConf->ntrodes[nTrodeNum]->thresh_rangeconvert[i]);
        data->setTriggerMode(i,spikeConf->ntrodes[nTrodeNum]->triggerOn[i]);
        waveForms[i].resize(POINTSINWAVEFORM);
        waveforms_int16[i].resize(POINTSINWAVEFORM);
        peaks[i] = 0;
        peaks[i+numChannels] = 0; //trough info will be stored too
        for (int j = 0; j < POINTSINWAVEFORM; j++) {
            waveForms[i][j].x = j;
            waveForms[i][j].y = 0;
        }
    }
}

void Trigger::runLoop() {


    keepLooping = true;
    quitNow = false;
    while (keepLooping) {
        pullTimerExpired();
        QThread::usleep(CHECKINTERVAL_US); //wait for a little while before next trigger check
        if (quitNow == true) {
            keepLooping = false;
        }
    }
    closeLogFile();

    emit finished();
}

void Trigger::endTrigger() {
     //pullTimer->stop();
    keepLooping = false;

    //quit();
}

Trigger::~Trigger() {


    delete data;
    delete logFile;

    //pullTimer->stop();

   // delete(pullTimer);
    //quit();
    //wait();
}


void Trigger::addValue(int channel, int value, uint32_t time) {

    data->writeData(channel, value, time);

}

void Trigger::setThresh(int channel, int newThresh) {

    data->setThresh(channel, newThresh);

}

void Trigger::setTriggerMode(int channel, bool triggerOn) {
    data->setTriggerMode(channel,triggerOn);
}

/*void trigger::run()
{
    exec();
}*/

void Trigger::pullTimerExpired() {


    char *charPtr;
    bool continueChecking = true;
    if (data->samplesWritten[0]-data->samplesRead[0] > (data->bufferSize-5) ){
        qDebug() << "Spike detection buffer overrun!!";
        emit bufferOverrun();
    }
    while (continueChecking) {
        if (data->findEvent()) {
            //if an event was found, the stream stops at
            //the point when the threshold crossing happened
            //so that we can collect the data.  Then we continue
            //looking for more events until we reach the end of the
            //buffer.

            data->readData(POINTSINWAVEFORM,POINTSTOREWIND, waveForms.data(), peaks.data(), waveforms_int16.data());

            if ((dataHandler != NULL) && (dataHandler->isModuleDataStreamingOn())) {
                //Write the spike waveform and the timestamp to the socket
                QByteArray out;
                TrodesDataStream spikeOutStream(&out, QIODevice::WriteOnly);
                spikeOutStream.setVersion(TrodesDataStream::Qt_4_0);
                //charPtr = (char*)(&(data->lastTriggerTime));
                spikeOutStream << (quint32) data->lastTriggerTime;
                //Send spike waveform
                qint16 dataValue;

                for (int wavetimepoint = 0; wavetimepoint < waveforms_int16[0].size(); wavetimepoint++) {
                    for (int waveChannel = 0; waveChannel < waveforms_int16.size(); waveChannel++) {
                        //Write the waveform data for this timepoint and this channel
                        dataValue = waveforms_int16[waveChannel][wavetimepoint];
                        spikeOutStream << dataValue;
                    }
                }

                dataHandler->sendMessage(TRODESMESSAGE_NTRODESPIKE,out);

            }

            //emits the waveform data for all channels
            //peaks contains the peak amplitude for all channels, followed by the minimum amplitude for all channels
            //Normally, sending a pointer to another thread to data that is going to be overwritten by
            //this thread during the next event is a bad idea.  However, this particular signal/slot is connected via
            //a direct connection instead of a queued connection, which means that this thread will do the work
            //of copying the data before it moves on.
            emit triggerEvent(waveForms.constData(), peaks.constData());



            if (writeSpikesToDisk) {
                //Write the time stamp of the spike
                charPtr = (char*)(&(data->lastTriggerTime));
                outStream->writeRawData(charPtr,4);

                int16_t dataValue;
                for (int wavetimepoint = 0; wavetimepoint < waveforms_int16[0].size(); wavetimepoint++) {
                    for (int waveChannel = 0; waveChannel < waveforms_int16.size(); waveChannel++) {
                        //Write the waveform data for this timepoint and this channel
                        dataValue = waveforms_int16[waveChannel][wavetimepoint];
                        charPtr = (char*)(&dataValue);
                        outStream->writeRawData(charPtr,2);
                    }
                }
            }

        } else {
            continueChecking = false;
        }
    }
}

void Trigger::createLogFile(QString path) {


    QString fileName = path+QDir::toNativeSeparators("/")+QString("nTrode%1").arg(nTrodeNum+1)+".spikes";
    logFile->setFileName(fileName);
    if (logFile->exists()) {
        logFile->remove();
    }
    writeTrodesConfig(fileName); //write the current configuration and settings to disk

    //append recorded data after the config info
    if (!logFile->open(QIODevice::Append)) {
        qDebug() << "Error opening file";
    }
    outStream = new TrodesDataStream(logFile); //link outStream to the file
    writeSpikesToDisk = true;
}

void Trigger::closeLogFile() {
    if ((logFile->isOpen())) {
        writeSpikesToDisk = false;
        QThread::msleep(10);
        delete outStream;
        logFile->close();
    }
}

void Trigger::newNTrodeTriggerHandler(TrodesSocketMessageHandler *messageHandler, qint16 requestedNTrode) {
    if (requestedNTrode == nTrodeNum) {
        dataHandler = messageHandler;
        connect(dataHandler, SIGNAL(socketDisconnected()),this,SLOT(removeTriggerHandler()));
    }
}

void Trigger::removeTriggerHandler() {
    dataHandler = NULL;
}
