/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "simulateSpikesThread.h"
#include "globalObjects.h"

simulateSpikesRuntime::simulateSpikesRuntime(QObject *parent):
    waveModulatorFrequency(0.0)
    , waveFrequency(30.0)
    , waveAmplitude(50.0)
    , threshold(0)
    , nSecElapsed(0)
    , currentSampleNum(0)
    , waveRes(5000)
    , currentCyclePosition(0)
    , cyclePositionCarryOver(0)
    , loopFinished(true)
    , currentModulatorCyclePosition(0)
    , insertingSpike(false), spikeIdx(0)
{

    generateData();

}

simulateSpikesRuntime::~simulateSpikesRuntime() {

}

int16_t simulateSpikesRuntime::spikeWaveform[60] = {0, -3, -11, -25, -39, -54, -45, 25, 208, 520,
817, 901, 759, 503, 228, -17, -219, -374, -490, -573, -624, -649, -654, -643,
-624, -591, -546, -493, -436, -377, -316, -255, -193, -135, -83, -36, 7, 44,
73, 96, 116, 135, 151, 162, 166, 165, 158, 145, 129, 109, 90, 72, 57, 44, 34,
26, 18, 9, 2, 0};

void simulateSpikesRuntime::Run() {

  static int cycle = 0;
    //int PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + 1 + 1;

    quitNow = false;
    acquiring = true;
    //timeKeeper->start();
    stopWatch.start();
    loopFinished = false;


    //Some variables for implementing a 'brake' for the loop
    QElapsedTimer loopTimer;
    qint64 optimalLoopTime_nS = (qint64)(1/((double) hardwareConf->sourceSamplingRate)*1000000000);
    qint64 loopTimeSurplus_nS;

    qDebug() << "Signal generator loop running....";
    qint64 samplesWritten = 0;
    int16_t currentValue;
    int16_t currentValue_withSpike;
    // float cycleTime; // UNUSED
    float cycleStepsPerSample;
    float nSecPerSample;

    double modulatorFreqStepsDue = 0.0;
    modulatorFreqStepSize = waveModulatorFrequency *((double) waveRes / (double) hardwareConf->sourceSamplingRate);

    double convertFactor = (double) 65536/ (double) AD_CONVERSION_FACTOR;

    while (!quitNow) {

        if (!acquiring) {

            samplesWritten = 0;
            //currentTimeStamp = 0;
            QThread::msleep(100);
            stopWatch.restart();
        } else {
            loopTimer.restart();
            samplesPerCycle = (hardwareConf->sourceSamplingRate/waveFrequency);
            cycleStepsPerSample = waveRes/samplesPerCycle;

            nSecPerSample = 1000.0/hardwareConf->sourceSamplingRate;
            nSecElapsed = stopWatch.elapsed();

            int samplesDue = floor(nSecElapsed/nSecPerSample)-samplesWritten;

            while (samplesDue > 0) {
                // cycleTime =  (float) currentTimeStamp/ (float) hardwareConf->sourceSamplingRate; // UNUSED

                if (waveModulatorFrequency == 0) {
                    //No modulator wave
                    currentValue = (int16_t)(sourceData[currentCyclePosition]*waveAmplitude*convertFactor);
                } else {

                    //Calulate the current position in the modulator frequency
                    modulatorFreqStepsDue = modulatorFreqStepsDue+modulatorFreqStepSize;
                    currentModulatorCyclePosition = (currentModulatorCyclePosition+ (int)modulatorFreqStepsDue)%waveRes;
                    //the current output value is multipleied by the modulator vaue
                    currentValue = (int16_t)(sourceData[currentCyclePosition]*waveAmplitude*((sourceData[currentModulatorCyclePosition]+1.0)/2)*convertFactor);
                    modulatorFreqStepsDue = modulatorFreqStepsDue - floor(modulatorFreqStepsDue);

                }


                if (qAbs(currentValue) < threshold) {
                    currentValue = 0;
                }

                if (insertingSpike) {
                  currentValue_withSpike = currentValue - spikeWaveform[spikeIdx++]; //
                  if (spikeIdx >= 60) {
                    spikeIdx = 0;
                    insertingSpike = false;
                  }
                }
                else if (currentTimeStamp % (hardwareConf->sourceSamplingRate/4) == 0) {
                  currentValue_withSpike = currentValue;
                  insertingSpike = true;
                  cycle = (cycle + 1) % 2;
                } else {
                    currentValue_withSpike = currentValue;
                }

                //header
                for (int headerNum = 0; headerNum < hardwareConf->headerSize; headerNum++) {
                    if (headerNum==0) {
                        rawData.digitalInfo[(rawData.writeIdx*hardwareConf->headerSize)+headerNum] = currentValue; //Decomposes the waveform value into bits
                    } else {
                        rawData.digitalInfo[(rawData.writeIdx*hardwareConf->headerSize)+headerNum] = currentValue; //Write waveform value for the other slots
                    }
                }

                rawData.timestamps[rawData.writeIdx] = currentTimeStamp;
                currentTimeStamp++;
                if (benchConfig->isRecordingSysTime()) {
                    rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();
                }

                for (int sampNum = 0; sampNum < hardwareConf->NCHAN; sampNum++) {
                  if ((sampNum % 2) == cycle)
                    rawData.data[(rawData.writeIdx*hardwareConf->NCHAN)+sampNum] = currentValue_withSpike;
                  else
                    rawData.data[(rawData.writeIdx*hardwareConf->NCHAN)+sampNum] = currentValue;
                }

                samplesDue--;
                samplesWritten++;

                //Advance the write markers and release a semaphore
                writeMarkerMutex.lock();
                rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
                rawDataWritten++;
                writeMarkerMutex.unlock();



                for (int a = 0; a < rawDataAvailable.length(); a++) {
                    rawDataAvailable[a]->release(1);
                }




                //Calculate the next position in the sine wave
                cyclePositionCarryOver = cyclePositionCarryOver + cycleStepsPerSample;
                currentCyclePosition = (currentCyclePosition+(int)cyclePositionCarryOver) % waveRes;
                cyclePositionCarryOver = cyclePositionCarryOver - floor(cyclePositionCarryOver);


            }


            //A brake to make sure that the loop doesn't run faster than is has to
            loopTimeSurplus_nS = optimalLoopTime_nS-loopTimer.nsecsElapsed();

            if (loopTimeSurplus_nS > 1000) {
                QThread::usleep(5*loopTimeSurplus_nS/1000);
            }

        }

    }

    loopFinished = true;
    qDebug() << "Sim Spike Data Runtime loop finished";
    emit finished();

}


/*
void simulateSpikesRuntime::endThread() {
    quit();
}*/


void simulateSpikesRuntime::generateData() {

   //int waveResolution = 5000;

    for (int sampleIndex = 0; sampleIndex < waveRes; sampleIndex++) {
        //sourceData[sampleIndex] = (int16_t) (qSin(2 * M_PI * (sampleIndex/waveRes)) * (65536/12500) );
        sourceData[sampleIndex] = (double) ((qSin(2 * M_PI * ((float) sampleIndex/ (float) waveRes)) ));
    }

}

void simulateSpikesRuntime::setModulatorFrequency(double cf) {
    waveModulatorFrequency = cf;
    modulatorFreqStepSize = waveModulatorFrequency *((double) waveRes / (double) hardwareConf->sourceSamplingRate);

}

void simulateSpikesRuntime::pullTimerExpired() {

}





simulateSpikesInterface::simulateSpikesInterface(QObject *) {
  state = SOURCE_STATE_NOT_CONNECTED;
  acquisitionThread = NULL;

}

simulateSpikesInterface::~simulateSpikesInterface() {


    /*if (acquisitionThread != NULL) {
        delete(acquisitionThread);
    }*/
}

void simulateSpikesInterface::InitInterface() {
  //initialization went ok, so start the runtime thread
  acquisitionThread = new simulateSpikesRuntime(NULL);
  setUpThread(acquisitionThread);

  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);

  HeadstageSettings headstageSettings;
  headstageSettings.autoSettleOn = false;
  headstageSettings.percentChannelsForSettle = 0;
  headstageSettings.threshForSettle = 0;
  headstageSettings.smartRefOn = false;
  headstageSettings.accelSensorOn = false;
  headstageSettings.gyroSensorOn = false;
  headstageSettings.magSensorOn = false;

  headstageSettings.smartRefAvailable = false;
  headstageSettings.autosettleAvailable = false;
  headstageSettings.accelSensorAvailable = false;
  headstageSettings.gyroSensorAvailable = false;
  headstageSettings.magSensorAvailable = false;

  emit headstageSettingsReturned(headstageSettings);
}

void simulateSpikesInterface::StartAcquisition(void) {

  rawData.writeIdx = 0; // location where we're currently writing

  acquisitionThread->acquiring = true;

  if (acquisitionThread->loopFinished) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}


void simulateSpikesInterface::StopAcquisition(void) {


  emit acquisitionStopped();
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);
  acquisitionThread->acquiring = false;
}

void simulateSpikesInterface::CloseInterface(void) {
  if (state == SOURCE_STATE_RUNNING)
    StopAcquisition();

  if (state == SOURCE_STATE_INITIALIZED) {

    //if the runtime thread is running, kill it
    acquisitionThread->quitNow = true;

    /*
    if (acquisitionThread != NULL) {
        acquisitionThread->quitNow = true;
        acquisitionThread->endThread();
        acquisitionThread->wait(); //block until the thread has fully terminated
    }*/

    //close device here


    qDebug() << "Closed device.";
    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);

  }
}


double simulateSpikesInterface::getModulatorFrequency() {
    return (double) acquisitionThread->waveModulatorFrequency;
}

int simulateSpikesInterface::getFrequency() {
    return (int) acquisitionThread->waveFrequency;
}

int simulateSpikesInterface::getAmplitude() {
    return (int) acquisitionThread->waveAmplitude;
}

int simulateSpikesInterface::getThreshold() {
    return acquisitionThread->threshold;
}

void simulateSpikesInterface::setModulatorFrequency(double freqIn) {
    acquisitionThread->setModulatorFrequency(freqIn);
    //acquisitionThread->waveModulatorFrequency = freqIn;

}

void simulateSpikesInterface::setFrequency(int freqIn) {
    acquisitionThread->waveFrequency = (double) freqIn;
}

void simulateSpikesInterface::setAmplitude(int ampIn) {

    acquisitionThread->waveAmplitude = (double) ampIn;

}

void simulateSpikesInterface::setThreshold(int threshIn) {
    acquisitionThread->threshold = threshIn;
}
/*
void simulateSpikesRuntime::StopAcquisition() {
    pullTimer->stop();
    quit();
}*/

