/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2015 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPIKEDETECTORTHREAD_H
#define SPIKEDETECTORTHREAD_H

#include <QObject>
#include <QtCore>

#include "configuration.h"
#include "trodesSocket.h"


//#include "triggerThread.h"

//length at least [SNIPPET_SIZE] (SNIPPET_SIZE * 2?)
#define BUFFERSIZE 2048

#define POINTSTOREWIND 10

typedef struct {
  int16_t x, y;
} int2d;

class GenericSpikeDetector : public QObject
{
  Q_OBJECT

public:
    GenericSpikeDetector(QObject *parent, int nt);
    ~GenericSpikeDetector();

    int nTrode; // index in spikeConf->ntrodes for which I am respoonsible

public slots:
    virtual void newData(const int16_t*, const uint32_t) { } // newData(newData, time)
    virtual void newChannelData(int, int16_t, uint32_t) { } // newChannelData(channel, newData, time)
    virtual void processNewData() {}
    void newTriggerOn(int channel, bool triggerOn);
    void createLogFile(QString path);
    void closeLogFile();



protected:
    int numChannels;
    QVector <bool> triggersOn;
    QFile       *logFile;

    TrodesDataStream *outStream;
    bool        writeSpikesToDisk;


    QList<TrodesSocketMessageHandler *> dataHandler;



signals:
    //void spikeDetectionEvent(int nTrodeNum, const QVector<vertex2d>*, const int*, uint32_t time);
    void spikeDetectionEvent(int nTrodeNum, const QVector<int2d>*, const int*, uint32_t time);

};

class ThresholdSpikeDetector : public GenericSpikeDetector
{
  Q_OBJECT

public:
    ThresholdSpikeDetector(QObject *parent, int nt);
    ~ThresholdSpikeDetector();

    int16_t** dataBuffer;
    uint32_t* timeBuffer;
    int* sysTimebuffer;
    //int sysTimeOfCurPacket;


public slots:
    void newData(const int16_t *newData, const uint32_t time, const int sysTime);
    void newChannelData(int channel, int16_t newData, uint32_t time);
    void newSpikeThreshold(int newThresh);
    void processNewData();
    void reportLatency();
    void clearHistory();

    void newDataHandler(TrodesSocketMessageHandler* messageHandler, qint16 nTrode);
    void removeDataHandler();


private:
    QVector<quint16> dataHandlersOn;
    int tPtr, snipStartPtr, threshPtr;
    QVector <int> thresholds;
    QVector <QVector<int2d> > waveForms;
    //QVector <QVector<vertex2d> > waveForms;
    QVector<int> peaks;
    QVector <bool> isLocked;

    quint64 pointsSinceLastSpike;
    bool nTrodeLockedOut;

    bool spikeDetected, spikeVetoed;
    bool flag;

    int last_tPtr;
    int largest_latency;
    double average_latency;

    int     nSpikePoints;
    int16_t *waveformData;

    avgCalculator<int> detectSpikeLatency;
    int latencyToken;
};

class SpikeDetectorManager : public QObject
{
  Q_OBJECT
public:
  SpikeDetectorManager(QObject *parent, QList<int> ntList, QList<ThresholdSpikeDetector *> *spikeDetectorList);
  ~SpikeDetectorManager();

  QList <ThresholdSpikeDetector *> managedSpikeDetectors;

public slots:
  void setupAndRun(void);
  void updateSpikeThreshold(int nTrode, int newThresh);
  void updateSpikeMode(int nTrode, int channel, bool triggerOn);
  void stopRunning();

private:
  QList<int> nTrodeList; // indices of nTrodes which this manager is managing
  QList<int> nTrodeLookup; // lookup table of size spikeConf->ntrodes that is either (-1) or index in nTrodeList

  QList<ThresholdSpikeDetector *> *streamManagerSpikeDetectorList;
  int queueSize; // number of samples to wait before processing data
  int numInQueue; // number waiting

  bool quitNow;


  // we need a dataServer and a list of messageHandlers to send out data from our nTrodes
  TrodesServer *dataServer;
  DataTypeSpec dataProvided;




signals:
  void triggerDetectorProcessing();

  //void newDataHandler(TrodesSocketMessageHandler* messageHandler, qint16 nTrode);
  void addDataProvided(DataTypeSpec *dp);


};

#endif // SPIKEDETECTORTHREAD_H
