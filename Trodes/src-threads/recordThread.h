/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef RECORDTHREAD_H
#define RECORDTHREAD_H



#include <QGLWidget>
#include <QThread>
#include <QTimer>
#include <QFile>
#include "configuration.h"
#include "sharedVariables.h"

class RecordThread: public QObject
{
    Q_OBJECT
public:
    explicit RecordThread(QObject *parent = 0);
    ~RecordThread();
    //void run();
    int openFile(QString fileName);
    void closeFile();
    void startRecord();
    void pauseRecord();
    quint64 getBytesWritten();
    qint64  getBytesFree();

    bool fileOpen;
    bool recording;

    QFile *file;

private:

    QTimer *pullTimer;
    QDataStream *outStream;
    quint64 saveMarker;
    quint64 bytesWritten;
    QString baseName;
    int bufferLocation;
    int currentFilePart;
    bool errorSignalEmitted;

    uint32_t currentRecordTimestamp;
    uint32_t lastRecordTimestamp;
    uint32_t *tsPtr;

    short *dataArray;
    bool startNextFilePart();

signals:
    void writeError();
    void finished();


private slots:
    void pullTimerExpired();

public slots:
    void setUp();
    void setupSaveDisplayedChan();
    void endRecordThread();
};


#endif // RECORDTHREAD_H
