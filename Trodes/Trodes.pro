include(../build_defaults.pri)

TARGET = Trodes

QT += opengl widgets xml multimedia multimediawidgets testlib

#The TRODES_CODE define is necessary to allow code to be excluded for modules
DEFINES += TRODES_CODE

INCLUDEPATH += src-main
INCLUDEPATH += src-display
INCLUDEPATH += src-config
INCLUDEPATH += src-threads

# List all unit test directories that need to be copied to the install dir
TESTDIRS = WorkspaceLoading \
       WorkspaceHardwareStreaming \
       Playback

MAINTESTDIR = Testing
TESTFILESTOCOPYDIR = $$PWD/../Resources/Testing

DIR_COPY_PATH = $$DESTDIR/$$MAINTESTDIR

# Loop over all given directories and append the 'install' directory to make absolute paths
for(DIR, TESTDIRS) ABS_DIRS += $$DIR_COPY_PATH/$$DIR
# Create 'copy' commands for $DIRS
for(DIR, TESTDIRS) TEST_CP_CMD += $(COPY) $$TESTFILESTOCOPYDIR/$$DIR/* $$DESTDIR/$$MAINTESTDIR/$$DIR ;

testfolders.commands += $(MKDIR) $$ABS_DIRS ;
testfolders.commands += $$TEST_CP_CMD
QMAKE_EXTRA_TARGETS += testfolders
POST_TARGETDEPS += testfolders

# List other directories that need to be copied to the base install dir
OTHERDIRS = ReleaseNotes \
       Resources/SampleWorkspaces \
       Resources/SetupHelp/Linux

OTHERFILESTOCOPYDIR = $$PWD/..

DIR_COPY_PATH = $$DESTDIR

# Loop over all given directories and append the 'install' directory to make absolute paths
for(DIR, OTHERDIRS) ABS_OTHER_DIRS += $$DIR_COPY_PATH/$$DIR
# Create 'copy' commands for $DIRS
for(DIR, OTHERDIRS) OTHER_CP_CMD += $(COPY) $$OTHERFILESTOCOPYDIR/$$DIR/* $$DESTDIR/$$DIR ;

otherfolders.commands += $(MKDIR) $$ABS_OTHER_DIRS ;
otherfolders.commands += $$OTHER_CP_CMD
QMAKE_EXTRA_TARGETS += otherfolders
POST_TARGETDEPS += otherfolders

#copy over the matlab path helper file
PLACEHOLDERFILE = $$PWD/../Resources/trodes_path_placeholder.m
placeholdercommand.commands += $(COPY) $$PLACEHOLDERFILE $$DESTDIR
QMAKE_EXTRA_TARGETS += placeholdercommand
POST_TARGETDEPS += placeholdercommand

#stuff specific for mac
macx {
INCLUDEPATH += Libraries/Mac
HEADERS    += Libraries/Mac/WinTypes.h
HEADERS    += Libraries/Mac/ftd2xx.h
LIBS       += /usr/local/lib/libftd2xx.dylib
ICON        = src-main/trodesIcon.icns
OTHER_FILES += \
    src-main/Info.plist
QMAKE_INFO_PLIST += src-main/Info.plist

# On Mac, require that the FTDI drivers are already installed
}

unix:!macx {
INCLUDEPATH += Libraries/Linux
LIBS += -LLibraries/Linux -lftd2xx
HEADERS    += Libraries/Linux/WinTypes.h
HEADERS    += Libraries/Linux/ftd2xx.h

# Copy required shared libraries to the install directory on make install
libraries.path = $$TRODES_LIB_INSTALL_DIR
libraries.files += $$PWD/Libraries/Linux/libftd2xx.so

#Copy required directory structure for unit testing
INSTALL_DIR_COPY_PATH = $$TRODES_BIN_INSTALL_DIR/$$MAINTESTDIR
movetestfiles.path = $$INSTALL_DIR_COPY_PATH
movetestfiles.files = $$TESTFILESTOCOPYDIR/*

#Copy required directory structure for release notes
RELEASENOTES_COPY_PATH = $$TRODES_BIN_INSTALL_DIR/ReleaseNotes
releasenotes.path = $$RELEASENOTES_COPY_PATH
releasenotes.files = $$PWD/../ReleaseNotes/*

#Copy required directory structure for resources
RESOURCES_COPY_PATH = $$TRODES_BIN_INSTALL_DIR/Resources
resources.path = $$RESOURCES_COPY_PATH
resources.files = $$PWD/../Resources/SetupHelp
resources.files += $$PWD/../Resources/SampleWorkspaces


matlabplaceholder.path = $$TRODES_BIN_INSTALL_DIR
matlabplaceholder.files = $$PWD/../Resources/trodes_path_placeholder.m

INSTALLS += libraries movetestfiles matlabplaceholder releasenotes resources

}

win32 {
    RC_ICONS += trodesIcon.ico
    INCLUDEPATH += Libraries/Windows
    HEADERS    += ftd2xx.h
    win32-g++ { # MinGW
        LIBS       += -L$$quote($$PWD/Libraries/Windows/FTDI/i386) -lftd2xx
        copy_ftdi_dll.commands = $(COPY) $$quote($$shell_path($$PWD/Libraries/Windows/FTDI/i386/ftd2xx.dll)) $$quote($$shell_path($$DESTDIR/))
    }
    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # MSVC-32
            LFLAGS += /VERBOSE
            LIBS   += $$quote($$PWD/Libraries/Windows/FTDI/i386/ftd2xx.lib)
            copy_ftdi_dll.commands = copy /y $$quote($$shell_path($$PWD/Libraries/Windows/FTDI/i386/ftd2xx.dll)) $$quote($$shell_path($$DESTDIR/))
        } else { # MSVC-64
            LIBS   += $$quote($$PWD/Libraries/Windows/FTDI/amd64/ftd2xx.lib)
            copy_ftdi_dll.commands = copy /y $$quote($$shell_path($$PWD/Libraries/Windows/FTDI/amd64/ftd2xx64.dll)) $$quote($$shell_path($$DESTDIR/ftd2xx.dll))
        }
    }
    QMAKE_EXTRA_TARGETS += copy_ftdi_dll
    #POST_TARGETDEPS += copy_ftdi_dll

    first.depends = $(first) copy_ftdi_dll
    export(first.depends)
    export(copy_ftdi_dll.commands)
    QMAKE_EXTRA_TARGETS += first copy_ftdi_dll
}



# Use Qt Resource System for images, across mac/linux/win
RESOURCES += \
    $$PWD/../Resources/Images/buttons.qrc


HEADERS   += src-main/sharedVariables.h \
    src-config/configuration.h \
    src-threads/usbdaqThread.h \
    src-threads/audioThread.h \
    src-main/iirFilter.h \
    src-threads/triggerThread.h \
    src-main/globalObjects.h\
    src-main/mainWindow.h\
    src-threads/streamProcessorThread.h \
    src-threads/recordThread.h \
    src-display/streamDisplay.h \
    src-main/cocoaInitializer.h \
    src-threads/simulateDataThread.h \
    src-main/sourceController.h \
    src-threads/fileSourceThread.h \
    src-display/spikeDisplay.h \
    src-display/dialogs.h \
    src-main/networkMessage.h \
    src-main/trodesSocket.h\
    src-main/customApplication.h \
    src-main/trodesSocketDefines.h \
    src-threads/ethernetSourceThread.h \
    src-main/abstractTrodesSource.h \
    src-threads/simulateSpikesThread.h \
    src-threads/spikeDetectorThread.h \
    src-display/rfDisplay.h \
    src-main/trodesdatastructures.h \
    src-main/eventHandler.h \
    src-display/sharedtrodesstyles.h \
    src-main/benchmarkWidget.h




SOURCES   += \
    src-threads/usbdaqThread.cpp \
    src-threads/audioThread.cpp \
    src-main/iirFilter.cpp \
    src-threads/triggerThread.cpp \
    src-config/configuration.cpp\
    src-main/main.cpp\
    src-main/mainWindow.cpp\
    src-threads/streamProcessorThread.cpp \
    src-threads/recordThread.cpp \
    src-display/streamDisplay.cpp \
    src-threads/simulateDataThread.cpp \
    src-main/sourceController.cpp \
    src-threads/fileSourceThread.cpp \
    src-display/spikeDisplay.cpp \
    src-display/dialogs.cpp \
    src-main/trodesSocket.cpp \
    src-main/customApplication.cpp \
    src-threads/ethernetSourceThread.cpp \
    src-main/abstractTrodesSource.cpp \
    src-threads/simulateSpikesThread.cpp \
    src-threads/spikeDetectorThread.cpp \
    src-display/rfDisplay.cpp \
    src-main/trodesdatastructures.cpp \
    src-main/eventHandler.cpp \
    src-display/sharedtrodesstyles.cpp \
    src-main/benchmarkWidget.cpp

OTHER_FILES += \
    src-main/cocoaInitializer.mm


#-------------------------------------------------
# Settings required to use the Intan/OpenEphys interface
#
# NOTE: you must link libudev.so to libudev.so.0!!
#  For Ubuntu 14.04, this is ln -s /usr/lib/x86_64-linux-gnu/libudev.so /usr/lib/x86_64-linux-gnu/libudev.so.0
#
# Uncomment the following line for OpenEphys ability
#CONFIG += RHYTHMINTERFACE

RHYTHMINTERFACE {
DEFINES += RHYTHM

SOURCES +=     src-threads/rhythmThread.cpp \
    \ # src-threads/rhythm-api/okFrontPanelDLL.cpp \
    src-threads/rhythm-api/rhd2000datablock.cpp \
    src-threads/rhythm-api/rhd2000evalboard.cpp \
    src-threads/rhythm-api/rhd2000registers.cpp
HEADERS +=     src-threads/rhythmThread.h \
    src-threads/rhythm-api/okFrontPanelDLL.h \
    src-threads/rhythm-api/rhd2000datablock.h \
    src-threads/rhythm-api/rhd2000evalboard.h \
    src-threads/rhythm-api/rhd2000registers.h

#stuff specific for mac
macx {
    LIBS += /usr/local/lib/libftd2xx.dylib
    LIBS += -L"Libraries/Mac" -lokFrontPanel
    #LIBS += -F$$quote($$PWD/Libraries/Mac) $$quote($$PWD/Libraries/Mac/libokFrontPanel.dylib)
    copy_opalkelly_lib.commands = cp $$quote($$PWD/Libraries/Mac/libokFrontPanel.dylib) $$quote($$DESTDIR/Trodes.app/Contents/MacOS/); \
                                  cp $$quote($$PWD/../Resources/Rhythm/main.bit) $$quote($$DESTDIR/Trodes.app/Contents/MacOS/)

# Set the install_name of libokFrontPanel.dylib to its absolute location so that it will
# be linked correctly
#OPAL_DYLIB=$${PWD}/Libraries/Mac/libokFrontPanel.dylib
#copy_opalkelly_lib.commands += "install_name_tool -id $$OPAL_DYLIB $$OPAL_DYLIB && echo \"==> libokFrontPanel.dylib install_name updated before build:\" ;"
#copy_opalkelly_lib.commands += "otool -L $$OPAL_DYLIB | head -2;"



}

unix:!macx {
    LIBS += -ldl # needed for dynamic linking to FrontPanel library
    LIBS += -L$$quote($$PWD/Libraries/Linux) -lokFrontPanel
    copy_opalkelly_lib.commands = cp $$quote($$PWD/Libraries/Linux/libokFrontPanel.so) $$quote($$DESTDIR/); \
                                  cp $$quote($$PWD/../Resources/Rhythm/main.bit) $$quote($$DESTDIR/);
}

win32 {
    win32-g++ { # MinGW
       # LIBS  += -L$$shell_path($$PWD/Trodes/Libraries/Windows/OpalKelly/Win32/) -lokFrontPanel
        LIBS += -L$$quote($$PWD/Libraries/Windows/OpalKelly/Win32) -lokFrontPanel

        copy_opalkelly_lib.commands =  cmd /c copy /y $$quote($$shell_path($$PWD/Libraries/Windows/OpalKelly/Win32/okFrontPanel.dll)) $$quote($$shell_path($$DESTDIR/)) && \
                                         cmd /c copy /y $$quote($$shell_path($$PWD/../Resources/Rhythm/main.bit)) $$quote($$shell_path($$DESTDIR/))

#        copy_opalkelly_lib.commands =  cp $$quote($$shell_path($$PWD/Trodes/Libraries/Windows/OpalKelly/Win32/okFrontPanel.dll)$$escape_expand(\n\t)) $$quote($$shell_path($$DESTDIR/)) && \
#                                       cp $$quote($$shell_path($$PWD/Resources/Rhythm/main.bit)) $$quote($$shell_path($$DESTDIR/))
    }
    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # MSVC-32
            LIBS   += $$quote($$PWD/Libraries/Windows/OpalKelly/Win32/okFrontPanel.lib)
            copy_opalkelly_lib.commands = copy /y $$quote($$shell_path($$PWD/Libraries/Windows/OpalKelly/Win32/okFrontPanel.dll)) $$quote($$shell_path($$DESTDIR/)) && \
                                         copy /y $$quote($$shell_path($$PWD/../Resources/Rhythm/main.bit)) $$quote($$shell_path($$DESTDIR/))
        } else { # MSVC-64
            LIBS   += $$PWD/Libraries/Windows/OpalKelly/x64/okFrontPanel.lib
            copy_opalkelly_lib.commands = copy /y $$quote($$shell_path($$PWD/Libraries/Windows/OpalKelly/x64/okFrontPanel.dll)) $$quote($$shell_path($$DESTDIR/)) && \
                                        copy /y $$quote($$shell_path($$PWD/../Resources/Rhythm/main.bit)) $$quote($$shell_path($$DESTDIR/))
        }
    }
}


#QMAKE_EXTRA_TARGETS += copy_opalkelly_lib
#POST_TARGETDEPS += copy_opalkelly_lib
first.depends = $(first) copy_opalkelly_lib
export(first.depends)
export(copy_opalkelly_lib.commands)
QMAKE_EXTRA_TARGETS += first copy_opalkelly_lib
}
#-------------------------------------------------


