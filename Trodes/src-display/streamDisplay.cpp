/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "streamDisplay.h"
#include "spikeDisplay.h"
#include "streamDisplay.h"
#include "globalObjects.h"


/* ========================================================================= */
/* ========================================================================= */
/* StreamDisplayManager
       StreamDisplayManager is the high level container for the stream display.
   It presumes the existence of a global configuration object,
   "streamConf", which specifies everything. The container object
   manages a multicolumn layout, disributed across multiple tabs.

*/
StreamDisplayManager::StreamDisplayManager(QWidget *parent,StreamProcessorManager* managerPtr) :
    QWidget(parent),
    streamManager(managerPtr),
    displayFrozen(false) {

  int totalChannels = 0;
  for (int trodeCount = 0; trodeCount < spikeConf->ntrodes.length(); trodeCount++) {
      for (int trodeChan = 0; trodeChan < spikeConf->ntrodes[trodeCount]->maxDisp.length(); trodeChan++) {
          totalChannels++;
      }
  }

  //QPixmap pixmap(":/buttons/speaker.png");
  //QIcon ButtonIcon(pixmap);

  QFont labelFont;
  labelFont.setPixelSize(12);
  labelFont.setFamily("Console");

  int prefferedChanPerColumn = 32;
  int prefferedHeaderChanPerColumn = 20;
  //int prefferedColPerPage = eegDispConf->nColumns;
  int prefferedColPerPage = 2;
  int currentPage = 0;
  int currentColumn = 0;
  int currentTrode = 0;
  int currentChan = 0;
  int currentHeaderChan = 0;
  // int firstHeaderColumn = -1; // UNUSED
  int firstHeaderPage = -1;
  bool doneWithNeuralChannels = false;

  if (totalChannels == 0) {
      doneWithNeuralChannels = true;
      firstHeaderPage = 0;
  }

  prefferedChanPerColumn = ceil(totalChannels/streamConf->nColumns);


  QList<int> columnPageAssignment; //which page does each column belong to?
  int unAssignedChannels = totalChannels;
  int unAssingedHeaderChannels = headerConf->headerChannels.length();



  //The streaming display is organized into columns.  In the
  //config file, the user sets the preferred column number.  Here,
  //we calculate how many pages (tabs) will need to be displayed
  //to show all the channels.
  while ((unAssignedChannels > 0)||(unAssingedHeaderChannels > 0)) {

      columnsPerPage.push_back(0); //keep track of how many columns are on each page
      isHeaderDisplayPage.push_back(false);
      bool doneWithPage = false;
      while (!doneWithPage) {

          //add new column to the list
          streamDisplayChannels.append(QList<int>());
          nTrodeIDs.append(QList<int>());
          columnPageAssignment.push_back(currentPage);
          int currentChannelInColumn = 0;
          bool doneWithColumn = false;

          while (!doneWithColumn) {

              if (!doneWithNeuralChannels) {


                  //we do not want any nTrodes to spill over from one column to another,
                  //so we make sure that the next nTrode will fit into the column while
                  //not going over the channel limit per column
                  int numChanInCurrentNTrode = spikeConf->ntrodes[currentTrode]->maxDisp.length();
                  if ((currentChannelInColumn+numChanInCurrentNTrode) <= prefferedChanPerColumn) {
                      for (int trodeChan = 0; trodeChan < numChanInCurrentNTrode; trodeChan++) {
                          streamDisplayChannels[currentColumn].append(spikeConf->ntrodes[currentTrode]->hw_chan[trodeChan]);
//                          streamDisplayChannels[currentColumn].append(currentChan);
                          currentChan++;
                          unAssignedChannels--;
                          currentChannelInColumn++;
                      }
                      currentTrode++;
                  }

                  if ((currentChannelInColumn+numChanInCurrentNTrode) > prefferedChanPerColumn) {
                      doneWithColumn = true;
                  }
                  if (currentChan >= totalChannels) {
                      doneWithColumn = true;
                  }
              } else {

                  streamDisplayChannels[currentColumn].append(currentHeaderChan);
                  isHeaderDisplayPage.last() = true;
                  currentHeaderChan++;
                  currentChannelInColumn++;
                  unAssingedHeaderChannels--;
                  if ((currentChannelInColumn) >= prefferedHeaderChanPerColumn) {
                      doneWithColumn = true;
                  }
                  if (unAssingedHeaderChannels <= 0) {
                      doneWithColumn = true;
                  }
              }

          }
          currentColumn++;

          columnsPerPage.last()++; //increment the number of columns on this page

          if (columnsPerPage.last() >= prefferedColPerPage) {
              doneWithPage = true;
          }
          if ((currentChan >= totalChannels)&&(doneWithNeuralChannels == false)) {
              doneWithPage = true;
              doneWithNeuralChannels = true;

              // firstHeaderColumn = currentColumn; // UNUSED
              firstHeaderPage = currentPage+1;

          } else if ((doneWithNeuralChannels == true) && (unAssingedHeaderChannels == 0)) {
              doneWithPage = true;

          }

      } //page
      currentPage++;
  }



  //Now that we have assigned which channel go to which columns, we create
  //the widgets.
  currentColumn = 0;
  currentTrode = 0;
  int currentLabelTrode = 0;
  currentChan = 0;
  currentHeaderChan = 0;
  for (int pInd = 0; pInd < firstHeaderPage; pInd++) {

      eegDisplayLayout.push_back(new QGridLayout());
      eegDisplayWidgets.push_back(new QWidget());
      eegDisplayWidgets[pInd]->setLayout(eegDisplayLayout[pInd]);
      int lastNTrodeID = -999;
      for (int col = 0; col < columnsPerPage[pInd]; col++) {
          int columnChan = 0;
          QGridLayout* numberGrid = new QGridLayout();
          numberGrid->setVerticalSpacing(0);
          numberGrid->setContentsMargins(0,0,0,0);
          numberGrid->setMargin(0);
          //First determine the number of channels labels that should be displayed

          int numNTrodesInColumn = 0;
          while (columnChan < streamDisplayChannels[currentColumn].length()) {
              for (int trodeChan = 0; trodeChan < spikeConf->ntrodes[currentLabelTrode]->maxDisp.length(); trodeChan++) {
                  columnChan++;
              }
              numNTrodesInColumn++;
          }
          columnChan = 0;
          currentLabelTrode = 0;
          while (columnChan < streamDisplayChannels[currentColumn].length()) {
              for (int trodeChan = 0; trodeChan < spikeConf->ntrodes[currentTrode]->maxDisp.length(); trodeChan++) {

                  QLabel *channelLabel = new QLabel(this);
                  int ntrodeId = spikeConf->ntrodes[currentTrode]->nTrodeId;
                  if (ntrodeId != lastNTrodeID) {

                      currentLabelTrode++;
                      lastNTrodeID = ntrodeId;
                      nTrodeIDs[currentColumn].append(ntrodeId);
                  }
                  //channelLabel->setText(QString::number(ntrode));
                  channelLabel->setText(QString::number(ntrodeId));
                  channelLabel->setAlignment(Qt::AlignCenter);
                  channelLabel->setFont(labelFont);

                  //We only display one label per nTrode
                  if (trodeChan > 0) {                     
                      channelLabel->setVisible(false);
                  } else {
                      if (numNTrodesInColumn < 30) {
                        labelFont.setPixelSize(12);
                        channelLabel->setFont(labelFont);
                        numberGrid->addWidget(channelLabel,columnChan,0);
                      } else if (numNTrodesInColumn < 65) {
                          labelFont.setPixelSize(8);
                          channelLabel->setFont(labelFont);
                          numberGrid->addWidget(channelLabel,columnChan,0);
                      }else if ((currentLabelTrode % 10) == 0) {
                        labelFont.setPixelSize(12);
                        channelLabel->setFont(labelFont);
                        numberGrid->addWidget(channelLabel,columnChan,0);
                      }
                      //eegDisplayLayout[currentColumn]->addWidget(channelLabel,row,col);
                  }

                  columnChan++;
                  currentChan++;

              } //trode
              currentTrode++;

          }
          eegDisplayLayout[pInd]->addLayout(numberGrid,0,col*2);
          eegDisplayLayout[pInd]->setColumnMinimumWidth(col*2,30);
          glStreamWidgets.append(new StreamWidgetGL(eegDisplayWidgets.last(), streamDisplayChannels[currentColumn],false, streamManager));

          connect(glStreamWidgets.last(),SIGNAL(channelClicked(int)),this,SLOT(updateAudioHighlightChannel(int)));
          connect(glStreamWidgets.last(),SIGNAL(channelClicked(int)),this,SLOT(relayChannelClick(int)));
          connect(glStreamWidgets.last(),SIGNAL(newPSTHTrigger(int,bool)),this,SIGNAL(newPSTHTrigger(int,bool)));
          connect(glStreamWidgets.last(),SIGNAL(newSettleControlChannel(int,quint8)),this,SLOT(setSettleControlChannel(int,quint8)));
          connect(this,SIGNAL(newPSTHTrigger(int,bool)),glStreamWidgets.last(),SLOT(setPSTHTrigger(int,bool)));
          connect(this,SIGNAL(newSettleControlChannel(int,quint8)),glStreamWidgets.last(),SLOT(setSettleControlChannel(int,quint8)));
          eegDisplayLayout[pInd]->addWidget(glStreamWidgets.last(), 0, 2*col + 1);

          currentColumn++;

      } //column
      eegDisplayLayout[pInd]->setContentsMargins(10,10,10,10);
      //eegDisplayWidgets[pInd]->setLayout(eegDisplayLayout[pInd]);



  } //page (neural channels)


  //now we do the header channels, if configured for display
  for (int pInd = firstHeaderPage; pInd < currentPage; pInd++) {

      labelFont.setPixelSize(8);


      eegDisplayLayout.push_back(new QGridLayout());
      eegDisplayWidgets.push_back(new QWidget());

      eegDisplayWidgets[pInd]->setLayout(eegDisplayLayout[pInd]);

      for (int col = 0; col < columnsPerPage[pInd]; col++) {

          int columnChan = 0;
          QGridLayout* numberGrid = new QGridLayout();
          while (columnChan < streamDisplayChannels[currentColumn].length()) {

                  //Label each channel
                  QLabel *channelLabel = new QLabel(this);

                  //int chanID = headerConf->headerChannels[currentHeaderChan].idNumber;
                  //channelLabel->setText(QString::number(chanID));
                  channelLabel->setText(headerConf->headerChannels[currentHeaderChan].idString);
                  channelLabel->setAlignment(Qt::AlignCenter);
                  channelLabel->setFont(labelFont);
                  numberGrid->addWidget(channelLabel,columnChan,0);

                  columnChan++;
                  currentHeaderChan++;


          }

          eegDisplayLayout[pInd]->addLayout(numberGrid,0,col*2);
          eegDisplayLayout[pInd]->setColumnMinimumWidth(col*2,30);
          glStreamWidgets.append(new StreamWidgetGL(eegDisplayWidgets.last(), streamDisplayChannels[currentColumn],true, streamManager));

          glStreamWidgets.last()->headerDisplay = true; //set the header display flag as true

          //connect(glStreamWidgets.last(),SIGNAL(channelClicked(int)),this,SLOT(relayChannelClick(int)));
          connect(glStreamWidgets.last(),SIGNAL(newPSTHTrigger(int,bool)),this,SIGNAL(newPSTHTrigger(int,bool)));
          connect(glStreamWidgets.last(),SIGNAL(newSettleControlChannel(int,quint8)),this,SLOT(setSettleControlChannel(int,quint8)));
          connect(this,SIGNAL(newPSTHTrigger(int,bool)),glStreamWidgets.last(),SLOT(setPSTHTrigger(int,bool)));
          connect(this,SIGNAL(newSettleControlChannel(int,quint8)),glStreamWidgets.last(),SLOT(setSettleControlChannel(int,quint8)));

          eegDisplayLayout[pInd]->addWidget(glStreamWidgets.last(), 0, 2*col + 1);

          currentColumn++;

      } //column
      eegDisplayLayout[pInd]->setContentsMargins(10,10,10,10);

  }

  updateAudioHighlightChannel(streamDisplayChannels[0][0]);

  updateTimer = new QTimer(this);
  connect(updateTimer,SIGNAL(timeout()),this,SLOT(updateAllColumns()));
  updateTimer->start(40); // screen refresh
}

StreamDisplayManager::~StreamDisplayManager() {

    updateTimer->stop();

    while (!glStreamWidgets.isEmpty()) {

        delete glStreamWidgets.takeLast();
    }

}

void StreamDisplayManager::updateAllColumns() {

    if (!displayFrozen && !exportMode && updateTimer->isActive()) {
        for (int i=0; i < glStreamWidgets.length(); i++) {
            glStreamWidgets[i]->update();
        }
    }
}


void StreamDisplayManager::relayChannelClick(int hwchannel) {
    //qDebug() << "[Relay] Channel clicked: " << hwchannel;

    emit trodeSelected(streamConf->trodeIndexLookupByHWChan[hwchannel]);
    emit streamChannelClicked(hwchannel);
}

void StreamDisplayManager::updateAudioHighlightChannel(int hwChan) {
    for (int i = 0; i < glStreamWidgets.length(); i++) {
        glStreamWidgets[i]->setHighlightChannel(hwChan); //sets the highlight on the widget to the current audio channel
    }
}

void StreamDisplayManager::freezeDisplay(bool freeze) {
    displayFrozen = freeze;
}

void StreamDisplayManager::setSettleControlChannel(int headerChannel, quint8 triggerState) {

    int byteInPacket = headerConf->headerChannels[headerChannel].startByte;
    quint8 bit = headerConf->headerChannels[headerChannel].digitalBit;


    emit newSettleControlChannel(byteInPacket, bit, triggerState);

    emit newSettleControlChannel(headerChannel,triggerState);
}




/* ========================================================================= */


/* ========================================================================= */
/* ========================================================================= */
/* StreamWidgetGL
      StreamWidgetGL is the QGLWidget which actually draws the data. It handles
   amplitude scaling and time length changes based on signals received from
   the global streamConf object. In order to allow for asynchronous updates
   of the data, StreamWidgetGL holds another object DisplayProcessor, which lives
   inside a different thread. The data within this object is what is drawn.
*/

StreamWidgetGL::StreamWidgetGL(QWidget *parent, QList<int> channelList,bool isHeaderDisplay, StreamProcessorManager* managerPtr)
    : QGLWidget(QGLFormat(QGL::DoubleBuffer), parent),
      headerDisplay(isHeaderDisplay),
      streamManager(managerPtr) {

  setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  setAutoFillBackground(false);
  TLength = streamConf->tLength;
  FS = hardwareConf->sourceSamplingRate;
  PSTHTriggerHeaderChannel = -1;
  PSTHTriggerState = true;
  settleControlChannel = -1;
  settleControlChannelTriggerState = 0;

  dispHWChannels = channelList;
  nChan = channelList.length();
  currentHighlightChannel = -1;

  //The DisplayProcessor thread also feeds data to the trigger threads, so it's a little
  //awkward that it lives inside this object.  However, it is intuitive that each visual streaming channels
  //box has it's own associated processing thread.

  //dataObj = new DisplayProcessor(0, channelList, groupNumber, headerDisplay);
  //connect(sourceControl, SIGNAL(acquisitionStarted()), dataObj, SLOT(runLoop()));
  //connect(sourceControl, SIGNAL(acquisitionStopped()), this, SLOT(stopAcquisition()));
  backgroundColor = streamConf->backgroundColor;
  maxAmplitude.resize(nChan);
  traceColor.resize(nChan);
  for (int i = 0; i < nChan; i++) {
      if (!headerDisplay) {
        int nt = streamConf->trodeIndexLookupByHWChan[channelList[i]];
        int ch = streamConf->trodeChannelLookupByHWChan[channelList[i]];
        maxAmplitude[i] = (spikeConf->ntrodes[nt]->maxDisp[ch]*65536)/AD_CONVERSION_FACTOR;
        traceColor[i] = spikeConf->ntrodes[nt]->color;
      } else {
          maxAmplitude[i] = headerConf->headerChannels[channelList[i]].maxDisp;
          traceColor[i] = headerConf->headerChannels[channelList[i]].color;
      }
  }

  if (!headerDisplay) {
    connect(spikeConf, SIGNAL(updatedMaxDisplay(void)), this, SLOT(updateMaxDisplay(void)));
    connect(spikeConf, SIGNAL(updatedTraceColor(void)), this, SLOT(updateTraceColor(void)));
    connect(spikeConf, SIGNAL(updatedRef(void)), this, SLOT(setChannels(void)));
    connect(spikeConf, SIGNAL(updatedFilter(void)), this, SLOT(setChannels(void)));
  }

  connect(streamConf,SIGNAL(updatedBackgroundColor(QColor)),this,SLOT(updateBackgroundColor()));
  //No need to set the x-axis, so we skip this step
  //connect(streamConf, SIGNAL(updatedTLength(double)), this, SLOT(setTLength(double)));

}

void StreamWidgetGL::setPSTHTrigger(int headerChannel, bool state) {
    PSTHTriggerHeaderChannel = headerChannel;
    PSTHTriggerState = state;
}

void StreamWidgetGL::setSettleControlChannel(int headerChannel, quint8 triggerState) {
    settleControlChannel = headerChannel;
    settleControlChannelTriggerState = triggerState;
}

void StreamWidgetGL::stopAcquisition()
{
  //dataObj->quitNow = 1;
}

StreamWidgetGL::~StreamWidgetGL()
{

  //dataObj->quit();
  //dataObj->wait();

  //delete dataObj;

}


void StreamWidgetGL::paintEvent(QPaintEvent *event) {
  makeCurrent();
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setViewTransformEnabled(true);
  painter.setViewport(0,0,width(), height());

  painter.beginNativePainting();

  QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
  f->glViewport(0, 0, (GLint)width()*this->windowHandle()->devicePixelRatio(), (GLint)height()*this->windowHandle()->devicePixelRatio());

    // Copy new display data into graphics memory
  f->glBindBuffer(GL_ARRAY_BUFFER, m_linesBuf);
  for (int c=0; c < nChan; c++) {
      if (!headerDisplay) {
          f->glBufferSubData(GL_ARRAY_BUFFER, c * 2 * sizeof(vertex2d) * EEG_TIME_POINTS, 2 * sizeof(vertex2d) * EEG_TIME_POINTS,
                             streamManager->neuralDataMinMax[dispHWChannels[c]]);
      } else {
          f->glBufferSubData(GL_ARRAY_BUFFER, c * 2 * sizeof(vertex2d) * EEG_TIME_POINTS, 2 * sizeof(vertex2d) * EEG_TIME_POINTS,
                             streamManager->auxDataMinMax[dispHWChannels[c]]);
      }
  }
  f->glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Initial set up
  f->glLineWidth(1);
  m_proj.setToIdentity();
  m_proj.ortho(0, EEG_TIME_POINTS, 0, nChan, -1.0, 1.0); // scaling for each column

  // ORDER IS IMPORTANT! BACKGROUND FIRST THEN FOREGROUND
  m_program->bind();
  m_program->setUniformValue(m_projMatrixLoc, m_proj);

  // Paint each channel's background using "rectangle" program
  f->glBindBuffer(GL_ARRAY_BUFFER, m_rectVertBuf); // four vertices
  f->glEnableVertexAttribArray(0);
  f->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
  f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_rectElemBuf); // two triangles
  for (int c=0; c < nChan; c++) {
      // Translation matrix for each channel
      m_modelView.setToIdentity();
      m_modelView.translate(0, (nChan - c) - 0.5, 0.0);
      m_program->setUniformValue(m_mvMatrixLoc, m_modelView);

      // Set color as appropriate
      if ((c == currentHighlightChannel)&&(!headerDisplay)) {
          m_program->setUniformValue(m_colorLoc, QColor(150,100,100));
      } else if ((dispHWChannels[c] == PSTHTriggerHeaderChannel)&&(headerDisplay)) {
          m_program->setUniformValue(m_colorLoc, QColor(100,100,150));
      } else if ((dispHWChannels[c] == settleControlChannel)&&(headerDisplay)&&(settleControlChannelTriggerState > 0)) {
          m_program->setUniformValue(m_colorLoc, QColor(100,150,100));
      } else {
          m_program->setUniformValue(m_colorLoc, backgroundColor);
      }
      f->glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);
  }
  f->glDisableVertexAttribArray(0);
  f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

  // Paint each channel's lines
  f->glBindBuffer(GL_ARRAY_BUFFER, m_linesBuf);
  f->glEnableVertexAttribArray(0);
  f->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
  for (int c=0; c < nChan; c++) {
      m_modelView.setToIdentity();
      m_modelView.translate(0, (nChan - c) - 0.5, 0.0);
      m_modelView.scale(1.0, 1.0/(2*maxAmplitude[c]), 1.0);
      m_program->setUniformValue(m_mvMatrixLoc, m_modelView);

      m_program->setUniformValue(m_colorLoc, traceColor[c]);

      f->glDrawArrays(GL_LINE_STRIP,c * 2 * EEG_TIME_POINTS,EEG_TIME_POINTS*2);
  }
  f->glDisableVertexAttribArray(0);
  f->glBindBuffer(GL_ARRAY_BUFFER, 0);
  m_program->release();

  painter.endNativePainting();

  QPen pen;
  //We want the lines to be a set number of pixels in width
  //units per pixel = unitrange/window_width
  pen.setWidth((TLength)/this->geometry().width());
  pen.setStyle(Qt::SolidLine);
  pen.setColor(QColor(100,100,150));
  painter.setPen(pen);
  //painter.setPen(Qt::green);

  painter.setWindow(0, 0, EEG_TIME_POINTS, height());

  //Show DIO state for the user-selected channel by highlighting the background
  if (!headerDisplay && PSTHTriggerHeaderChannel > -1) {
      bool currentDState = (streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][0].y > 0);
      int currentStateStart = 0;
      int minMaxInd;
      for (int i=0; i < EEG_TIME_POINTS; i++) {
          minMaxInd=i*2;
          if (currentDState && streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][minMaxInd].y == 0) {

            painter.fillRect(currentStateStart,0,i-currentStateStart,height(),QBrush(QColor(100,100,150,100)));
            currentDState = false;
        } else if (!currentDState && streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][minMaxInd].y > 0) {
            currentStateStart = i;
            currentDState = true;
        }
      }
      if (currentDState) {
          painter.fillRect(currentStateStart,0,EEG_TIME_POINTS - 1 -currentStateStart,height(),QBrush(QColor(100,100,150,100)));
      }


  }

  int backgroundDarkness = backgroundColor.toCmyk().black();
  if (backgroundDarkness > 150) {
      pen.setColor(Qt::white);
  } else {
    pen.setColor(Qt::black);
  }

  //Draw the vertical time marker line
  pen.setStyle(Qt::DotLine);
  painter.setPen(pen);
  float currentTimeLine = streamManager->streamProcessors[0]->dataIdx;
  painter.drawLine(currentTimeLine,0,currentTimeLine, height());

  painter.end();

  //update(); // this yields continuous updating
}

void StreamWidgetGL::initializeGL()
{
  //glEnable(GL_MULTISAMPLE);
    m_program = new QOpenGLShaderProgram;
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex,
        "attribute vec2 vertex;\n"
        "uniform mat4 projMatrix;\n"
        "uniform mat4 mvMatrix;\n"
        "void main(void)\n"
        "{\n"
        "   gl_Position = projMatrix * mvMatrix * vec4(vertex, 0.0, 1.0);\n"
        "}");
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment,
        "uniform mediump vec4 color;\n"
        "void main(void)\n"
        "{\n"
        "   gl_FragColor = color;\n"
        "}");
    m_program->bindAttributeLocation("vertex", 0);
    m_program->link();
    m_program->bind();
    m_projMatrixLoc = m_program->uniformLocation("projMatrix");
    m_mvMatrixLoc = m_program->uniformLocation("mvMatrix");
    m_colorLoc = m_program->uniformLocation("color");
    m_program->release();

//    m_vbo->create();
//    m_vbo->bind();
//    m_vbo->allocate(EEG_TIME_POINTS * 2 * sizeof(vertex2d));
//    m_vbo->release();
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

    f->glGenBuffers(1, &m_linesBuf);
    f->glBindBuffer(GL_ARRAY_BUFFER, m_linesBuf);
    f->glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(vertex2d) * EEG_TIME_POINTS * nChan, 0, GL_DYNAMIC_DRAW);
    f->glBindBuffer(GL_ARRAY_BUFFER, 0);

    float rect_vertices[] = {
         0.0f,  0.5f, // Top-left
         EEG_TIME_POINTS,  0.5f, // Top-right
         EEG_TIME_POINTS, -0.5f, // Bottom-right
         0.0f, -0.5f  // Bottom-left
    };

    GLuint rect_elements[] = {
        0, 1, 2,
        2, 3, 0
    };
    f->glGenBuffers(1, &m_rectVertBuf);
    f->glBindBuffer(GL_ARRAY_BUFFER, m_rectVertBuf);
    f->glBufferData(GL_ARRAY_BUFFER, sizeof(rect_vertices), rect_vertices, GL_DYNAMIC_DRAW);
    f->glBindBuffer(GL_ARRAY_BUFFER, 0);
    f->glGenBuffers(1, &m_rectElemBuf);
    f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_rectElemBuf);
    f->glBufferData(GL_ELEMENT_ARRAY_BUFFER,
        sizeof(rect_elements), rect_elements, GL_DYNAMIC_DRAW);
    f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);



}

void StreamWidgetGL::resizeGL(int w, int h)
{
  QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
  f->glViewport(0, 0, (GLint)(w*this->windowHandle()->devicePixelRatio()), (GLint)(h*this->windowHandle()->devicePixelRatio()));
  //glViewport(0, 0, (GLint)w, (GLint)h);

//  glMatrixMode(GL_PROJECTION);
//  glLoadIdentity();
//  glOrtho(0, FS*TLength, 0, nChan, -1.0, 1.0);
//  glMatrixMode(GL_MODELVIEW);
  m_proj.setToIdentity();
  m_proj.ortho(0, FS*TLength, 0, nChan, -1.0, 1.0);
}

void StreamWidgetGL::mousePressEvent(QMouseEvent* mouseEvent) {

    //if the user clicked on the plotting widget, determine which channel was closest
    //to the mouse to update the audio

    qreal ypos = mouseEvent->localPos().y();
    //qDebug() << ypos << "/" << height();
    int selection = floor((nChan * (ypos/height())));
    int hwChan = dispHWChannels[selection];

    if (mouseEvent->button() == Qt::LeftButton) {
        if ((!headerDisplay)) {
            emit channelClicked(hwChan);
        }
    } else if (mouseEvent->button() == Qt::RightButton) {
        showChannelContextMenu(mouseEvent->pos(), selection);
    }


 }

void StreamWidgetGL::wheelEvent(QWheelEvent *wheelEvent) {
    qreal ypos = wheelEvent->pos().y();
    int selection = floor((nChan * (ypos/height())));
    int changeAmount;
    if (wheelEvent->delta() > 0) {
        changeAmount = -25;
    } else {
        changeAmount = 25;
    }

    if (!headerDisplay) {
        int trodeNum = streamConf->trodeIndexLookupByHWChan[dispHWChannels[selection]];
        int trodeChannel = streamConf->trodeChannelLookupByHWChan[dispHWChannels[selection]];
        int currentMaxDisp = spikeConf->ntrodes[trodeNum]->maxDisp[trodeChannel];
        int newMaxDisp = currentMaxDisp+changeAmount;

        if (newMaxDisp >= 50) {
            //spikeConf->setMaxDisp(trodeNum,trodeChannel,newMaxDisp);
            spikeConf->setMaxDisp(trodeNum,newMaxDisp);
        }
    }

}

void StreamWidgetGL::showChannelContextMenu(const QPoint& pos, int channel) {

    QPoint globalPos = this->mapToGlobal(pos);
    QMenu channelMenu;
    //channelMenu.addAction("nTrode settings...");

    if (headerDisplay) {
        //If this display is for the auxilliary channels, then the context menu is
        //different than for the neural channels.  Here, we set up a menu for things like the
        //PSTH trigger channels, etc.

        bool availableForLogging = false;
        if (headerConf->headerChannels[dispHWChannels[channel]].storeStateChanges) {
            availableForLogging = true;
        }

        channelMenu.addAction(QString("Enable ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " for analysis (increases processor overhead)");
        channelMenu.actions().last()->setCheckable(true);
        if (availableForLogging) {
            channelMenu.actions().last()->setChecked(true);
        } else {
            channelMenu.actions().last()->setChecked(false);
        }

        QMenu *PSTHMenu = channelMenu.addMenu("PSTH trigger...");
        if (!availableForLogging) {
            PSTHMenu->setEnabled(false);
        } else {
            PSTHMenu->setEnabled(true);
        }
        PSTHMenu->addAction(QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " upward edge as PSTH trigger");
        PSTHMenu->actions().last()->setCheckable(true);
        if (!availableForLogging) {
            PSTHMenu->actions().last()->setEnabled(false);
        } else if (PSTHTriggerHeaderChannel == dispHWChannels[channel] && PSTHTriggerState){
            PSTHMenu->actions().last()->setChecked(true);
        }
        PSTHMenu->addAction(QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " downward edge as PSTH trigger");
        PSTHMenu->actions().last()->setCheckable(true);
        if (!availableForLogging) {
            PSTHMenu->actions().last()->setEnabled(false);
        } else if (PSTHTriggerHeaderChannel == dispHWChannels[channel] && !PSTHTriggerState){
            PSTHMenu->actions().last()->setChecked(true);
        }

        if (headerConf->headerChannels[dispHWChannels[channel]].dataType == DeviceChannel::DIGITALTYPE) {
            QMenu *ampMenu = channelMenu.addMenu("Amplifier settle control...");


            ampMenu->addAction(QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " upward edge as settle trigger");
            ampMenu->actions().last()->setCheckable(true);
             if (settleControlChannel == dispHWChannels[channel] && (settleControlChannelTriggerState == 1)){
                ampMenu->actions().last()->setChecked(true);
            }
            ampMenu->addAction(QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " downward edge as settle trigger");
            ampMenu->actions().last()->setCheckable(true);
            if (settleControlChannel == dispHWChannels[channel] && (settleControlChannelTriggerState == 2)){
                ampMenu->actions().last()->setChecked(true);
            }
        }


        QAction* selectedItem = channelMenu.exec(globalPos);
        if (selectedItem) {
            // something was chosen
            if (selectedItem->text() == QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " upward edge as PSTH trigger") {
                if (PSTHTriggerHeaderChannel == dispHWChannels[channel] && PSTHTriggerState){
                    emit newPSTHTrigger(-1,true);
                } else {
                    emit newPSTHTrigger(dispHWChannels[channel],true);
                }
            } else if (selectedItem->text() == QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " downward edge as PSTH trigger") {
                if (PSTHTriggerHeaderChannel == dispHWChannels[channel] && !PSTHTriggerState){
                    emit newPSTHTrigger(-1,true);
                } else {
                    emit newPSTHTrigger(dispHWChannels[channel],false);
                }
            } else if (selectedItem->text() == QString("Enable ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " for analysis (increases processor overhead)") {
                headerConf->headerChannels[dispHWChannels[channel]].storeStateChanges = !headerConf->headerChannels[dispHWChannels[channel]].storeStateChanges;
                if (PSTHTriggerHeaderChannel == dispHWChannels[channel]) {
                    emit newPSTHTrigger(-1,true);
                }
            } else if (selectedItem->text() == QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " upward edge as settle trigger") {
                if (settleControlChannel == dispHWChannels[channel] && (settleControlChannelTriggerState == 2)){
                    emit newSettleControlChannel(dispHWChannels[channel],0);
                } else {
                    emit newSettleControlChannel(dispHWChannels[channel],1);
                }
            } else if (selectedItem->text() == QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " downward edge as settle trigger") {
                if (settleControlChannel == dispHWChannels[channel] && (settleControlChannelTriggerState == 1)){
                    emit newSettleControlChannel(dispHWChannels[channel],0);
                } else {
                    emit newSettleControlChannel(dispHWChannels[channel],2);
                }
            }

        } else {
            // nothing was chosen
        }

    } else {
        channelMenu.addAction("Change nTrode color...");
        channelMenu.addAction("Change background color...");
        QAction* selectedItem = channelMenu.exec(globalPos);
        if (selectedItem) {
            // something was chosen, do stuff
            if (selectedItem->text() == "nTrode settings...") {

            } else if (selectedItem->text() == "Change nTrode color...") {
                showNtrodeColorSelector(channel);
            }  else if (selectedItem->text() == "Change background color...") {
                showBackgroundColorSelector();
            }

        } else {
            // nothing was chosen
        }
    }
}

void StreamWidgetGL::showNtrodeColorSelector(int channel) {
    int trodeNum = streamConf->trodeIndexLookupByHWChan[dispHWChannels[channel]];
    QColorDialog *colorSelector = new QColorDialog(this);
    colorSelector->setCurrentColor(spikeConf->ntrodes[trodeNum]->color);
    colorSelector->setProperty("initialColor",spikeConf->ntrodes[trodeNum]->color);
    colorSelector->setProperty("nTrode",trodeNum);
    connect(colorSelector,SIGNAL(colorSelected(QColor)),this,SLOT(nTrodeColorChosen(QColor)));
    connect(colorSelector,SIGNAL(rejected()),this,SLOT(nTrodeColorSelectorCanceled()));
    colorSelector->exec();
}

void StreamWidgetGL::nTrodeColorChosen(QColor c) {
    int nTrode = sender()->property("nTrode").toInt();
    spikeConf->setColor(nTrode,c);
}

void StreamWidgetGL::nTrodeColorSelectorCanceled() {
    int nTrode = sender()->property("nTrode").toInt();
    QVariant v = sender()->property("initialColor");
    QColor initColor = v.value<QColor>();
    spikeConf->setColor(nTrode,initColor);
}

void StreamWidgetGL::showBackgroundColorSelector() {

    QColorDialog *colorSelector = new QColorDialog(this);
    colorSelector->setCurrentColor(streamConf->backgroundColor);
    colorSelector->setProperty("initialColor",streamConf->backgroundColor);
    connect(colorSelector,SIGNAL(colorSelected(QColor)),this,SLOT(backgroundColorChosen(QColor)));
    connect(colorSelector,SIGNAL(rejected()),this,SLOT(backgroundColorSelectorCanceled()));
    colorSelector->exec();
}

void StreamWidgetGL::backgroundColorChosen(QColor c) {
    streamConf->setBackgroundColor(c);
}

void StreamWidgetGL::backgroundColorSelectorCanceled() {

    QVariant v = sender()->property("initialColor");
    QColor initColor = v.value<QColor>();
    streamConf->setBackgroundColor(initColor);

}



void StreamWidgetGL::setHighlightChannel(int hardwareChannel) {
  currentHighlightChannel = dispHWChannels.indexOf(hardwareChannel);
}

void StreamWidgetGL::updateAxes() {
  makeCurrent();
  resizeGL(width(),height());
}

void StreamWidgetGL::updateMaxDisplay(void){
  // protect totalMaxAmplitude until ready to swap it
  for (int i = 0; i < nChan; i++) {

      maxAmplitude[i] = ((spikeConf->ntrodes[streamConf->trodeIndexLookupByHWChan[dispHWChannels[i]]]->maxDisp[streamConf->trodeChannelLookupByHWChan[dispHWChannels[i]]])*65536)/AD_CONVERSION_FACTOR;

  }
}

void StreamWidgetGL::updateTraceColor(void){
  for (int i = 0; i < nChan; i++) {
      traceColor[i] = spikeConf->ntrodes[streamConf->trodeIndexLookupByHWChan[dispHWChannels[i]]]->color;
  }
}

void StreamWidgetGL::updateBackgroundColor() {
    backgroundColor = streamConf->backgroundColor;
}

void StreamWidgetGL::setChannels(void) {
  //dataObj->updateChannelsFlag = true;
  //qDebug() << "Channel info changed";
}

void StreamWidgetGL::setTLength(double T) {
  TLength = T;
  updateAxes();

}

