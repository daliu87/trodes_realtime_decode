/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "globalObjects.h"
#include "dialogs.h"

#include <cmath>
#include <cfloat>
#include <QtGui>
#include <QGridLayout>
#include <QtAlgorithms>


ExportDialog::ExportDialog(QWidget *) {

    QGridLayout *widgetLayout = new QGridLayout();
    QFont labelFont;
    labelFont.setPixelSize(12);
    labelFont.setFamily("Console");

    //Create the spike export controls
    spikeBox = new QGroupBox(tr("Spikes"),this);
    triggerSelector = new QComboBox();
    triggerSelector->setFont(labelFont);
    triggerSelector->addItem("Current thresholds");
    //triggerSelector->addItem("Standard deviation: 3");
    //triggerSelector->addItem("Standard deviation: 4");
    //triggerSelector->addItem("Standard deviation: 5");

    noiseRemoveSelector = new QComboBox();
    noiseRemoveSelector->setFont(labelFont);
    noiseRemoveSelector->addItem("None");
    //noiseRemoveSelector->addItem("Common events");

    triggerModeLabel = new QLabel("Trigger mode");
    triggerModeLabel->setFont(labelFont);
    noiseLabel = new QLabel("Noise exclusion");
    noiseLabel->setFont(labelFont);
    QGridLayout *spikeBoxLayout = new QGridLayout();
    spikeBoxLayout->addWidget(triggerModeLabel,0,0);
    spikeBoxLayout->addWidget(noiseLabel,0,1);
    spikeBoxLayout->addWidget(triggerSelector,1,0);
    spikeBoxLayout->addWidget(noiseRemoveSelector,1,1);
    spikeBox->setLayout(spikeBoxLayout);
    spikeBox->setCheckable(true);
    spikeBox->setChecked(true);
    //spikeBox->setFixedHeight();
    widgetLayout->addWidget(spikeBox,0,0);

    //Create the ModuleData export controls
    ModuleDataBox = new QGroupBox(tr("ModuleData"),this);
    ModuleDataChannelSelector = new QComboBox();
    ModuleDataChannelSelector->setFont(labelFont);
    ModuleDataChannelSelector->addItem("One per nTrode");
    ModuleDataChannelSelector->addItem("All channels");
    ModuleDataFilterSelector = new QComboBox();
    ModuleDataFilterSelector->setFont(labelFont);
    ModuleDataFilterSelector->setEnabled(false);
    ModuleDataChannelLabel = new QLabel("Channels");
    ModuleDataChannelLabel->setFont(labelFont);
    ModuleDataFilterLabel = new QLabel("Filter");
    ModuleDataFilterLabel->setFont(labelFont);
    QGridLayout *ModuleDataBoxLayout = new QGridLayout();
    ModuleDataBoxLayout->addWidget(ModuleDataChannelLabel,0,0);
    ModuleDataBoxLayout->addWidget(ModuleDataFilterLabel,0,1);
    ModuleDataBoxLayout->addWidget(ModuleDataChannelSelector,1,0);
    ModuleDataBoxLayout->addWidget(ModuleDataFilterSelector,1,1);
    ModuleDataBox->setLayout(ModuleDataBoxLayout);
    ModuleDataBox->setCheckable(true);
    ModuleDataBox->setChecked(false);
    widgetLayout->addWidget(ModuleDataBox,1,0);
    ModuleDataBox->setEnabled(false);

    //Add the buttons
    QGridLayout *buttonLayout = new QGridLayout();
    cancelButton = new QPushButton("Cancel");
    exportButton = new QPushButton("Export");
    buttonLayout->addWidget(cancelButton,0,1);
    buttonLayout->addWidget(exportButton,0,2);
    buttonLayout->setColumnStretch(0,1);
    widgetLayout->addLayout(buttonLayout,2,0);

    //Add the progressbar
    progressBar = new QProgressBar();
    progressBar->setVisible(false);
    widgetLayout->addWidget(progressBar,3,0);

    connect(cancelButton,SIGNAL(clicked()),this,SLOT(cancelButtonPushed()));
    connect(exportButton,SIGNAL(clicked()),this,SLOT(exportButtonBushed()));


    setLayout(widgetLayout);
    setWindowTitle(tr("Export Settings"));

}

void ExportDialog::cancelButtonPushed() {
    emit exportCancelled();
    emit closing();
    this->close();
}

void ExportDialog::exportButtonBushed() {
    progressBar->setVisible(true);
    progressBar->setMinimum(0);
    progressBar->setMaximum(playbackFileSize);
    progressBar->setValue(playbackFileCurrentLocation);
    progressCheckTimer = new QTimer(this);
    progressCheckTimer->setInterval(2000);
    connect(progressCheckTimer,SIGNAL(timeout()),this,SLOT(timerExpired()));

    filePlaybackSpeed = 10; //Fast-forward speed
    emit  startExport(spikeBox->isChecked(), ModuleDataBox->isChecked(), triggerSelector->currentIndex(), noiseRemoveSelector->currentIndex(), ModuleDataChannelSelector->currentIndex(), ModuleDataFilterSelector->currentIndex());

    progressCheckTimer->start();

}

void ExportDialog::timerExpired() {

    //filePlaybackSpeed++;
    progressBar->setValue(playbackFileCurrentLocation);
}


soundDialog::soundDialog(int currentGain, int currentThresh, QWidget *parent)
    :QWidget(parent) {

    TrodesFont dispFont;

    gainSlider = new QSlider(Qt::Vertical);
    gainSlider->setMaximum(100);
    gainSlider->setMinimum(0);
    gainSlider->setSingleStep(1);
    gainSlider->setValue(currentGain);
    gainSlider->setFont(dispFont);
    gainSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Vertical);
    threshSlider->setMaximum(100);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentThresh);
    threshSlider->setFont(dispFont);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    gainDisplay = new QLabel;
    gainDisplay->setNum(currentGain);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentThresh);

    gainTitle = new QLabel(tr("Gain"));
    threshTitle = new QLabel(tr("Threshold"));
    gainTitle->setFont(dispFont);
    threshTitle->setFont(dispFont);
    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(gainTitle,0,0,Qt::AlignCenter);
    mainLayout->addWidget(threshTitle,0,1,Qt::AlignCenter);
    mainLayout->addWidget(gainDisplay,1,0,Qt::AlignCenter);
    mainLayout->addWidget(threshDisplay,1,1,Qt::AlignCenter);
    mainLayout->addWidget(gainSlider,2,0,Qt::AlignHCenter);
    mainLayout->addWidget(threshSlider,2,1,Qt::AlignHCenter);
    mainLayout->setRowStretch(2,1);
    mainLayout->setMargin(10);
    connect(gainSlider,SIGNAL(valueChanged(int)),gainDisplay,SLOT(setNum(int)));
    connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Sound"));

}


waveformGeneratorDialog::waveformGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent)
//waveformGeneratorDialog::waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent)
    :QWidget(parent)
{

    modulatorFreqSpinBox = new QDoubleSpinBox(this);
    modulatorFreqSpinBox->setMaximum(10);
    modulatorFreqSpinBox->setMinimum(0.0);
    modulatorFreqSpinBox->setSingleStep(0.1);
    modulatorFreqSpinBox->setValue(currentModulatorFreq);
    modulatorFreqSpinBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    freqSlider = new QSlider(Qt::Vertical);
    freqSlider->setMaximum(1000);
    freqSlider->setMinimum(1);
    freqSlider->setSingleStep(1);
    freqSlider->setValue(currentFreq);
    freqSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    ampSlider = new QSlider(Qt::Vertical);
    ampSlider->setMaximum(1000);
    ampSlider->setMinimum(0);
    ampSlider->setSingleStep(1);
    ampSlider->setValue(currentAmp);
    ampSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Vertical);
    threshSlider->setMaximum(1000);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentThresh);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


    freqDisplay = new QLabel;
    freqDisplay->setNum(currentFreq);
    ampDisplay = new QLabel;
    ampDisplay->setNum(currentAmp);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentThresh);

    modulatorFreqTitle = new QLabel(tr("Modulator (Hz)"));
    freqTitle = new QLabel(tr("Frequency (Hz)"));
    ampTitle = new QLabel(tr("Amplitude (uV)"));
    threshTitle = new QLabel(tr("Threshold (uV)"));


    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(modulatorFreqTitle,0,0,Qt::AlignCenter);
    mainLayout->addWidget(freqTitle,0,1,Qt::AlignCenter);
    mainLayout->addWidget(ampTitle,0,2,Qt::AlignCenter);
    mainLayout->addWidget(threshTitle,0,3,Qt::AlignCenter);

    mainLayout->addWidget(freqDisplay,1,1,Qt::AlignCenter);
    mainLayout->addWidget(ampDisplay,1,2,Qt::AlignCenter);
    mainLayout->addWidget(threshDisplay,1,3,Qt::AlignCenter);

    mainLayout->addWidget(modulatorFreqSpinBox,2,0,Qt::AlignHCenter);
    mainLayout->addWidget(freqSlider,2,1,Qt::AlignHCenter);
    mainLayout->addWidget(ampSlider,2,2,Qt::AlignHCenter);
    mainLayout->addWidget(threshSlider,2,3,Qt::AlignHCenter);

    mainLayout->setRowStretch(2,1);
    mainLayout->setMargin(10);

    connect(freqSlider,SIGNAL(valueChanged(int)),freqDisplay,SLOT(setNum(int)));
    connect(ampSlider,SIGNAL(valueChanged(int)),ampDisplay,SLOT(setNum(int)));
    connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Waveform generator"));

}

void waveformGeneratorDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    event->accept();
}



spikeGeneratorDialog::spikeGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent)
  :QWidget(parent)

{
  modulatorFreqSpinBox = new QDoubleSpinBox(this);
  modulatorFreqSpinBox->setMaximum(10);
  modulatorFreqSpinBox->setMinimum(0.0);
  modulatorFreqSpinBox->setSingleStep(0.1);
  modulatorFreqSpinBox->setValue(currentModulatorFreq);
  modulatorFreqSpinBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  freqSlider = new QSlider(Qt::Vertical);
  freqSlider->setMaximum(1000);
  freqSlider->setMinimum(1);
  freqSlider->setSingleStep(1);
  freqSlider->setValue(currentFreq);
  freqSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  ampSlider = new QSlider(Qt::Vertical);
  ampSlider->setMaximum(1000);
  ampSlider->setMinimum(0);
  ampSlider->setSingleStep(1);
  ampSlider->setValue(currentAmp);
  ampSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  threshSlider = new QSlider(Qt::Vertical);
  threshSlider->setMaximum(1000);
  threshSlider->setMinimum(0);
  threshSlider->setSingleStep(1);
  threshSlider->setValue(currentThresh);
  threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


  freqDisplay = new QLabel;
  freqDisplay->setNum(currentFreq);
  ampDisplay = new QLabel;
  ampDisplay->setNum(currentAmp);
  threshDisplay = new QLabel;
  threshDisplay->setNum(currentThresh);

  modulatorFreqTitle = new QLabel(tr("Modulator (Hz)"));
  freqTitle = new QLabel(tr("Frequency (Hz)"));
  ampTitle = new QLabel(tr("Amplitude (uV)"));
  threshTitle = new QLabel(tr("Threshold (uV)"));


  QGridLayout *mainLayout = new QGridLayout;

  mainLayout->addWidget(modulatorFreqTitle,0,0,Qt::AlignCenter);
  mainLayout->addWidget(freqTitle,0,1,Qt::AlignCenter);
  mainLayout->addWidget(ampTitle,0,2,Qt::AlignCenter);
  mainLayout->addWidget(threshTitle,0,3,Qt::AlignCenter);

  mainLayout->addWidget(freqDisplay,1,1,Qt::AlignCenter);
  mainLayout->addWidget(ampDisplay,1,2,Qt::AlignCenter);
  mainLayout->addWidget(threshDisplay,1,3,Qt::AlignCenter);

  mainLayout->addWidget(modulatorFreqSpinBox,2,0,Qt::AlignHCenter);
  mainLayout->addWidget(freqSlider,2,1,Qt::AlignHCenter);
  mainLayout->addWidget(ampSlider,2,2,Qt::AlignHCenter);
  mainLayout->addWidget(threshSlider,2,3,Qt::AlignHCenter);

  mainLayout->setRowStretch(2,1);
  mainLayout->setMargin(10);

  connect(freqSlider,SIGNAL(valueChanged(int)),freqDisplay,SLOT(setNum(int)));
  connect(ampSlider,SIGNAL(valueChanged(int)),ampDisplay,SLOT(setNum(int)));
  connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

  setLayout(mainLayout);
  setWindowTitle(tr("Spikes+Waveform generator"));

}

void spikeGeneratorDialog::closeEvent(QCloseEvent *event)
{
  emit windowClosed();
  event->accept();
}


//-----------------------------------------------------
HeadstageSettingsDialog::HeadstageSettingsDialog(HeadstageSettings settings, QWidget *parent):QWidget(parent)
{
    setMinimumWidth(300);

    currentSettings = settings;
    settingsChanged = false;

    //Create the auto settle controls
    autoSettleBox = new QGroupBox(tr("Auto settle"),this);
    autoSettleBox->setCheckable(true);
    QGridLayout *autoSettleLayout = new QGridLayout;

    percentChannelsSlider = new QSlider(Qt::Horizontal);
    percentChannelsSlider->setMaximum(100);
    percentChannelsSlider->setMinimum(0);
    percentChannelsSlider->setSingleStep(1);
    percentChannelsSlider->setValue(currentSettings.percentChannelsForSettle);
    percentChannelsSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Horizontal);
    threshSlider->setMaximum(2000);
    threshSlider->setMinimum(500);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentSettings.threshForSettle);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    connect(percentChannelsSlider,SIGNAL(sliderMoved(int)),this,SLOT(percentSliderChanged(int)));
    connect(threshSlider,SIGNAL(sliderMoved(int)),this,SLOT(threshSliderChanged(int)));

    threshIndicator = new QLabel(QString().number(currentSettings.threshForSettle));
    percentIndicator = new QLabel(QString().number(currentSettings.percentChannelsForSettle));

    percentTitle = new QLabel(tr("Channels over thresh (%)"));
    threshTitle = new QLabel(tr("Threshold (uV)"));

    autoSettleLayout->addWidget(threshTitle,0,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(threshSlider,1,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(percentTitle,2,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(percentChannelsSlider,3,0,Qt::AlignLeft);

    autoSettleLayout->addWidget(threshIndicator,1,1,Qt::AlignRight);
    autoSettleLayout->addWidget(percentIndicator,3,1,Qt::AlignRight);

    autoSettleBox->setLayout(autoSettleLayout);
    autoSettleBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);




    QGridLayout *secRowLayout = new QGridLayout;

    //Create smart ref controls
    smartReferenceBox = new QGroupBox(tr("Sampling sequence correction"),this);
    smartReferenceBox->setCheckable(false);
    QGridLayout *smartReferenceLayout = new QGridLayout;
    smartRefCheckBox = new QCheckBox();
    smartRefCheckBox->setText("Enable");
    if (!currentSettings.smartRefAvailable) {
        smartRefCheckBox->setChecked(false);
        smartRefCheckBox->setEnabled(false);
    } else {
        smartRefCheckBox->setChecked(currentSettings.smartRefOn);
    }
    smartRefCheckBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    smartReferenceLayout->addWidget(smartRefCheckBox,0,0);
    smartReferenceBox->setLayout(smartReferenceLayout);
    smartReferenceBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    connect(smartRefCheckBox,SIGNAL(toggled(bool)),this,SLOT(smartRefToggled(bool)));
    secRowLayout->addWidget(smartReferenceBox,0,0,Qt::AlignCenter);

    //Create sensor controls
    sensorBox = new QGroupBox(tr("Motion sensors"),this);
    sensorBox->setCheckable(false);
    QGridLayout *sensorLayout = new QGridLayout;
    accelCheckBox = new QCheckBox();
    accelCheckBox->setText("Accel.");
    sensorLayout->addWidget(accelCheckBox,0,0);
    gyroCheckBox = new QCheckBox();
    gyroCheckBox->setText("Gyro");
    sensorLayout->addWidget(gyroCheckBox,0,1);
    magnetCheckBox = new QCheckBox();
    magnetCheckBox->setText("Compass");
    sensorLayout->addWidget(magnetCheckBox,0,2);
    if (!currentSettings.accelSensorAvailable) {
        accelCheckBox->setChecked(false);
        accelCheckBox->setEnabled(false);
    } else {
        accelCheckBox->setChecked(currentSettings.accelSensorOn);
    }

    if (!currentSettings.gyroSensorAvailable) {
        gyroCheckBox->setChecked(false);
        gyroCheckBox->setEnabled(false);
    } else {
        gyroCheckBox->setChecked(currentSettings.gyroSensorOn);
    }

    if (!currentSettings.magSensorAvailable) {
        magnetCheckBox->setChecked(false);
        magnetCheckBox->setEnabled(false);
    } else {
        magnetCheckBox->setChecked(currentSettings.magSensorOn);
    }

    sensorBox->setLayout(sensorLayout);
    sensorBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    connect(accelCheckBox,SIGNAL(toggled(bool)),this,SLOT(accelToggled(bool)));
    connect(gyroCheckBox,SIGNAL(toggled(bool)),this,SLOT(gyroToggled(bool)));
    connect(magnetCheckBox,SIGNAL(toggled(bool)),this,SLOT(magToggled(bool)));
    secRowLayout->addWidget(sensorBox,0,2,Qt::AlignCenter);

    secRowLayout->setColumnStretch(1,1);
    secRowLayout->setMargin(20);

    okButton = new TrodesButton();
    okButton->setText("Send");
    okButton->setEnabled(false);
    cancelButton = new TrodesButton();
    cancelButton->setText("Cancel");
    connect(okButton,SIGNAL(pressed()),this,SLOT(okButtonPressed()));
    connect(cancelButton,SIGNAL(pressed()),this,SLOT(cancelButtonPressed()));

    QGridLayout *buttonLayout = new QGridLayout();
    buttonLayout->addWidget(cancelButton,0,1);
    buttonLayout->addWidget(okButton,0,2);
    buttonLayout->setColumnStretch(0,1);
    buttonLayout->setContentsMargins(10,10,10,10);


    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setMargin(10);
    mainLayout->setContentsMargins(10,10,10,10);
    mainLayout->setVerticalSpacing(10);

    mainLayout->addWidget(autoSettleBox,0,0,Qt::AlignCenter);
    mainLayout->addLayout(secRowLayout,1,0,Qt::AlignCenter);
    mainLayout->addLayout(buttonLayout,2,0);

    setLayout(mainLayout);

    if (currentSettings.autosettleAvailable) {
        autoSettleBox->setChecked(currentSettings.autoSettleOn);
    } else {
        autoSettleBox->setChecked(false);
        autoSettleBox->setEnabled(false);
    }

    connect(autoSettleBox,SIGNAL(toggled(bool)),this,SLOT(autoSettleOnToggled(bool)));




    setWindowTitle(tr("Headstage firmware settings"));

}

void HeadstageSettingsDialog::percentSliderChanged(int value) {
    percentIndicator->setText(QString().number(value));
    currentSettings.percentChannelsForSettle = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::threshSliderChanged(int value) {
    threshIndicator->setText(QString().number(value));
    currentSettings.threshForSettle = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::autoSettleOnToggled(bool on) {
    currentSettings.autoSettleOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::smartRefToggled(bool on) {
    currentSettings.smartRefOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::accelToggled(bool on) {
    currentSettings.accelSensorOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::gyroToggled(bool on) {
    currentSettings.gyroSensorOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::magToggled(bool on) {
    currentSettings.magSensorOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::okButtonPressed() {
    emit newSettings(currentSettings); //send the new settings to the source controller
    this->close();
}

void HeadstageSettingsDialog::cancelButtonPressed() {
    this->close();
}

void HeadstageSettingsDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    event->accept();
}

void HeadstageSettingsDialog::resizeEvent(QResizeEvent *event) {
    //autoSettleBox->setGeometry(this->geometry());
    autoSettleBox->setFixedWidth(this->geometry().width()-50);
    percentChannelsSlider->setFixedWidth(this->geometry().width()-120);
    threshSlider->setFixedWidth(this->geometry().width()-120);
}



//-----------------------------------------------------
//This dialog pops up when the "Annotate" button is pressed.
//Allows the user to add comments to the recording session. All comments are
//stores in file that is separate from the main recording file, but with the same base name.

CommentDialog::CommentDialog(QString fileNameIn, QWidget *parent)
    :QWidget(parent),
    fileName(fileNameIn) {

    bool enableControls = false;
    TrodesFont dispFont;

    if (!fileName.isEmpty()) {
        enableControls = true;
        QFile commentFile(fileName);
    }

    newCommentEdit = new QLineEdit();
    newCommentEdit->setFont(dispFont);
    newCommentEdit->setMinimumWidth(200);
    newCommentEdit->setFrame(true);
    newCommentEdit->setStyleSheet("QLineEdit {border: 2px solid lightgray;"
                                "border-radius: 4px;"
                                "padding: 2px;"
                                "background-color: white}");
    newCommentEdit->setEnabled(enableControls);

    connect(newCommentEdit,SIGNAL(returnPressed()),this,SLOT(saveCurrentComment()));
    connect(newCommentEdit,SIGNAL(textChanged(QString)),this,SLOT(somethingEntered()));

    saveButton = new TrodesButton();
    saveButton->setText("Save");
    saveButton->setEnabled(false);

    QLabel* historyLabel = new QLabel();
    historyLabel->setFont(dispFont);
    historyLabel->setText("History:");
    historyLabel->setEnabled(enableControls);
    //lastComment = new QLabel();
    lastComment = new QTextEdit();
    lastComment->setFont(dispFont);
    //sec2lastComment = new QLabel();
    lastComment->setStyleSheet("QFrame {border: 2px solid lightgray;"
                                "border-radius: 4px;"
                                "padding: 2px;"
                                "background-color: white}"
                                "QLabel {color : gray;}");
    lastComment->setMinimumWidth(200);
    lastComment->setEnabled(enableControls);

    commentLabel = new QLabel();
    commentLabel->setFont(dispFont);
    commentLabel->setText("New comment:");
    commentLabel->setEnabled(enableControls);

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *commentLayout = new QGridLayout;
    QGridLayout *buttonLayout = new QGridLayout;

    //commentLayout->addWidget(sec2lastComment,0,0,Qt::AlignCenter);
    commentLayout->addWidget(historyLabel,0,0,Qt::AlignLeft);
    commentLayout->addWidget(lastComment,1,0,Qt::AlignCenter);

    commentLayout->setColumnStretch(0,1);
    commentLayout->setContentsMargins(0,0,0,0);

    buttonLayout->addWidget(saveButton,0,2,Qt::AlignHCenter);
    buttonLayout->setColumnStretch(0,1);

    mainLayout->addLayout(commentLayout,1,0);
    //mainLayout->addWidget(historyFrame,1,0,Qt::AlignRight);
    mainLayout->addWidget(commentLabel,2,0,Qt::AlignLeft);
    mainLayout->addWidget(newCommentEdit,3,0,Qt::AlignCenter);
    mainLayout->addLayout(buttonLayout,4,0);
    //mainLayout->setMargin(10);

    connect(saveButton,SIGNAL(pressed()),this,SLOT(saveCurrentComment()));

    setLayout(mainLayout);
    getHistory();

}

void CommentDialog::saveLine() {
    //Saves the current line to file
    if (saveButton->isEnabled()) {
        if (!fileName.isEmpty()) {
            //append the current timestamp to the comment
            QString currentComment = QString("%1  ").arg(currentTimeStamp) + newCommentEdit->text()+"\n";
            //write to the file
            QFile commentFile(fileName);
            commentFile.open(QIODevice::Append);
            commentFile.write(currentComment.toLocal8Bit());
            commentFile.close();
        }
        newCommentEdit->clear();
        //update the history box
        getHistory();
    }
}

void CommentDialog::getHistory() {
    //Read the contents of the file and populate the "history" box

    QString contents;

    if (!fileName.isEmpty()) {
        QFile commentFile(fileName);
        if (commentFile.exists()) {
            //read the entire file
            commentFile.open(QIODevice::ReadOnly);
            contents = QString(commentFile.readAll());
            commentFile.close();
        }
    }

    lastComment->setText(contents);
    //set the scrollbar to show the last few lines
    QScrollBar *bar = lastComment->verticalScrollBar();
    bar->setValue(bar->maximum());

}


void CommentDialog::saveCurrentComment() {

    //add the comment to the file
    saveLine();
    //close();
}


void CommentDialog::somethingEntered() {
    //we don't enable the save button unless something has been entered
    saveButton->setEnabled(true);
}

void CommentDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}

//---------------------------------------------


//-------------------------------------------
RasterPlot::RasterPlot(QWidget *parent) {

    setMinimumHeight(10);
    setMinimumWidth(200);
    minX = 0;
    maxX = 10;
    setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding );

    /*
    QVector<qreal> tmpValues;
    for (int i = 0; i < 200; i++) {
        tmpValues.append(i);
    }

    setData(tmpValues);*/

}


void RasterPlot::setXRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    //xTickLabels.clear();
    //xTickLabels.append(minX);
    //xTickLabels.append(maxX);
    //xTickLabels.append((maxX-minX)/2);
    update();
}

void RasterPlot::setXLabel(QString l) {
    xLabel = l;
    update();
}

void RasterPlot::setYLabel(QString l) {
    yLabel = l;
    update();
}

void RasterPlot::addRaster(const QVector<qreal> &times) {
    eventTimes.push_back(times);
    //setMinimumHeight(eventTimes.length()*4);
    update();
}

void RasterPlot::setRasters(const QVector<QVector<qreal> > &times) {
    eventTimes.clear();
    for (int i=0; i<times.length(); i++) {
        eventTimes.push_back(times[i]);
    }

    //Add y axis ticks
    yTickLabels.clear();
    yTickLabels.append(0);
    yTickLabels.append(eventTimes.length()/2);
    yTickLabels.append(eventTimes.length());

    update();
}

void RasterPlot::clearRasters() {
    eventTimes.clear();
    update();
}

void RasterPlot::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;
    //We want the lines to be a set number of pixels in width
    //units per pixel = unitrange/window_width
    pen.setWidth(1);

    //pen.setWidth(100*TLength);

    pen.setColor(Qt::black);
    painter.setPen(pen);
    QFont f;
    f.setPixelSize(10);
    painter.setFont(f);
    //painter.setPen(Qt::green);
    painter.setViewTransformEnabled(true);
    painter.setViewport(0,0,width(), height());


    qreal numRows = eventTimes.length();
    //qreal maxRange = maxY;

    int margin = 30;

    int topOfGraph = 5;
    int bottomOfGraph = height()-5;
    // int graphHeight = bottomOfGraph-topOfGraph; // UNUSED

    int leftOfGraph = margin+20;
    int rightOfGraph = width()-margin;
    // int graphWidth = rightOfGraph-leftOfGraph; // UNUSED

    int rowSpacing = 1;
    int tickHeight = ((bottomOfGraph - topOfGraph)/numRows) - (2*rowSpacing);
    if (tickHeight < 1) {
        tickHeight = 1;
    }


    if ((maxX-minX) > 0) {
        for (int r=0; r < eventTimes.length(); r++) {
            int currentRowPix = topOfGraph + rowSpacing + (r*((bottomOfGraph - topOfGraph)/numRows));
            for (int e = 0; e < eventTimes[r].length(); e++) {
                if ((eventTimes[r][e] >= minX) && (eventTimes[r][e] <= maxX)) {
                    int xLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*((eventTimes[r][e]-minX)/(maxX-minX)));
                    painter.drawLine(xLoc,currentRowPix,xLoc,currentRowPix+tickHeight);
                }
            }
        }
    }



//    //Draw the axes
//    painter.drawLine(leftOfGraph,bottomOfGraph,rightOfGraph+5,bottomOfGraph);
    painter.drawLine(leftOfGraph,bottomOfGraph,leftOfGraph,topOfGraph-5);

//    //Display the axis labels
//    QRect YLabelBox1= QRect(leftOfGraph, height()-25, rightOfGraph-leftOfGraph, 25);
//    painter.drawText(YLabelBox1, Qt::AlignCenter, xLabel);

    painter.rotate(-90);
    QRect YLabelBox= QRect(-bottomOfGraph, 0, bottomOfGraph-topOfGraph, 25);
    painter.drawText(YLabelBox, Qt::AlignCenter, yLabel);
    painter.rotate(90);

//    //Put unit labels on the x axis

//    for (int i = 0; i < xTickLabels.length(); i++) {
//        int xTickLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*(xTickLabels[i]/(maxX-minX)));
//        QRect labelBox = QRect(xTickLoc-10, bottomOfGraph, 20, 20);
//        painter.drawText(labelBox, Qt::AlignCenter,
//                         QString("%1").arg(xTickLabels[i]));
//        painter.drawLine(xTickLoc,bottomOfGraph,xTickLoc,bottomOfGraph+2);
//    }



    //Put unit labels on the y axis
    for (int i = 0; i < yTickLabels.length(); i++) {
//        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY)));
        int yTickLoc =// bottomOfGraph-((bottomOfGraph-topOfGraph)*((qreal)i/(yTickLabels.length()-1)));
        topOfGraph + (i*((bottomOfGraph - topOfGraph)/(yTickLabels.length()-1)));
        QRect labelBox= QRect(leftOfGraph-margin, yTickLoc-10, 25, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString("%1").arg(yTickLabels[i]));
        painter.drawLine(leftOfGraph,yTickLoc,leftOfGraph-2,yTickLoc);

    }

    painter.end();
}

void RasterPlot::resizeEvent(QResizeEvent *event) {

    update();

}



//--------------------------------------------

HistogramPlot::HistogramPlot(QWidget *parent) {

    setMinimumHeight(100);
    setMinimumWidth(200);
    maxY = 0.0;

    //setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding );



//    QVector<qreal> tmpValues;
//    for (int i = 0; i < 200; i++) {
//        tmpValues.append(i);
//    }

//    setData(tmpValues);
    errorsIndicator = false;
    binsIndicator = false;
    bin1 = new QRectF();
    bin2 = new QRectF();

    margin = 30;
    topOfGraph = 5;
    for(int i = 0; i < 4; i++)
        checked[i] = false;

    clicking = false;
}


void HistogramPlot::setXRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    xTickLabels.clear();
    xTickLabels.append(minX);
    xTickLabels.append(maxX);
    xTickLabels.append(((maxX-minX)/2)+minX);
    update();
}

void HistogramPlot::setXLabel(QString l) {
    xLabel = l;
    update();
}

void HistogramPlot::setYLabel(QString l) {
    yLabel = l;
    update();
}

void HistogramPlot::setData(QVector<qreal> bvalues){
    //Copy values and set MaxY
    maxY = 0.0;
    barValues.clear();
    for (int i=0; i<bvalues.length(); i++) {
        barValues.append(bvalues[i]);
        if (barValues[i] > maxY) {
            maxY = barValues[i];
        }
    }
    minX = 0;
    maxX = bvalues.length();

    xTickLabels.clear();
    xTickLabels.append(minX);
    xTickLabels.append(maxX);
    xTickLabels.append(((maxX-minX)/2)+minX);

    yTickLabels.clear();
    yTickLabels.append(maxY);

    update();
}

void HistogramPlot::clearData(){
    barValues.clear();
    update();
}

void HistogramPlot::clickingOn(bool on, int cl){
    clicking = on;
    cluster = cl;
    setMouseTracking(on);
}

void HistogramPlot::setErrorBars(QVector<QVector<qreal> > hilowErrValues){
    for(int i = 2; i<hilowErrValues.length(); i+=2)
        for(int j = 0; j<hilowErrValues[i].length(); j++)
            if(checked[i/2] && hilowErrValues[i][j] > maxY)
                maxY = hilowErrValues[i][j];

    hiloerrorBarValues.clear();
    hiloerrorBarValues = hilowErrValues;
}

void HistogramPlot::setChecked(bool checked[]){
    errorsIndicator = true;
//    for(int i = 0; i < 4; i++){
//        this->checked[i] = checked[i];
//    }
    this->checked[1] = checked[1];
}

void HistogramPlot::setBinValues(qreal bin1Start, qreal bin1Size, qreal bin2Start, qreal bin2Size){
    this->binsIndicator = true;
    this->bin1Start = bin1Start;
    this->bin1Size = bin1Size;
    this->bin2Start = bin2Start;
    this->bin2Size = bin2Size;
}

void HistogramPlot::mouseReleaseEvent(QMouseEvent *event){
    if(clicking){
        emit PSTHRequest(cluster);
//        qDebug() << "emitted mouse release";
    }
}

void HistogramPlot::mouseMoveEvent(QMouseEvent *event){
    if(clicking){
        this->setStyleSheet("HistogramPlot:hover {border: 2px solid red;"
                            "border-radius: 4px;"
                            "padding: 2px;}");
//        hover = true;
    }
}

void HistogramPlot::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;

    QStyleOption opt;
    opt.init(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
    //We want the lines to be a set number of pixels in width
    //units per pixel = unitrange/window_width
    pen.setWidth(1);

    //pen.setWidth(100*TLength);

    pen.setColor(Qt::black);
    painter.setPen(pen);
    QFont f;
    f.setPixelSize(10);
    painter.setFont(f);
    //painter.setPen(Qt::green);
    painter.setViewTransformEnabled(true);
    painter.setViewport(0,0,width(), height());

    qreal numBars = barValues.length();
    qreal maxRange = maxY;


    int bottomOfGraph = height()-margin-10;
    int graphHeight = bottomOfGraph-topOfGraph;

    int leftOfGraph = margin+20;
    int rightOfGraph = width()-margin;
    int graphWidth = rightOfGraph-leftOfGraph;

    Qt::GlobalColor colors[4] = {Qt::red, Qt::black, Qt::blue, Qt::magenta};


    //Draw the axes
    painter.drawLine(leftOfGraph,bottomOfGraph,rightOfGraph+5,bottomOfGraph);
    painter.drawLine(leftOfGraph,bottomOfGraph,leftOfGraph,topOfGraph-5);
    for (int c=0; c < barValues.length(); c++) {
        //bar values
        qreal xcorner = leftOfGraph + (c*(graphWidth/numBars));
        qreal ycorner = bottomOfGraph-((barValues[c]/maxRange)*graphHeight);
        qreal barwidth = graphWidth/numBars;
        qreal barheight = (barValues[c]/maxRange)*graphHeight;

        //Draw histogram bars
        QRectF rect = QRectF(xcorner, ycorner, barwidth, barheight);
        //QRectF rect = QRectF(c*(width()/numBars), height()-((barValues[c]/maxRange)*height()), (width()/numBars), ((barValues[c]/maxRange)*height()));
        painter.drawRect(rect);
        painter.fillRect(rect,QBrush(QColor(100,100,100)));
    }


    if(errorsIndicator){
        int numOfErrBars = 0;
        for(int i = 0; i < 4; i++)
            numOfErrBars += (int)checked[i];

        for (int c=0; c < barValues.length(); c++) {
            int ithbar = 0;
            qreal xcorner = leftOfGraph + (c*(graphWidth/numBars));
            for(int i = 0; i < 4; i++){
                if(!checked[i])
                    continue;
                ithbar++;
                painter.setPen(colors[i]);
                qreal xMid = xcorner + (((qreal)(ithbar)/(numOfErrBars+1))*(graphWidth/numBars));
                qreal yMidhigher = bottomOfGraph-((hiloerrorBarValues[2*i][c])*graphHeight/maxRange);
                qreal yMidlower = bottomOfGraph-((hiloerrorBarValues[2*i+1][c])*graphHeight/maxRange);

                QLine line = QLine(xMid, yMidhigher, xMid, yMidlower);
                QLine tick1 = QLine(xMid-2, yMidhigher, xMid+2, yMidhigher);
                QLine tick2 = QLine(xMid-2, yMidlower, xMid+2, yMidlower);

                painter.drawLine(line);
                painter.drawLine(tick1);
                painter.drawLine(tick2);
            }
            painter.setPen(pen);
        }
    }

    if(binsIndicator){
        //Draw bin 1 and bin 2 indicators
        QPen bin1Pen(QColor(255, 0, 0, 100));
        QPen bin2Pen(QColor(0, 0, 255, 100));
        qreal bin1Left = leftOfGraph + (bin1Start*(graphWidth)/numBars);
        qreal bin1Width = bin1Size*(graphWidth/numBars);
        QRectF bin1Rect(bin1Left, topOfGraph, bin1Width, graphHeight);
        qreal bin2Left = leftOfGraph + (bin2Start*(graphWidth)/numBars);
        qreal bin2Width = bin2Size*(graphWidth/numBars);
        QRectF bin2Rect(bin2Left, topOfGraph, bin2Width, graphHeight);
        painter.setPen(bin1Pen);
        painter.drawRect(bin1Rect);
        painter.setPen(bin2Pen);
        painter.drawRect(bin2Rect);
        painter.fillRect(bin1Rect, QColor(255, 0, 0, 15));
        painter.fillRect(bin2Rect, QColor(0, 0, 255, 15));

        painter.setPen(pen);
    }
    //Put unit labels on the x axis

    for (int i = 0; i < xTickLabels.length(); i++) {
        int xTickLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*((xTickLabels[i]-minX)/(maxX-minX)));
        QRect labelBox = QRect(xTickLoc-10, bottomOfGraph, 20, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString::number(xTickLabels[i], 'f', 1 ));
        painter.drawLine(xTickLoc,bottomOfGraph,xTickLoc,bottomOfGraph+2);
    }

    //Put unit labels on the y axis
    for (int i = 0; i < yTickLabels.length(); i++) {
        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY)));
        QRect labelBox= QRect(leftOfGraph-margin, yTickLoc-10, 25, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString::number(yTickLabels[i], 'f', 1 ));
        painter.drawLine(leftOfGraph,yTickLoc,leftOfGraph-2,yTickLoc);

    }


    //Display the axis labels
    QRect YLabelBox1= QRect(leftOfGraph, height()-25, rightOfGraph-leftOfGraph, 25);
    painter.drawText(YLabelBox1, Qt::AlignCenter, xLabel);

    painter.rotate(-90);
    QRect YLabelBox= QRect(-bottomOfGraph, 0, bottomOfGraph-topOfGraph, 25);
    painter.drawText(YLabelBox, Qt::AlignCenter, yLabel);
    painter.rotate(90);

    painter.end();
}


void HistogramPlot::resizeEvent(QResizeEvent *event) {
    //scene->setSceneRect(0,0,event->size().width(),event->size().height());
    //scene->setSceneRect(0,0,event->size().width(),event->size().height());
    update();

}

//-------------------------------------------

PSTHDialog::PSTHDialog(QWidget *parent)
    :QWidget(parent){

    setMinimumHeight(300);
    setMinimumWidth(300);


    rasterWindow = new RasterPlot(this);
    rasterWindow->setYLabel("Trial Number");
    window = new HistogramPlot(this);
    window->setMaximumHeight(200);
    window->setXLabel("Time relative to event (sec)");
    window->setYLabel("Rate (Hz)");

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *plotLayout = new QGridLayout;
    plotLayout->setVerticalSpacing(0);
    plotLayout->setContentsMargins(0,0,0,0);
    plotLayout->addWidget(rasterWindow,0,0);
    plotLayout->addWidget(window,1,0);
    plotLayout->setRowStretch(0,3);
    plotLayout->setRowStretch(1,1);
    mainLayout->addLayout(plotLayout,0,0);

    setUpControlPanel();
    //controlLayout->setColumnStretch(0,1);
    mainLayout->addLayout(controlLayout,1,0);
    mainLayout->setRowStretch(0,1);


    setLayout(mainLayout);
    window->show();
    rasterWindow->show();

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("PSTHposition")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    settings.beginGroup(QLatin1String("PSTHsettings"));
    int tempNumBins = settings.value(QLatin1String("numbins")).toInt();
    if (tempNumBins > 0) {
        numBinsSpinBox->setValue(tempNumBins);
    } else {
        numBinsSpinBox->setValue(100);
    }

    int tempMsecRange = settings.value(QLatin1String("msecRange")).toInt();
    if (tempMsecRange > 0) {
        rangeSpinBox->setValue(tempMsecRange);
    } else {
        rangeSpinBox->setValue(500);
    }

    /*absTimeRange = 1.0;
    numBins = 80; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;*/
    int tmpValue;
//    tmpValue = settings.value(QLatin1String("sdCheck")).toInt();
//    setSDChecked(tmpValue);
//    SDCheckbox->setChecked((bool)tmpValue);

    tmpValue = settings.value(QLatin1String("seCheck")).toInt();
    setSEChecked(tmpValue);
    SECheckbox->setChecked((bool)tmpValue);

//    tmpValue = settings.value(QLatin1String("rCheck")).toInt();
//    setRChecked(tmpValue);
//    RCheckbox->setChecked((bool)tmpValue);

//    tmpValue = settings.value(QLatin1String("ciCheck")).toInt();
//    setCIChecked(settings.value(QLatin1String("ciCheck")).toInt());
//    CICheckbox->setChecked((bool)tmpValue);


    tmpValue = settings.value(QLatin1String("bin1Start")).toInt();
    if(tmpValue)
        binOneStartBox->setValue(tmpValue);
    else
        binOneStartBox->setValue(-200);

    tmpValue = settings.value(QLatin1String("bin1Size")).toInt();
    if(tmpValue)
        binOneSizeBox->setValue(tmpValue);
    else
        binOneSizeBox->setValue(100);

    tmpValue = settings.value(QLatin1String("bin2Start")).toInt();
    if(tmpValue)
        binTwoStartBox->setValue(tmpValue);
    else
        binTwoStartBox->setValue(200);

    tmpValue = settings.value(QLatin1String("bin2Size")).toInt();
    if(tmpValue)
        binTwoSizeBox->setValue(tmpValue);
    else
        binTwoSizeBox->setValue(100);

    settings.endGroup();


    /*
    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;

    for (uint32_t t = 5; t < 1000000; t = t+30000) {
        trialTimes.append(t);
    }

    for (uint32_t t = 5; t < 1000000; t = t+5000) {
        eventTimes.append(t);
    }

    plot(trialTimes,eventTimes);*/

}

//Creates everything for bottom of PSTH window
void PSTHDialog::setUpControlPanel(){
    rangeLabel = new QLabel("+/- msec");
    binsLabel = new QLabel("Bins");
    QFont labelFont;
    labelFont.setPixelSize(12);
    rangeLabel->setFont(labelFont);
    binsLabel->setFont(labelFont);
    rangeLabel->setMaximumHeight(25);
    binsLabel->setMaximumHeight(25);

    rangeSpinBox = new QSpinBox(this);
    rangeSpinBox->setFrame(false);
    rangeSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    rangeSpinBox->setMinimum(50);
    rangeSpinBox->setMaximum(1000);
    rangeSpinBox->setSingleStep(10);
    rangeSpinBox->setFixedSize(50,22);
    //rangeSpinBox->setAlignment(Qt::AlignRight);
    rangeSpinBox->setFocusPolicy(Qt::NoFocus);
    rangeSpinBox->setToolTip(tr("Display range (+/- msec)"));

    numBinsSpinBox = new QSpinBox(this);
    numBinsSpinBox->setFrame(false);
    numBinsSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    numBinsSpinBox->setMinimum(20);
    numBinsSpinBox->setMaximum(200);
    numBinsSpinBox->setSingleStep(1);
    numBinsSpinBox->setFixedSize(50,22);
    //rangeSpinBox->setAlignment(Qt::AlignRight);
    numBinsSpinBox->setFocusPolicy(Qt::NoFocus);
    numBinsSpinBox->setToolTip(tr("Number of bins"));

//    SDCheckbox = new QCheckBox("Std. Dev",this);
    SECheckbox = new QCheckBox("Std. Error", this);
//    RCheckbox = new QCheckBox("Range", this);
//    CICheckbox = new QCheckBox("Conf. Interval", this);
//    SDCheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: red }");
    SECheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: black }");
//    RCheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: blue }");
//    CICheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: magenta }");

    binOneStartLabel = new QLabel("Bin 1 Start");
    binOneSizeLabel = new QLabel("Bin 1 Size");
    binTwoStartLabel = new QLabel("Bin 2 Start");
    binTwoSizeLabel = new QLabel("Bin 2 Size");
    binOneStartLabel->setFont(labelFont);
    binOneSizeLabel->setFont(labelFont);
    binTwoStartLabel->setFont(labelFont);
    binTwoSizeLabel->setFont(labelFont);

    binOneStartBox = new QSpinBox(this);
    binOneStartBox->setMinimum(-1000);
    binOneStartBox->setMaximum(1000);
    binOneStartBox->setSingleStep(10);
    binOneStartBox->setFrame(false);
    binOneStartBox->setStyleSheet("QSpinBox { background-color: white; }");

    binOneSizeBox = new QSpinBox(this);
    binOneSizeBox->setMinimum(0);
    binOneSizeBox->setMaximum(2*1000);
    binOneSizeBox->setSingleStep(10);
    binOneSizeBox->setFrame(false);

    binTwoStartBox = new QSpinBox(this);
    binTwoStartBox->setMinimum(-1000);
    binTwoStartBox->setMaximum(1000);
    binTwoStartBox->setSingleStep(10);
    binTwoStartBox->setFrame(false);

    binTwoSizeBox = new QSpinBox(this);
    binTwoSizeBox->setMinimum(0);
    binTwoSizeBox->setMaximum(2*1000);
    binTwoSizeBox->setSingleStep(10);
    binTwoSizeBox->setFrame(false);

    binSumStatsText = new QTextEdit();
    binSumStatsText->setText(generateSumStatsText());

    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setRange(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setNumBins(int)));
    connect(rangeSpinBox, SIGNAL(valueChanged(int)), this, SIGNAL(rangeChanged(int)));
    connect(numBinsSpinBox, SIGNAL(valueChanged(int)), this, SIGNAL(binsChanged(int)));

//    connect(SDCheckbox, SIGNAL(stateChanged(int)), this, SLOT(setSDChecked(int)));
    connect(SECheckbox, SIGNAL(stateChanged(int)), this, SLOT(setSEChecked(int)));
//    connect(RCheckbox, SIGNAL(stateChanged(int)), this, SLOT(setRChecked(int)));
//    connect(CICheckbox, SIGNAL(stateChanged(int)), this, SLOT(setCIChecked(int)));

    connect(binOneStartBox, SIGNAL(valueChanged(int)), this, SLOT(setBinOneStart(int)));
    connect(binOneSizeBox, SIGNAL(valueChanged(int)), this, SLOT(setBinOneSize(int)));
    connect(binTwoStartBox, SIGNAL(valueChanged(int)), this, SLOT(setBinTwoStart(int)));
    connect(binTwoSizeBox, SIGNAL(valueChanged(int)), this, SLOT(setBinTwoSize(int)));

    /*rangeSpinBox->setMaximumWidth(30);
    numBinsSpinBox->setMaximumWidth(30);
    rangeLabel->setMaximumWidth(30);
    binsLabel->setMaximumWidth(30);*/

    //rangeSpinBox->setMaximumHeight(30);
    //numBinsSpinBox->setMaximumHeight(30);

    controlLayout = new QGridLayout;
    controlLayout->setVerticalSpacing(2);
    controlLayout->setHorizontalSpacing(10);
    controlLayout->setContentsMargins(0,0,0,0);

    int binstatsColSpan = 4;
    generateSumStatsLabel();

//    controlLayout->addWidget(binSumStatsGrid, 0, 1);
    controlLayout->addWidget(binOneStartLabel, 0,2+binstatsColSpan);
    controlLayout->addWidget(binOneSizeLabel, 0,3+binstatsColSpan);

    controlLayout->addWidget(binOneStartBox, 1,2+binstatsColSpan);
    controlLayout->addWidget(binOneSizeBox, 1,3+binstatsColSpan);

    controlLayout->addWidget(binTwoStartLabel, 2,2+binstatsColSpan);
    controlLayout->addWidget(binTwoSizeLabel, 2,3+binstatsColSpan);

    controlLayout->addWidget(binTwoStartBox, 3,2+binstatsColSpan);
    controlLayout->addWidget(binTwoSizeBox, 3,3+binstatsColSpan);

    controlLayout->addWidget(rangeLabel,0,4+binstatsColSpan);
    controlLayout->addWidget(binsLabel,0,5+binstatsColSpan);
    controlLayout->addWidget(rangeSpinBox,1,4+binstatsColSpan);
    controlLayout->addWidget(numBinsSpinBox,1,5+binstatsColSpan);

//    controlLayout->addWidget(SDCheckbox, 0, 6);
    controlLayout->addWidget(SECheckbox, 2, 4+binstatsColSpan);
//    controlLayout->addWidget(RCheckbox, 2, 6);
//    controlLayout->addWidget(CICheckbox, 3, 6);

    controlLayout->setColumnStretch(0, 3);
    controlLayout->setColumnStretch(5, 1);
}

void PSTHDialog::generateSumStatsLabel(){
    //First quick and ugly version to get sumstats on the ui
    minLabel1 = new QLabel();
    medLabel1 = new QLabel();
    meanLabel1 = new QLabel();
    maxLabel1 = new QLabel();
    numLabel1 = new QLabel();
    minLabel2 = new QLabel();
    medLabel2 = new QLabel();
    meanLabel2 = new QLabel();
    maxLabel2 = new QLabel();
    numLabel2 = new QLabel();
    QLabel *bin1 = new QLabel("Bin 1");
    bin1->setStyleSheet("QLabel { color : red; }");
    QLabel *bin2 = new QLabel("Bin 2");
    bin2->setStyleSheet("QLabel { color : blue; }");

    controlLayout->addWidget(new QLabel("Min"), 1, 1);
    controlLayout->addWidget(new QLabel("Med"), 2, 1);
    controlLayout->addWidget(new QLabel("Mean"), 3, 1);
    controlLayout->addWidget(new QLabel("Max"), 4, 1);
    controlLayout->addWidget(new QLabel("Num"), 5, 1);
    controlLayout->addWidget(bin1, 0, 2);
    controlLayout->addWidget(bin2, 0, 4);
    controlLayout->addWidget(minLabel1, 1, 2);
    controlLayout->addWidget(medLabel1, 2, 2);
    controlLayout->addWidget(meanLabel1, 3, 2);
    controlLayout->addWidget(maxLabel1, 4, 2);
    controlLayout->addWidget(numLabel1, 5, 2);
    controlLayout->addWidget(minLabel2, 1, 4);
    controlLayout->addWidget(medLabel2, 2, 4);
    controlLayout->addWidget(meanLabel2, 3, 4);
    controlLayout->addWidget(maxLabel2, 4, 4);
    controlLayout->addWidget(numLabel2, 5, 4);

    controlLayout->setColumnMinimumWidth(1, 10);
    controlLayout->setColumnMinimumWidth(2, 10);
    controlLayout->setColumnMinimumWidth(3, 10);
}


void PSTHDialog::resizeEvent(QResizeEvent *event) {
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("PSTHposition"), this->geometry());
    settings.endGroup();
}

void PSTHDialog::plot(const QVector<uint32_t> &trialTimesIn, const QVector<uint32_t> &eventTimesIn) {

    trialTimes.clear();
    trialTimes.resize(trialTimesIn.length());
    for (int i=0; i < trialTimesIn.length(); i++) {
        trialTimes[i] = trialTimesIn[i];
    }

    eventTimes.clear();
    eventTimes.resize(eventTimesIn.length());
    for (int i=0; i < eventTimesIn.length(); i++) {
        eventTimes[i] = eventTimesIn[i];
    }

    calcDisplay();

}

//SLOT functions
void PSTHDialog::setNumBins(int nBins) {
    //absTimeRange = 1.0;
    numBins = nBins; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("numbins"), numBins);
    settings.endGroup();
}

void PSTHDialog::setRange(int msecRange) {
    absTimeRange = (qreal)msecRange/1000;
    binSize = (2*absTimeRange)/numBins;
    calcDisplay();
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("msecRange"), msecRange);
    settings.endGroup();
}


void PSTHDialog::setSDChecked(int c){
    checked[0] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("sdCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setSEChecked(int c){
    checked[1] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("seCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setRChecked(int c){
    checked[2] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("rCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setCIChecked(int c){
    checked[3] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("ciCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setBinOneStart(int num){
    binOneStart = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin1Start"), num);
    settings.endGroup();
}
void PSTHDialog::setBinOneSize(int num){
    binOneSize = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin1Size"), num);
    settings.endGroup();
}
void PSTHDialog::setBinTwoStart(int num){
    binTwoStart = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin2Start"), num);
    settings.endGroup();
}
void PSTHDialog::setBinTwoSize(int num){
    binTwoSize = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin2Size"), num);
    settings.endGroup();
}
//Organizes trials and events according to specified settings to be passed to displays
void PSTHDialog::calcDisplay() {

    uint sampRate = hardwareConf->sourceSamplingRate;
    uint rangeInt = absTimeRange*sampRate;
    QVector<QVector<qreal> > trialEventTimes;
    QVector<QVector<int> > binTrialFreqs;
    numTrials = trialTimes.length();
//    if(numTrials<=1){
//        qDebug() << "Warning: Number of trials too low!";
//        return;
//    }
    QVector<int> binOneCounts(numTrials, 0);
    QVector<int> binTwoCounts(numTrials, 0);

    counts.clear();
    counts.fill(0, numBins);
//    for (int i=0; i < numBins; i++) {
//        counts.push_back(0);
//    }

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        QVector<qreal> relEventTimes;
        QVector<int> trialFreqs(numBins, 0);

        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((eventTimes[currentEventInd] < trialWindowStart) && (currentEventInd < eventTimes.length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((eventTimes[currentEventInd+trialEventInd] < trialWindowEnd)
               && ((currentEventInd+trialEventInd) < eventTimes.length())) {

            qreal tmpRelEventTime = ((qreal)eventTimes[currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;
            relEventTimes.append(tmpRelEventTime);

            //Counts for all bins for histogram and for error bars
            for (int i=0; i < numBins; i++) {
                if (tmpRelEventTime <= -absTimeRange + (i*binSize)+binSize) {
                    trialFreqs[i]++;
                    counts[i]++;
                    break;
                }
            }

            //Counts for two user defined bins for sum stats
            if(tmpRelEventTime >= binOneStart && tmpRelEventTime < binOneStart+binOneSize)
                binOneCounts[trialInd]++;
            if(tmpRelEventTime >= binTwoStart && tmpRelEventTime < binTwoStart+binTwoSize)
                binTwoCounts[trialInd]++;

            trialEventInd++;
        }
        binTrialFreqs.append(trialFreqs);
        trialEventTimes.append(relEventTimes);
        binOneCounts[trialInd] /= (binOneSize);
        binTwoCounts[trialInd] /= (binTwoSize);
    }

    //Bin sizes/rates calculation
    QVector<qreal> rates;
    for (int i=0; i < numBins; i++) {
        rates.push_back(counts[i]/(binSize*numTrials));
    }

    //Calculates all error functions so that user can pick which to display in histogramplot
    QVector<QVector<qreal> > eValues;
    eValues = calcErrorBars(binTrialFreqs, 0.95);

    calcSumStats(binOneCounts, 1);
    calcSumStats(binTwoCounts, 2);
//    binSumStatsText->clear();
//    binSumStatsText->setText(generateSumStatsText());

    window->setErrorBars(eValues);
    window->setChecked(checked);
    window->setBinValues((absTimeRange+binOneStart)/binSize,
                    binOneSize/binSize,
                    (absTimeRange+binTwoStart)/binSize,
                    binTwoSize/binSize);
    window->setData(rates);
    window->setXRange(-absTimeRange, absTimeRange);


    rasterWindow->setXRange(-absTimeRange, absTimeRange);
    rasterWindow->setRasters(trialEventTimes);

    /*
    QVector<qreal> tmpValues;
    for (int i = 0; i < 200; i++) {
        tmpValues.append(i);
    }
    window->setData(tmpValues);
    window->setXRange(-absTimeRange, absTimeRange);

    rasterWindow->setXRange(-absTimeRange, absTimeRange);
    QVector<qreal> faketimes;
    faketimes.append(-1.5);
    faketimes.append(-1);
    faketimes.append(-.75);
    faketimes.append(-.1);
    faketimes.append(1.7);
    faketimes.append(3.3);
    faketimes.append(5.6);
    faketimes.append(7.7);

    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    */

}

QString PSTHDialog::generateSumStatsText(){
    QMapIterator<QString, qreal> i(binOneSumStats);
    QMapIterator<QString, qreal> j(binTwoSumStats);
    QString txt = "Stat\t\tBin 1\t\tBin 2\n";

    while(i.hasNext() && j.hasNext()){
        i.next();
        j.next();
        txt += i.key() + "\t\t" + QString::number(i.value(), 'g', 3) + "\t\t";
        txt += QString::number(j.value(), 'g', 3) + "\n";

    }
    return txt;
}

void PSTHDialog::calcSumStats(QVector<int> data, int bin){
    qreal min = DBL_MAX;
    qreal max = DBL_MIN;
    qreal median = 0;
    qreal mean = 0;

    qSort(data);
    for(int i = 0; i < data.length(); i++){
        if(data[i] > max)
            max = data[i];
        if(data[i] < min)
            min = data[i];
        if(i == data.length()/2){
            if(data.length()%2 != 0)
                median = data[i];
            else
                median = (data[i] + data[i-1])/2;
        }
        mean += data[i];
    }
    mean = mean/data.length();

    if(bin == 1){
        minLabel1->setText(QString::number(min, 'f',3));
        medLabel1->setText(QString::number(median, 'f',3));
        meanLabel1->setText(QString::number(mean, 'f', 3));
        maxLabel1->setText(QString::number(max, 'f',3));
        numLabel1->setText(QString::number(data.length()));
    }
    else if(bin==2){
        minLabel2->setText(QString::number(min, 'f',3));
        medLabel2->setText(QString::number(median, 'f', 3));
        meanLabel2->setText(QString::number(mean, 'f', 3));
        maxLabel2->setText(QString::number(max, 'f',3));
        numLabel2->setText(QString::number(data.length()));

    }

    return;
}

QVector<QVector<qreal> > PSTHDialog::calcErrorBars(QVector<QVector<int> > data, qreal pct){
    QVector<QVector<qreal> > allHiLoValues;
    allHiLoValues.append(stdDev(data));
    allHiLoValues.append(stdError(data));
    allHiLoValues.append(range(data));
    allHiLoValues.append(confInt(data, pct));
    return allHiLoValues;
}


//All of the following functions will break if variable --data-- is not of length --numTrials-- and each vector inside
//is not of length --numBins--, serves as implicit check for parameter accuracy

QVector<QVector<qreal> > PSTHDialog::stdDev(QVector<QVector<int> > data){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;

    for(int j = 0; j < numBins; j++){
        qreal avg = 0;
        qreal sumSq = 0;
        avg = counts[j]/(binSize*numTrials);
        for(int i = 0; i < numTrials; i++){
            sumSq += pow(data[i][j]/(binSize) - avg, 2);
        }

        hiValues.push_back(avg+sqrt(sumSq/(numTrials-1)));
        loValues.push_back(avg-sqrt(sumSq/(numTrials-1)));
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}

QVector<QVector<qreal> > PSTHDialog::stdError(QVector<QVector<int> > data){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;
    for(int j = 0; j < numBins; j++){
        qreal avg = 0;
        qreal sumSq = 0;
        avg = counts[j]/(binSize*numTrials);
        for(int i = 0; i < numTrials; i++){
            sumSq += pow(data[i][j]/(binSize) - avg, 2);
        }

        hiValues.push_back(avg+sqrt(sumSq/((numTrials-1)*numTrials)));
        loValues.push_back(avg-sqrt(sumSq/((numTrials-1)*numTrials)));
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}

QVector<QVector<qreal> > PSTHDialog::range(QVector<QVector<int> > data){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;
    for(int j = 0; j < numBins; j++){
        qreal min = DBL_MAX;
        qreal max = DBL_MIN;
        for(int i = 0; i < numTrials; i++){
            if(data[i][j]/binSize > max)
                max = data[i][j]/binSize;
            else if(data[i][j]/binSize < min)
                min = data[i][j]/binSize;
        }
        hiValues.push_back(max);
        loValues.push_back(min);
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}

QVector<QVector<qreal> > PSTHDialog::confInt(QVector<QVector<int> > data, qreal pct){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;
    qreal crit = 1;
    if(pct == 0.9)
        crit = 1.645;
    else if(pct==0.95)
        crit = 1.96;
    else if(pct==0.99)
        crit = 2.576;

    for(int j = 0; j < numBins; j++){
        qreal avg = 0;
        qreal sumSq = 0;
        avg = counts[j]/(binSize*numTrials);
        for(int i = 0; i < numTrials; i++){
            sumSq += pow(data[i][j]/(binSize) - avg, 2);
        }

        hiValues.push_back(avg+crit*sqrt(sumSq/((numTrials-1)*numTrials)));
        loValues.push_back(avg-crit*sqrt(sumSq/((numTrials-1)*numTrials)));
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}


//-----------------------------------------------------------------------------------

MultiPlotDialog::MultiPlotDialog(QWidget *parent) : QWidget(parent){
    setMinimumHeight(300);
    setMinimumWidth(300);

    topHistogram = new HistogramPlot(this);
    topHistogram->setXLabel("Time relative to event (sec)");
    topHistogram->setYLabel("Rate (Hz)");



    clusterPlotGrid = new QGridLayout();

    topPlotLabel = new QLabel(this);
    topPlotLabel->setText("All data for NTrode ");
    botPlotLabel = new QLabel(this);
    botPlotLabel->setText("Data for clusters ");

    updateButton = new QPushButton();
    updateButton->setText("Update Histograms");

//    nextPage = new QPushButton();
//    nextPage->setText("Next page");

    currClInd = 0;
    newNTrode = false;
    currentNTrode = -1;
    currPage = 1;
    plotsPerPage = 8;
    setupGridSize(QString::number(plotsPerPage));

    connect(updateButton, SIGNAL(pressed()), this, SIGNAL(requestUpdate()));
//    connect(nextPage, SIGNAL(pressed()), this, SLOT(openNextPage()));

//-------------------------------------------------------------
    rangeLabel = new QLabel("+/- msec");
    binsLabel = new QLabel("Bins");
//    numPlotsLabel = new QLabel("Num. Plots");
    rangeLabel->setMaximumHeight(25);
    binsLabel->setMaximumHeight(25);
//    numPlotsLabel->setMaximumHeight(25);

    rangeSpinBox = new QSpinBox(this);
    rangeSpinBox->setFrame(false);
    rangeSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    rangeSpinBox->setMinimum(50);
    rangeSpinBox->setMaximum(1000);
    rangeSpinBox->setSingleStep(10);
    rangeSpinBox->setFixedSize(50,22);
    rangeSpinBox->setFocusPolicy(Qt::NoFocus);
    rangeSpinBox->setToolTip(tr("Display range (+/- msec)"));

    numBinsSpinBox = new QSpinBox(this);
    numBinsSpinBox->setFrame(false);
    numBinsSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    numBinsSpinBox->setMinimum(20);
    numBinsSpinBox->setMaximum(200);
    numBinsSpinBox->setSingleStep(1);
    numBinsSpinBox->setFixedSize(50,22);
    numBinsSpinBox->setFocusPolicy(Qt::NoFocus);
    numBinsSpinBox->setToolTip(tr("Number of bins"));

//    numPlotsDropdown = new QComboBox(this);
//    numPlotsDropdown->setFrame(false);
//    numPlotsDropdown->setStyleSheet("QComboBox { background-color: white; }");
//    numPlotsDropdown->setFixedSize(50,22);
//    numPlotsDropdown->setFocusPolicy(Qt::NoFocus);
//    numPlotsDropdown->setToolTip(tr("Number of bins"));
//    numPlotsDropdown->insertItem(0, "1");
//    numPlotsDropdown->insertItem(1, "2");
//    numPlotsDropdown->insertItem(2, "4");
//    numPlotsDropdown->insertItem(3, "6");
//    numPlotsDropdown->insertItem(4, "8");
//    numPlotsDropdown->setCurrentText("6");

    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setRange(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setNumBins(int)));
    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SIGNAL(rangeChanged(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SIGNAL(binsChanged(int)));
//    connect(numPlotsDropdown, SIGNAL(currentIndexChanged(QString)), this, SLOT(setupGridSize(QString)));

//-------------------------------------------------------------
    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *plotLayout = new QGridLayout;
    plotLayout->setVerticalSpacing(0);
    plotLayout->setContentsMargins(0,0,0,0);
    plotLayout->addWidget(topHistogram,2,0, 1, 3);
    plotLayout->addLayout(clusterPlotGrid, 4, 0, 1, 3);
    plotLayout->addWidget(topPlotLabel, 1, 1, Qt::AlignCenter);
    plotLayout->addWidget(botPlotLabel, 3, 1, Qt::AlignCenter);
    plotLayout->setRowStretch(2, 1);
    plotLayout->setRowStretch(4, 1);

    QGridLayout *controlLayout = new QGridLayout;
//    controlLayout->addWidget(nextPage, 0, 1, Qt::AlignRight);
    controlLayout->addWidget(updateButton, 1, 1, Qt::AlignRight);
//    controlLayout->addWidget(numPlotsLabel, 0, 2);
    controlLayout->addWidget(rangeLabel, 0, 3);
    controlLayout->addWidget(binsLabel, 0, 4);
//    controlLayout->addWidget(numPlotsDropdown, 1, 2);
    controlLayout->addWidget(rangeSpinBox, 1, 3);
    controlLayout->addWidget(numBinsSpinBox, 1, 4);
    controlLayout->setVerticalSpacing(2);

    mainLayout->addLayout(plotLayout,0,0);
    mainLayout->addLayout(controlLayout, 1,0);
//    mainLayout->setRowStretch(0,1);


    setLayout(mainLayout);
    topHistogram->show();
//-------------------------------------------------------------
    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    int tempNumBins = settings.value(QLatin1String("numbins")).toInt();
    if (tempNumBins > 0) {
        numBinsSpinBox->setValue(tempNumBins);
    } else {
        numBinsSpinBox->setValue(100);
    }

    int tempMsecRange = settings.value(QLatin1String("msecRange")).toInt();
    if (tempMsecRange > 0) {
        rangeSpinBox->setValue(tempMsecRange);
    } else {
        rangeSpinBox->setValue(500);
    }

    settings.endGroup();

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("MultiPlotposition")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }

    settings.endGroup();

//    settings.beginGroup(QLatin1String("MultiPlotsettings"));
//    int tempplotsperpage = settings.value(QLatin1String("plotsPerPage")).toInt();
//    if (tempplotsperpage > 0) {
//        numPlotsDropdown->setCurrentText(QString::number(tempplotsperpage));
//    } else {
//        numPlotsDropdown->setCurrentText("8");
//    }
}

void MultiPlotDialog::plot(const QVector<uint32_t> &trialTimesIn, const QVector<uint32_t> &eventTimesIn, const QVector<QVector<uint32_t> > &clusterEventTimesIn){
    trialTimes.clear();
    trialTimes.resize(trialTimesIn.length());
    for (int i=0; i < trialTimesIn.length(); i++) {
        trialTimes[i] = trialTimesIn[i];
    }

    eventTimes.clear();
    eventTimes.resize(eventTimesIn.length());
    for (int i=0; i < eventTimesIn.length(); i++) {
        eventTimes[i] = eventTimesIn[i];
    }

    clusterEventTimes.clear();
    clusterEventTimes.resize(clusterEventTimesIn.length());
    for(int i=0; i < clusterEventTimesIn.length(); i++){
        clusterEventTimes[i] = clusterEventTimesIn[i];
    }

    calcTopDisplay();
    setupClusterHistograms();
}

//Creates and adds new clusters created
void MultiPlotDialog::setupClusterHistograms(){
    for(int i = 0; i < clusterEventTimes.size(); i++){
        if(!clusterEventTimes[i].empty() && !clusterPlots.contains(i)){
            HistogramPlot *tmp = new HistogramPlot(this);
            connect(tmp, SIGNAL(PSTHRequest(int)), this, SIGNAL(requestPSTH(int)));
            clusterPlots.insert(i, tmp);
        }
    }
    if(clusterPlots.size()==0)
        botPlotLabel->hide();
    displayClusterGrid();
}

//Re-displays the cluster histogram plots
void MultiPlotDialog::displayClusterGrid(){

    int ithPlot = 0;
    QList<int> keys = clusterPlots.keys();    
    for(int i = 0; i < keys.size(); i++){
        clusterPlots.value(keys[i])->hide();
        clusterPlots.value(keys[i])->clickingOn(false, keys[i]);
//        if(i < currPage*plotsPerPage && i >= (currPage-1)*plotsPerPage){
            int rownum = ithPlot/gridCols;
            int colnum = ithPlot%gridRows;
            if(plotsPerPage == 2)
                colnum = ithPlot;

            calcClusterHistogramDisplay(keys[i]);
            HistogramPlot *ptr = clusterPlots.value(keys[i]);
            ptr->clickingOn(true, keys[i]);
            ptr->setXLabel("Time relative to event (sec): Cluster " + QString::number(keys[i]));
            ptr->setYLabel("Rate (Hz)");
            ptr->show();

            clusterPlotGrid->addWidget(clusterPlots.value(keys[i]), rownum, colnum);
            ithPlot++;
//        }
    }

}


void MultiPlotDialog::calcTopDisplay() {

    uint sampRate = hardwareConf->sourceSamplingRate;
    uint rangeInt = absTimeRange*sampRate;
    numTrials = trialTimes.length();
//    if(numTrials<=1){
//        qDebug() << "Warning: Number of trials too low!";
//        return;
//    }

    QVector<int> counts;//.clear();
    counts.fill(0, numBins);

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((eventTimes[currentEventInd] < trialWindowStart) && (currentEventInd < eventTimes.length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((eventTimes[currentEventInd+trialEventInd] < trialWindowEnd)
               && ((currentEventInd+trialEventInd) < eventTimes.length())) {

            qreal tmpRelEventTime = ((qreal)eventTimes[currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;

            //Counts for all bins for histogram and for error bars
            for (int i=0; i < numBins; i++) {
                if (tmpRelEventTime >= -absTimeRange && tmpRelEventTime <= -absTimeRange + (i*binSize)+binSize) {
                    counts[i]++;
                    break;
                }
            }
            trialEventInd++;
        }
    }

    //Bin sizes/rates calculation
    QVector<qreal> rates;
    for (int i=0; i < counts.size(); i++) {
        rates.push_back(counts[i]/(qreal)(binSize*numTrials));
    }
    topHistogram->setData(rates);
    topHistogram->setXRange(-absTimeRange, absTimeRange);
    topPlotLabel->setText("All data for NTrode " + QString::number(currentNTrode+1));

}

//Calculates histogram data given a cluster
void MultiPlotDialog::calcClusterHistogramDisplay(int clusterInd){
    uint sampRate = hardwareConf->sourceSamplingRate;
    uint rangeInt = absTimeRange*sampRate;
    numTrials = trialTimes.length();
//    if(numTrials<=1){
//        qDebug() << "Warning: Number of trials too low!";
//        return;
//    }

    QVector<int> clCounts = QVector<int>(numBins, 0);

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((clusterEventTimes[clusterInd][currentEventInd] < trialWindowStart) && (currentEventInd < clusterEventTimes[clusterInd].length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((clusterEventTimes[clusterInd][currentEventInd+trialEventInd] < trialWindowEnd)
               && ((currentEventInd+trialEventInd) < clusterEventTimes[clusterInd].length())) {

            qreal tmpRelEventTime = ((qreal)clusterEventTimes[clusterInd][currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;

            //Counts for all bins for histogram and for error bars
            for (int i=0; i < numBins; i++) {
                if (tmpRelEventTime >= -absTimeRange && tmpRelEventTime <= -absTimeRange + (i*binSize)+binSize) {
                    clCounts[i]++;
                    break;
                }
            }
            trialEventInd++;
        }
    }

    //Bin sizes/rates calculation
    QVector<qreal> rates;
    for (int i=0; i < clCounts.size(); i++) {
        rates.push_back(clCounts[i]/(qreal)(binSize*numTrials));
    }
    clusterPlots.value(clusterInd)->setData(rates);
    clusterPlots.value(clusterInd)->setXRange(-absTimeRange, absTimeRange);
}

//Called when signal is generated by parent window
void MultiPlotDialog::update(int ntrode, QVector<uint32_t> deventTimes, QVector<uint32_t> spikeTimes, QVector<QVector<uint32_t> > clusterSpikeTimes){
//    for(int i = 0; i < clusterSpikeTimes.size())
//    clusterPlots[0]->clearData();
    if(currentNTrode != ntrode){
        QList<int> keys = clusterPlots.keys();
        for(int i = 0; i < keys.size(); i++){
            clusterPlots.value(keys[i])->hide();
            clusterPlotGrid->removeWidget(clusterPlots.value(keys[i]));
            delete clusterPlots.value(keys[i]);
        }
        clusterPlots.clear();
        this->setWindowTitle("nTrode " + QString::number(ntrode));
    }
    currentNTrode = ntrode;
    plot(deventTimes, spikeTimes, clusterSpikeTimes);
}

void MultiPlotDialog::setNumBins(int nBins) {
    //absTimeRange = 1.0;
    numBins = nBins; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;
    calcTopDisplay();
    displayClusterGrid();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("numbins"), numBins);
    settings.endGroup();
}

void MultiPlotDialog::setRange(int msecRange) {
    absTimeRange = (qreal)msecRange/1000;
    binSize = (2*absTimeRange)/numBins;
    calcTopDisplay();
    displayClusterGrid();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("msecRange"), msecRange);
    settings.endGroup();
}

void MultiPlotDialog::resizeEvent(QResizeEvent *event) {
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("MultiPlotposition"), this->geometry());
    settings.endGroup();
}

void MultiPlotDialog::getPSTH(){
    emit requestPSTH(currClInd);
}
void MultiPlotDialog::openNextPage(){
    currPage++;

    if(plotsPerPage == 1 && currPage > clusterPlots.size())
        currPage = 1;
//    if(plotsPerPage == 2 && currPage > (clusterPlots.size()-1)/plotsPerPage + 1)
//        currPage = 1;

    if(currPage > (clusterPlots.size()-1)/plotsPerPage + 1){
        currPage = 1;
    }
    displayClusterGrid();
}

//Hard coded number of plots shown. To extend, add here and also to dropdown menu in constructor
void MultiPlotDialog::setupGridSize(QString numplots){
    switch (numplots.toInt()) {
    case 1:
        gridRows = 1;
        gridCols = 1;
        break;
    case 2:
        gridRows = 1;
        gridCols = 2;
        break;
    case 4:
        gridRows = 2;
        gridCols = 2;
        break;
    case 6:
        gridRows = 2;
        gridCols = 3;
    case 8:
        gridRows = 2;
        gridCols = 4;
    default:
        gridRows = 2;
        gridCols = 2;
        break;
    }
    plotsPerPage = numplots.toInt();
    currPage = 0;
//    openNextPage();
    displayClusterGrid();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("MultiPlotsettings"));
    settings.setValue(QLatin1String("plotsPerPage"), plotsPerPage);
    settings.endGroup();
}
