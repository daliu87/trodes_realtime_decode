#include "rfDisplay.h"
#include "dialogs.h"

SDDisplay::SDDisplay(QWidget *parent)
{
    setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    panelSlotsTaken = 0; //A bitwise record of all panels
    panelLayout = new QGridLayout(this);

    this->setLayout(panelLayout);
    //panelLayout->setMargin(0);

}

SDDisplay::~SDDisplay()
{

}

bool SDDisplay::addPanel()
{
    //first find the first empty slot from the record
    int firstEmptySlot = -1;
    for (int i=0; i < 32; i++) {
        if (!(panelSlotsTaken & (1 << i))) {
            firstEmptySlot = i;
            break;
        }
    }

    if (firstEmptySlot == -1) {
        //No slots available!
        qDebug() << "Error creating RF panel";
        return false;
    }

    //Max 3 columns
    controlPanels.push_back(new SDDisplayPanel());
    controlPanels.back()->setRecordIndex(firstEmptySlot);

    int gridLocationX = (firstEmptySlot % 3);
    int gridLocationY = floor(firstEmptySlot/3)+1; //First row is empty for stretching


    panelLayout->addWidget(controlPanels.back(),gridLocationY,gridLocationX);

    for (int i = 0; i < panelLayout->rowCount(); i++) {
        panelLayout->setRowStretch(i, 0);
    }
    panelLayout->setRowStretch(0, 1);
    panelLayout->setRowStretch(panelLayout->rowCount(), 1);

    //for (int i = 0; i < panelLayout->columnCount(); i++) {
    //    panelLayout->setColumnStretch(i, 1);
    //}
    //panelLayout->setColumnStretch(0, 1);
    //panelLayout->setColumnStretch(panelLayout->columnCount(), 1);

    //Set the record bit for this slot to 1
    panelSlotsTaken |= (1 << firstEmptySlot);



    return true;
}



//----------------------------------------------
SDDisplayPanel::SDDisplayPanel(QWidget *parent)
{
    setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    recordIndex = -1;

    isCardConnected = false;
    numChanConfigured = -1;
    cardUnlocked = false;
    cardHasData = false;

    /*
    setStyleSheet("QFrame {border: 2px solid lightgray;"
                  "border-radius: 4px;"
                  "padding: 2px;}"
                  "QLabel {border: none;}"
                          );

    */

    QGridLayout* mainLayout = new QGridLayout();

    // int headstageNum = 0; // UNUSED
    QString labelText = QString("SD Card");
    QFont labelFont;
    labelFont.setPixelSize(18);
    labelFont.setFamily("Console");

    label = new QLabel();

    label->setText(labelText);
    label->setFont(labelFont);
    mainLayout->addWidget(label,0,0);

    QGridLayout* buttonLayout = new QGridLayout();
    connectButton = new TrodesButton();
    connectButton->setText("Connect");
    enableRecordingButton = new TrodesButton();
    enableRecordingButton->setText("Enable");
    enableRecordingButton->setEnabled(false);
    reconfigureButton = new TrodesButton();
    reconfigureButton->setText("Reconfigure");
    reconfigureButton->setEnabled(false);
    channelNumSelector = new QComboBox();
    channelNumSelector->setEnabled(false);
    QStringList channelOptions;
    channelOptions << "32" << "64" << "96" << "128" << "160" << "192" << "224" << "256";
    channelNumSelector->addItems(channelOptions);
    QLabel *channelNumLabel = new QLabel("Channels");
    labelFont.setPixelSize(12);
    channelNumLabel->setFont(labelFont);

    downloadButton = new TrodesButton();
    downloadButton->setText("Download");
    downloadButton->setEnabled(false);

    connect(connectButton,SIGNAL(pressed()),this,SLOT(connectButtonPressed()));
    connect(enableRecordingButton,SIGNAL(pressed()),this,SLOT(enableButtonPressed()));
    connect(reconfigureButton,SIGNAL(pressed()),this,SLOT(reconfigureButtonPressed()));
    connect(downloadButton,SIGNAL(pressed()),this,SLOT(downloadButtonPressed()));

    buttonLayout->addWidget(connectButton,1,0);
    buttonLayout->addWidget(enableRecordingButton,1,1);
    buttonLayout->addWidget(reconfigureButton,1,2);
    buttonLayout->addWidget(channelNumSelector,1,3);
    buttonLayout->addWidget(downloadButton,1,5);
    buttonLayout->addWidget(channelNumLabel,0,3);
    buttonLayout->setColumnStretch(4,1);

    mainLayout->addItem(buttonLayout,1,0);


    //mainLayout->addWidget(new TrodesButton(),0,0);

    QGridLayout* statusLayout = new QGridLayout();


    statusIndicator = new QLabel();
    statusIndicator->setText("Status: not connected");
    labelFont.setPixelSize(14);
    statusIndicator->setFont(labelFont);
    statusLayout->addWidget(statusIndicator,0,0);

    consoleWindow = new QTextEdit();
    consoleWindow->setStyleSheet("QFrame {border: 2px solid lightgray;"
                                "border-radius: 4px;"
                                "padding: 2px;"
                                "background-color: white}"
                                "QLabel {color : gray;}");
    consoleWindow->setMinimumWidth(200);
    consoleWindow->setReadOnly(true);
    consoleWindow->setEnabled(true);
    statusLayout->addWidget(consoleWindow,1,0);
    mainLayout->addItem(statusLayout,2,0);

    this->setLayout(mainLayout);




}

SDDisplayPanel::~SDDisplayPanel()
{

}

void SDDisplayPanel::connectButtonPressed() {
    emit connectionRequest();
}

void SDDisplayPanel::enableButtonPressed() {
    emit cardEnableRequest();
}

void SDDisplayPanel::reconfigureButtonPressed() {
    emit reconfigureRequest(channelNumSelector->currentText().toInt());
}

void SDDisplayPanel::downloadButtonPressed() {

}

void SDDisplayPanel::updateSDCardStatus(bool cardConnected,int numChan, bool unlocked, bool hasData) {
    isCardConnected = cardConnected;
    numChanConfigured = numChan;
    cardUnlocked = unlocked;
    cardHasData = hasData;

    if (cardConnected) {
        statusIndicator->setText("Status: connected");
        enableRecordingButton->setEnabled(true);
        reconfigureButton->setEnabled(true);
        channelNumSelector->setEnabled(true);
        consoleWindow->insertPlainText(QString("\n"));
        if (numChan > 0) {
            consoleWindow->insertPlainText(QString("Configuration found: %1 channels.\n").arg(numChan));
        } else {
            consoleWindow->insertPlainText(QString("No configuration on card.\n").arg(numChan));
        }
        if (unlocked) {
            consoleWindow->insertPlainText(QString("Card is enabled for recording.\n"));
        } else {
            consoleWindow->insertPlainText(QString("Card is not enabled for recording.\n"));
        }
        if (hasData) {
            consoleWindow->insertPlainText(QString("Card has data on it.\n"));
            downloadButton->setEnabled(true);
        } else {
            consoleWindow->insertPlainText(QString("Card has no data on it.\n"));
            downloadButton->setEnabled(false);
        }

     } else {
        statusIndicator->setText("Status: not connected");
        enableRecordingButton->setEnabled(false);
        reconfigureButton->setEnabled(false);
        channelNumSelector->setEnabled(false);
        downloadButton->setEnabled(false);
        consoleWindow->insertPlainText(QString("\nNo SD card found.\n"));
    }

}

void SDDisplayPanel::setRecordIndex(int index) {
    recordIndex = index;
}
