/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SOUNDDIALOG_H
#define SOUNDDIALOG_H

#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QtWidgets>
#include "sharedtrodesstyles.h"
//-----------------------------------------

//--------------------------------------

class ExportDialog : public QWidget {


Q_OBJECT

public:
    ExportDialog(QWidget *parent = 0);

    QGroupBox     *spikeBox;
    QComboBox     *triggerSelector;
    QComboBox     *noiseRemoveSelector;
    QGroupBox     *ModuleDataBox;
    QComboBox     *ModuleDataChannelSelector;
    QComboBox     *ModuleDataFilterSelector;

    QLabel        *triggerModeLabel;
    QLabel        *noiseLabel;
    QLabel        *ModuleDataChannelLabel;
    QLabel        *ModuleDataFilterLabel;

    QPushButton   *cancelButton;
    QPushButton   *exportButton;

    QProgressBar  *progressBar;
    QTimer        *progressCheckTimer;


private:

signals:

    void startExport(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting);
    void exportCancelled();
    void closing();

private slots:

    void exportButtonBushed();
    void cancelButtonPushed();
    void timerExpired();
};


class soundDialog : public QWidget
{

Q_OBJECT

public:
    soundDialog(int currentGain, int currentThresh, QWidget *parent = 0);
    QSlider *gainSlider;
    QSlider *threshSlider;
    QLabel *gainDisplay;
    QLabel *threshDisplay;
    QLabel *gainTitle;
    QLabel *threshTitle;

private:

signals:

};


class waveformGeneratorDialog : public QWidget
{

Q_OBJECT

public:
    waveformGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);

    QDoubleSpinBox *modulatorFreqSpinBox;
    QSlider *freqSlider;
    QSlider *ampSlider;
    QSlider *threshSlider;
    QLabel *freqDisplay;
    QLabel *ampDisplay;
    QLabel *threshDisplay;
    QLabel *modulatorFreqTitle;
    QLabel *freqTitle;
    QLabel *ampTitle;
    QLabel *threshTitle;

protected:
    void closeEvent(QCloseEvent* event);

private:

signals:
    void windowClosed(void);
};


class spikeGeneratorDialog : public QWidget
{

Q_OBJECT

public:
    spikeGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);

    QDoubleSpinBox *modulatorFreqSpinBox;
    QSlider *freqSlider;
    QSlider *ampSlider;
    QSlider *threshSlider;
    QLabel *freqDisplay;
    QLabel *ampDisplay;
    QLabel *threshDisplay;
    QLabel *modulatorFreqTitle;
    QLabel *freqTitle;
    QLabel *ampTitle;
    QLabel *threshTitle;

protected:
    void closeEvent(QCloseEvent* event);

private:

signals:
    void windowClosed(void);
};


//-------------------------------------------------

class CommentDialog : public QWidget {

Q_OBJECT

public:
    CommentDialog(QString fileName, QWidget *parent = 0);


private:

    QLineEdit* newCommentEdit;

    QPushButton* saveButton;

    QLabel* commentLabel;
    //QLabel* lastComment;
    QTextEdit* lastComment;

    QString fileName;

    void    getHistory();
    void    saveLine();


private slots:

    void saveCurrentComment();
    void somethingEntered();


public slots:


protected:
    void closeEvent(QCloseEvent* event);

signals:

    void windowOpenState(bool);
    void windowClosed();

};

struct HeadstageSettings {
    uint16_t numberOfChannels;
    uint16_t hsTypeCode;
    uint16_t hsConfigCode;

    bool autoSettleOn;
    uint16_t percentChannelsForSettle;
    uint16_t threshForSettle;

    bool accelSensorOn;
    bool gyroSensorOn;
    bool magSensorOn;

    bool smartRefOn;

    bool smartRefAvailable;
    bool autosettleAvailable;
    bool accelSensorAvailable;
    bool gyroSensorAvailable;
    bool magSensorAvailable;
};

class HeadstageSettingsDialog : public QWidget
{

Q_OBJECT

public:
    HeadstageSettingsDialog(HeadstageSettings settings, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);


    QGroupBox       *autoSettleBox;
    QSlider         *percentChannelsSlider;
    QSlider         *threshSlider;
    QLabel          *percentIndicator;
    QLabel          *threshIndicator;
    QLabel          *percentTitle;
    QLabel          *threshTitle;

    QGroupBox       *smartReferenceBox;
    QCheckBox       *smartRefCheckBox;

    QGroupBox       *sensorBox;
    QCheckBox       *accelCheckBox;
    QCheckBox       *gyroCheckBox;
    QCheckBox       *magnetCheckBox;


    TrodesButton    *okButton;
    TrodesButton    *cancelButton;

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent* event);

private:

    HeadstageSettings currentSettings;
    bool settingsChanged;

private slots:

    void percentSliderChanged(int value);
    void threshSliderChanged(int value);
    void autoSettleOnToggled(bool on);

    void smartRefToggled(bool on);
    void accelToggled(bool on);
    void gyroToggled(bool on);
    void magToggled(bool on);


    void okButtonPressed();
    void cancelButtonPressed();

signals:
    void windowClosed(void);
    void newSettings(HeadstageSettings settings);
};



//-----------------------------------
class RasterPlot : public QWidget {

    Q_OBJECT

public:
    RasterPlot(QWidget *parent);

    void addRaster(const QVector<qreal> &times);
    void setRasters(const QVector<QVector<qreal> > &times);
    void clearRasters();
    void setXRange(qreal minR, qreal maxR);
    void setXLabel(QString l);
    void setYLabel(QString l);

protected:

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

private:


    qreal maxX;
    qreal minX;
    QVector<QVector<qreal> > eventTimes;
    QVector<qreal> xTickLabels;
    QVector<qreal> yTickLabels;
    QString xLabel;
    QString yLabel;


public slots:

private slots:

signals:

};
//-----------------------------------
class HistogramPlot : public QWidget {

    Q_OBJECT

public:
    HistogramPlot(QWidget *parent);

    void setXRange(qreal minR, qreal maxR);
    void setXLabel(QString l);
    void setYLabel(QString l);
    void setData(QVector<qreal> bvalues);
    void setErrorBars(QVector<QVector<qreal> > hilowErrValues);
    void setChecked(bool checked[4]);
    void setBinValues(qreal bin1Start,
                      qreal bin1Size,
                      qreal bin2Start,
                      qreal bin2Size);
    void clearData();
    void clickingOn(bool on, int cl);

protected:

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

    //void paint()

    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    /*
    void mouseDoubleClickEvent(QMouseEvent *event);
    void leaveEvent(QEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *);
    */

private:

//    QPainter painter;
//    QPen pen;
    int margin;
    int topOfGraph;

    qreal maxX;
    qreal minX;
    qreal maxY;
    QVector<qreal> barValues;
    QVector<QVector<qreal> > hiloerrorBarValues;
    bool checked[4];
    QVector<qreal> xTickLabels;
    QVector<qreal> yTickLabels;
    QString xLabel;
    QString yLabel;
    qreal bin1Start;
    qreal bin1Size;
    qreal bin2Start;
    qreal bin2Size;
    QRectF *bin1;
    QRectF *bin2;
    bool errorsIndicator;
    bool binsIndicator;
    bool clicking;
    int cluster;

public slots:

private slots:

signals:
    void PSTHRequest(int);
    void highlightBorder(int);
};

class PSTHDialog : public QWidget {

Q_OBJECT

public:
    PSTHDialog(QWidget *parent = 0);
    void plot(const QVector<uint32_t> &trialTimes, const QVector<uint32_t> &eventTimes);


private:

    QLabel   *rangeLabel;
    QLabel   *binsLabel;
    QLabel   *binOneStartLabel;
    QLabel   *binOneSizeLabel;
    QLabel   *binTwoStartLabel;
    QLabel   *binTwoSizeLabel;
    QLabel   *minLabel1;
    QLabel   *maxLabel1;
    QLabel   *medLabel1;
    QLabel   *meanLabel1;
    QLabel   *numLabel1;
    QLabel   *minLabel2;
    QLabel   *maxLabel2;
    QLabel   *medLabel2;
    QLabel   *meanLabel2;
    QLabel   *numLabel2;

    QSpinBox *rangeSpinBox;
    QSpinBox *numBinsSpinBox;
    QSpinBox *binOneStartBox;
    QSpinBox *binOneSizeBox;
    QSpinBox *binTwoStartBox;
    QSpinBox *binTwoSizeBox;
//    QCheckBox *SDCheckbox;
    QCheckBox *SECheckbox;
//    QCheckBox *RCheckbox;
//    QCheckBox *CICheckbox;
    QTextEdit *binSumStatsText;
    QGridLayout *binSumStatsGrid;

    QGridLayout *controlLayout;
    HistogramPlot *window;
    RasterPlot *rasterWindow;

    qreal binSize;
    int numBins;
    qreal absTimeRange;
    QVector<int> counts;
    int numTrials;
    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;
    bool checked[4];

    qreal binOneStart, binOneSize, binTwoStart, binTwoSize;
    QMap<QString, qreal> binOneSumStats, binTwoSumStats;

    void setUpControlPanel();
    QString generateSumStatsText();
    void generateSumStatsLabel();
    void calcDisplay();
    void calcSumStats(QVector<int> data, int bin);
    QVector<QVector<qreal> > calcErrorBars(QVector<QVector<int> > data, qreal pct);
    QVector<QVector<qreal> > stdDev(QVector<QVector<int> > data);
    QVector<QVector<qreal> > stdError(QVector<QVector<int> > data);
    QVector<QVector<qreal> > range(QVector<QVector<int> > data);
    QVector<QVector<qreal> > confInt(QVector<QVector<int> > data, qreal pct);



private slots:
    void setNumBins(int nBins);
    void setRange(int msecRange);
    void setSDChecked(int c);
    void setSEChecked(int c);
    void setRChecked(int c);
    void setCIChecked(int c);
    void setBinOneStart(int num);
    void setBinOneSize(int num);
    void setBinTwoStart(int num);
    void setBinTwoSize(int num);


public slots:


protected:
    //void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *event);
//    void paintEvent(QPaintEvent *event);

signals:

    void windowOpenState(bool);
    void windowClosed();
    void binsChanged(int);
    void rangeChanged(int);

};

class MultiPlotDialog : public QWidget {

Q_OBJECT

public:
    MultiPlotDialog(QWidget *parent = 0);
    void plot(const QVector<uint32_t> &trialTimesIn,
              const QVector<uint32_t> &eventTimesIn,
              const QVector<QVector<uint32_t> > &clusterEventTimesIn);
    void setupClusterHistograms();

private:

    HistogramPlot *topHistogram;
//    HistogramPlot *clusterHistogram;
    QGridLayout   *clusterPlotGrid;
    QLabel        *topPlotLabel;
    QLabel        *botPlotLabel;

    QPushButton   *updateButton;
    QPushButton   *PSTHButton;  //TODO: request PSTH detailed plot
    QPushButton   *nextPage;    //TODO: display next page of cluster plots

    QLabel        *rangeLabel;
    QLabel        *binsLabel;
    QLabel        *numPlotsLabel;
    QSpinBox      *rangeSpinBox;
    QSpinBox      *numBinsSpinBox;
    QComboBox     *numPlotsDropdown; //TODO: select 1, 2, or 4 plots to display at a time

//    QVector<HistogramPlot*> clusterPlots; //TODO: create all cluster plots and store, to easily display many at a time
    QMap<int, HistogramPlot*> clusterPlots;

    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;
    QVector<QVector<uint32_t> > clusterEventTimes;
    int currClInd;
    int currentNTrode;

    int plotsPerPage;       //TODO: number of cluster plots per page
    int gridRows;
    int gridCols;
    int currPage;           //TODO: current page

    int numBins;
    int numTrials;
    qreal binSize;
    qreal absTimeRange;

    bool newNTrode;

    void calcTopDisplay();
    void calcClusterHistogramDisplay(int clusterInd);

private slots:
    void setNumBins(int nBins);
    void setRange(int msecRange);
    void getPSTH();
    void openNextPage();
    void displayClusterGrid();
//    void organizeClusterGrid();
    void setupGridSize(QString numplots);


public slots:
    void update(int, QVector<uint32_t>, QVector<uint32_t> , QVector<QVector<uint32_t> >);

protected:
    //void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *event);
//    void paintEvent(QPaintEvent *event);

signals:

    void windowOpenState(bool);
    void windowClosed();
    void requestUpdate();
    void requestPSTH(int);
    void binsChanged(int);
    void rangeChanged(int);

};

#endif // SOUNDDIALOG_H
