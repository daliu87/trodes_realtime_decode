/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "mainWindow.h"

QGLFormat qglFormat;
bool linkStreamToFilters;
bool linkChangesBool;
bool exportMode;
extern QString lastDebugMsg;
extern bool unitTestMode;

MainWindow::MainWindow()
{

    networkConf=NULL;
    moduleConf=NULL;
    nTrodeTable=NULL;
    streamConf=NULL;
    spikeConf=NULL;
    headerConf=NULL;
    hardwareConf=NULL;
    globalConf=NULL;
    //benchConfig=NULL;


    qDebug().noquote() << "Trodes Version Info:\n" << GlobalConfiguration::getVersionInfo(false); //print version info to debug log

    quitting = false;
    unitTestFlag = false;  //set to true if we are doing unit testing


  if (objectName().isEmpty())
    setObjectName(QString("Main"));

  resize(800, 600);
  exportMode = false; //Whether or not data displays should be updated

  hardwareConf = new HardwareConfiguration(NULL); // this is requried for creating the audio generator, but this object
                                                  // is overwritten when the config file is read
  sdDisp = NULL;
  spikeDisp = NULL;

  setAutoFillBackground(true);
  soundDialogOpen = false;
  recordFileOpen = false;
  dataStreaming = false;
  recording = false;
  timerTick = 0;
  channelsConfigured = false;
  visibleTime = 0;
  eventTabWasChanged = false;
  for (int i = 0; i < 64; i++) {
    eventTabsInitialized[i] = false;
  }
  eventTabsInitialized[0] = true;
  currentTrodeSelected = 0;
  singleTriggerWindowOpen = false;

  settleChannelByteInPacket = 0;
  settleChannelBit = 0;
  settleChannelTriggerState = 0;
  settleChannelDelay = 0;

  headstageSettings.autoSettleOn = false;
  headstageSettings.percentChannelsForSettle = 0;
  headstageSettings.threshForSettle = 0;
  headstageSettings.smartRefOn = false;
  headstageSettings.accelSensorOn = false;
  headstageSettings.gyroSensorOn = false;
  headstageSettings.magSensorOn = false;

  headstageSettings.smartRefAvailable = false;
  headstageSettings.autosettleAvailable = false;
  headstageSettings.accelSensorAvailable = false;
  headstageSettings.gyroSensorAvailable = false;
  headstageSettings.magSensorAvailable = false;


  //qglFormat.setVersion(3,2);
  qglFormat.setProfile(QGLFormat::CoreProfile);
  qglFormat.setDoubleBuffer(true);


  //Statusbar setup---------------------------
  statusbar = new QStatusBar(this);
  TrodesFont statusFont;
  statusbar->setFont(statusFont);
  setStatusBar(statusbar);
  //statusbar = statusbar;
  statusbar->showMessage(tr("Not connected to device"));
  //------------------------------------------


  //File menu--------------------------------
  menuFile = new QMenu;
  menuFile->setTitle("File");
  menuBar()->addAction(menuFile->menuAction());

  menuConfig = new QMenu;
  menuFile->addAction(menuConfig->menuAction());

  actionLoadConfig = new QAction(this);
  actionLoadConfig->setShortcut(QKeySequence(tr("Ctrl+O")));
  menuConfig->addAction(actionLoadConfig);
  actionCloseConfig = new QAction(this);
  actionCloseConfig->setEnabled(false);
  actionCloseConfig->setShortcut(QKeySequence(tr("Shift+Ctrl+O")));
  menuConfig->addAction(actionCloseConfig);
  actionSaveConfig = new QAction(this);
  actionSaveConfig->setShortcut(QKeySequence(tr("Ctrl+S")));
  actionSaveConfig->setEnabled(false);
  menuConfig->addAction(actionSaveConfig);
  actionReConfig = new QAction(this);
  actionReConfig->setEnabled(false);
  menuConfig->addAction(actionReConfig);

  actionAboutConfig = new QAction(this);
  actionAboutConfig->setEnabled(false);
  actionAboutConfig->setText("About Workspace");
  menuConfig->addAction(actionAboutConfig);


  //menuFile->addSeparator();
  actionPlaybackOpen = new QAction(this);
  actionPlaybackOpen->setShortcut(QKeySequence(tr("Ctrl+P")));
  actionPlaybackOpen->setData(QVariant::fromValue(SourceFile));
  menuFile->addAction(actionPlaybackOpen);
  actionOpenRecordDialog = new QAction(this);
  actionOpenRecordDialog->setShortcut(QKeySequence(tr("Ctrl+N")));
  menuFile->addAction(actionOpenRecordDialog);
  actionOpenRecordDialog->setEnabled(false);  
  actionCloseFile = new QAction(this);
  actionCloseFile->setShortcut(QKeySequence(tr("Ctrl+W")));
  actionCloseFile->setEnabled(false);
  menuFile->addAction(actionCloseFile);
  menuFile->addSeparator();
  actionRecord = new QAction(this);
  actionRecord->setEnabled(false);
  menuFile->addAction(actionRecord);
  actionPause = new QAction(this);
  actionPause->setEnabled(false);
  menuFile->addAction(actionPause);
  actionPlay = new QAction(this);
  actionPlay->setEnabled(false);
  menuFile->addAction(actionPlay);
  actionExport = new QAction(this);
  actionExport->setEnabled(false);
  menuFile->addAction(actionExport);


  menuFile->addSeparator();
  // Module Menu

  actionRestartModules = new QAction(this);
  menuFile->addAction(actionRestartModules);
  actionRestartModules->setEnabled(false);

  menuFile->addSeparator();

  actionQuit = new QAction(this);
  actionQuit->setShortcut(Qt::CTRL + Qt::Key_Q);
  //actionQuit->setMenuRole(QAction::QuitRole);
  menuFile->addAction(actionQuit);


  connect(actionLoadConfig, SIGNAL(triggered()), this, SLOT(loadConfig()));
  connect(actionReConfig, SIGNAL(triggered()), this, SLOT(reConfig()));
  QObject::connect(actionPlaybackOpen,SIGNAL(triggered()),this,SLOT(setSource()));
  connect(actionCloseConfig, SIGNAL(triggered()), this, SLOT(closeConfig()));
  connect(actionSaveConfig, SIGNAL(triggered()), this, SLOT(saveConfig()));
  connect(actionAboutConfig, SIGNAL(triggered()), this, SLOT(aboutCurConfig()));


  QObject::connect(actionOpenRecordDialog, SIGNAL(triggered()), this, SLOT(openRecordDialog()));
  QObject::connect(actionCloseFile, SIGNAL(triggered()), this, SLOT(closeFile()));
  connect(actionRecord,SIGNAL(triggered()),this,SLOT(actionRecordSelected()));
  connect(actionPause,SIGNAL(triggered()),this,SLOT(actionPauseSelected()));
  connect(actionPlay,SIGNAL(triggered()),this,SLOT(actionPlaySelected()));
  connect(actionExport,SIGNAL(triggered()),this,SLOT(openExportDialog()));
  connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));
  connect(actionRestartModules, SIGNAL(triggered()), this, SLOT(checkRestartModules()));


  //----------------------------------------------



  //Connection menu--------------------------------
  menuSystem = new QMenu;
  menuSystem->setTitle("Connection");
  menuBar()->addAction(menuSystem->menuAction());
  //mainMenuBar->addAction(menuSystem->menuAction());
  //actionReconfigure = new QAction(this);
  //menuSystem->addAction(actionReconfigure);
  sourceMenu = new QMenu;
  menuSystem->addAction(sourceMenu->menuAction());
  menuSystem->addSeparator();
  menuSimulationSource = new QMenu;
  menuSpikeGadgetsSource = new QMenu;
  actionSourceNone = new QAction(this);
  actionSourceFake = new QAction(this);
  actionSourceFakeSpikes = new QAction(this);
  menuSimulationSource->addAction(actionSourceFake);
  menuSimulationSource->addAction(actionSourceFakeSpikes);
  actionSourceFile = new QAction(this);
  actionSourceEthernet = new QAction(this);
  actionSourceUSB = new QAction(this);
  menuSpikeGadgetsSource->addAction(actionSourceUSB);
  menuSpikeGadgetsSource->addAction(actionSourceEthernet);
  actionSourceRhythm = new QAction(this);
  actionSourceNone->setCheckable(true);
  actionSourceNone->setChecked(true);
  actionSourceFake->setCheckable(true);
  actionSourceFake->setChecked(false);
  actionSourceFakeSpikes->setCheckable(true);
  actionSourceFakeSpikes->setChecked(false);
  actionSourceFile->setCheckable(true);
  actionSourceFile->setChecked(false);
  actionSourceUSB->setCheckable(true);
  actionSourceUSB->setChecked(false);
  actionSourceRhythm->setCheckable(true);
  actionSourceRhythm->setChecked(false);
  actionSourceEthernet->setCheckable(true);
  actionSourceEthernet->setChecked(false);
  sourceMenu->addAction(actionSourceNone);
  //sourceMenu->addAction(actionSourceFake);
  //sourceMenu->addAction(actionSourceFakeSpikes);
  sourceMenu->addAction(actionSourceFile);
  //sourceMenu->addAction(actionSourceEthernet);
  //sourceMenu->addAction(actionSourceUSB);
  sourceMenu->addAction(menuSimulationSource->menuAction());
  sourceMenu->addAction(menuSpikeGadgetsSource->menuAction());
  sourceMenu->addAction(actionSourceRhythm);
  actionSourceNone->setData(QVariant::fromValue(SourceNone));
  actionSourceFake->setData(QVariant::fromValue(SourceFake));
  actionSourceFakeSpikes->setData(QVariant::fromValue(SourceFakeSpikes));
  actionSourceFile->setData(QVariant::fromValue(SourceFile));
  actionSourceEthernet->setData(QVariant::fromValue(SourceEthernet));
  actionSourceUSB->setData(QVariant::fromValue(SourceUSBDAQ));
  actionSourceRhythm->setData(QVariant::fromValue(SourceRhythm));
#ifndef RHYTHM
    actionSourceRhythm->setEnabled(false);
#endif
  actionConnect = new QAction(this);
  menuSystem->addAction(actionConnect);
  actionConnect->setEnabled(false);
  actionDisconnect = new QAction(this);
  menuSystem->addAction(actionDisconnect);
  actionDisconnect->setEnabled(false);
  actionClearBuffers = new QAction(this);
  menuSystem->addAction(actionClearBuffers);
  actionClearBuffers->setEnabled(false);
  actionSendSettle = new QAction(this);
  menuSystem->addAction(actionSendSettle);
  actionSendSettle->setEnabled(false);
  menuSystem->addSeparator();
  actionOpenGeneratorDialog = new QAction(this);
  menuSystem->addAction(actionOpenGeneratorDialog);
  actionOpenGeneratorDialog->setEnabled(false);




  QObject::connect(actionSourceNone,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFake,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFakeSpikes,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFile,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceEthernet,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceUSB,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceRhythm,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionConnect, SIGNAL(triggered()), this, SLOT(connectToSource()));
  QObject::connect(actionDisconnect, SIGNAL(triggered()), this, SLOT(disconnectFromSource()));
  QObject::connect(actionSendSettle, SIGNAL(triggered()), this, SLOT(sendSettleCommand()));

  QObject::connect(actionOpenGeneratorDialog, SIGNAL(triggered()), this, SLOT(openGeneratorDialog()));

  //-----------------------------------------------------



  //Display menu-------------------------------------------
  menuDisplay = new QMenu;
  menuDisplay->setTitle("View");
  menuBar()->addAction(menuDisplay->menuAction());
  //mainMenuBar->addAction(menuDisplay->menuAction());
  actionShowCurrentTrode = new QAction(this);
  actionShowCurrentTrode->setEnabled(false);
  menuDisplay->addAction(actionShowCurrentTrode);
  menuEEGDisplay = new QMenu;
  menuDisplay->addAction(menuEEGDisplay->menuAction());
  menuSetTLength = new QMenu(this);
  actionSetTLength0_2 = new QAction(this);
  actionSetTLength0_2->setData(0.2);
  actionSetTLength0_2->setCheckable(true);
  actionSetTLength0_2->setChecked(false);
  actionSetTLength0_5 = new QAction(this);
  actionSetTLength0_5->setData(0.5);
  actionSetTLength0_5->setCheckable(true);
  actionSetTLength0_5->setChecked(false);
  actionSetTLength1_0 = new QAction(this);
  actionSetTLength1_0->setData(1.0);
  actionSetTLength1_0->setCheckable(true);
  actionSetTLength1_0->setChecked(true);
  actionSetTLength2_0 = new QAction(this);
  actionSetTLength2_0->setData(2.0);
  actionSetTLength2_0->setCheckable(true);
  actionSetTLength2_0->setChecked(false);
  actionSetTLength5_0 = new QAction(this);
  actionSetTLength5_0->setData(5.0);
  actionSetTLength5_0->setCheckable(true);
  actionSetTLength5_0->setChecked(false);
  menuSetTLength->addAction(actionSetTLength0_2);
  menuSetTLength->addAction(actionSetTLength0_5);
  menuSetTLength->addAction(actionSetTLength1_0);
  menuSetTLength->addAction(actionSetTLength2_0);
  menuSetTLength->addAction(actionSetTLength5_0);
  menuEEGDisplay->addMenu(menuSetTLength);
  streamFilterMenu = new QMenu;
  menuEEGDisplay->addAction(streamFilterMenu->menuAction());
  streamFilterLink = new QAction(this);
  streamFilterLink->setCheckable(true);
  streamFilterLink->setChecked(true);
  streamFilterUnLink = new QAction(this);
  streamFilterUnLink->setCheckable(true);
  streamFilterUnLink->setChecked(false);
  streamFilterMenu->addAction(streamFilterLink);
  streamFilterMenu->addAction(streamFilterUnLink);
  linkStreamToFilters = true;
  actionSound = new QAction(this);
  menuDisplay->addAction(actionSound);


  connect(actionSetTLength0_2,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength0_5,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength1_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength2_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength5_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  QObject::connect(actionShowCurrentTrode,SIGNAL(triggered()),this,SLOT(openTrodeWindow()));
  QObject::connect(streamFilterLink, SIGNAL(triggered()), this, SLOT(linkFilters()));
  QObject::connect(streamFilterUnLink, SIGNAL(triggered()), this, SLOT(unLinkFilters()));
  QObject::connect(actionSound, SIGNAL(triggered()), this, SLOT(openSoundDialog()));

  //--------------------------------------------------------
  //Settings menu
  menuSettings = new QMenu;
  menuBar()->addAction(menuSettings->menuAction());

  //nTrode settings menu-------------------------------------------
  menuNTrode = new QMenu;
  menuSettings->addAction(menuNTrode->menuAction());
  //mainMenuBar->addAction(menuNTrode->menuAction());
  menuLinkChanges = new QMenu;
  menuNTrode->addAction(menuLinkChanges->menuAction());
  actionLinkChanges = new QAction(this);
  actionLinkChanges->setCheckable(true);
  actionLinkChanges->setChecked(false);
  actionUnLinkChanges = new QAction(this);
  actionUnLinkChanges->setCheckable(true);
  actionUnLinkChanges->setChecked(true);
  menuLinkChanges->addAction(actionUnLinkChanges);
  menuLinkChanges->addAction(actionLinkChanges);
  linkChangesBool = false;
  clearAllNTrodes = new QAction(this);
  menuNTrode->addAction(clearAllNTrodes);

  menuHeadstage = new QMenu;
  menuSettings->addAction(menuHeadstage->menuAction());
  actionHeadstageSettings = new QAction(this);
  actionHeadstageSettings->setEnabled(false);
  menuHeadstage->addAction(actionHeadstageSettings);
  actionSettleDelay = new QAction(this);
  menuHeadstage->addAction(actionSettleDelay);



  QObject::connect(actionUnLinkChanges, SIGNAL(triggered()), this, SLOT(unLinkChanges()));
  QObject::connect(actionLinkChanges, SIGNAL(triggered()), this, SLOT(linkChanges()));
  QObject::connect(clearAllNTrodes, SIGNAL(triggered()), this , SLOT(clearAll()));
  connect(actionHeadstageSettings,SIGNAL(triggered()),this,SLOT(openHeadstageDialog()));
  connect(actionSettleDelay, SIGNAL(triggered()),this,SLOT(openSettleChannelDelayDialog()));
  //-----------------------------------------------------

  //Debug menu -----------------------------------------
  menuDebug = new QMenu;
  menuBar()->addAction(menuDebug->menuAction());
  actionBenchmarkingSettings = new QAction(this);
  actionBenchmarkingSettings->setShortcut(QKeySequence(tr("Ctrl+B")));
  menuDebug->setTitle("Debug");
  actionBenchmarkingSettings->setText("Benchmarking");
  menuDebug->setEnabled(true);
  actionBenchmarkingSettings->setEnabled(true);
  menuDebug->addAction(actionBenchmarkingSettings);

  connect(actionBenchmarkingSettings, SIGNAL(triggered()), this, SLOT(openBenchmarkingDialog()));

  //----------------------------------------------------

  //Help menu-----------------------------------------

  actionAbout = new QAction(this);
  actionAbout->setMenuRole(QAction::AboutRole);
  actionAboutQT = new QAction(this);
  actionAboutQT->setMenuRole(QAction::AboutQtRole);
  menuHelp = new QMenu;
  menuBar()->addAction(menuHelp->menuAction());
  //mainMenuBar->addAction(menuHelp->menuAction());
  menuHelp->addAction(actionAbout);
  menuHelp->addAction(actionAboutQT);

  QObject::connect(actionAbout, SIGNAL(triggered()), this, SLOT(about()));
  QObject::connect(actionAboutQT, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

  //----------------------------------------------------



  //Layouts and tabs-----------------------------------------
  mainLayout =  new QGridLayout();
  mainLayout->setContentsMargins(QMargins(10,0,10,0));
  mainLayout->setVerticalSpacing(3);
  TrodesFont dispFont;
  tabs = new QTabWidget(this);
  //tabs->addTab(eventTabs,tr("nTrodes"));
  //tabs->addTab(eegDisp,tr("Streaming"));
  tabs->setStyleSheet("QTabWidget::pane {margin: 0px}") ;
  tabs->setTabPosition(QTabWidget::West);
  tabs->setFont(dispFont);
  mainLayout->addWidget(tabs,2,0);
  //---------------------------------------------------------

  //Top control panel setup----------------------------------
  headerLayout = new QGridLayout(); //contains the buttons and clock at the top of the screen
  headerLayout->setContentsMargins(QMargins(1,1,1,1));
  headerLayout->setHorizontalSpacing(3);



  int totalRightItems = 8;
  int totalLeftItems = 3;
  //Sound settings button
  soundSettingsButton = new TrodesButton;
  soundSettingsButton->setText(tr("Audio"));
  //soundSettingsButton->setFont(buttonFont);
  soundSettingsButton->setCheckable(false);
  connect(soundSettingsButton,SIGNAL(pressed()),this,SLOT(soundButtonPressed()));
  soundSettingsButton->setFixedSize(70,20);
  headerLayout->addWidget(soundSettingsButton,0,totalLeftItems+7);

  trodeSettingsButton = new TrodesButton;
  trodeSettingsButton->setText(tr("nTrode"));
  //trodeSettingsButton->setFont(buttonFont);
  trodeSettingsButton->setCheckable(false);
  connect(trodeSettingsButton,SIGNAL(pressed()),this,SLOT(trodeButtonPressed()));
  trodeSettingsButton->setFixedSize(70,20);
  headerLayout->addWidget(trodeSettingsButton,0,totalLeftItems+6);

  spikesButton = new TrodesButton;
  spikesButton->setText(tr("Spikes"));
  //spikesButton->setFont(buttonFont);
  spikesButton->setCheckable(false);
  connect(spikesButton,SIGNAL(clicked()),this,SLOT(spikesButtonPressed()));
  spikesButton->setFixedSize(70,20);
  headerLayout->addWidget(spikesButton,0,totalLeftItems+5);

  videoButton = new TrodesButton;
  videoButton->setText(tr("Video"));
  //videoButton->setFont(buttonFont);
  videoButton->setCheckable(false);
  connect(videoButton,SIGNAL(clicked()),this,SLOT(videoButtonPressed()));
  videoButton->setFixedSize(70,20);
  headerLayout->addWidget(videoButton,0,totalLeftItems+4);

  statescriptButton = new TrodesButton;
  statescriptButton->setText(tr("StateScript"));
  //statescriptButton->setFont(buttonFont);
  statescriptButton->setCheckable(false);
  connect(statescriptButton,SIGNAL(clicked()),this,SLOT(statescriptButtonPressed()));
  statescriptButton->setFixedSize(80,20);
  headerLayout->addWidget(statescriptButton,0,totalLeftItems+3);


  commentButton = new TrodesButton;
  commentButton->setText(tr("Annotate"));
  //commentButton->setFont(buttonFont);
  commentButton->setCheckable(false);
  connect(commentButton,SIGNAL(pressed()),this,SLOT(commentButtonPressed()));
  commentButton->setFixedSize(70,20);
  headerLayout->addWidget(commentButton,0,totalLeftItems+2);


  linkChangesButton = new TrodesButton;
  linkChangesButton->setText(tr("Link nTrodes"));
  //linkChangesButton->setFont(buttonFont);
  linkChangesButton->setCheckable(true);
  connect(linkChangesButton,SIGNAL(toggled(bool)),this,SLOT(linkChanges(bool)));
  linkChangesButton->setFixedSize(90,20);
  headerLayout->addWidget(linkChangesButton,0,totalLeftItems+1);


  //Time display
  QTime mainClock(0,0,0,0);
  QFont labelFont;
  labelFont.setPixelSize(20);
  labelFont.setFamily("Console");
  timeLabel  = new QLabel;
  timeLabel->setText(mainClock.toString("hh:mm:ss.z"));
  timeLabel->setFont(labelFont);
  timeLabel->setMinimumWidth(100);
  timeLabel->setAlignment(Qt::AlignLeft);
  pullTimer = new QTimer(this);
  connect(pullTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
  pullTimer->start(100); //update timer every 100 ms
  headerLayout->addWidget(timeLabel,0,totalRightItems+totalLeftItems);

    //Record, pause, and play buttons
    recordButton = new TrodesButton;
    pauseButton = new TrodesButton;
    playButton = new TrodesButton;
    //Using QT Resource System
    QPixmap playPixmap(":/buttons/playImage.png");
    QPixmap pausePixmap(":/buttons/pauseImage.png");
    QPixmap recordPixmap(":/buttons/recordImage.png");

    QIcon recordButtonIcon(recordPixmap);
    QIcon pauseButtonIcon(pausePixmap);
    QIcon playButtonIcon(playPixmap);
    recordButton->setIcon(recordButtonIcon);
    recordButton->setRedDown(true);
    pauseButton->setIcon(pauseButtonIcon);
    playButton->setIcon(playButtonIcon);
    recordButton->setIconSize(QSize(15, 15));
    pauseButton->setIconSize(QSize(10, 10));
    playButton->setIconSize(QSize(15, 15));
    recordButton->setFixedSize(50, 20);
    pauseButton->setFixedSize(50, 20);
    playButton->setFixedSize(50, 20);
    recordButton->setToolTip(tr("Record"));
    pauseButton->setToolTip(tr("Pause"));
    playButton->setToolTip(tr("Play file"));
    recordButton->setEnabled(false);
    pauseButton->setEnabled(false);
    playButton->setEnabled(false);
    //recordButton->setCheckable(true);
    //pauseButton->setCheckable(true);
    //playButton->setCheckable(true);
    headerLayout->addWidget(recordButton, 0, 0);
    headerLayout->addWidget(pauseButton, 0, 1);
    headerLayout->addWidget(playButton, 0, 2);
    connect(recordButton, SIGNAL(pressed()), this, SLOT(recordButtonPressed()));
    connect(pauseButton, SIGNAL(pressed()), this, SLOT(pauseButtonPressed()));
    connect(playButton, SIGNAL(pressed()), this, SLOT(playButtonPressed()));
    connect(recordButton, SIGNAL(released()), this, SLOT(recordButtonReleased()));
    connect(pauseButton, SIGNAL(released()), this, SLOT(pauseButtonReleased()));
    connect(playButton, SIGNAL(released()), this, SLOT(playButtonReleased()));

    //File name label

    fileLabel = new QLabel;
    fileLabel->setMinimumWidth(200);
    QString FileLabelColor("gray");
    QString FileLabelText("No file open");
    fileLabel->setFont(dispFont);
    QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
    fileString = fileLabelTextTemplate.arg(FileLabelColor, FileLabelText);
    fileLabel->setText(fileString);
    //fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));
    //headerLayout->addWidget(fileLabel,0,3);
    mainLayout->addWidget(fileLabel, 1, 0);

    headerLayout->setColumnStretch(totalLeftItems, 1);
    mainLayout->addLayout(headerLayout, 0, 0);
    //---------------------------------------------

    //when the state of the source stream changes, the menus need to reflect that
    sourceControl = new SourceController(NULL);
    connect(sourceControl, SIGNAL(stateChanged(int)), this, SLOT(setSourceMenuState(int)));

    soundOut = new AudioController();
    soundOut->setChannel(-1); //set the audio channel to listen to
    //soundOut->updateAudio();
    connect(this, SIGNAL(setAudioChannel(int)), soundOut, SLOT(setChannel(int)));
    connect(this, SIGNAL(updateAudio()), soundOut, SLOT(updateAudio()));
    //connect(this,SIGNAL(endAllThreads()),soundOut,SLOT(endAudio()));
    connect(this, SIGNAL(endAudioThread()),soundOut,SLOT(endAudio()));
    connect(sourceControl, SIGNAL(acquisitionStarted()), soundOut, SLOT(startAudio()));
    connect(sourceControl, SIGNAL(acquisitionStopped()), soundOut, SLOT(stopAudio()));
    connect(sourceControl, SIGNAL(acquisitionPaused()), soundOut, SLOT(stopAudio()));
    connect(sourceControl, SIGNAL(headstageSettingsReturned(HeadstageSettings)),this, SLOT(headstageSettingsChanged(HeadstageSettings)));

    QWidget *window = new QWidget();
    window->setLayout(mainLayout);
    setCentralWidget(window);
    QMetaObject::connectSlotsByName(this);
    retranslateUi();


    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("position")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    //check if multiple instances of trodes are open
    checkMultipleInstances("Warning: An active Trodes server host was detected.  You will not be able to open a workspace file until closing all other instances of Trodes.");

    //create the benchmarking control panel
    benchmarkingControlPanel = new BenchmarkWidget();
}

MainWindow::~MainWindow()
{
    quitModules();
}

void MainWindow::startThreads()
{
    //Threads are started after the main window is created and showing

    //Audio thread
    QThread* audioThread = new QThread();

    connect(audioThread, SIGNAL(started()), soundOut, SLOT(startAudio()));
    connect(soundOut, SIGNAL(finished()), audioThread, SLOT(quit()));
    connect(soundOut, SIGNAL(finished()), soundOut, SLOT(deleteLater()));
    connect(audioThread, SIGNAL(finished()), audioThread, SLOT(deleteLater()));
    soundOut->moveToThread(audioThread);
    audioThread->start();
}


void MainWindow::saveConfig()
{
    //qDebug() << QDir::currentPath();
    //qDebug() << QCoreApplication::applicationDirPath();
    //qDebug() << currentConfigFileName;

    QFileInfo fI = QFileInfo(currentConfigFileName);
    QString configPath = fI.absolutePath();
    QString configDefaultName = fI.baseName();


    QStringList filenames;
    QString filename;
    QFileDialog dialog(0, "Save configuration as");

    //dialog.selectFile(QString("test.xml"));
    dialog.setDirectory(configPath);
    dialog.setDefaultSuffix("trodesconf");
    dialog.selectFile(configDefaultName);
    //dialog.selectFile(configDefaultName);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setOption(QFileDialog::DontConfirmOverwrite, false);
    //dialog.setOption(QFileDialog::DontUseNativeDialog);
    if (dialog.exec()) {
        filenames = dialog.selectedFiles();
    }
    if (filenames.size() == 1) {
        filename = filenames.first();
    }
    if (!filename.isEmpty()) {
        if (!writeTrodesConfig(filename)) {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Unable to save file.");
            messageBox.setFixedSize(500, 200);
        }
    }
}

void MainWindow::closeConfig()
{
    if (channelsConfigured) {
        quitModules();


        disconnectFromSource();


        soundOut->setChannel(-1); //set the audio channel to -1 (off)

        //this if statement prevents an infinite loop from occuring if switching from playback source
        if (!playbackFileOpen) {
            setSource(SourceNone);
        }


        //delete the trodesNet->tcpServer if it was started

        emit endAllThreads();
        //recordOut->deleteLater(); //We need to add the proper thread shutdown to the record thread (currently
                                    //thread quitting in endAllThreads()



        /*
            if (moduleConf->singleModuleConf.length()) {
                //trodesNet->tcpServer->close();
                //delete trodesNet->tcpServer;

                trodesNet->tcpServer->deleteServer();
            }*/

        // delete the configuration data


        //hardwareConf = NULL;



        //remove each display tab
        while (tabs->count() > 0) {
            tabs->removeTab(0);
        }


        delete eegDisp;



        if (sdDisp != NULL) {
            delete sdDisp;
            sdDisp = NULL;
        }


        //delete soundOut;






        streamManager->removeAllProcessors();




        delete streamManager;



        //streamManager->deleteLater();

        delete spikeDisp;
        spikeDisp = NULL;



        //ntrodeDisplayWidgetPtrs.clear();


        delete nTrodeTable;
        delete streamConf;
        delete spikeConf;
        delete headerConf;
        delete moduleConf;
        delete networkConf;




        networkConf=NULL;
        moduleConf=NULL;
        nTrodeTable=NULL;
        streamConf=NULL;
        spikeConf=NULL;
        headerConf=NULL;
        //hardwareConf=NULL;
        //globalConf=NULL;


        //delete hardwareConf;




        //sourceControl->clearBuffers();

        channelsConfigured = false;

        actionCloseConfig->setEnabled(false);
        actionSaveConfig->setEnabled(false);
        actionReConfig->setEnabled(false);
        actionLoadConfig->setEnabled(true);
        actionAboutConfig->setEnabled(false);
        actionHeadstageSettings->setEnabled(false);
        actionConnect->setEnabled(false);
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        actionOpenRecordDialog->setEnabled(false);
        actionShowCurrentTrode->setEnabled(false);
        currentConfigFileName = "";
    }
}

void MainWindow::reConfig()
{
    //Used the saved system settings from the last session as the default folder
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("paths"));
    QString tempPath = settings.value(QLatin1String("configPath")).toString();

    settings.endGroup();

    QString fileName = QFileDialog::getOpenFileName(0, QString("Open configuration file"), tempPath, "Trodes config files (*.trodesconf *.xml)");
    if (!fileName.isEmpty()) {
        //Save the folder in system setting for the next session
        QFileInfo fi(fileName);
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("configPath"), fi.absoluteFilePath());
        settings.endGroup();

        closeConfig();

        //Load the config file
        int result = loadConfig(fileName);

    }
}

void MainWindow::loadConfig()
{
    QString multInstError = "Error: An active Trodes server host was detected. Please close any other currently running instances of Trodes before continuing.";
    if (!unitTestFlag && checkMultipleInstances(multInstError))
        return;

    //Used the saved system settings from the last session as the default folder
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("paths"));
    QString tempPath = settings.value(QLatin1String("configPath")).toString();

    settings.endGroup();

    QString fileName = QFileDialog::getOpenFileName(0, QString("Open configuration file"), tempPath, "Trodes config files (*.trodesconf *.xml)");
    if (!fileName.isEmpty()) {
        //Save the folder in system setting for the next session
        QFileInfo fi(fileName);
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("configPath"), fi.absoluteFilePath());
        settings.endGroup();

        //Load the config file
        loadConfig(fileName);
    }
}

int MainWindow::loadConfig(QString fileName)
{
    // This is used to load a configuration and create all the control/display widgets
    // according to the settings in the config file
    QString multInstError = "Error: An active Trodes server host was detected. Please close any other currently running instances of Trodes before continuing.";
    if (!unitTestFlag && checkMultipleInstances(multInstError))
        return -1;

    loadedConfigFile = fileName;
    if (!channelsConfigured) {
        //Read the config .xml file; the second argument specifies that this is being called within trodes
        int parseCode = nsParseTrodesConfig(fileName);

        if (parseCode < 0) {
            //QMessageBox messageBox;
            //messageBox.critical(0,"Error in workspace file",lastDebugMsg);

            showErrorMessage(lastDebugMsg);
            //messageBox.setFixedSize(500,200);
            return parseCode;
        }
        currentConfigFileName = fileName;
        qDebug() << "[MainWindow::loadConfig] Number of ntrodes" << nTrodeTable->ntrodes.length();
        qDebug() << "[MainWindow::loadConfig] Number of ntrodes (spikeConf)" << spikeConf->ntrodes.length();

        if (unitTestFlag) {
            unitTestMode = true;
        } else {
            unitTestMode = false;
        }

        //Set up the stream controller
        streamManager = new StreamProcessorManager(0);
        connect(streamManager, SIGNAL(bufferOverrun()), this, SLOT(bufferOverrunHandler())); //In case data rates are too fast, use an emergency stop signal
        connect(streamManager, SIGNAL(sourceFail_Sig()), sourceControl, SLOT(noDataComing()));
        connect(this, SIGNAL(newTraceLength(double)), streamManager, SLOT(updateDataLength(double)));
        connect(spikeConf, SIGNAL(updatedRef(void)), streamManager, SLOT(updateChannels()));
        connect(spikeConf, SIGNAL(updatedFilter(void)), streamManager, SLOT(updateChannels()));
        connect(spikeConf, SIGNAL(changeAllMaxDisp(int)), this, SLOT(setAllMaxDisp(int)));
        connect(spikeConf, SIGNAL(changeAllThresh(int)), this, SLOT(setAllThresh(int)));
        connect(sourceControl, SIGNAL(acquisitionStarted()), streamManager, SLOT(startAcquisition()));
        connect(sourceControl, SIGNAL(acquisitionStopped()), streamManager, SLOT(stopAcquisition()));
        connect(streamManager, SIGNAL(functionTriggerRequest(int)), sourceControl, SLOT(sendFunctionTriggerCommand(int)));
        qDebug() << "[MainWindow::loadConfig] Set up stream manager";

        // connect the signal from the server that we need a  data stream sent to a module to the slot that
        // will cause the StreamProcessor thread to start the appropriate data client
        //connect(TrodesServer, SIGNAL(newContDataClient(quint8)), streamManager, SLOT(startContDataClient(qunit8)));
        //connect(TrodesServer, SIGNAL(newSpikeDataClient(quint8)), streamManager, SLOT(startSpikeDataClient(qunit8)));
        //connect(TrodesServer, SIGNAL(newHeaderDataClient(quint8)), streamManager, SLOT(startHeaderDataClient(qunit8)));
        //Stream display setup---------------------------



        eegDisp = new StreamDisplayManager(0, streamManager);

        int columnCount = 0;
        for (int tnum = 0; tnum < eegDisp->eegDisplayWidgets.length(); tnum++) {

            int columnsOnPage = eegDisp->columnsPerPage[tnum];
            if (!eegDisp->isHeaderDisplayPage[tnum]) {
                tabs->addTab(eegDisp->eegDisplayWidgets[tnum], QString("Tr ") + QString("%1").arg(eegDisp->nTrodeIDs[columnCount].first()) + "-" + QString("%1").arg(eegDisp->nTrodeIDs[columnCount + columnsOnPage - 1].last()));
            } else {
                if (eegDisp->isHeaderDisplayPage[tnum]) {
                    tabs->addTab(eegDisp->eegDisplayWidgets[tnum], QString("Aux"));

                    //tabs->setTabText(tnum, QString("Aux  ") + QString("%1").arg(eegDisp->streamDisplayChannels[columnCount].first() + 1) + "-" + QString("%1").arg(eegDisp->streamDisplayChannels[columnCount + columnsOnPage - 1].last() + 1));
                    //tabs->setTabText(tnum, QString("Aux"));
                }
            }
            columnCount += columnsOnPage;
        }
        qDebug() << "[MainWindow::loadConfig] Set up stream display";

        //RFDisplay
        bool rfConfigured = false;
        if (rfConfigured) {
            //sdDisp = new SDDisplay();
            sdDisp = new SDDisplayPanel();
            connect(sdDisp,SIGNAL(connectionRequest()),sourceControl,SLOT(connectToSDCard()));
            connect(sdDisp,SIGNAL(cardEnableRequest()),sourceControl,SLOT(enableSDCard()));
            connect(sdDisp,SIGNAL(reconfigureRequest(int)),sourceControl,SLOT(reconfigureSDCard(int)));
            connect(sourceControl,SIGNAL(SDCardStatus(bool,int,bool,bool)),sdDisp,SLOT(updateSDCardStatus(bool,int,bool,bool)));
            tabs->addTab(sdDisp,"SD card");
            //sdDisp->addPanel();
            //rfDisp->addPanel();

            qDebug() << "[MainWindow::loadConfig] Set up RF display";
        }


        //As of qt5.1.1, you have to switch between the tabs in order for the mousePressEvent callback in the glWidgets to work.
        tabs->setCurrentIndex(eegDisp->eegDisplayWidgets.length() - 1);
        tabs->setCurrentIndex(0);

        //------------------------------------------------
        //connect(this, SIGNAL(setAudioChannel(int)), eegDisp, SLOT(updateAudioHighlightChannel(int)));
        connect(eegDisp, SIGNAL(trodeSelected(int)), this, SLOT(selectTrode(int)));
        connect(eegDisp, SIGNAL(streamChannelClicked(int)),this,SLOT(audioChannelChanged(int)));
        connect(eegDisp, SIGNAL(newPSTHTrigger(int,bool)),streamManager,SLOT(setPSTHTrigger(int,bool)));
        connect(eegDisp, SIGNAL(newSettleControlChannel(int,quint8,quint8)),this,SLOT(setSettleControlChannel(int,quint8,quint8)));




        /*
        spikeDisp = new SpikeDisplayWidget(NULL, &ntrodeDisplayWidgetPtrs);
        connect(spikeDisp, SIGNAL(resetAudioButtons()), this, SLOT(resetAllAudioButtons()));
        //connect(spikeDisp, SIGNAL(setAudioChannel(int)), this, SLOT(audioChannelChanged(int)));
        connect(spikeDisp, SIGNAL(setAudioChannel(int)), this, SIGNAL(setAudioChannel(int)));
        connect(spikeDisp, SIGNAL(updateAudio()), this, SIGNAL(updateAudio()));
        //connect(spikeDisp, SIGNAL(changeAllMaxDisp(int)), this, SLOT(setAllMaxDisp(int)));
        //connect(spikeDisp, SIGNAL(changeAllThresh(int)), this, SLOT(setAllThresh(int)));
        connect(spikeDisp, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
        connect(spikeDisp, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
        connect(spikeDisp, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
        connect(spikeDisp, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
        connect(spikeDisp, SIGNAL(nTrodeWindowClosed(int)), this, SLOT(removeFromOpenNtrodeList(int)));
        */

        if (hardwareConf->NCHAN > 0) {
            spikeDisp = new MultiNtrodeDisplayWidget(NULL);
            spikeDisp->setStreamManagerPtr(streamManager);

            //spikeDisp->setWindowFlags(Qt::WindowStaysOnTopHint);
            //spikeDisp->show();
            singleTriggerWindowOpen = true;

            connect(eegDisp, SIGNAL(streamChannelClicked(int)),spikeDisp,SLOT(changeAudioChannel(int)));
            //connect(spikeDisp, SIGNAL(resetAudioButtons()), this, SLOT(resetAllAudioButtons()));
            connect(spikeDisp, SIGNAL(channelClicked(int)), this, SLOT(audioChannelChanged(int)));
            connect(spikeDisp,SIGNAL(channelClicked(int)),eegDisp,SLOT(updateAudioHighlightChannel(int)));
            connect(spikeConf,SIGNAL(newMaxDisplay(int,int)),spikeDisp,SLOT(setMaxDisplay(int,int)));


            //connect(spikeDisp, SIGNAL(updateAudio()), this, SIGNAL(updateAudio()));
            //connect(spikeDisp, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
            //connect(spikeDisp, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
            //connect(spikeDisp, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
            //connect(spikeDisp, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
            //connect(spikeDisp, SIGNAL(nTrodeWindowClosed(int)), this, SLOT(removeFromOpenNtrodeList(int)));


            qDebug() << "[MainWindow::loadConfig] Set up spike trigger display";
        }


        // Set up connections for spikeDetectors
        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
            connect(streamManager->spikeDetectors[i],
                    SIGNAL(spikeDetectionEvent(int, const QVector<int2d>*, const int*, uint32_t)),
                    spikeDisp, SLOT(receiveNewEvent(int,const QVector<int2d>*,const int*,uint32_t)), Qt::DirectConnection); //This might not be thread-safe. Needs to be double checked
        }

       //connect(eegDisp, SIGNAL(streamChannelClicked(int)), spikeDisp, SLOT(relayChangeAudioSignal(int)));

        //Audio thread setup--------------------------
        if (hardwareConf->NCHAN > 0) {
        soundOut->setChannel(spikeConf->ntrodes[0]->hw_chan[0]); //set the audio channel to listen to
        soundOut->updateAudio();
        }
        //---------------------------------------------

        //Record thread setup---------------------------

        recordOut = new RecordThread();
        QThread* workerThread = new QThread();
        recordOut->moveToThread(workerThread);
        connect(workerThread, SIGNAL(started()), recordOut, SLOT(setUp()));
        connect(recordOut, SIGNAL(finished()), workerThread, SLOT(quit()));
        connect(recordOut,SIGNAL(finished()), recordOut, SLOT(deleteLater()));
        connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
        workerThread->setObjectName("Recorder");
        workerThread->start();

        connect(this, SIGNAL(endAllThreads()), recordOut, SLOT(endRecordThread()));
        // update the list of channels to save once a configuration file has been loaded
        connect(this, SIGNAL(configFileLoaded()), recordOut, SLOT(setupSaveDisplayedChan()));
        connect(recordOut,SIGNAL(writeError()),this,SLOT(errorSaving()));

        //----------------------------------------------


        channelsConfigured = true;
        actionLoadConfig->setEnabled(false);
        actionCloseConfig->setEnabled(true);
        actionSaveConfig->setEnabled(true);
        actionReConfig->setEnabled(true);
        actionAboutConfig->setEnabled(true);
        if (sourceControl->currentSource > 0) {
            actionConnect->setEnabled(true);
            actionHeadstageSettings->setEnabled(true);
        }
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        //actionOpenRecordDialog->setEnabled(true);
        actionShowCurrentTrode->setEnabled(true);

        // start the local TCP server


        startMainNetworkMessaging();


        // set up the network connections with the modules if modules are defined
        // set myID to -1 to indicate that this is trodes
        moduleConf->myID = TRODES_ID;
        startModules(fileName);

        //MARK: Event

        connect(spikeDisp, SIGNAL(broadcastEvent(TrodesEventMessage)),this,SLOT(broadcastEvent(TrodesEventMessage)));
        connect(this, SIGNAL(sendEvent(uint32_t,int,QString)),trodesNet->tcpServer, SLOT(eventOccurred(uint32_t,int,QString)));
        connect(spikeDisp, SIGNAL(broadcastNewEventReq(QString)), trodesNet->tcpServer, SLOT(addEventTypeToList(QString)));

        // update the list of channels to save for the record thread

        emit configFileLoaded();
        //only read benchmarking settings from the config file if the user hasn't edited benchConf via either the GUI or cmdLine yet
        if (benchConfig != NULL && !benchConfig->wasEditedByUser() && !benchConfig->wasInitiatedFromCommandLine())
            benchmarkingControlPanel->getSettingsFromConfig();

        return parseCode;
    }
    else {
        return -1;
    }
}

void MainWindow::startModules(QString configFileName)
{
    if (moduleConf->singleModuleConf.length() && !playbackFileOpen) {

        SingleModuleConf s;
        // launch the modules and start the TCPIP server to each one as we do so. This is slower than launching
        // all the modules at once, but allows us to keep their connections in order

        for (int i = 0; i < moduleConf->singleModuleConf.length(); i++) {
            s = moduleConf->singleModuleConf[i];

            // Set up stderr/stdout forwarding (will be rerouted to qDebug and then logged)
            QProcess *moduleProcess = new QProcess(this);
            connect(moduleProcess,SIGNAL(readyReadStandardError()),this,SLOT(forwardProcessOutput()));
            connect(moduleProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(forwardProcessOutput()));

            trodesNet->startSingleModule(s, moduleProcess);
        }
    }
    // enable the Restart Modules menu item
    actionRestartModules->setEnabled(true);
}


void MainWindow::restartCrashedModule(QString modName) {
    if (modName == "Camera") {
        videoButtonPressed();
    }
}

void MainWindow::forwardProcessOutput() {
    QProcess* senderProcess = (QProcess*)sender();

    qDebug().noquote() << senderProcess->readAllStandardError();
}

void MainWindow::openSettleChannelDelayDialog() {

    bool ok;
    int newDelay = QInputDialog::getInt(0, "Enter settle delay after trigger",
                "Delay (samples):", settleChannelDelay, 0, 30000,1,&ok);
    if (ok) {
        setSettleChannelDelay(newDelay);
    }
}

void MainWindow::setSettleChannelDelay(int delay) {
    settleChannelDelay = delay;
    sourceControl->sendSettleChannel(settleChannelByteInPacket,settleChannelBit,settleChannelDelay,settleChannelTriggerState);
}

void MainWindow::setSettleControlChannel(int byteInPacket, quint8 bit, quint8 triggerState) {

    settleChannelByteInPacket = byteInPacket;
    settleChannelBit = bit;
    settleChannelTriggerState = triggerState;
    //settleChannelDelay = 0;
    sourceControl->sendSettleChannel(byteInPacket,bit,settleChannelDelay,triggerState);
}

void MainWindow::startMainNetworkMessaging(void)
{
    // Create the global trodes module network object
    trodesNet = new TrodesModuleNetwork();


    // Set the ID to the specified ID for the main program.
    trodesNet->moduleID = TRODES_ID;
    trodesNet->tcpServer = new TrodesServer();

    //The server is able to respond to small data requests from the modules.
    //Here is where we give the server access to the possible data types requested.
    //So far just time.
    trodesNet->tcpServer->setModuleTimePtr(&currentTimeStamp);
    connect(this, SIGNAL(recordFileOpened(QString)), trodesNet->tcpServer, SLOT(sendFileOpened(QString)));
    connect(this, SIGNAL(sourceConnected(QString)), trodesNet->tcpServer, SLOT(sendSourceConnect(QString)));
    connect(this, SIGNAL(recordFileClosed()), trodesNet->tcpServer, SLOT(sendFileClose()));
    connect(this, SIGNAL(recordingStarted()), trodesNet->tcpServer, SLOT(sendStartRecord()));
    connect(this, SIGNAL(recordingStopped()), trodesNet->tcpServer, SLOT(sendStopRecord()));
    connect(this, SIGNAL(endAllThreads()), trodesNet->tcpServer, SLOT(deleteServer()));
    connect(trodesNet->tcpServer, SIGNAL(trodesServerError(QString)), this, SLOT(threadError(QString)));
    connect(trodesNet->tcpServer,SIGNAL(settleCommandTriggered()),this,SLOT(sendSettleCommand()));

    //MARK: cur
    //Connection to update all modules whenever the user edits the benchmark settings
    connect(this->benchmarkingControlPanel, SIGNAL(signal_benchConfigUpdated()), trodesNet->tcpServer, SLOT(sendBenchConfigToModules()));

    //Here is where we set up the connections for when a module is
    //asking for a dedicated data stream.  Individual thread (such as
    //the spike triggering threads) will then stream data to the socket.
    //connect(trodesNet->tcpServer,SIGNAL(newAnalogIODataSocket(TrodesSocketMessageHandler*,qint16)),streamManager, SLOT(newAnalogIOHandler(TrodesSocketMessageHandler*,qint16)));
    //connect(trodesNet->tcpServer,SIGNAL(newDigitalIODataSocket(TrodesSocketMessageHandler*,qint16)),streamManager, SLOT(newDigitalIOHandler(TrodesSocketMessageHandler*,qint16)));
    //connect(trodesNet->tcpServer,SIGNAL(newContinuousDataSocket(TrodesSocketMessageHandler*,qint16)),streamManager, SLOT(newContinuousHandler(TrodesSocketMessageHandler*,qint16)));
    //connect(trodesNet->tcpServer,SIGNAL(newSpikeDataSocket(TrodesSocketMessageHandler*,qint16)),streamManager, SLOT(newNTrodeTriggerHandler(TrodesSocketMessageHandler*,qint16)));

    // connect slots related to data exchange among modules.  These just pass signals from a message handler through
    // tcpServer and on to trodesNet
    connect(trodesNet->tcpServer, SIGNAL(doSendAllDataAvailable(TrodesSocketMessageHandler*)), trodesNet, SLOT(sendAllDataAvailableToModule(TrodesSocketMessageHandler*)));
    connect(trodesNet->tcpServer, SIGNAL(doAddDataAvailable(DataTypeSpec*)), trodesNet, SLOT(addDataAvailable(DataTypeSpec*)));
    connect(trodesNet->tcpServer, SIGNAL(doRemoveDataAvailable(qint8)), trodesNet, SLOT(removeDataAvailable(qint8)));
    connect(trodesNet->tcpServer, SIGNAL(nameReceived(TrodesSocketMessageHandler*,QString)), trodesNet->tcpServer,
            SLOT(setNamedModuleMessageHandler(TrodesSocketMessageHandler*,QString)));


    connect(trodesNet->tcpServer, SIGNAL(moduleDataStreamOn(bool)), this, SLOT(setModuleDataStreaming(bool)));
    connect(this, SIGNAL(messageForModules(TrodesMessage*)), trodesNet->tcpServer, SLOT(sendMessageToModules(TrodesMessage*)));
    connect(trodesNet, SIGNAL(messageForModule(TrodesSocketMessageHandler*, TrodesMessage*)),
            trodesNet->tcpServer, SLOT(sendMessageToModule(TrodesSocketMessageHandler*, TrodesMessage*)));

    connect(trodesNet->tcpServer, SIGNAL(restartCrashedModule(QString)), this, SLOT(restartCrashedModule(QString)));


    // add a signal for internal DataAvailable information to be added to the main list
    connect(streamManager, SIGNAL(addDataProvided(DataTypeSpec*)), trodesNet, SLOT(addDataAvailable(DataTypeSpec*)));


    //check Connection status


    // set the port for communication to the hardware
    if ((networkConf->networkConfigFound) && (networkConf->trodesHost != "")) {
        trodesNet->tcpServer->setAddress(networkConf->trodesHost);
    }
    if ((networkConf->networkConfigFound) && (networkConf->trodesPort != 0)) {
        trodesNet->tcpServer->startServer("Trodes main", networkConf->trodesPort);
    }
    else {
        //otherwise just find an available address and port
        //localhost is the last option if nothing else is available
        trodesNet->tcpServer->startServer("Trodes main");
        // set the host name and the port
        networkConf->trodesHost = QHostInfo::localHostName();
        networkConf->trodesPort = trodesNet->tcpServer->serverPort();

    }

    qDebug() << "[MainWindow::startMainNetworkMessaging] trodesHost =" << networkConf->trodesHost << "port =" << networkConf->trodesPort;

    //Move the trodesNet object to a separate thread so that networking can go on outside the GUI thread

    trodesNetThread = new QThread;
    //connect(trodesNetThread,SIGNAL(started()),trodesNet->tcpServer,SLOT());
    connect(trodesNet->tcpServer, SIGNAL(finished()), trodesNetThread, SLOT(quit()));
    connect(trodesNet->tcpServer, SIGNAL(finished()), trodesNet->tcpServer, SLOT(deleteLater()));
    trodesNet->tcpServer->moveToThread(trodesNetThread);
    connect(trodesNetThread, SIGNAL(finished()), trodesNetThread, SLOT(deleteLater()));
    trodesNetThread->start();
}



void MainWindow::audioChannelChanged(int hwchannel)
{

    soundOut->setChannel(hwchannel);
    //emit setAudioChannel(hwchannel);
}

void MainWindow::selectTrode(int nTrode)
{
    if (nTrode != currentTrodeSelected) {
        currentTrodeSelected = nTrode;
        if (singleTriggerWindowOpen) {
            spikeDisp->setShownNtrode(currentTrodeSelected);
        }
    }
}

int MainWindow::findEndOfConfigSection(QString configFileName) {


    QFile file;
    int filePos = -1;

    Q_ASSERT(!configFileName.isEmpty());

    file.setFileName(configFileName);
    if (!file.open(QIODevice::ReadOnly)) {
        return -1;
    }

    QFileInfo fi(configFileName);
    QString ext = fi.suffix();
    if (ext.compare("rec") == 0) {
        //this is a rec file with a configuration in the header

        QString configContent;
        QString configLine;
        bool foundEndOfConfig = false;

        while (file.pos() < 1000000) {
            configLine += file.readLine();
            configContent += configLine;
            if (configLine.indexOf("</Configuration>") > -1) {
                foundEndOfConfig = true;
                break;
            }
            configLine = "";
        }

        if (foundEndOfConfig) {
            filePos = file.pos();
        }
    }
    file.close();
    return filePos;
}

bool MainWindow::openPlaybackFile(const QString fileName)
{
    qDebug() << "[MainWindow::openPlaybackFile] Opening file" << fileName;


    if ((!playbackFileOpen) && (!recordFileOpen)) {
        int filePos;
        bool usingExternalWorkspace = false;

        QFileInfo fI(fileName);

        QString baseName = fI.baseName();
        QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";

        QFileInfo workspaceFile(workspaceCheckName);
        if (workspaceFile.exists()) {
            qDebug() << "[MainWindow::openPlaybackFile] Using the following workspace file: " << workspaceFile.fileName();
            usingExternalWorkspace = true;
            filePos = findEndOfConfigSection(fileName);
        }


        //Open up the configuration settings used when the
        //data were recorded. x current workspace.
        if (channelsConfigured) {
            closeConfig();
        }

        int loadReturn;
        playbackFileOpen = true; //we set this gloval flag before opening the config file, because that way we can use it to stop opening modules
        if (usingExternalWorkspace) {
            loadReturn = loadConfig(workspaceFile.absoluteFilePath());
            if (loadReturn != 0) {
                playbackFileOpen = false;
                return false;
            }
        } else {
           //No external workspace with the same name found, so try to load the settings
           //embedded in the recording file
           filePos = loadConfig(fileName);
        }

        if (filePos < 0) {
            //QMessageBox messageBox;
            //messageBox.critical(0, "Error", "The configuration settings associated with this recording could not be parsed.");
            showErrorMessage("The configuration settings associated with this recording could not be parsed.");

            actionSourceNone->setChecked(true);
            sourceControl->setSource(SourceNone);
            playbackFileOpen = false;
            return false;
        }




        //int packetBytes = 2*hardwareConf->NCHAN+(2*hardwareConf->headerSize)+4;

        //int32_t recTimeInSec = (fI.size()-filePos)/(packetBytes*hardwareConf->sourceSamplingRate);

        actionCloseFile->setEnabled(true);
        playbackFile = fileName;
        fileString = fI.fileName();
        fileLabel->setText(fileString);

        fileDataPos = filePos;
        actionSourceFile->setChecked(true);
        sourceControl->setSource(SourceFile);
        actionPlay->setEnabled(true);
        playButton->setEnabled(true);
        pauseButton->setEnabled(true);
        actionExport->setEnabled(true);

        actionSourceNone->setChecked(false);
        actionSourceFake->setChecked(false);
        actionSourceFakeSpikes->setChecked(false);
        actionSourceUSB->setChecked(false);
        actionSourceRhythm->setChecked(false);
        actionSourceEthernet->setChecked(false);
        actionOpenGeneratorDialog->setEnabled(false);

        //we could in priciple record data from the playback, but this seems like an odd thing
        //to do.  If we don't allow this, we can use the pause button for both playback and record.
        actionOpenRecordDialog->setEnabled(false);
    }

    return true;
}

void MainWindow::threadError(QString errorString) {
    // show a box with the error message
    if (!unitTestFlag) {
        QMessageBox::warning(this, "Error", errorString);
    }
}

void MainWindow::openRecordDialog()
{
    //dialog to create a new record file
    QString dataDir = globalConf->filePath;
    QString fileName;
    QString defaultFileName;
    QDateTime fileCreateTime = QDateTime::currentDateTime();

    if (!QDir(dataDir).exists()) {
        dataDir = QDir::homePath();
    }
    else {
        dataDir = QDir(dataDir).absolutePath();
    }


    defaultFileName = globalConf->filePrefix + fileCreateTime.toString("MM.dd.yyyy").replace(".", "-") +
                      tr("(") + fileCreateTime.toString("hh.mm.ss").replace(".", "_") + tr(").rec");
    dataDir = QDir().toNativeSeparators(dataDir + "/" + defaultFileName);
    qDebug() << "[MainWindow::openRecordDialog] Default file " << dataDir;

    fileName = QFileDialog::getSaveFileName(this, tr("Create File"), dataDir, tr("*.rec"));

    if (!(fileName == "")) {
        if (!fileName.endsWith(".rec")) {
            fileName += ".rec";
        }
        int fileOpenStatus = recordOut->openFile(fileName); //creates the file and writes the current config info
        if (fileOpenStatus == -1) {
            QMessageBox::information(0, "error", tr("File already exists. Please rename existing file first."));
            return;
        }
        if (fileOpenStatus == -2) {
            QMessageBox::information(0, "error", tr("Error: File could not be created."));
            return;
        }

        recordFileOpen = true;
        recordFileName = fileName;

        QFileInfo fI(fileName);
        fileString = fI.fileName();
        fileLabel->setText(fileString + tr("   (0 MB)")); //append the current size of the file in the display
        actionOpenRecordDialog->setEnabled(false);
        actionCloseFile->setEnabled(true);
        actionCloseConfig->setEnabled(false);

        if (dataStreaming) {
            actionRecord->setEnabled(true);
            recordButton->setEnabled(true);
            pauseButton->setEnabled(true);
            pauseButton->setDown(true);
        }

        emit recordFileOpened(fileName);
    }
}

void MainWindow::exportData(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting)
{
    //TODO: many of the inputs are not yet used.  Don't delete!!

    if (playbackFileOpen && !dataStreaming) {
        qDebug() << "[MainWindow::exportData] Exporting: " << "Spikes: " << spikesOn << " ModuleData: " << ModuleDataon;
        QFileInfo fileInfo;
        fileInfo.setFile(playbackFile);
        QString baseName = fileInfo.baseName();
        QDir fileDir = fileInfo.absoluteDir();
        if (spikesOn) {
            QString spikeDirName = baseName + "_Spikes";
            fileDir.mkdir(spikeDirName);
            if (!fileDir.cd(spikeDirName)) {
                qDebug() << "[MainWindow::exportData] Error making spike directory";
                return;
            }
            streamManager->createSpikeLogs(fileDir.absolutePath());
        }
        qDebug() << "[MainWindow::exportData] Path: " << fileDir.path() << " Name: " << fileInfo.baseName();

        disconnectFromSource();
        exportMode = true;
        connectToSource();
    }
}

void MainWindow::cancelExport()
{
    disconnectFromSource();
    exportMode = false;
}

void MainWindow::bufferOverrunHandler()
{
    if (!exportMode) {
        //disconnectFromSource();
        //qDebug() << "Data streaming stopped-- data rate is too fast";
        qDebug() << "[MainWindow::bufferOverrunHandler] Buffer overrun!";
    }
    else {
        sourceControl->waitForThreads();
    }
}

void MainWindow::showErrorMessage(QString msg) {
    //Multi-purpose error dialog
    if (!unitTestFlag) {
        QMessageBox messageBox;
        messageBox.critical(0,"Error",msg);
        messageBox.setFixedSize(500,200);
    }
}

void MainWindow::errorSaving() {
    pauseButtonPressed();
    showErrorMessage(QString("Error writing to disk.  Please try again or check drive."));
}

void MainWindow::closeFile()
{

    if (playbackFileOpen) {
        setSource(SourceNone);
    } else {
        recordOut->closeFile();
        recordFileOpen = false;
        QString FileLabelColor("gray");
        QString FileLabelText("No file open");
        QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
        fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));
        actionOpenRecordDialog->setEnabled(true);
        actionCloseFile->setEnabled(false);
        actionRecord->setEnabled(false);
        recordButton->setEnabled(false);
        actionPause->setEnabled(false);
        pauseButton->setEnabled(false);

        if (channelsConfigured) {
            actionCloseConfig->setEnabled(true);
        }
        emit recordFileClosed();
    }
}

void MainWindow::recordButtonPressed()
{
    pauseButton->setDown(false);
    actionPause->setEnabled(true);
    actionRecord->setEnabled(false);
    recordOut->startRecord();
    actionCloseFile->setEnabled(false);
    recording = true;
    actionDisconnect->setEnabled(false);
    statusbar->showMessage(tr("Started recording at ") + calcTimeString());
    emit recordingStarted();
}
void MainWindow::recordButtonReleased()
{
    recordButton->setDown(true);
}
void MainWindow::actionRecordSelected()
{
    recordButton->setDown(true);
    recordButtonPressed();
}


void MainWindow::pauseButtonPressed()
{
    if (recordFileOpen) {
        recordButton->setDown(false);
        actionPause->setEnabled(false);
        actionDisconnect->setEnabled(true);
        actionCloseFile->setEnabled(true);
        recordOut->pauseRecord();
        recording = false;
        actionRecord->setEnabled(true);
        statusbar->showMessage(tr("Paused recording at ") + calcTimeString());
        emit recordingStopped();
    }
    else if (playbackFileOpen) {
        actionPause->setEnabled(false);
        actionPlay->setEnabled(true);
        playButton->setDown(false);
        sourceControl->pauseSource();
        actionExport->setEnabled(true);
    }
}
void MainWindow::pauseButtonReleased()
{
    pauseButton->setDown(true);
}
void MainWindow::actionPauseSelected()
{
    pauseButton->setDown(true);
    pauseButtonPressed();
}

void MainWindow::playButtonPressed()
{
    pauseButton->setDown(false);
    actionPause->setEnabled(true);
    actionPlay->setEnabled(false);
    actionExport->setEnabled(false); //We don't allow exporting when the file is being played back
    filePlaybackSpeed = 1; //Normal speed
    exportMode = false;

    connectToSource();
}
void MainWindow::playButtonReleased()
{
    playButton->setDown(true);
}
void MainWindow::actionPlaySelected()
{
    playButton->setDown(true);
    playButtonPressed();
}

void MainWindow::sendSettleCommand() {
    sourceControl->sendSettleCommand();
}

void MainWindow::disconnectFromSource()
{

    if (recordFileOpen) {
        if (recordOut->getBytesWritten() > 0) {
            //There is a record file open with data in it
            //This needs to be closed
            QMessageBox messageBox;
            int answer = messageBox.question(0, "File is open", "The record file will be closed. Proceed?");
            //messageBox.setFixedSize(500,200);
            if (answer == 1) {
                closeFile();
            }
            else {
                return;
            }
        }
    }

    if (playbackFileOpen) {
        actionPlay->setEnabled(true);
        actionPause->setEnabled(false);
        pauseButton->setDown(true);
        playButton->setDown(false);
        actionExport->setEnabled(false);
    }

    //disconnect from source
    sourceControl->disconnectFromSource();
}

void MainWindow::connectToSource()
{
    //Pointless routing right now, but probably good to retain this step in case we
    //want menus to change, etc.

    sourceControl->connectToSource();

}

void MainWindow::setSource()
{
    //source selected with menu ( wrapper for setSource(int) )
    QAction* action = (QAction*)sender();

    setSource(action->data().value<DataSource>());
}

void MainWindow::setSource(DataSource source)
{
    //changes the source of the data stream

    if (source != sourceControl->currentSource) {
        //set the menu state
        actionSourceNone->setChecked(false);
        actionSourceFake->setChecked(false);
        actionSourceFakeSpikes->setChecked(false);
        actionSourceFile->setChecked(false);
        actionSourceUSB->setChecked(false);
        actionSourceRhythm->setChecked(false);
        actionSourceEthernet->setChecked(false);
        actionOpenGeneratorDialog->setEnabled(false);

        bool needToClosePlaybackConfig = false;
        if (playbackFileOpen && (source != SourceFile)) {
            needToClosePlaybackConfig = true;

        }

        //Used the saved system settings from the last session as the default folder
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        QString tempPath = settings.value(QLatin1String("playbackPath")).toString();
        settings.endGroup();
        QString pFileName;

        switch (source) {
        case SourceNone:

            actionSourceNone->setChecked(true);
            sourceControl->setSource(source);
            actionSourceNone->setEnabled(true);
            actionSourceFake->setEnabled(true);
            actionSourceFile->setEnabled(true);
            actionPlaybackOpen->setEnabled(true);
            actionSourceFakeSpikes->setEnabled(true);
            actionSourceEthernet->setEnabled(true);
            actionSourceUSB->setEnabled(true);
            actionSourceRhythm->setEnabled(true);
            if (!recordFileOpen) {
                actionCloseFile->setEnabled(false);
            }
            break;

        case SourceFake:
            actionSourceFake->setChecked(true);
            actionOpenGeneratorDialog->setEnabled(true);
            sourceControl->setSource(source);
            break;

        case SourceFakeSpikes:
            actionSourceFakeSpikes->setChecked(true);
            //actionOpenGeneratorDialog->setEnabled(true);
            sourceControl->setSource(source);
            break;

        case SourceFile:


            pFileName = QFileDialog::getOpenFileName(0, tr("Open file for playback"), tempPath, tr("Rec files (*.rec)"));
            if (!pFileName.isEmpty()) {
                //Save the folder in system setting for the next session
                QFileInfo fi(pFileName);
                QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
                settings.beginGroup(QLatin1String("paths"));
                settings.setValue(QLatin1String("playbackPath"), fi.absoluteFilePath());
                settings.endGroup();

                //Load the config file
                openPlaybackFile(pFileName);
            }



            //openPlaybackFile(QFileDialog::getOpenFileName(this, tr("Open file for playback"), "", tr("Rec files (*.rec)")));

            break;

        case SourceEthernet:
            actionSourceEthernet->setChecked(true);
            sourceControl->setSource(source);
            break;

        case SourceUSBDAQ:
            actionSourceUSB->setChecked(true);
            sourceControl->setSource(source);
            break;

        case SourceRhythm:
            actionSourceRhythm->setChecked(true);
            sourceControl->setSource(source);
            break;
        }


        if (needToClosePlaybackConfig) {

            qDebug() << "[MainWindow::setSource] Closing playback config.";
            closeConfig(); //if the source was a file, we close the associated configuration

            QString FileLabelColor("gray");
            QString FileLabelText("No file open");
            QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
            fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));

            actionPlay->setEnabled(false);
            actionPause->setEnabled(false);
            playButton->setEnabled(false);
            pauseButton->setEnabled(false);
            actionExport->setEnabled(false);
        }
    }
}

void MainWindow::setTLength()
{
    QAction* action = (QAction*)sender();

    actionSetTLength0_2->setChecked(false);
    actionSetTLength0_5->setChecked(false);
    actionSetTLength1_0->setChecked(false);
    actionSetTLength2_0->setChecked(false);
    actionSetTLength5_0->setChecked(false);
    action->setChecked(true);
    double value = action->data().toDouble();

    eegDisp->freezeDisplay(true);

    //send signal to streamProcessor to change trace length
    emit newTraceLength(value);

    //There is really no need to update the xaxis on the traces, so for now we skip this step
    /*
       for (int i = 0; i < eegDisp->glStreamWidgets.length(); i++) {
          eegDisp->glStreamWidgets[i]->setTLength(value);
       }*/

    eegDisp->freezeDisplay(false);
}

void MainWindow::soundButtonPressed()
{
    soundSettingsButton->setDown(false);

    soundDialog *newSoundDialog = new soundDialog(0, 0);
    //if (channelsConfigured) {
    newSoundDialog->threshSlider->setValue(soundOut->getThresh());
    newSoundDialog->gainSlider->setValue(soundOut->getGain());
    //soundDialog *newSoundDialog = new soundDialog(soundOut->getGain(),soundOut->getThresh());
    //}
    newSoundDialog->setWindowFlags(Qt::Popup);

    newSoundDialog->setGeometry(QRect(this->geometry().x()+soundSettingsButton->x(),this->geometry().y()+soundSettingsButton->y()+soundSettingsButton->height()+this->menuBar()->height(),40,200));

/*
    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.
#if defined (__WIN32__)
    newSoundDialog->setGeometry(QRect(this->x() + soundSettingsButton->x() + 8, this->y() + soundSettingsButton->y() + 70, 40, 200));
#else
    newSoundDialog->setGeometry(QRect(this->x() + soundSettingsButton->x(), this->y() + soundSettingsButton->y() + 43, 40, 200));
#endif

*/
    //if (channelsConfigured) {
    connect(newSoundDialog->gainSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setGain(int)));
    connect(newSoundDialog->threshSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setThresh(int)));
    //}
    connect(this, SIGNAL(closeAllWindows()), newSoundDialog, SLOT(close()));
    connect(this, SIGNAL(closeSoundDialog()), newSoundDialog, SLOT(close()));
    newSoundDialog->show();
}

void MainWindow::trodeButtonPressed()
{
    trodeSettingsButton->setDown(false);

    if (channelsConfigured && hardwareConf->NCHAN > 0) {
        TriggerScopeSettingsWidget* triggerSettings = new TriggerScopeSettingsWidget(0, currentTrodeSelected);
        connect(triggerSettings, SIGNAL(updateAudioSettings()), this, SIGNAL(updateAudio()));
        connect(triggerSettings, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
        connect(triggerSettings, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
        connect(triggerSettings, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
        connect(triggerSettings, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
        connect(triggerSettings, SIGNAL(toggleLinkChanges(bool)), this, SLOT(linkChanges(bool)));
        connect(triggerSettings, SIGNAL(moduleDataChannelChanged(int, int)), this, SLOT(sendModuleDataChanToModules(int, int)));
        connect(trodesNet->tcpServer, SIGNAL(moduleDataStreamOn(bool)), triggerSettings, SLOT(setEnabledForStreaming(bool)));
        triggerSettings->setEnabledForStreaming(isModuleDataStreaming());

        triggerSettings->setWindowFlags(Qt::Popup);
        triggerSettings->setGeometry(QRect(this->geometry().x()+trodeSettingsButton->x(),this->geometry().y()+trodeSettingsButton->y()+trodeSettingsButton->height()+this->menuBar()->height(),40,200));

 /*
        //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
        //add something for Linux here too.
#if defined (__WIN32__)
        triggerSettings->setGeometry(QRect(this->x() + trodeSettingsButton->x() + 8, this->y() + trodeSettingsButton->y() + 70, 40, 200));
#else
        triggerSettings->setGeometry(QRect(this->x() + trodeSettingsButton->x(), this->y() + trodeSettingsButton->y() + 43, 40, 200));
#endif
*/

        connect(this, SIGNAL(closeAllWindows()), triggerSettings, SLOT(close()));
        connect(this, SIGNAL(closeSoundDialog()), triggerSettings, SLOT(close()));
        triggerSettings->show();
    }
}

void MainWindow::statescriptButtonPressed() {
    //open video window

    if (channelsConfigured) {
        QStringList arglist;
        QString modName;

        int modNum = moduleConf->findModule("stateScript");

        SingleModuleConf s;
        if (modNum > -1) {
            s = moduleConf->singleModuleConf[modNum];
            // launch the module with the main configuration file and the specified configuration file
            qDebug() << "[MainWindow::videoButtonPressed] Launching module: " << s.moduleName;
        } else {
            // Try the default location
            s.moduleName = "stateScript";
            s.sendTrodesConfig = 1;
            s.sendNetworkInfo = 1;
            qDebug() << "[MainWindow::statescriptButtonPressed] No stateScript entry in config file, launching: " << s.moduleName;
        }

        // Set up stderr/stdout forwarding (will be rerouted to qDebug and then logged)
        QProcess *moduleProcess = new QProcess(this);
        connect(moduleProcess,SIGNAL(readyReadStandardError()),this,SLOT(forwardProcessOutput()));
        connect(moduleProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(forwardProcessOutput()));

        trodesNet->startSingleModule(s, moduleProcess);
    }
}


void MainWindow::videoButtonPressed() {
    //open video window
    //videoButton->setDown(false);
    if (channelsConfigured) {
        QStringList arglist;
        QString modName;

        int camModNum = moduleConf->findModule("cameraModule");

        SingleModuleConf s;
        if (camModNum > -1) {
            s = moduleConf->singleModuleConf[camModNum];
            // launch the module with the main configuration file and the specified configuration file
            qDebug() << "[MainWindow::videoButtonPressed] Launching module: " << s.moduleName;
        } else {
            // Try the default cameraModule location
            s.moduleName = "cameraModule";
            s.sendTrodesConfig = 1;
            s.sendNetworkInfo = 1;
            qDebug() << "[MainWindow::videoButtonPressed] No cameraModule entry in config file, launching: " << s.moduleName;
        }

        // Set up stderr/stdout forwarding (will be rerouted to qDebug and then logged)
        QProcess *moduleProcess = new QProcess(this);
        connect(moduleProcess,SIGNAL(readyReadStandardError()),this,SLOT(forwardProcessOutput()));
        connect(moduleProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(forwardProcessOutput()));

        trodesNet->startSingleModule(s, moduleProcess);
    }
}

void MainWindow::spikesButtonPressed()
{
    //open spike trigger window
    //spikesButton->setDown(false);
    if (channelsConfigured && hardwareConf->NCHAN > 0) {
        spikeDisp->show();
        spikeDisp->raise();
        //openTrodeWindow();
    }
}


void MainWindow::commentButtonPressed()
{
    commentButton->setDown(false);

    QString commentFileName;
    if (recordFileOpen) {
        QFileInfo fileInfo(recordFileName);
        commentFileName = fileInfo.absolutePath() + "/" + fileInfo.baseName() + ".trodesComments";
    }
    else {
        commentFileName = "";
    }

    CommentDialog *newCommentDialog = new CommentDialog(commentFileName, this);

    newCommentDialog->setWindowFlags(Qt::Popup);
    newCommentDialog->setGeometry(QRect(this->geometry().x()+commentButton->x(),this->geometry().y()+commentButton->y()+commentButton->height()+this->menuBar()->height(),40,200));

/*
    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.
#if defined (__WIN32__)
    newCommentDialog->setGeometry(QRect(this->x() + commentButton->x() + 8, this->y() + commentButton->y() + 70, 40, 200));
#else
    newCommentDialog->setGeometry(QRect(this->x() + commentButton->x(), this->y() + commentButton->y() + 43, 40, 200));
#endif
*/

    connect(this, SIGNAL(closeAllWindows()), newCommentDialog, SLOT(close()));
    connect(this, SIGNAL(closeSoundDialog()), newCommentDialog, SLOT(close()));
    newCommentDialog->show();
}

void MainWindow::openHeadstageDialog() {
    //opens the dialog used to control the signal generator for debugging without hardware connected

    HeadstageSettingsDialog *newDialog = new HeadstageSettingsDialog(headstageSettings);

    newDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    newDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
    connect(this, SIGNAL(closeAllWindows()), newDialog, SLOT(close()));
    connect(newDialog, SIGNAL(windowClosed()), this, SLOT(enableHeadstageDialogMenu()));
    connect(newDialog,SIGNAL(newSettings(HeadstageSettings)),sourceControl,SLOT(setHeadstageSettings(HeadstageSettings)));
    //connect(newDialog,SIGNAL(newSettings(HeadstageSettings)),this,SLOT(headstageSettingsChanged(HeadstageSettings)));
    newDialog->show();
    actionHeadstageSettings->setEnabled(false);
}

void MainWindow::headstageSettingsChanged(HeadstageSettings s) {
    qDebug() << "New headstage settings";
    headstageSettings = s;
}

void MainWindow::openGeneratorDialog()
{
    //opens the dialog used to control the signal generator for debugging without hardware connected
    waveformGeneratorDialog *newDialog = new waveformGeneratorDialog(sourceControl->waveGeneratorSource->getModulatorFrequency(),
                                                                     sourceControl->waveGeneratorSource->getFrequency(),
                                                                     sourceControl->waveGeneratorSource->getAmplitude(),
                                                                     sourceControl->waveGeneratorSource->getThreshold());

    newDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    newDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
    connect(newDialog->modulatorFreqSpinBox, SIGNAL(valueChanged(double)), sourceControl->waveGeneratorSource, SLOT(setModulatorFrequency(double)));
    connect(newDialog->freqSlider, SIGNAL(valueChanged(int)), sourceControl->waveGeneratorSource, SLOT(setFrequency(int)));
    connect(newDialog->ampSlider, SIGNAL(valueChanged(int)), sourceControl->waveGeneratorSource, SLOT(setAmplitude(int)));
    connect(newDialog->threshSlider, SIGNAL(valueChanged(int)), sourceControl->waveGeneratorSource, SLOT(setThreshold(int)));
    connect(this, SIGNAL(closeAllWindows()), newDialog, SLOT(close()));
    connect(this, SIGNAL(closeWaveformDialog()), newDialog, SLOT(close()));
    connect(newDialog, SIGNAL(windowClosed()), this, SLOT(enableGeneratorDialogMenu()));
    newDialog->show();

    actionOpenGeneratorDialog->setEnabled(false);
}

void MainWindow::openTrodeSettingsWindow()
{
    //opens the settings dialog for a selected nTrode
    TriggerScopeSettingsWidget* triggerSettings = new TriggerScopeSettingsWidget(0, currentTrodeSelected);

    triggerSettings->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    connect(triggerSettings, SIGNAL(updateAudioSettings()), this, SIGNAL(updateAudio()));
    connect(triggerSettings, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
    connect(triggerSettings, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
    connect(triggerSettings, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
    connect(triggerSettings, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
    connect(triggerSettings, SIGNAL(moduleDataChannelChanged(int, int)), this, SLOT(sendModuleDataChanToModules(int, int)));
    connect(trodesNet->tcpServer, SIGNAL(moduleDataStreamOn(bool)), triggerSettings, SLOT(setEnabledForStreaming(bool)));

    triggerSettings->setEnabledForStreaming(isModuleDataStreaming());
    triggerSettings->show();
}

void MainWindow::openTrodeWindow()
{
    spikeDisp->setShownNtrode(currentTrodeSelected);
    singleTriggerWindowOpen = true;
    //spikeDisp[0]->ntrodeWidgets[currentTrodeSelected]->setFocusPolicy(Qt::FocusPolicy(0));
}

void MainWindow::removeFromOpenNtrodeList(int nTrodeNum)
{
    //TODO:  if multiple nTrode windows are open, we will use the nTrodeNum input

    if (singleTriggerWindowOpen) {
        singleTriggerWindowOpen = false;
    }
}

void MainWindow::enableHeadstageDialogMenu()
{
    actionHeadstageSettings->setEnabled(true);
}

void MainWindow::enableGeneratorDialogMenu()
{
    actionOpenGeneratorDialog->setEnabled(true);
}

void MainWindow::openExportDialog()
{
    ExportDialog *newExportDialog = new ExportDialog(this);

    newExportDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    newExportDialog->setWindowModality(Qt::WindowModal);
    //this->setEnabled(false);
    connect(newExportDialog, SIGNAL(exportCancelled()), this, SLOT(cancelExport()));
    connect(this, SIGNAL(closeAllWindows()), newExportDialog, SLOT(close()));
    connect(newExportDialog, SIGNAL(startExport(bool, bool, int, int, int, int)), this, SLOT(exportData(bool, bool, int, int, int, int)));

    newExportDialog->show();
}

void MainWindow::openSoundDialog()
{
    //opens the dialog used to control the sound output.
    if (channelsConfigured) {
        soundDialog *newSoundDialog = new soundDialog(soundOut->getGain(), soundOut->getThresh());
        newSoundDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
        connect(newSoundDialog->gainSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setGain(int)));
        connect(newSoundDialog->threshSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setThresh(int)));
        connect(this, SIGNAL(closeAllWindows()), newSoundDialog, SLOT(close()));
        newSoundDialog->show();
    }
    else { //No config file loaded, so make a dummy sound controller
        soundDialog *newSoundDialog = new soundDialog(30, 30);
        newSoundDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
        connect(this, SIGNAL(closeAllWindows()), newSoundDialog, SLOT(close()));
        newSoundDialog->show();
    }
}

void MainWindow::setSourceMenuState(int state)
{
    //When the state of the source changes, the state is emitted and this function is called
    //to set the menus
    if (state == SOURCE_STATE_NOT_CONNECTED) {
        if (channelsConfigured && (sourceControl->currentSource > 0)) {
            actionConnect->setEnabled(true);
            actionHeadstageSettings->setEnabled(true);
            actionCloseConfig->setEnabled(true);
        }
        else {
            actionHeadstageSettings->setEnabled(false);
            actionConnect->setEnabled(false);
        }
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        statusbar->showMessage(tr("Not connected to device"));
        dataStreaming = false;
        actionOpenRecordDialog->setEnabled(false);
        emit sourceConnected("None");
    }
    else if (state == SOURCE_STATE_CONNECTERROR) {
        //setSource(1);
        qDebug() << "[MainWindow::setSourceMenuState] Connection to source failed.";
        //QMessageBox messageBox;
        //messageBox.critical(0, "Error", "Connection to source failed.");
        //messageBox.setFixedSize(500, 200);

        showErrorMessage("Connection to source failed.");
        setSource(SourceNone);
    }
    else if (state == SOURCE_STATE_INITIALIZED) {
        if (channelsConfigured) {
            actionConnect->setEnabled(true);
            actionHeadstageSettings->setEnabled(true);
            actionCloseConfig->setEnabled(true);

        }
        else {
            actionConnect->setEnabled(false);
        }
        actionOpenRecordDialog->setEnabled(false);
        if (playbackFileOpen) {
            actionPause->setEnabled(false);
            actionPlay->setEnabled(true);
            playButton->setDown(false);
            pauseButton->setDown(true);
            actionExport->setEnabled(true);
            actionSourceFile->setEnabled(false);
            actionPlaybackOpen->setEnabled(false);
        } else {
            actionSourceFile->setEnabled(true);
            actionPlaybackOpen->setEnabled(true);
        }
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        statusbar->showMessage(tr("Connected to device. Currently not streaming."));
        dataStreaming = false;

        actionSourceNone->setEnabled(true);
        actionSourceFake->setEnabled(true);
        //actionSourceFile->setEnabled(true);
        //actionPlaybackOpen->setEnabled(true);
        actionSourceFakeSpikes->setEnabled(true);
        actionSourceEthernet->setEnabled(true);
        actionSourceUSB->setEnabled(true);
        actionSourceRhythm->setEnabled(true);


        actionRecord->setEnabled(false);
        recordButton->setEnabled(false);

        //Emit connection signal
        //Listeners:
        //TrodesServer -> modules
        switch (sourceControl->currentSource) {
        case SourceFake:
            emit sourceConnected("Generator");
            break;

        case SourceFile:
            emit sourceConnected(QString("File: ") + playbackFile);
            break;

        case SourceEthernet:
            emit sourceConnected("Ethernet");
            break;

        case SourceUSBDAQ:
            emit sourceConnected("USB");
            break;

        case SourceRhythm:
            emit sourceConnected("Rhythm");
            break;

        case SourceFakeSpikes:
            emit sourceConnected("Spikes Generator");
            break;

        case SourceNone:
        default:
            break;
        }

        emit closeWaveformDialog();

        if (quitting) {
            closeEvent(new QCloseEvent());
            return;
        }
    }
    else if (state == SOURCE_STATE_RUNNING) {
        actionLoadConfig->setEnabled(false);
        actionCloseConfig->setEnabled(false);
        actionConnect->setEnabled(false);
        actionDisconnect->setEnabled(true);
        actionSendSettle->setEnabled(true);
        actionSourceNone->setEnabled(false);
        actionSourceFake->setEnabled(false);
        actionSourceFakeSpikes->setEnabled(false);
        actionSourceFile->setEnabled(false);
        actionPlaybackOpen->setEnabled(false);
        actionSourceEthernet->setEnabled(false);
        actionSourceUSB->setEnabled(false);
        actionSourceRhythm->setEnabled(false);
        actionHeadstageSettings->setEnabled(false);
        statusbar->showMessage(tr("Data currently streaming from device"));
        dataStreaming = true;

        streamManager->clearAllDigitalStateChanges(); //clears remembered DIO state changes
        clearAll(); // clears skipe scatter plots.

        if (sourceControl->currentSource != SourceFile) {
            actionOpenRecordDialog->setEnabled(true);
        }

        if (recordFileOpen) {
            actionRecord->setEnabled(true);
            recordButton->setEnabled(true);
            pauseButton->setEnabled(true);
            pauseButton->setDown(true);
        }
        if (playbackFileOpen) {
            actionPause->setEnabled(true);
            actionPlay->setEnabled(false);
            actionExport->setEnabled(false);
            playButton->setDown(true);
            pauseButton->setDown(false);
        }
    }
    else if (state == SOURCE_STATE_PAUSED) {
        dataStreaming = false;
    }
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Trodes"), tr(qPrintable(GlobalConfiguration::getVersionInfo())));
}

void MainWindow::aboutCurConfig()
{
    QString configInfo;
    if (strcmp(qPrintable(globalConf->trodesVersion),"0") != 0)
        configInfo = QString("Workspace created for:\n-Trodes Version %1\n-Compiled on %2 at %3 using Qt v%4\n-Git commit head: '%5'").arg(globalConf->trodesVersion).arg(globalConf->compileDate).arg(globalConf->compileTime).arg(globalConf->qtVersion).arg(globalConf->commitHeadStr);
    else
        configInfo = "No workspace information available.";

    QMessageBox::about(this, tr("About Configuration"), tr(qPrintable(configInfo)));
}

bool MainWindow::checkMultipleInstances(QString errorMessage) {
    //qDebug() << "Checking if Trodes is already open...";
    TrodesClient dummyClient;
    QStringList activeTrodesHosts = dummyClient.findLocalTrodesServers();
    if (activeTrodesHosts.length() > 0) {
        qDebug() << "Error: Multiple instances of Trodes detected [" << activeTrodesHosts.length() << "]";
        QMessageBox::critical(this, tr("Multiple Instance Error"),tr(qPrintable(errorMessage)));
        return(true);
    }

    return(false); //return false if there are multiple instances of trodes detected
}

void MainWindow::updateTime()
{
    visibleTime++;
    if (visibleTime > 5) {
        if (eventTabWasChanged) {
            eventTabWasChanged = false;
            update();
        }
    }

    if (!exportMode) {
        //QTime currentTime;
        QString currentTimeString("");
        uint32_t tmpTimeStamp = currentTimeStamp;
        int hoursPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60 * 60));
        tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * hardwareConf->sourceSamplingRate);
        int minutesPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60));
        tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * hardwareConf->sourceSamplingRate);
        int secondsPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate));
        tmpTimeStamp = tmpTimeStamp - (secondsPassed * hardwareConf->sourceSamplingRate);
        //int tenthsPassed = floor(((tmpTimeStamp*10)/hardwareConf->sourceSamplingRate));
        //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];

        if (hoursPassed < 10)
            currentTimeString.append("0");
        currentTimeString.append(QString::number(hoursPassed));
        currentTimeString.append(":");
        if (minutesPassed < 10)
            currentTimeString.append("0");
        currentTimeString.append(QString::number(minutesPassed));
        currentTimeString.append(":");
        if (secondsPassed < 10)
            currentTimeString.append("0");
        currentTimeString.append(QString::number(secondsPassed));
        //currentTimeString.append(".");
        //currentTimeString.append(QString::number(tenthsPassed));
        timeLabel->setText(currentTimeString);

        timerTick = (timerTick + 1) % 5;
        if ((recordFileOpen) && (timerTick == 0)) {
            fileLabel->setText(fileString + QString("   (%1 MB    Available: %2 MB)").arg(recordOut->getBytesWritten() / 1000000).arg(recordOut->getBytesFree() / 1000000));
        }
    }
}

void MainWindow::linkFilters()
{
    linkStreamToFilters = true;
    streamFilterLink->setChecked(true);
    streamFilterUnLink->setChecked(false);
}

void MainWindow::unLinkFilters()
{
    linkStreamToFilters = false;
    streamFilterUnLink->setChecked(true);
    streamFilterLink->setChecked(false);
}

void MainWindow::linkChanges(bool link)
{
    linkChangesBool = link;
    if (link) {
        actionLinkChanges->setChecked(true);
        actionUnLinkChanges->setChecked(false);
        linkChangesButton->setChecked(true);
    }
    else {
        actionLinkChanges->setChecked(false);
        actionUnLinkChanges->setChecked(true);
        linkChangesButton->setChecked(false);
    }
}

//link methods also separated by the true and false components
void MainWindow::linkChanges()
{
    linkChangesBool = true;
    actionLinkChanges->setChecked(true);
    actionUnLinkChanges->setChecked(false);
    linkChangesButton->setChecked(true);
}

void MainWindow::unLinkChanges()
{
    linkChangesBool = false;
    actionLinkChanges->setChecked(false);
    actionUnLinkChanges->setChecked(true);
}

void MainWindow::toggleAllFilters(bool on) {

  linkChangesBool = false;

  for (int i = 0; i < spikeConf->ntrodes.length();i++) {
    spikeConf->setFilterSwitch(i,on);

  }
  emit updateAudio();
  linkChangesBool = true;
}

void MainWindow::toggleAllRefs(bool on)
{
    linkChangesBool = false;
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setRefSwitch(i, on);
    }
    updateAudio();

    linkChangesBool = true;
}

void MainWindow::setAllMaxDisp(int newMaxDisp)
{
    linkChangesBool = false;
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setMaxDisp(i, newMaxDisp);
        /*
        for (int j = 0; j < spikeConf->ntrodes[i]->maxDisp.length(); j++) {
            spikeConf->setMaxDisp(i, j, newMaxDisp);
        }*/
    }


    linkChangesBool = true;
}

void MainWindow::setAllRefs(int nTrode, int channel)
{
    linkChangesBool = false;
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setReference(i, nTrode, channel);
    }
    updateAudio();

    linkChangesBool = true;
}

void MainWindow::setAllFilters(int low, int high)
{
    linkChangesBool = false;

    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setLowFilter(i, low);
        spikeConf->setHighFilter(i, high);
    }

    emit updateAudio();

    linkChangesBool = true;
}

void MainWindow::setAllThresh(int newThresh)
{
    linkChangesBool = false;

    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setThresh(i, newThresh);
        /*
        for (int j = 0; j < spikeConf->ntrodes[i]->thresh.length(); j++) {
            spikeConf->setThresh(i, j, newThresh);
        }*/
    }

    linkChangesBool = true;
}

void MainWindow::checkRestartModules(void)
{
    int ret = QMessageBox::warning(this, tr("Restart Modules?"),
                                   tr("Are you sure you want to quit and restart the modules?"),
                                   QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);

    if (ret == QMessageBox::Yes) {
        quitModules();
        startModules(loadedConfigFile);
    }
}

void MainWindow::quitModules(void)
{
    //Send out a quit signal to all modules
    if ((channelsConfigured) && (trodesNet->tcpServer->nConnections())) {
        TrodesMessage *trodesMessage = new TrodesMessage;
        trodesMessage->messageType = TRODESMESSAGE_QUIT;
        emit messageForModules(trodesMessage);
    }

    QThread::msleep(250); //Give the modules some time to quit
}

void MainWindow::clearAll()
{

    if (spikeDisp != NULL) {
        spikeDisp->clearAllButtonPressed();
    }
    //for (int i = 0; i < ntrodeDisplayWidgetPtrs.length(); i++) {
    //    ntrodeDisplayWidgetPtrs[i]->clearButtonPressed();
    //}
}


void MainWindow::closeEvent(QCloseEvent* event)
{
    //Quit all threads and close all windows before accepting the close event
    qDebug() << "[MainWindow::closeEvent] closing";
    if (dataStreaming || playbackFileOpen) {
        //stop streaming data
        //quitting = true;
        disconnectFromSource();
        //event->ignore();

        //return;
        QThread::msleep(250);
    }

    sourceControl->setSource(SourceNone); //closes and deletes all source threads
    QThread::msleep(250);

    //quitModules();
    if (channelsConfigured) {
        closeConfig();
    }
    soundOut->endAudio();


    emit endAudioThread();
    QThread::msleep(250);

    emit endAllThreads();
     QThread::msleep(250);

    benchmarkingControlPanel->close();

    emit closeAllWindows();
    event->accept();
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    emit closeSoundDialog();
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::moveEvent(QMoveEvent *)
{
    emit closeSoundDialog();
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}


void MainWindow::paintEvent(QPaintEvent *event)
{
    if (!eventTabWasChanged) {
        event->accept();
    }
    else {
        //QPixmap pixmap(size());
        QPainter painter;

        //painter.begin(&pixmap);
        //render(&painter);
        //painter.end();

        // Do processing on pixmap here

        painter.begin(this);
        //painter.drawPixmap(0, 0, pixmap);
        render(&painter);
        painter.end();
    }
}


void MainWindow::eventTabChanged(int newTab)
{
    if (eventTabsInitialized[newTab] == false) {
        eventTabWasChanged = true;
        visibleTime = 0;
        eventTabsInitialized[newTab] = true;
    }
}

void MainWindow::resetAllAudioButtons()
{
    //for (int i = 0; i < spikeDisp->ntrodeWidgets.length(); i++) {
    //    spikeDisp->ntrodeWidgets[i]->triggerScope->turnOffAudio();
    //}
}

QString MainWindow::calcTimeString()
{
    QString currentTimeString("");
    uint32_t tmpTimeStamp = currentTimeStamp;
    int hoursPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60 * 60));

    tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * hardwareConf->sourceSamplingRate);
    int minutesPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * hardwareConf->sourceSamplingRate);
    int secondsPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed * hardwareConf->sourceSamplingRate);
    int tenthsPassed = floor(((tmpTimeStamp * 10) / hardwareConf->sourceSamplingRate));

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));
    currentTimeString.append(".");
    currentTimeString.append(QString::number(tenthsPassed));

    return currentTimeString;
}

void MainWindow::sendModuleDataChanToModules(int nTrode, int chan)
{
    spikeConf->ntrodes[nTrode]->moduleDataChan = chan;

    QByteArray msg;
    TrodesDataStream tmpStream(&msg, QIODevice::ReadWrite);

    tmpStream << nTrode << chan;
    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_NTRODEMODULEDATACHAN, msg, this);
    qDebug() << "[MainWindow::sendModuleDataChanToModules]: sending new moduleData chan to modules";
    emit messageForModules(tm);
    //trodesNet->tcpServer->sendMessageToModules(tm);
}

void MainWindow::setModuleDataStreaming(bool streamOn)
{
    moduleDataStreaming = streamOn;
}

bool MainWindow::isModuleDataStreaming(void)
{
    return moduleDataStreaming;
}

//MARK: event
void MainWindow::broadcastEvent(TrodesEventMessage ev) {
    //qDebug() << "Broadcasting Event: " << ev;
    //trodesNet->sendEvent(currentTimeStamp,ev);
    emit sendEvent(currentTimeStamp, ev.getTime(), ev.getEventMessage());
    //trodesNet->tcpServer->sendEvent(currentTimeStamp, ev);
}

//MARK: benchmarking dialog
void MainWindow::openBenchmarkingDialog() {
    //qDebug() << "open benchmarking control pannel!";
    benchmarkingControlPanel->show();
    benchmarkingControlPanel->raise();
}
