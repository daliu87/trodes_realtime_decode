/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UI_MAIN_H
#define UI_MAIN_H

#include <sys/types.h>
#include <QtGui>
#include "spikeDisplay.h"
#include "streamDisplay.h"
#include "streamProcessorThread.h"
#include <QAudioOutput>
#include <QAudioFormat>
#include "audioThread.h"
#include "triggerThread.h"
#include "recordThread.h"
#include "trodesSocket.h"
#include "networkMessage.h"
#include "dialogs.h"
#include "globalObjects.h"
#include "sourceController.h"
#include "rfDisplay.h"
#include "benchmarkWidget.h"

QT_BEGIN_NAMESPACE


class Style_tweaks : public QProxyStyle
{
    public:

        void drawPrimitive(PrimitiveElement element, const QStyleOption *option,
                           QPainter *painter, const QWidget *widget) const
        {
            /* do not draw focus rectangles - this permits modern styling */
            if (element == QStyle::PE_FrameFocusRect)
                return;

            QProxyStyle::drawPrimitive(element, option, painter, widget);
        }
};

class MainWindow : public QMainWindow
{
  Q_OBJECT


private slots:
    void about();
    void aboutCurConfig();
    bool checkMultipleInstances(QString errorMessage);

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);
    void paintEvent(QPaintEvent *);


public:
    int                         findEndOfConfigSection(QString filename);

    QTabWidget                  *tabs;
    QTabWidget                  *eventTabs;
    //QList<NtrodeDisplayWidget*> ntrodeDisplayWidgetPtrs;
    QGridLayout                 *mainLayout;
    QGridLayout                 *headerLayout;
    QLabel                      *timeLabel;
    QLabel                      *fileLabel;
    TrodesButton                 *soundSettingsButton;
    TrodesButton                 *trodeSettingsButton;
    TrodesButton                 *commentButton;
    TrodesButton                 *spikesButton;
    TrodesButton                 *videoButton;
    TrodesButton                 *statescriptButton;
    TrodesButton                 *recordButton;
    TrodesButton                 *pauseButton;
    TrodesButton                 *playButton;
    TrodesButton                 *linkChangesButton;
    QString                     fileString;
    bool                        soundDialogOpen;
    bool                        recordFileOpen;
    QString                     recordFileName;
    bool                        dataStreaming;
    bool                        moduleDataStreaming;
    bool                        recording;
    int                         timerTick;
    bool                        eventTabWasChanged;
    QString                     calcTimeString();
    QString                     currentConfigFileName;
    QString                     loadedConfigFile;
    int64_t                     visibleTime;
    bool                        eventTabsInitialized[64];
    int                         currentTrodeSelected;
    bool                        singleTriggerWindowOpen;
    bool                        channelsConfigured;
    QTimer                      *pullTimer;
    bool                        quitting;  //True if the program is in the process of quitting
    bool                        unitTestFlag;

    int settleChannelByteInPacket;
    quint8 settleChannelBit;
    quint8 settleChannelTriggerState;
    int settleChannelDelay;


    HeadstageSettings headstageSettings;

    StreamProcessorManager      *streamManager;
    SourceController            *sourceControl;
    StreamDisplayManager        *eegDisp;
    //SpikeDisplayWidget          *spikeDisp;
    MultiNtrodeDisplayWidget    *spikeDisp;
    //SDDisplay                   *sdDisp;
    SDDisplayPanel              *sdDisp;
    AudioController             *soundOut;
    RecordThread                *recordOut;
    TrodesModuleNetwork         *trodesNet;
    QThread                     *trodesNetThread;
    BenchmarkWidget             *benchmarkingControlPanel;


    void startThreads();

    QMenu *menuFile;
    QMenu *menuConfig;
    QAction *actionAboutConfig;
    QAction *actionLoadConfig;
    QAction *actionCloseConfig;
    QAction *actionSaveConfig;
    QAction *actionReConfig;
    QAction *actionOpenRecordDialog;
    QAction *actionPlaybackOpen;
    QAction *actionCloseFile;
    QAction *actionExport;
    QAction *actionRecord;
    QAction *actionPause;
    QAction *actionPlay;
    QAction *actionQuit;


    QMenu *menuSystem;
    QMenu *sourceMenu;

    QMenu *menuSimulationSource;
    QMenu *menuSpikeGadgetsSource;

    QAction *actionSourceNone;
    QAction *actionSourceFake;
    QAction *actionSourceFakeSpikes;
    QAction *actionSourceFile;
    QAction *actionSourceUSB;
    QAction *actionSourceRhythm;
    QAction *actionSourceEthernet;
    QAction *actionSound;



    QAction *actionConnect;
    QAction *actionDisconnect;
    QAction *actionClearBuffers; //a debugging tool
    QAction *actionSendSettle;
    QAction *actionOpenGeneratorDialog;

    QAction *actionRestartModules;


    QMenu *menuHelp;
    QAction *actionAbout;
    QAction *actionAboutQT;

    QMenu   *menuDisplay;
    QMenu   *menuEEGDisplay;
    QMenu   *menuSetTLength;
    QAction *actionSetTLength0_2;
    QAction *actionSetTLength0_5;
    QAction *actionSetTLength1_0;
    QAction *actionSetTLength2_0;
    QAction *actionSetTLength5_0;
    QMenu   *streamFilterMenu;
    QAction *streamFilterLink;
    QAction *streamFilterUnLink;


    QMenu   *menuSettings;
    QMenu   *menuDebug;
    QMenu   *menuNTrode;
    QAction *actionShowCurrentTrode;
    QMenu   *menuLinkChanges;
    QAction *actionLinkChanges;
    QAction *actionUnLinkChanges;
    QAction *clearAllNTrodes;
    QMenu   *menuHeadstage;
    QAction *actionHeadstageSettings;
    QAction *actionSettleDelay;
    QAction *actionBenchmarkingSettings;


    QStatusBar *statusbar;
    QWidget *centralwidget;

    MainWindow();
    ~MainWindow();
    void retranslateUi()
    {
        setWindowTitle(QApplication::translate("Main", "Trodes", 0));
        //menuFile->setTitle(QApplication::translate("Main", "File", 0));
        menuConfig->setTitle(QApplication::translate("Main", "Workspace", 0));
        actionLoadConfig->setText(QApplication::translate("Main", "Open...", 0));
        actionCloseConfig->setText(QApplication::translate("Main", "Close", 0));
        actionSaveConfig->setText(QApplication::translate("Main", "Save workspace as...", 0));
        actionReConfig->setText(QApplication::translate("Main", "Reconfigure...", 0));
        actionOpenRecordDialog->setText(QApplication::translate("Main", "New recording...", 0));
        actionPlaybackOpen->setText(QApplication::translate("Main", "Playback...", 0));
        actionCloseFile->setText(QApplication::translate("Main", "Close file", 0));
        actionExport->setText(QApplication::translate("Main", "Export...", 0));
        actionRecord->setText(QApplication::translate("Main", "Record", 0));
        actionPause->setText(QApplication::translate("Main", "Pause", 0));
        actionPlay->setText(QApplication::translate("Main", "Play file", 0));
        menuSimulationSource->setTitle(QApplication::translate("Main", "Simulation", 0));
        menuSpikeGadgetsSource->setTitle(QApplication::translate("Main", "SpikeGadgets", 0));
        actionSourceNone->setText(QApplication::translate("Main", "None", 0));
        actionSourceUSB->setText(QApplication::translate("Main", "USB", 0));
        actionSourceRhythm->setText(QApplication::translate("Main", "Rhythm", 0));
        actionSourceFake->setText(QApplication::translate("Main", "Signal generator", 0));
        actionSourceFakeSpikes->setText(QApplication::translate("Main", "Signal generator (w/spikes)", 0));
        actionSourceFile->setText(QApplication::translate("Main", "&File", 0));
        actionSourceEthernet->setText(QApplication::translate("Main", "Ethernet", 0));
        //actionReconfigure->setText(QApplication::translate("Main", "Reconfigure...", 0));
        actionConnect->setText(QApplication::translate("Main", "Stream from source", 0));
        actionDisconnect->setText(QApplication::translate("Main", "Disconnect", 0));
        actionClearBuffers->setText(QApplication::translate("Main", "Clear buffers", 0));
        actionSendSettle->setText(QApplication::translate("Main", "Settle amplifiers", 0));

        actionOpenGeneratorDialog->setText(QApplication::translate("Main", "Generator controls...", 0));
        actionSound->setText(QApplication::translate("Main", "Audio window", 0));
        actionRestartModules->setText(QApplication::translate("Main", "Restart modules", 0));

        actionQuit->setText(QApplication::translate("Main", "Exit", 0));

        actionAbout->setText(QApplication::translate("Main", "About", 0));
        actionAboutQT->setText(QApplication::translate("Main", "About QT", 0));

        //menuSystem->setTitle(QApplication::translate("Main", "Connection", 0));
        sourceMenu->setTitle(QApplication::translate("Main", "Source", 0));
        menuHelp->setTitle(QApplication::translate("Main", "Help", 0));
        //menuDisplay->setTitle(QApplication::translate("Main", "Display", 0));
        menuEEGDisplay->setTitle(QApplication::translate("Main", "Streaming", 0));
        menuSetTLength->setTitle(QApplication::translate("Main", "Trace length", 0));
        actionSetTLength0_2->setText(QApplication::translate("Main", "0.2 Seconds", 0));
        actionSetTLength0_5->setText(QApplication::translate("Main", "0.5 Seconds", 0));
        actionSetTLength1_0->setText(QApplication::translate("Main", "1.0 Seconds", 0));
        actionSetTLength2_0->setText(QApplication::translate("Main", "2.0 Seconds", 0));
        actionSetTLength5_0->setText(QApplication::translate("Main", "5.0 Seconds", 0));
        streamFilterMenu->setTitle(QApplication::translate("Main", "Filters", 0));
        streamFilterLink->setText(QApplication::translate("Main", "Link to spike filters", 0));
        streamFilterUnLink->setText(QApplication::translate("Main", "No filters", 0));      


        menuSettings->setTitle(QApplication::translate("Main", "Settings", 0));
        menuNTrode->setTitle(QApplication::translate("Main", "nTrodes", 0));
        actionShowCurrentTrode->setText(QApplication::translate("Main", "nTrode trigger window", 0));
        menuLinkChanges->setTitle(QApplication::translate("Main", "Link changes", 0));
        actionLinkChanges->setText(QApplication::translate("Main", "Link changes across nTrodes", 0));
        actionUnLinkChanges->setText(QApplication::translate("Main", "Change settings independently", 0));
        clearAllNTrodes->setText(QApplication::translate("Main", "Clear all scatterplots", 0));
        menuHeadstage->setTitle(QApplication::translate("Main", "Headstage", 0));
        actionHeadstageSettings->setText(QApplication::translate("Main", "Settings...", 0));
        actionSettleDelay->setText(QApplication::translate("Main", "Settle delay...", 0));


    } // retranslateUi

signals:
    void configFileLoaded();
    void setAudioChannel(int hwchannel);
    void updateAudio();
    void closeAllWindows();
    void closeSoundDialog();
    void closeWaveformDialog();
    void endAllThreads();
    void endAudioThread();
    void newTraceLength(double);
    void newTimeStamp(uint32_t);
    void recordFileOpened(QString filename);
    void sourceConnected(QString source);
    void recordFileClosed();
    void recordingStarted();
    void recordingStopped();
    void messageForModules(TrodesMessage *tm);

    void sendEvent(uint32_t t, int evTime, QString eventName);

public slots:
    void audioChannelChanged(int hwchannel);
    void selectTrode(int nTrode);
    bool openPlaybackFile(const QString fileName);
    void eventTabChanged(int);
    void setSourceMenuState(int);
    void resetAllAudioButtons();
    void updateTime();
    void linkFilters();
    void unLinkFilters();
    void linkChanges(bool link);
    void linkChanges();
    void unLinkChanges();
    void openSoundDialog();
    void openGeneratorDialog();
    void openHeadstageDialog();
    void headstageSettingsChanged(HeadstageSettings s);
    void openSettleChannelDelayDialog();
    void setSettleChannelDelay(int delay);
    void openTrodeSettingsWindow();
    void openTrodeWindow();
    void enableHeadstageDialogMenu();
    void enableGeneratorDialogMenu();
    void soundButtonPressed();
    void commentButtonPressed();
    void spikesButtonPressed();
    void videoButtonPressed();
    void statescriptButtonPressed();
    void openExportDialog();
    void trodeButtonPressed();
    void setAllRefs(int nTrode, int channel);
    void setAllMaxDisp(int newMaxDisp);
    void setAllThresh(int newThresh);
    void setAllFilters(int low, int high);
    void toggleAllFilters(bool on);
    void toggleAllRefs(bool on);
    void setTLength();
    void setSource();
    void setSource(DataSource source);
    void connectToSource();
    void disconnectFromSource();
    void sendSettleCommand();
    void clearAll();
    void loadConfig();
    int loadConfig(QString);
    void closeConfig();
    void saveConfig();
    void reConfig();
    void openRecordDialog();
    void closeFile();
    void recordButtonPressed();
    void pauseButtonPressed();
    void playButtonPressed();
    void recordButtonReleased();
    void pauseButtonReleased();
    void playButtonReleased();
    void actionRecordSelected();
    void actionPauseSelected();
    void actionPlaySelected();
    void exportData(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting);
    void cancelExport();
    void removeFromOpenNtrodeList(int nTrodeNum);
    void checkRestartModules(void);
    void quitModules(void);
    void bufferOverrunHandler();
    void showErrorMessage(QString msg);
    void errorSaving();
    void restartCrashedModule(QString modName);

    void startMainNetworkMessaging();
    void startModules(QString fileName);

    void sendModuleDataChanToModules(int nTrode, int chan);

    void setModuleDataStreaming(bool);
    bool isModuleDataStreaming(void);

    void threadError(QString errorString);
    void forwardProcessOutput();

    void setSettleControlChannel(int byteInPacket, quint8 bit, quint8 triggerState); //receives signal from streamDisplay


    //MARK: event
    void broadcastEvent(TrodesEventMessage ev);

    //MARK: benchmarking dialog
    void openBenchmarkingDialog(void);

};


  QT_END_NAMESPACE

#endif // MAIN_H
