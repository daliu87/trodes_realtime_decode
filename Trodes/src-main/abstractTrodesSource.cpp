#include "abstractTrodesSource.h"
#include "globalObjects.h"
extern bool unitTestMode;

AbstractSourceRuntime::AbstractSourceRuntime(){
    quitNow = false;
    acquiring = false;
    lastTimeStamp = currentTimeStamp;
    numConsecJumps = 0;
    totalDroppedPacketEvents = 0;
    badFrameAlignment = false;


}

void AbstractSourceRuntime::checkForCorrectTimeSequence() {


    //If the time does not increase by 1 throw an error
    if ((currentTimeStamp-lastTimeStamp) != 1) {
        qDebug() << "Jump in timestamps: " << ((double)currentTimeStamp-(double)lastTimeStamp) << currentTimeStamp;
        numConsecJumps++;
        totalDroppedPacketEvents = totalDroppedPacketEvents + 1;
        if ((numConsecJumps) > 30000) {
            numConsecJumps = 0;
            qDebug() << "Signal error-- check config file";
            emit timeStampError();
        }
    } else {
        numConsecJumps = 0;
    }
    lastTimeStamp = currentTimeStamp;
}


bool AbstractSourceRuntime::checkFrameAlignment(unsigned char *packet) {
    if (*packet != 0x55) {
        //We don't have alignment!  So now we drop everything
        //and try to find it. We need two 0x55's separated by PACKET_SIZE.
        qDebug() << "Bad frame alignment in source packet";
        badFrameAlignment = true;
        tempSyncByteLocation = 0;
        findNextSyncByte(packet);
        return false;
    }

    badFrameAlignment = false;
    return true;
}


void AbstractSourceRuntime::findNextSyncByte(unsigned char *packet) {
    //Look for the next sync byte in the current packet
    for (unsigned int i = 0; i < PACKET_SIZE; i++) {
        if (*(packet+i) == 0x55) {
            tempSyncByteLocation = i;
            break;
        }
    }
}



//---------------------------------------
//AbstractTrodesSource

AbstractTrodesSource::AbstractTrodesSource () {

    startCommandValue = command_startNoECU; // start data capture without ECU

    //startCommandValue = 0x64; // start data capture with ECU
    connectErrorThrown = false;


}

void AbstractTrodesSource::setECUConnected(bool ECUconnected) {
    if (ECUconnected) {
        startCommandValue = command_startWithECU;
    } else {
        startCommandValue = command_startNoECU;
    }
}


void AbstractTrodesSource::setUpThread(AbstractSourceRuntime *rtPtr) {


    workerThread = new QThread();
    rtPtr->moveToThread(workerThread);
    rtPtr->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
    connect(this, SIGNAL(startRuntime()), this, SLOT(setThreadRunning()));
    connect(rtPtr, SIGNAL(finished()), workerThread, SLOT(quit()));
    connect(rtPtr,SIGNAL(finished()), rtPtr, SLOT(deleteLater()));
    connect(rtPtr,SIGNAL(finished()), this, SLOT(setThreadNotRunning()));
    connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
    connect(rtPtr,SIGNAL(timeStampError()),this,SIGNAL(timeStampError()));
    connect(rtPtr,SIGNAL(failure()),this,SLOT(RunTimeError()));
    workerThread->setObjectName("DataSource");
    workerThread->start();
}

void AbstractTrodesSource::setThreadRunning() {
    threadRunning = true;
}

void AbstractTrodesSource::setThreadNotRunning() {
    threadRunning = false;
}

bool AbstractTrodesSource::isThreadRunning() {
    return threadRunning;
}

quint64 AbstractTrodesSource::getTotalDroppedPacketEvents() {
    return 0;
}

void AbstractTrodesSource::RunTimeError() {

    //Here we should display a message that something has gone wrong,
    //but perhaps not stop data acquisition altogether?
    //connectErrorThrown = true;
    //StopAcquisition();
    qDebug() << "An error occured during data acquisition.";

    if (!unitTestMode) {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","An error occured during data acquisition.");
        messageBox.setFixedSize(500,200);
    }

}

void AbstractTrodesSource::StartSimulation() {

}

void AbstractTrodesSource::SendSettleCommand() {

}

void AbstractTrodesSource::SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) {

}

void AbstractTrodesSource::SendFunctionTrigger(int funcNum) {

}

void AbstractTrodesSource::SendHeadstageSettings(HeadstageSettings s) {

}

void AbstractTrodesSource::GetHeadstageSettings() {

}

void AbstractTrodesSource::SendSDCardUnlock() {

}

void AbstractTrodesSource::ConnectToSDCard() {

}

void AbstractTrodesSource::ReconfigureSDCard(int numChannels) {

}
