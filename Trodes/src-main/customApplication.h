/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CUSTOMAPPLICATION_H
#define CUSTOMAPPLICATION_H

#include <QtGui>
#include "mainWindow.h"
#include "configuration.h"
#include <QtTest/QtTest>
#include <QDir>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>

class UnitTest: public QObject
{
    Q_OBJECT

public:
    //UnitTest(MainWindow *win);
    UnitTest();

private slots:
    void workspace_loading();
    void playback();
    void hardware_connect_ethernet();
    void hardware_connect_usb();
    void hardware_stream_usb();
    void hardware_stream_ethernet();

    void init(); //called before each test
    void initTestCase(); //Test called once before all other tests (init GUI)
    void cleanupTestCase();//Test called once after all tests (close GUI)

private:
    MainWindow *window;

    QDir getTestPath();


};


class customApplication : public QApplication
{
Q_OBJECT
public:
    customApplication(int & argc, char **argv);
    virtual ~customApplication();

    bool testMode;

protected:
        bool event(QEvent *);
private:
        void loadFile(const QString &fileName);

public:
MainWindow *win;

};

#endif // CUSTOMAPPLICATION_H
