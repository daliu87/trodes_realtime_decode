#include "eventHandler.h"

//***********************************************************
//EventDialog class definitions

EventDialog::EventDialog(QWidget *parent) : QDialog(parent) {
    //header = new QLabel(tr("Header"));
    eventList = new QListWidget;
    actionList = new QListWidget;

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok |QDialogButtonBox::Cancel);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &EventDialog::sendSelectedEventAction);


    headerLayout = new QHBoxLayout;
    QLabel *eventLabel = new QLabel(tr("Visable Events"));
    QLabel *actionLabel = new QLabel(tr("Available Actions"));
    headerLayout->addWidget(eventLabel);
    headerLayout->addWidget(actionLabel);

    listLayout = new QHBoxLayout;
    listLayout->addWidget(eventList);
    listLayout->addWidget(actionList);

    verticalLayout = new QVBoxLayout;
    verticalLayout->addLayout(headerLayout);
    verticalLayout->addLayout(listLayout);
    verticalLayout->addWidget(buttonBox);
    setLayout(verticalLayout);
    setWindowTitle(tr("Event/Action Menu"));
}

void EventDialog::updateEvents(QVector<TrodesEvent> evList) {
    eventList->clear();
    for (int i = 0; i < evList.length(); i++) {
        QString event = QString("[%1-%2] [%3]").arg(evList.at(i).getParentModule()).arg(evList.at(i).getParentModuleID()).arg(evList.at(i).getEventName());
        eventList->addItem(event);
    }
}

void EventDialog::updateActions(QList<QString> actList) {
    actionList->clear();
    for (int i = 0; i < actList.length(); i++) {
        QString action = QString("[%1] [%2]").arg(i).arg(actList.at(i));
        actionList->addItem(action);
    }
}

void EventDialog::sendSelectedEventAction(void) {
    int eventIndex = eventList->currentRow();
    int actionIndex = actionList->currentRow();
    if (eventIndex > -1 && actionIndex > -1) {
        emit sig_selectedEventAction(eventIndex, actionIndex);
    }
}

void EventDialog::bringToFront(int code) {
    raise();
    activateWindow();
}

//***********************************************************
//EventConnectionDialog class definitions

EventConnectionDialog::EventConnectionDialog(QWidget *parent) : QDialog(parent) {
    eventActionMenu = new EventDialog(this);
    eventConnectionList = new QListWidget;
/**
    QPushButton *addNewConnection = new QPushButton(tr("Add"));
    addNewConnection->setDefault(true);
    QPushButton *deleteConnection = new QPushButton(tr("Delete"));
    QPushButton *cancel = new QPushButton(tr("Cancel"));
    connect(addNewConnection,SIGNAL(toggled()),this,SLOT(setEventActionMenuVisable()));
    connect(cancel,SIGNAL(toggled(bool)),this,SLOT(reject()));

    buttonBox = new QHBoxLayout;
    buttonBox->addWidget(cancel);
    buttonBox->addWidget(deleteConnection);
    buttonBox->addWidget(addNewConnection);
**/
    buttonBox = new QDialogButtonBox;

    //buttonBox->addButton();
    buttonBox->addButton("Add", QDialogButtonBox::AcceptRole);
    buttonBox->addButton("Remove", QDialogButtonBox::ActionRole);
    buttonBox->addButton("Cancel", QDialogButtonBox::RejectRole);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(setEventActionMenuVisable()));
    connect(buttonBox, SIGNAL(clicked(QAbstractButton*)), this, SLOT(buttonPushed(QAbstractButton*)));
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    connect(eventActionMenu, SIGNAL(finished(int)), this, SLOT(bringToFront(int)));

    mainLayout = new QVBoxLayout;
    mainLayout->addWidget(eventConnectionList);
    mainLayout->addWidget(buttonBox);
    //mainLayout->addLayout(buttonBox);

    setLayout(mainLayout);
    setWindowTitle(tr("Event Connections"));
}

void EventConnectionDialog::updateConnectionList(QVector<EventConnection> connections, QList<QString> actionList) {
    eventConnectionList->clear();
    //int maxRowSize = 0;
    for (int i = 0; i < connections.length(); i++) {
        QString connection = QString("[%1] [%2] -- [%3]").arg(connections.at(i).ev.getParentModule()).arg(connections.at(i).ev.getEventName()).arg(actionList.at(connections.at(i).methodInd));
        eventConnectionList->addItem(connection);
    }
    int newListWidth = eventConnectionList->sizeHintForColumn(0);
    if (newListWidth <= MAX_CONNECTION_MENU_SIZE)
        eventConnectionList->setMinimumWidth(newListWidth); //resize list to hold largest string
}

void EventConnectionDialog::keyPressEvent(QKeyEvent *event) {
    //add custom key bindings here
    switch(event->key()) {
    case (Qt::Key_Backspace):
    {
        removeConnection();
        break;
    }
    default:
        break;
    }
    QDialog::keyPressEvent(event);
}

void EventConnectionDialog::setEventActionMenuVisable() {
    eventActionMenu->show();
    eventActionMenu->bringToFront();
}

void EventConnectionDialog::removeConnection() {
    int connectionIndex = eventConnectionList->currentRow();
    //qDebug() << "cur Row: " << eventConnectionList->currentRow();
    emit sig_removeConnection(connectionIndex);
}

void EventConnectionDialog::buttonPushed(QAbstractButton *button) {
    //this can handle any button pushed from buttonBox, for now it's only used for the "Remove" button
    if (button->text().compare("Remove") == 0) {
        removeConnection();
    }
}

void EventConnectionDialog::bringToFront(int code) {
    //code is an integer that describes the end event
    raise();
    activateWindow();
}

//***********************************************************
//EventHandler class definitions

EventHandler::EventHandler(QList<QString> actionList, QWidget *parent) : QWidget(parent) {
    eventConnectionMenu = new EventConnectionDialog(this);
    updateActionList(actionList);
    connect(eventConnectionMenu->eventActionMenu, SIGNAL(sig_selectedEventAction(int,int)), this, SLOT(addConnection(int,int)));
    connect(eventConnectionMenu, SIGNAL(sig_removeConnection(int)), this, SLOT(removeConnection(int)));
    eventSysLatencyToken = 0;

}

void EventHandler::setUpConnections(TrodesModuleNetwork *moduleNet) {
    connect(moduleNet->trodesClient, SIGNAL(eventListReceived(QVector<TrodesEvent>)), this, SLOT(updateEventList(QVector<TrodesEvent>)));
    connect(moduleNet, SIGNAL(sig_eventReceived(uint32_t,int,TrodesEvent)), this, SLOT(eventReceiver(uint32_t,int,TrodesEvent)));
    connect(this, SIGNAL(sig_eventSubscribeRequest(int)), moduleNet, SLOT(sendEventSubscribe(int)));
}

void EventHandler::promptEventConnectionMenu() {
    eventConnectionMenu->show();
    eventConnectionMenu->bringToFront();
    if (eventConnectionMenu->eventActionMenu->isVisible()) //if the actionMenu is still open, bring it to the front
        eventConnectionMenu->eventActionMenu->bringToFront();
}

void EventHandler::updateEventList(QVector<TrodesEvent> evList) {
    eventList.clear();
    eventList = evList;
    //verify that all connections are still valid
    //connections.at(0).
    eventConnectionMenu->eventActionMenu->updateEvents(eventList);
    verifyConnections();
}

void EventHandler::updateActionList(QList<QString> actList) {
    actionList.clear();
    actionList = actList;
    eventConnectionMenu->eventActionMenu->updateActions(actionList);
}

void EventHandler::addConnection(int eventInd, int actionInd) {
    //qDebug() << "Sending connection between event[" << eventInd << "] and action[" << actionInd << "]";
    EventConnection connection;
    connection.ev = eventList.at(eventInd);
    connection.methodInd = actionInd;
    bool addConn = true;
    for (int i = 0; i < connections.length(); i++) {
        if ((connections.at(i).ev.getEventName().compare(connection.ev.getEventName()) == 0) &&
                (connections.at(i).ev.getParentModule().compare(connection.ev.getParentModule()) == 0) &&
                (connections.at(i).methodInd == connection.methodInd)) { //if connection already exists, don't add it
            addConn = false;
        }

    }
    if (addConn) {
        connections.append(connection);
        eventConnectionMenu->updateConnectionList(connections, actionList);
        emit sig_eventSubscribeRequest(eventInd);
    }
}

void EventHandler::removeConnection(int connectionIndex) {
    if (connectionIndex > -1 && connectionIndex < connections.length()) {
        connections.remove(connectionIndex);
        eventConnectionMenu->updateConnectionList(connections, actionList);
    }
}

void EventHandler::eventReceiver(uint32_t evTimeStamp, int evSysTimeStamp, TrodesEvent event) {
    //qDebug() << "Received Event: " << event.getEventName() << " from time " << evSysTimeStamp;
    if (benchConfig->printEventSys()) {
        int diff = QTime::currentTime().msecsSinceStartOfDay() - evSysTimeStamp;
        eventLatency.insert(diff);
        if (eventSysLatencyToken == 0)
            qDebug() << "Event received, latency[" << diff << "] -avg[" << eventLatency.average() << "] -movAvg[" << eventLatency.movingAverage() << "] --Max/Min[" << eventLatency.getMax() << "/" << eventLatency.getMin() << "]";
        eventSysLatencyToken = (eventSysLatencyToken+1)%benchConfig->getFreqEventSys();
    }
    //event.printEventInfo();
    for (int i = 0; i < connections.length(); i++) {
        if ((connections.at(i).ev.getEventName().compare(event.getEventName()) == 0) && (connections.at(i).ev.getParentModule().compare(event.getParentModule()) == 0)) {
            //qDebug() << "Event:";
            //connections.at(i).ev.printEventInfo();
            //qDebug() << "Executing method: " << actionList.at(connections.at(i).methodInd);
            emit sig_executeAction(connections.at(i).methodInd, connections.at(i).ev);
        }
    }
}

void EventHandler::verifyConnections() {
    for(int i = 0; i < connections.length(); i++) { //check all connections...
        bool verified = false;
        for(int j = 0; j < eventList.length(); j++) { //...against all events in the eventList
            if ((connections.at(i).ev.getEventName().compare(eventList.at(j).getEventName()) == 0) && (connections.at(i).ev.getParentModule().compare(eventList.at(j).getParentModule()) == 0)
                    && (connections.at(i).ev.getParentModuleID() == eventList.at(j).getParentModuleID())) { //if found, connection is verified
                verified = true;
                break;
            }
        }
        if (!verified) {
            connections.remove(i); //remove the unverified connection
            i--; // move the iterator back one since we just removed the previous entry at 'i'
        }
    }
    eventConnectionMenu->updateConnectionList(connections, actionList); //update the list
}
