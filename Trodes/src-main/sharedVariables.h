/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SHAREDVARIABLES_H
#define SHAREDVARIABLES_H

#include <QGLWidget>

/*  Moved to trodesSocketDefines.h  --MK
#define CONTINUOUS_DATA_TYPE    1
#define CONTINUOUS_DATA_STRING  "CONTINUOUS"
#define SPIKE_DATA_TYPE         2
#define SPIKE_DATA_STRING       "SPIKE"
#define DIGITALIO_DATA_TYPE     4
#define DIGITALIO_DATA_STRING   "DIGITALIO"
#define POSITION_DATA_TYPE      8
#define POSITION_DATA_STRING    "POSITION"
*/


#define EEG_TIME_POINTS 2000

typedef struct {
  GLfloat x, y;
} vertex2d;


#endif // SHAREDVARIABLES_H
