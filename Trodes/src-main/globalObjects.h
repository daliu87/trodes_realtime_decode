/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLOBALOBJECTS_H
#define GLOBALOBJECTS_H


#include "iirFilter.h"
//#include "triggerThread.h"
#include "configuration.h"
#include "usbdaqThread.h"
#include <QStatusBar>
#include "sourceController.h"


//Global access to the loaded configuration info and the network object
extern GlobalConfiguration *globalConf;
extern NTrodeTable *nTrodeTable;
extern streamConfiguration *streamConf;
extern SpikeConfiguration *spikeConf;
extern headerDisplayConfiguration *headerConf;
extern ModuleConfiguration *moduleConf;
extern NetworkConfiguration *networkConf;
extern HardwareConfiguration *hardwareConf;

/*extern int hardwareConf->NCHAN;
extern int hardwareConf->sourceSamplingRate;
extern int hardwareConf->headerSize;
extern QString globalConf->filePrefix;
extern QString globalConf->filePath; */

//Global access to the current timestamp, orginating from the source thread
extern uint32_t currentTimeStamp;

//Global access to the raw data buffer
extern eegDataBuffer rawData;
extern QList<QSemaphore *> rawDataAvailable;
extern QSemaphore rawDataAvailableForSave;
extern QMutex rawDataAvailableMutex;
extern QMutex writeMarkerMutex;
extern QAtomicInteger<quint64> rawDataWritten;

//Global access to file playback info
extern QString playbackFile; //playback source file
extern bool playbackFileOpen; //true if a playback file is currently open
extern int fileDataPos; //location in file where data stream starts
extern int playbackFileSize;
extern int playbackFileCurrentLocation;
extern int filePlaybackSpeed; //playback speed

//Global access to some link settings
extern bool linkStreamToFilters;
extern bool linkChangesBool;

//Global access to whether or not displays are to be updated
extern bool exportMode;


extern QGLFormat qglFormat;

#endif // GLOBALOBJECTS_H
