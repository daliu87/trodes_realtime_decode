#ifndef ABSTRACTTRODESSOURCE_H
#define ABSTRACTTRODESSOURCE_H

#include <QtCore>
#include <QMessageBox>
#include "stdint.h"
#include "dialogs.h"


class AbstractSourceRuntime : public QObject {
  Q_OBJECT
public:
  AbstractSourceRuntime();
  virtual ~AbstractSourceRuntime() {}
  //QAtomicInt quitNow;

  bool quitNow;
  //QAtomicInt acquiring;
  bool acquiring;
  QAtomicInt totalDroppedPacketEvents;


protected:
  void checkForCorrectTimeSequence();
  bool checkFrameAlignment(unsigned char*);
  void findNextSyncByte(unsigned char*);
  bool badFrameAlignment;
  unsigned int tempSyncByteLocation;
  unsigned int PACKET_SIZE;
  uint32_t      lastTimeStamp;


private:
  int           numConsecJumps;



public slots:
  virtual void Run(void) = 0;

signals:
  void NewData(void);
  void finished();
  void timeStampError();
  void failure();
};

class AbstractTrodesSource : public QObject {
  Q_OBJECT

public:
    AbstractTrodesSource();
    virtual     ~AbstractTrodesSource() {}
    void        setECUConnected(bool);
    bool        isThreadRunning();
    virtual quint64 getTotalDroppedPacketEvents();

    enum CommandMessageCodes {
        command_startNoECU = 0x61,
        command_stop = 0x62,
        command_startWithECU = 0x64,
        command_sdCardUnlock = 0x65,
        command_settle = 0x66,
        command_connectToSD = 0x67,
        command_configureSD = 0x68,
        command_trigger = 0x69,
        command_setSettleTriggerChannel = 0x6A,
        command_startSimulation = 0x6B,
        command_getHeadstageSettings = 0x81,
        command_setHeadstageSettings = 0x82
    };

    enum ReceivedMessageCodes {
        received_headstageSettings = 0x81
    };

protected:

  QThread       *workerThread;
  void          setUpThread(AbstractSourceRuntime* rtPtr);
  char          startCommandValue;
  bool          connectErrorThrown;
  bool          threadRunning;

protected slots:
  virtual void RunTimeError(void);
  void setThreadNotRunning();
  void setThreadRunning();


public slots:
  virtual void InitInterface(void) = 0;  // This gets called after constructor
  virtual void StartAcquisition(void) = 0;
  virtual void StartSimulation(void); //Hardware will generate fake data
  virtual void StopAcquisition(void) = 0;
  virtual void CloseInterface(void) = 0;
  virtual void SendSettleCommand(void);
  virtual void SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState);
  virtual void SendFunctionTrigger(int funcNum);
  virtual void SendHeadstageSettings(HeadstageSettings s);
  virtual void GetHeadstageSettings();
  virtual void SendSDCardUnlock(void);
  virtual void ConnectToSDCard(void);
  virtual void ReconfigureSDCard(int numChannels);



signals:
  void stateChanged(int);
  void startRuntime(void);
  void acquisitionStarted(void);
  void acquisitionStopped(void);
  void acquisitionPaused(void);
  void timeStampError();
  void SDCardStatus(bool cardConnected,int numChan, bool unlocked, bool hasData);
  void headstageSettingsReturned(HeadstageSettings s);



};


#endif // ABSTRACTTRODESSOURCE_H
