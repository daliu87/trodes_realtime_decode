/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "trodesSocket.h"
#include <QMessageBox>
#include "time.h"
#include <signal.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

bool unitTestMode;


TrodesDataStream::TrodesDataStream() :
    QDataStream()
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

TrodesDataStream::TrodesDataStream(QIODevice *d) :
    QDataStream(d)
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

TrodesDataStream::TrodesDataStream(QByteArray *a, QIODevice::OpenMode mode) :
    QDataStream(a, mode)
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

TrodesDataStream::TrodesDataStream(const QByteArray & a) :
    QDataStream(a)
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

DataTypeSpec::DataTypeSpec()
{
    dataType = 0;
}

DataTypeSpec::~DataTypeSpec()
{
};

bool DataTypeSpec::dataTypeSelected()
{
    // returns true if any data type is available
    return (bool)dataType;
}

TrodesDataStream& operator<<(TrodesDataStream& dataStream, const DataTypeSpec& dataTypeSpec)
{
    dataStream << dataTypeSpec.moduleID;
    dataStream << dataTypeSpec.hostName;
    dataStream << dataTypeSpec.hostPort;
    dataStream << dataTypeSpec.socketType;
    dataStream << dataTypeSpec.dataType;
    dataStream << dataTypeSpec.contNTrodeIndexList;
    dataStream << dataTypeSpec.spikeNTrodeIndexList;
    dataStream << dataTypeSpec.contNTrodeUDPPortList;
    dataStream << dataTypeSpec.spikeNTrodeUDPPortList;
    dataStream << dataTypeSpec.digitalIOUDPPort;
    dataStream << dataTypeSpec.analogIOUDPPort;
    dataStream << dataTypeSpec.positionUDPPort;
    dataStream << dataTypeSpec.blockContinuousUDPPort;



    return dataStream;
}

// Important: this will throw a UserException on error
TrodesDataStream& operator>>(TrodesDataStream& dataStream, DataTypeSpec& dataTypeSpec)
{
    dataStream >> dataTypeSpec.moduleID;
    dataStream >> dataTypeSpec.hostName;
    dataStream >> dataTypeSpec.hostPort;
    dataStream >> dataTypeSpec.socketType;
    dataStream >> dataTypeSpec.dataType;
    dataStream >> dataTypeSpec.contNTrodeIndexList;
    dataStream >> dataTypeSpec.spikeNTrodeIndexList;
    dataStream >> dataTypeSpec.contNTrodeUDPPortList;
    dataStream >> dataTypeSpec.spikeNTrodeUDPPortList;
    dataStream >> dataTypeSpec.digitalIOUDPPort;
    dataStream >> dataTypeSpec.analogIOUDPPort;
    dataStream >> dataTypeSpec.positionUDPPort;
    dataStream >> dataTypeSpec.blockContinuousUDPPort;


    return dataStream;
}


DigitalIOSpinBox::DigitalIOSpinBox(QWidget *parent, bool input) :
    QSpinBox(parent),
    input(input)
{
    /* set the range */
    setRange(headerConf->minDigitalPort(input), headerConf->maxDigitalPort(input));
    setValue(headerConf->minDigitalPort(input));
    connect(this, SIGNAL(valueChanged(int)), this, SLOT(updateValue(int)));
    currentIndex = 0;
}



DigitalIOSpinBox::~DigitalIOSpinBox()
{
}



void DigitalIOSpinBox::stepBy(int steps)
{
    QStringList IDList;
    QList<int> portList;

    IDList = (input == 1) ? headerConf->digInIDList : headerConf->digOutIDList;
    portList = (input == 1) ? headerConf->digInPortList : headerConf->digOutPortList;

    int newIndex = currentIndex + steps;
    // check to see that this is a valid value
    if (newIndex < 0) newIndex = 0;
    if (newIndex > portList.length()) newIndex = portList.length();

    setValue(portList[newIndex]);

}

void DigitalIOSpinBox::updateValue(int newPort) {
    // see if this is a valid port

    QStringList IDList;
    QList<int> portList;

    IDList = (input == 1) ? headerConf->digInIDList : headerConf->digOutIDList;
    portList = (input == 1) ? headerConf->digInPortList : headerConf->digOutPortList;

    if (portList.indexOf(newPort) == -1) {
        setValue(currentIndex);
    }
    else {
        currentIndex = portList.indexOf(newPort);
    }
}






NTrodeSelectDialog::NTrodeSelectDialog(const QString &title, bool displayChan, QWidget *parent) :
    QDialog(parent)
{
    // use the spike configuration informaiton to create a dialog with a list box with one line per nTrode
    this->setWindowTitle(title);


    QGridLayout *mainGrid = new QGridLayout(this);
    QLabel *nTrodeLabel = new QLabel("NTrode");
    nTrodeLabel->setAlignment(Qt::AlignCenter);
    mainGrid->addWidget(nTrodeLabel, 0, 0, 1, 1);
    nTrodeSelector = new QListWidget(this);
    mainGrid->addWidget(nTrodeSelector, 1, 0, 3, 1);

    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        if (displayChan) {
            // add the item for this ntrode and channel.  Note that the channel numbers are displayed as 1-n, not 0 - n-1
            nTrodeSelector->addItem(QString("%1  chan %2").arg(spikeConf->ntrodes[i]->nTrodeId).arg(spikeConf->ntrodes[i]->moduleDataChan + 1));
        }
        else {
            nTrodeSelector->addItem(QString("%1").arg(spikeConf->ntrodes[i]->nTrodeId));
        }
    }
    nTrodeSelector->setSelectionMode(QAbstractItemView::ExtendedSelection);

    nSelected = new QLabel("nTrodes Selected: 0");
    mainGrid->addWidget(nSelected, 4, 0, 1, 1);
    connect(nTrodeSelector, SIGNAL(itemPressed(QListWidgetItem*)), this, SLOT(updateSelected(QListWidgetItem*)));

    done = new QPushButton("Done", this);
    connect(done, SIGNAL(pressed()), this, SLOT(hide()));
    mainGrid->addWidget(done, 5, 0, 1, 1);
}

NTrodeSelectDialog::~NTrodeSelectDialog()
{
}

void NTrodeSelectDialog::updateSelected(void)
{
    int numSelected = 0;

    // go through all of the items.  This seems to be necessary when more than one item can be selected
    for (int i = 0; i < nTrodeSelector->count(); i++) {
        bool selected = nTrodeSelector->item(i)->isSelected();
        if (selected) {
            numSelected++;
        }
        emit nTrodeSelected((quint16)i, selected);
    }
    nSelected->setText(QString("nTrodes Selected: %1").arg(numSelected));
    qDebug() << numSelected << "NTrodes selected";
}

void NTrodeSelectDialog::updateSelected(QListWidgetItem *item)
{
    updateSelected();
}

void NTrodeSelectDialog::updateNTrodeList(void)
{
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        nTrodeSelector->item(i)->setText(QString("%1  chan %2").arg(spikeConf->ntrodes[i]->nTrodeId).arg(spikeConf->ntrodes[i]->moduleDataChan));
    }
}

void NTrodeSelectDialog::updateNTrodeList(int nTrode, int chan)
{
    // this can be called when the ModuleData channel is updated
    spikeConf->ntrodes[nTrode]->moduleDataChan = chan;
    qDebug() << "updating NTrode List";
    updateNTrodeList();
}

int NTrodeSelectDialog::numberSelected()
{
    return nTrodeSelector->selectedItems().length();
}

int NTrodeSelectDialog::loadFromXML(QDomNode &nTrodeSelectNode)
{
    QDomNodeList nList = nTrodeSelectNode.childNodes();

    QDomElement nt;
    int index;

    for (int i = 0; i < nList.length(); i++) {
        nt = nList.at(i).toElement();
        index = nt.attribute("nTrodeIndex").toInt();
        nTrodeSelector->item(index)->setSelected((bool)nt.attribute("selected").toInt());
    }
    updateSelected();
    return 1;
}

void NTrodeSelectDialog::saveToXML(QDomDocument &doc, QDomElement &rootNode, bool continuousData)
{
    QDomElement nTrodeConf;

    if (continuousData) {
        nTrodeConf = doc.createElement("contNTrodeSelectConfiguration");
    }
    else {
        nTrodeConf = doc.createElement("spikeNTrodeSelectConfiguration");
    }
    rootNode.appendChild(nTrodeConf);

    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        QDomElement nt = doc.createElement("nTrodeInfo");
        nt.setAttribute("nTrodeIndex", i);
        nt.setAttribute("selected", nTrodeSelector->item(i)->isSelected());
        nTrodeConf.appendChild(nt);
    }
}

TrodesMessage::TrodesMessage(QObject *parent) :
    QObject(parent)
{
}

TrodesMessage::TrodesMessage(quint8 mType, QByteArray m, QObject *parent) :
    QObject(parent)
{
    messageType = mType;
    message = m;
}

TrodesMessage::~TrodesMessage()
{
}



TrodesSocketMessageHandler::TrodesSocketMessageHandler(QObject *parent) :
    QObject(parent),
    tcpSocket(NULL),
    udpSocket(NULL),
    moduleTime(NULL),
    _hasModulename(false),
    _moduleName(""),
    socketType(TRODESSOCKETTYPE_TCPIP),
    inputSize(0),
    dataTypeSpecific(false),
    nTrodeId(0), // by default, zero (which for ID's, means no nTrode)
    decimation(1), // by default no decimation
    moduleDataStreaming(false)
{
    sourceSampRate = 30000;
    testTimer.start();
    spikeSendLatencyToken = 0;
}

bool TrodesSocketMessageHandler::isConnected()
{
    return tcpSocket->isOpen();
}

void TrodesSocketMessageHandler::setSocketType(int sType)
{
    socketType = sType;
}

int TrodesSocketMessageHandler::getSocketType(void)
{
    return socketType;
}

void TrodesSocketMessageHandler::setRemoteHost(QHostAddress rhost)
{
    remoteHost = rhost;
}

QHostAddress TrodesSocketMessageHandler::getRemoteHost(void)
{
    return remoteHost;
}

void TrodesSocketMessageHandler::setRemotePort(quint16 port)
{
    remotePort = port;
}

quint16 TrodesSocketMessageHandler::getRemotePort(void)
{
    return remotePort;
}

void TrodesSocketMessageHandler::setModuleTimePtr(const uint32_t *t)
{
    moduleTime = t;
}

bool TrodesSocketMessageHandler::isDedicatedLine()
{
    return dataTypeSpecific;
}

void TrodesSocketMessageHandler::setDedicatedLine()
{
    dataTypeSpecific = true;
}

void TrodesSocketMessageHandler::setDedicatedLine(quint8 dType)
{
    dataTypeSpecific = true;
    dataType = dType;
}

bool TrodesSocketMessageHandler::isModuleDataStreamingOn()
{
    return moduleDataStreaming;
}

void TrodesSocketMessageHandler::resetSocketForNewThread()
{
    //If the TrodesSocketMessageHandler object has been passed to a different thread
    //then writing to the associated tcpSocket creates problems because it was created
    //in a different thread.  Qt has a way of dealing with this.  First, create a new "dummy"
    //socket, then then set it's socketDesciptor to point to the actual socket.
    QTcpSocket *tmpSocket = tcpSocket;

    tcpSocket = new QTcpSocket(); //TODO: delete later
    tcpSocket->setSocketDescriptor(tmpSocket->socketDescriptor());
    tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);

    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    //connect(tcpSocket,SIGNAL(disconnected()),this,SLOT(disconnectHandler()));
}

void TrodesSocketMessageHandler::resetSocketForNewThread(TrodesSocketMessageHandler *origMessageHandler)
{
    //If the TrodesSocketMessageHandler object has been passed to a different thread
    //then writing to the associated tcpSocket creates problems because it was created
    //in a different thread.  Qt has a way of dealing with this.  First, create a new "dummy"
    //socket, then then set it's socketDesciptor to point to the actual socket.

    tcpSocket = new QTcpSocket();
    tcpSocket->setSocketDescriptor(origMessageHandler->tcpSocket->socketDescriptor());
    tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);

    QTcpSocket *tmpSocket = new QTcpSocket;
    // copy the essential variables across
    copyMessageHandlerVar(origMessageHandler);

    origMessageHandler->tcpSocket->setSocketDescriptor(tmpSocket->socketDescriptor());
    origMessageHandler->tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);

    origMessageHandler->tcpSocket->close();

    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(disconnectHandler()));

    //disconnect(origMessageHandler->tcpSocket, SIGNAL(readyRead()));
}

void TrodesSocketMessageHandler::copyMessageHandlerVar(TrodesSocketMessageHandler *origMH)
{
    dataType = origMH->getDataType();
    dataTypeSpecific = origMH->isDedicatedLine();
}

void TrodesSocketMessageHandler::setSocket(int socketDescriptor)
{
    //The socket already exists, and the descriptor has been passed
    //(For server side)
    tcpSocket = new QTcpSocket(); //TODO: delete later
    tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);
    tcpSocket->setSocketDescriptor(socketDescriptor);
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(disconnectHandler()));

    //connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(errorHandler(QAbstractSocket::SocketError)),Qt::DirectConnection);
}

void TrodesSocketMessageHandler::setSocket(QTcpSocket *tcpSocketIn)
{
    //Create a new socket (for client side)
    tcpSocket = tcpSocketIn;
    tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(disconnectHandler()));
    //tcpSocket->setParent(this); //the socket needs to have a parent in this thread

    //connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(errorHandler(QAbstractSocket::SocketError)),Qt::DirectConnection);
}

TrodesSocketMessageHandler::~TrodesSocketMessageHandler()
{
    tcpSocket->deleteLater();
}


void TrodesSocketMessageHandler::readMessage()
{
    //This function is used to parse Trodes messages

    quint8 inputType;
    bool bytesAvailable = true;

    quint16 port;
    QHostAddress rHost;
    // char messageHeader; // UNUSED
    char udpDatagram[1024];
    QByteArray msg;
    char *temp;

    do {
        //qDebug() << "[TrodesSocketMessageHandler::readMessage]" << this->getModuleID() << this->getModuleName();
        //an inputSize of 0 means that we are not in the middle of reading an unfinished message
        if (inputSize == 0) {
            if ((socketType == TRODESSOCKETTYPE_TCPIP) &&
               (tcpSocket->bytesAvailable() < ((int)sizeof(quint32) + (int)sizeof(quint8)))) { //the header has not been fully recieved
                //qDebug() << "nothing in mesage";
                return;
            }
            if ((socketType == TRODESSOCKETTYPE_UDP) &&
                (udpSocket->bytesAvailable() < ((int)sizeof(quint32) + (int)sizeof(quint8)))) { //the header has not been fully recieved
                return;
            }
            //If the header has been fully recieved, get the data type and the data size
            if (socketType == TRODESSOCKETTYPE_TCPIP) {
                tcpSocket->read((char *) &inputType,1);
                tcpSocket->read((char *) &inputSize,sizeof(uint32_t));

                port = tcpSocket->localPort();

            }
            else if (socketType == TRODESSOCKETTYPE_UDP) {
                // we need to read in the whole datagram at once to avoid losing data
                qint64 datagramSize = udpSocket->pendingDatagramSize();
                udpSocket->readDatagram(udpDatagram, datagramSize, &rHost, &port);
                // get the remote host and port for sending data out later
                setRemoteHost(rHost);
                setRemotePort(port);

                port = udpSocket->localPort();

                // parse the message
                inputType = (uint8_t) udpDatagram[0];
                memcpy(&inputSize, &udpDatagram[1], sizeof(uint32_t));
                temp = udpDatagram + 5;
                msg.setRawData(temp, inputSize);
            }


            //qDebug() << "Module" << moduleConf->myID << "port" << port << "reading Message Type" << inputType << "size" <<  inputSize;
        }

        if (socketType == TRODESSOCKETTYPE_TCPIP) {
            //We only want to read in the correct size and leave
            //the next message (if one exists).  To do this, we copy inputSize bytes into
            //a char array, then pass the pointer of the char array to
            //a QByteArray (no aditional memory allocation).  The memory will be de-allocated when all copies of
            //the QByteArray are out of scope.
              if (inputSize > 0) {
                temp = new char[inputSize];
                if (socketType == TRODESSOCKETTYPE_TCPIP) {
                    //in.readRawData(temp, inputSize);
                    if (tcpSocket->read(temp, inputSize) != inputSize) {
                        qDebug() << "read error on socket";
                    }

                }
                msg.setRawData(temp, inputSize);
            }
        }
        //Reset the inputSize variable for the next message
        inputSize = 0;

        if (!dataTypeSpecific) {
            //We only parse incoming messages if the messageHandler
            //has not been switched to a dedicated data line. TO DO: Otherwise,
            //we simply store the incoming stream in a buffer.
            switch (inputType) {
            case TRODESMESSAGE_MODULEID: {
                qint8 id;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> id;
                qDebug() << "Module ID " << id << " received.";
                emit moduleIDReceived(id);
            }
            break;

            case TRODESMESSAGE_BENCHMARKCONFIG: {
                bool recSysTime, pSpikeDetect, pSpikeSent, pSpikeReceived, pPositionStreaming, pEventSys, piniFromCmdLine, pEditedByUser;
                int pFreqSpikeDetect, pFreqSpikeSent, pFreqSpikeReceived, pFreqPositionStream, pFreqEventSys;

                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);

                msgDecode >> recSysTime >> pSpikeDetect >> pSpikeSent >> pSpikeReceived >> pPositionStreaming >> pEventSys >> pEditedByUser >> piniFromCmdLine;
                msgDecode >> pFreqSpikeDetect >> pFreqSpikeSent >> pFreqSpikeReceived >> pFreqPositionStream >> pFreqEventSys;

                benchConfig = new BenchmarkConfig(recSysTime, pSpikeDetect, pSpikeSent, pSpikeReceived, pPositionStreaming, pEventSys);
                benchConfig->setFreqSpikeDetect(pFreqSpikeDetect);
                benchConfig->setFreqSpikeSent(pFreqSpikeSent);
                benchConfig->setFreqSpikeReceived(pFreqSpikeReceived);
                benchConfig->setFreqPositionStream(pFreqPositionStream);
                benchConfig->setFreqEventSys(pFreqEventSys);

                if (pEditedByUser)
                    benchConfig->editedByUser();

                if (piniFromCmdLine)
                    benchConfig->intiatedFromCommandLine();
                //qDebug() << "Benchmarking config received";
                //qDebug() << "frequecies:  " << pFreqSpikeDetect << pFreqSpikeSent << pFreqSpikeReceived << pFreqPositionStream << pFreqEventSys;
                //qDebug() << "Frequencies r from benchConf: " << qPrintable(benchConfig->getFreqStr());
                //qDebug() << benchConfig->getBoolStr();
                break;
            }

            case TRODESMESSAGE_DATATYPEAVAILABLE: {
                DataTypeSpec *da = new DataTypeSpec();
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> *da;
                //qDebug() << "[Module " << moduleID << "] got DATATYPEAVAILABLE message from module " << da->moduleID;
                //qDebug() << "port" << da->hostPort << "cont nTrodes" << da->contNTrodeIndexList;


                emit dataAvailableReceived(da);;
            }
            break;

            case TRODESMESSAGE_SENDDATATYPEAVAILABLE: {
                qDebug() << "[TrodesSocketMessageHandler::readMessage] Got SENDDATATYPEAVILABLE message";
                emit sendDataAvailableReceived(this);
            }
            break;

            case TRODESMESSAGE_SETDATATYPE: {
                // This is used for to set the type of data that should be sent
                quint16 userData;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> dataType >> userData;
                qDebug() << "[TrodesSocketMessageHandler::readMessage] Module ID(" << this->getModuleID() <<
                            "), port(" << this->getRemotePort() << "), nTrodeID(" << this->nTrodeId <<
                            "): Setting dataType to" << dataType <<
                            "userdata:" << userData;
                emit setDataTypeReceived(this, dataType, userData);
            }
            break;

            case TRODESMESSAGE_NTRODEMODULEDATACHAN: {
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                int tmpNTrode, tmpChan;
                msgDecode >> tmpNTrode >> tmpChan;
                // update the spikeConf structure and emit a signal that we've done so

                if (spikeConf!=NULL) {
                    spikeConf->ntrodes[tmpNTrode]->moduleDataChan = tmpChan;
                    qDebug() << "[TrodesSocketMessageHandler::readMessage] messageHandler got new nTrode and ModuleData chan" << tmpNTrode << tmpChan;
                    emit moduleDataChanUpdated(tmpNTrode, tmpChan);
                }
            }
            break;

            case TRODESMESSAGE_MODULEDISCONNECTED: {
                qint8 ID;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> ID;
                // emit a message that will remove this module's DataAvailable information
                qDebug() << "got message that module disconnected, ID " << ID << "datatype" << this->dataType << "ntrodeid" << this->nTrodeId;
                emit moduleDisconnected(ID);
            }
            break;

            case TRODESMESSAGE_EVENT: {
                //The server sent out an event time
                QString stringMessage;
                QString moduleName = "";
                qint8 modID;
                int32_t t;
                int evTime;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> t >> evTime >> stringMessage >> moduleName >> modID;

                //If the module name is empty, try to look it up
                if (moduleName.isEmpty() && hasName()) {
                    moduleName = getModuleName();
                }
                TrodesEvent eventOccured(stringMessage,moduleName,getModuleID());
                emit eventNameAddReqest(eventOccured); //if event doesn't already exist, add it to the table
                emit eventOccurred(t, evTime,eventOccured); //broadcast event
            }
            break;

            case TRODESMESSAGE_EVENTLIST: {
                //The server sent out a new event list
                //qDebug() << "received event list for sending";
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                int numEvents = 0;
                QVector<TrodesEvent> evList;
                msgDecode >> numEvents;

                QString evName, evModule;
                qint8 evModID;
                //TrodesEvent newEvent;
                for (int i = 0; i < numEvents; i++) {
                    msgDecode >> evName;
                    msgDecode >> evModule;
                    msgDecode >> evModID;
                    TrodesEvent newEvent(evName,evModule,evModID);
                    evList.append(newEvent);
                    //newEvent.printEventInfo();
                }
                emit eventListReceived(evList);
            }
            break;

            case TRODESMESSAGE_REQUESTEVENTLIST : {
                //The module requested an event list
                qDebug() << "Requesting event list";
                emit eventListReqested(getModuleID());
            }
            break;

            case TRODESMESSAGE_EVENTSUBSCRIBE: {
                //A module is requesting updates for a particular event type.
                //That module wants all times when the event occurs in the future
                int eventIndex;
                qint8 subModID;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> eventIndex >> subModID;
                qDebug() << "recieved sub request - " << eventIndex << " | " << subModID;
                emit eventUpdatesSubscribe(eventIndex, subModID);
            }
            break;

            case TRODESMESSAGE_EVENTUNSUBSCRIBE: {
                //A module is unsubscribing for updates for a particular event type.
                QString eventName;
                QString moduleName;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> eventName >> moduleName;
                emit eventUpdatesUnSubscribe(eventName,moduleName, getModuleID());
            }
            break;

            case TRODESMESSAGE_SUBSCRIPTIONLIST: {
                //The server sent the list of event that the module is currently subscribed to
                QStringList list;

                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> list;
                emit eventSubscriptionListReceived(list);
            }
            break;

            case TRODESMESSAGE_REQUESTSUBSCRIPTIONLIST: {
                //A module is asking what events it is currently subscribed to
                emit eventSubscriptionListRequested(getModuleID());
            }
            break;


            case TRODESMESSAGE_EVENTNAMEREQUEST: {
                //qDebug() << "new event added";
                QString stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                //qDebug() << "  --E: '" << stringMessage << "'" << getModuleName();
                if (hasName()) {
                    TrodesEvent newEvent(stringMessage, getModuleName(), getModuleID());
                    emit eventNameAddReqest(newEvent);
                } else {
                    TrodesEvent newEvent(stringMessage, "", getModuleID());
                    emit eventNameAddReqest(newEvent);
                }
            }
            break;

            case TRODESMESSAGE_EVENTREMOVALREQUEST: {
                QString stringMessage;
                QString eventName, eventParent, parentID;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                //qDebug() << "event removal request received";
                msgDecode >> eventName;
                /*
                qDebug() << "Received:";
                qDebug() << "   " << eventName;
                qDebug() << "   " << getModuleName();
                qDebug() << "   " << getModuleID(); */
                TrodesEvent ev(eventName, getModuleName(), getModuleID());
                //ev.printEventInfo();
                if (!hasName())
                    ev.setModuleName("");

                emit eventNameRemoveRequest(ev);
            }
            break;

            case TRODESMESSAGE_EVENTSENT: {
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                int eventTimeTrodes, eventTimeSys;
                QString eventName, parentModule;
                qint8 evParentModuleID;
                msgDecode >> eventTimeTrodes;
                msgDecode >> eventTimeSys;
                msgDecode >> eventName;
                msgDecode >> parentModule;
                msgDecode >> evParentModuleID;
                //qDebug() << "received event from time: " << eventTimeSys;
                TrodesEvent receivedEvent(eventName,parentModule,evParentModuleID);
                emit eventReceived(eventTimeTrodes, eventTimeSys, receivedEvent);
                //MARK: Cur
                break;
            }


            case TRODESMESSAGE_OPENFILE: {
                QString stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                qDebug() << "[TrodesSocketMessageHandler::readMessage] TRODESMESSAGE_OPENFILE" << this->moduleID <<
                            this->getModuleName() << stringMessage;
                emit openFileEventReceived(stringMessage);
            }
            break;

            case TRODESMESSAGE_SOURCECONNECT: {
                QString stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                emit sourceConnectEventRecieved(stringMessage);
            }
            break;

            case TRODESMESSAGE_MODULENAME: {
                QString stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                _moduleName = stringMessage;
                _hasModulename = true;
                emit nameReceived(this, stringMessage);

                qDebug() << "[TrodesSocketMessageHandler::readMessage] In module " << moduleID << "got moduleName" << _moduleName;
            }
            break;

            case TRODESMESSAGE_MODULEREADY: {

                _isReady = true;
                emit moduleReadyRecieved(this);
                qDebug() << "[TrodesSocketMessageHandler::readMessage] Module" << moduleID << "ready.";
            }
            break;

            case TRODESMESSAGE_INSTANCENUM: {
                int16_t t;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> t;

                emit instanceReveivedFromServer(t);
            }
            break;

            case TRODESMESSAGE_TURNONDATASTREAM: {
                // this is a general mess to let trodes know that data streaming is about to be enabled.
                // This disables changes in the moduleData channels and filters
                if (socketType == TRODESSOCKETTYPE_UDP)
                    qDebug() << "[TrodesSocketMessageHandler::readMessage] received turn on datastream message, (UDP) port", udpSocket->localPort();

                qDebug() << "[TrodesSocketMessageHandler::readMessage]  (main) received TURNONDATASTREAM" << "moduleid" << this->moduleID <<
                            "ntrodeid" << this->nTrodeId;

                moduleDataStreaming = true;
                emit moduleDataStreamOn(true);
            }
            break;

            case TRODESMESSAGE_TURNOFFDATASTREAM: {
                qDebug() << "[TrodesSocketMessageHandler::readMessage] got turn off datastream message";
                moduleDataStreaming = false;
                emit moduleDataStreamOn(false);
            }
            break;

            case TRODESMESSAGE_CLOSEFILE: {
                emit closeFileEventReceived();
            }
            break;

            case TRODESMESSAGE_STARTAQUISITION: {
                emit startAquisitionEventReceived();
            }
            break;

            case TRODESMESSAGE_STOPAQUISITION: {
                emit stopAquisitionEventReceived();
            }
            break;

            case TRODESMESSAGE_SETTLECOMMAND: {
                emit settleCommandTriggered();
            }
            break;

            case TRODESMESSAGE_CURRENTTIMEREQUEST: {
                //Immediately answer with the current time
                if (moduleTime != NULL) {
                    sendCurrentTime(*moduleTime);
                }

                //emit timeRequestReceived(this);
            }
            break;

            case TRODESMESSAGE_CURRENTTIME: {
                uint32_t t;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> t;
                quint32 outtime = (quint32)t;
                emit currentTimeReceived(outtime);
            }
            break;

            case TRODESMESSAGE_TIMERATEREQUEST: {
                //qDebug() << "Got time rate request " << QThread::currentThreadId();;
                //Immediately answer with the current time rate

                sendTimeRate();
            }
            break;

            case TRODESMESSAGE_TIMERATE: {
                uint32_t t;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> t;
                // quint32 outtime = (quint32)t; // UNUSED
                emit timeRateReceived(t);
            }
            break;

            case TRODESMESSAGE_CURRENTSTATEREQUEST: {
                //Used to get the current open file name and save state
                emit currentStateRequested(this);
            }
            break;


            case TRODESMESSAGE_STATESCRIPTCOMMAND: {
                //This is a command for the statescipt controller.  Convert the array to a QString
                QString stringMessage(msg);
                //TrodesDataStream msgDecode(&msg,QIODevice::ReadOnly);
                //msgDecode >> stringMessage;
                //qDebug() << "got stateScript command" << stringMessage;

                emit stateScriptCommandReceived(stringMessage);
            }
            break;

            case TRODESMESSAGE_CAMERAIMAGE0: {
                QImage cameraImage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> cameraImage;
                qDebug() << "[TrodesSocketMessageHandler::readMessage] Image recieved";
                emit cameraImageRecieved(cameraImage, 0);
            }
            break;

            case TRODESMESSAGE_CAMERAIMAGE1: {
                QImage cameraImage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> cameraImage;
                emit cameraImageRecieved(cameraImage, 1);
            }
            break;

            case TRODESMESSAGE_CAMERAIMAGE2: {
                QImage cameraImage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> cameraImage;
                emit cameraImageRecieved(cameraImage, 2);
            }
            break;

            case TRODESMESSAGE_QUIT: {
                qDebug() << "[TrodesSocketMessageHandler::readMessage] Got quit command";
                emit quitCommandReceived();
            }
            break;

            default: {
                emit messageReceived(inputType, msg);
            }
            break;
            }
        }
        else {
            //Message parsing largely disabled for dedicated line, but these calls allow a connecting client to turn on/off or set parameters for the data stream
            switch (inputType) {

            case TRODESMESSAGE_TURNONDATASTREAM: {
                qDebug() << "[TrodesSocketMessageHandler::readMessage] (dedicated) received TURNONDATASTREAM" << "moduleid" << this->moduleID <<
                            "ntrodeid" << this->nTrodeId << "dataType:" << dataType;

                moduleDataStreaming = true;
            }
            break;

            case TRODESMESSAGE_TURNOFFDATASTREAM: {
                moduleDataStreaming = false;
                qDebug() << "[TrodesSocketMessageHandler::readMessage] (dedicated) got turn off datastream message, datatype: " << dataType <<
                            " NTrodeId: " << nTrodeId;
            }
            break;

            case TRODESMESSAGE_SETDECIMATION: {
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> decimation;
                qDebug() << "[TrodesSocketMessageHandler::readMessage] Module ID(" << this->getModuleID() <<
                            "), port(" << this->getRemotePort() << "), nTrodeID(" << this->nTrodeId <<
                            "): Setting decimation to" << decimation;
                break;
            }

            default:
                // add the message to a circular buffer
                //emit messageReceived(inputType, msg);
                qDebug() << "[TrodesSocketMessageHandler::readMessage] Undecoded message" << inputType << "size" << inputSize;

                break;
            }
        }
        //qDebug() << "in readmessage, bytes available = " << tcpSocket->bytesAvailable();
        bytesAvailable = ((socketType == TRODESSOCKETTYPE_TCPIP) && (tcpSocket->bytesAvailable())) ||
                         ((socketType == TRODESSOCKETTYPE_UDP) && (udpSocket->bytesAvailable()));

     } while (bytesAvailable);
}


//Lower-level function to send data
//This function will be phased out, switched to private scope for now
/*void TrodesSocketMessageHandler::sendMessage(quint8 dataType, int message)
{
    // put the message variable in a ByteArray
    QByteArray temp;
    TrodesDataStream msg(&temp, QIODevice::ReadWrite);
    msg << message;
    sendMessage(dataType, temp);
}*/

template<class T>
void TrodesSocketMessageHandler::sendMessage(quint8 messageType, T message)
{
    // put the message variable in a ByteArray
    QByteArray temp;
    TrodesDataStream msg(&temp, QIODevice::ReadWrite);
    msg << message;
    sendMessage(messageType, temp);
}


//Lower-level function to send a message.  This is used for communication with non-qt modules
void TrodesSocketMessageHandler::sendMessage(quint8 messageType, const char *message, quint32 messageSize)
{
    char data[TRODESSOCKET_MAXMESSAGESIZE], *dataptr;

    // put the entire message into a single character array
    dataptr = data;
    *dataptr = messageType;
    dataptr++;
    memcpy(dataptr, &messageSize, sizeof(uint32_t));
    dataptr += sizeof(uint32_t);
    memcpy(dataptr, message, messageSize);
    if ((socketType == TRODESSOCKETTYPE_TCPIP) &&
        (tcpSocket->state() == QAbstractSocket::ConnectedState) && tcpSocket->isValid()) {
//        tcpSocket->write((char*)&messageType, sizeof(quint8));
//        tcpSocket->write((char*)&messageSize, sizeof(uint32_t));
//        tcpSocket->write(message, (qint64)messageSize);
        qint64 size =0;
        size = tcpSocket->write(data, sizeof(quint8)+sizeof(uint32_t)+messageSize);
        //signal(SIGPIPE, SIG_IGN);
        //qDebug() << "**TCP msg sent, size: " << size << " -- bytes available: " << tcpSocket->bytesAvailable();
        tcpSocket->flush(); // this is critical if we want the write to happen immediately
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        // create a single character array with the data, as udp datagrams must be written all at once
        udpSocket->writeDatagram(data, sizeof(quint8)+sizeof(uint32_t)+messageSize, getRemoteHost(), getRemotePort());
        //qDebug() << "**UDP msg sent";
        //qDebug() << "sent data on port" << udpSocket->localPort();
    }
    else {
        qDebug() << "Socket not ready for writing";
    }
}

void TrodesSocketMessageHandler::sendMessage(quint8 dataType, QByteArray message)
{
    QByteArray temp;
    TrodesDataStream msg(&temp, QIODevice::ReadWrite); //Each message has a header
    //msg.setVersion(TrodesDataStream::Qt_4_0); // WHY DON'T WE NEED THIS HERE?
    msg << dataType; //The first part of the header is the data type
    //The next part is a 32-bit value indicating how large the rest of the message is in bytes
    msg << (quint32)message.size();
    msg.writeRawData(message.data(), message.length());
    if ((socketType == TRODESSOCKETTYPE_TCPIP) &&
       (tcpSocket->state() == QAbstractSocket::ConnectedState) && tcpSocket->isValid()) {
        tcpSocket->write(temp); //Write the header to the socket
        tcpSocket->flush(); // this is critical if we want the write to happen immediately
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        if (udpSocket->state() == QAbstractSocket::ConnectedState)
            udpSocket->write(temp);
        else
            udpSocket->writeDatagram(temp, getRemoteHost(), getRemotePort());

        udpSocket->flush();
    }
    else {
        qDebug() << "Socket not ready for writing";
    }
}

void TrodesSocketMessageHandler::sendMessage(TrodesMessage *tm)
{
    sendMessage(tm->messageType, tm->message);
}

// send just the data type without any other data
void TrodesSocketMessageHandler::sendMessage(quint8 dataType)
{
    QByteArray temp(0);
    sendMessage(dataType, temp);
}


QString TrodesSocketMessageHandler::getModuleName()
{
    return _moduleName;
}

bool TrodesSocketMessageHandler::hasName()
{
    return _hasModulename;
}

void TrodesSocketMessageHandler::sendDecimationValue(quint16 dec) {
    //sendMessage(TRODESMESSAGE_SETDECIMATION, (const char *)&dec, sizeof(quint16));
    sendMessage(TRODESMESSAGE_SETDECIMATION, dec);
}

void TrodesSocketMessageHandler::sendStartAquisition()
{
    sendMessage(TRODESMESSAGE_STARTAQUISITION);
}

void TrodesSocketMessageHandler::sendStopAquisition()
{
    sendMessage(TRODESMESSAGE_STOPAQUISITION);
}

void TrodesSocketMessageHandler::sendTimeRequest()
{
    sendMessage(TRODESMESSAGE_CURRENTTIMEREQUEST);
}

void TrodesSocketMessageHandler::sendTimeRateRequest()
{
    sendMessage(TRODESMESSAGE_TIMERATEREQUEST);
}

void TrodesSocketMessageHandler::sendCurrentStateRequest() {
    sendMessage(TRODESMESSAGE_CURRENTSTATEREQUEST);
}

//MARK
void TrodesSocketMessageHandler::sendEventListRequest() {
    sendMessage(TRODESMESSAGE_REQUESTEVENTLIST);
}

void TrodesSocketMessageHandler::setDataType(quint8 dType, qint16 userData)
{
    //This is used to set the dataType for a messageHandler that connects to the main trodes server.
    //We set the local datatype and send that information back to trodes.
    dataType = dType;
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << dataType << userData;
//    QMetaObject::invokeMethod(this, "sendMessage",Qt::QueuedConnection, Q_ARG(quint8,TRODESMESSAGE_SETDATATYPE),
//                              Q_ARG(QByteArray, out));
    // qDebug() << "In module " << this->getModuleID() << ". Set data type to " << dType;
    sendMessage(TRODESMESSAGE_SETDATATYPE, out);
}

void TrodesSocketMessageHandler::sendContinuousDataPoint(uint32_t t, int16_t dataPoint)
{
//    QByteArray out;
//    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
//    outStream.setVersion(TrodesDataStream::Qt_4_0);
//    outStream << t << dataPoint;
//    sendMessage(TRODESDATATYPE_CONTINUOUS,out);
    // we need to use a character array to avoid the byte swap that the datastream requires.
    static char data[22];

    struct timespec send_time;

#ifdef __linux__
    clock_gettime(CLOCK_MONOTONIC, &send_time);
#elif __MACH__  // OS X does not have clock_gettime, use clock_get_time
    // see https://gist.github.com/jbenet/1087739
    // and http://stackoverflow.com/questions/11680461/monotonic-clock-on-osx
  clock_serv_t cclock;
  mach_timespec_t mts;
  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &mts);
  mach_port_deallocate(mach_task_self(), cclock);
  send_time.tv_sec = mts.tv_sec;
  send_time.tv_nsec = mts.tv_nsec;
#endif

    //printf("TIMESPEC SIZE: %d\n",sizeof(struct timespec));
    memcpy(data, &t, sizeof(uint32_t));
    memcpy(data + 4, &dataPoint, sizeof(int16_t));
    memcpy(data + 4 + sizeof(int16_t), &send_time, sizeof(struct timespec));
    if (t == 0)
        qDebug() << "0 timestamp???";

    sendMessage(TRODESDATATYPE_CONTINUOUS, data, 22); // Threads better align because invokeMethod causes things to break
}

void TrodesSocketMessageHandler::sendContinuousDataBlock(uint32_t t, QVector<int16_t> dataBlock)
{

    // Note that this function assumes that the receiver knows the nTrode list
    static char data[MAX_HARDWARE_CHANNELS + sizeof(uint32_t)];
    int dataSize = dataBlock.length() * sizeof(int16_t);

    memcpy(data, &t, sizeof(uint32_t));
    memcpy(data + sizeof(uint32_t), dataBlock.constData(), dataSize);
    if (t == 0)
        qDebug() << "0 timestamp???";

    sendMessage(TRODESDATATYPE_BLOCK_CONTINUOUS, data, sizeof(uint32_t) + dataSize); // Threads better align because invokeMethod causes things to break
}

void TrodesSocketMessageHandler::sendSpikeData(uint32_t t, int nPoints, int16_t *waveform, int sysTime)
{
    // Note that this function assumes that the receiver knows the nTrode characteristics
    int dataSize;
    if (benchConfig->isRecordingSysTime()) {
        dataSize = MAX_SPIKE_POINTS + sizeof(uint32_t) + sizeof(int);
    }
    else {
        dataSize = MAX_SPIKE_POINTS + sizeof(uint32_t);
    }
    char data[dataSize];

    int waveformSize = nPoints * sizeof(int16_t);
    int curSize = 0;
    memcpy(data, &t, sizeof(uint32_t));
    curSize += sizeof(uint32_t);

    memcpy(data + sizeof(uint32_t), waveform, waveformSize);
    //qDebug() << "Size of waveform: " << sizeof(waveform) << " -- " << waveformSize;
    curSize += waveformSize;

    if (benchConfig->isRecordingSysTime()) {
        memcpy(data + curSize, &sysTime, sizeof(sysTime));
        curSize += sizeof(sysTime);
        int diff = QTime::currentTime().msecsSinceStartOfDay() - sysTime;

        if (sysTime != 0) {
            spikeSendLatency.insert(diff);

            if (spikeSendLatencyToken == 0 && benchConfig->printSpikeSent()) {
                qDebug() << "Sending Spike data, Latency[" << diff << "] -avg[" << spikeSendLatency.average() << "] -movAvg[" << spikeSendLatency.movingAverage() << "] --Max/Min[" << spikeSendLatency.getMax() << "/" << spikeSendLatency.getMin() << "]";
            }
            spikeSendLatencyToken = (spikeSendLatencyToken+1)%benchConfig->getFreqSpikeSent();
        }
    }

    if (t == 0)
        qDebug() << "0 timestamp???";

    //mark: time this is where spike data is sent to other modules
    sendMessage(TRODESDATATYPE_SPIKES, data, curSize); // Threads better align because invokeMethod causes things to break
}

void TrodesSocketMessageHandler::sendDigitalIOData(uint32_t t, int port, char input, char value)
{
    static char data[10], *dataptr;

    dataptr = data;
    memcpy(dataptr, &t, sizeof(uint32_t));
    dataptr += sizeof(uint32_t);
    memcpy(dataptr, &port, sizeof(int));
    dataptr += sizeof(int);
    *dataptr = input;
    dataptr++;
    *dataptr = value;

    sendMessage(TRODESDATATYPE_DIGITALIO, data, 10);
}

void TrodesSocketMessageHandler::readData(quint8 *dataType, uint32_t *dataSize, char *data)
{
    if (tcpSocket->read((char*)dataType, sizeof(quint8)) != sizeof(quint8)) {
        qDebug() << "readData: error reading dataType";
        return;
    }
    if (tcpSocket->read((char*)dataSize, sizeof(uint32_t)) != sizeof(uint32_t)) {
        qDebug() << "readData: error reading dataSize";
        return;
    }
    if (tcpSocket->read(data, *dataSize) != *dataSize) {
        qDebug() << "readData: error reading data, size" << dataSize;
        return;
    }
}

void TrodesSocketMessageHandler::sendAnimalPosition(QVector<dataPacket> dataPackets)
{

    //LEGACY
    uint32_t time = 0; //LEGACY
    int16_t xPos = -1; //LEGACY
    int16_t yPos = -1; //LEGACY
    uint8_t cameraNumb = 0; //LEGACY
    bool is2Ddata = false; //LEGACY
    bool isHeader = false; //LEGACY

    int packetSize = 0;
    PPacketType flag = PPT_NULL; //define end flag

    for (int i = 0; i < dataPackets.length(); i++) {
        packetSize += dataPackets.at(i).getSize();
        packetSize += sizeof(flag); //each packet also has a flag attached to it
    }
    packetSize += sizeof(flag); //account for end flag's size
    //qDebug() << " ";
    //qDebug() << "calculated sendPackageSize: " << packetSize;
    char data[packetSize];
    //static char data[26];

    int sizer = 0;

    //readAllPackets
    for (int i = 0; i < dataPackets.length(); i++) {
        dataPacket packet = dataPackets.at(i);
        //get the packet type and send it
        int flg = packet.getType();

        if (flg == PPT_2DPos) { //LEGACY
            is2Ddata = true; //LEGACY
            isHeader = false; //LEGACY
        } //LEGACY
        else if (flg == PPT_Header) { //LEGACY
            is2Ddata = false; //LEGACY
            isHeader = true;  //LEGACY
        } //LEGACY
        else { //LEGACY
            is2Ddata = false; //LEGACY
            isHeader = false;  //LEGACY
        } //LEGACY

        //qDebug() << " -for flag: " << flg;
        memcpy(data +sizer, &flg , sizeof(flg));
        sizer += sizeof(flg);
        //qDebug() << "       -writing flag: " << flg;
        //for all data entries in each packet
        for (int j = 0; j < packet.dataLength(); j++) {
            //decode packet data type
            switch(packet.getDataAt(j).getType()) {
            case (DT_int): {
                int x = packet.getDataAt(j).getObj().toInt();
                //qDebug() << "       -writing int: " << x;
                memcpy(data + sizer, &x, sizeof(x));
                sizer += sizeof(x);
                break;
            }
            case (DT_qreal): {
                qreal x = packet.getDataAt(j).getObj().toReal();
                //qDebug() << "       -writing qreal: " << x;
                memcpy(data + sizer, &x, sizeof(x));
                sizer += sizeof(x);
                break;
            }
            case (DT_uint8_t): {
                uint8_t x = packet.getDataAt(j).getObj().toInt();
                //qDebug() << "       -writing uint8: " << x;
                if (isHeader) { //LEGACY
                    cameraNumb = x;
                }
                memcpy(data + sizer, &x, sizeof(x));
                sizer += sizeof(x);
                break;
            }
            case (DT_uint32_t): {
                uint32_t x = packet.getDataAt(j).getObj().toInt();
                if (isHeader)  {//LEGACY
                    time = x; //LEGACY
                } //LEGACY
                //qDebug() << "       -writing uint32: " << x;
                memcpy(data + sizer, &x, sizeof(x));
                sizer += sizeof(x);
                break;
            }
            case (DT_int16_t): {
                int16_t x = packet.getDataAt(j).getObj().toInt();
                if (is2Ddata) { //LEGACY
                    if (xPos == -1) { //LEGACY
                        xPos = x; //LEGACY
                    } //LEGACY
                    else //LEGACY
                        yPos = x; //LEGACY
                } //LEGACY
                //qDebug() << "       -writing int16: " << x;
                memcpy(data + sizer, &x, sizeof(x));
                sizer += sizeof(x);
                break;
            }
            default: {
                int x = -1; //error code
                memcpy(data + sizer, &x, sizeof(x));
                sizer += sizeof(x);
                qDebug() << "Bad Data Type in packet " << i << " container " << j;
                break;
            }
            } //end switch decoding statement
            //memcpy(data + sizer, &x, sizeof(x));
            //sizer += sizeof(x);
        }
    }

    //qDebug() << "       -writing endFlag: " << flag;
    memcpy(data +sizer, &flag, sizeof(flag));
    sizer += sizeof(flag);
    //qDebug() << "**actual sendPackageSize: " << sizer << " = " << sizeof(data);

    //LEGACY compatability statment
    if (TRODES_VERSION <= 142) { //if current version is 1.4.2 or previous
        static char data2[9];
        memcpy(data2, &time, sizeof(uint32_t));
        memcpy(data2 + 4, &xPos, sizeof(int16_t));
        memcpy(data2 + 6, &yPos, sizeof(int16_t));
        memcpy(data2 + 8, &cameraNumb, sizeof(uint8_t));
        sendMessage(TRODESDATATYPE_POSITION, data2, 9);
    }
    else {
        sendMessage(TRODESDATATYPE_POSITION, data, sizer);
    }
}

/**
void TrodesSocketMessageHandler::sendAnimalPosition(uint32_t t, int16_t x, int16_t y, int16_t linSeg, qreal linPos, uint8_t cameraNum, bool send2DPos, bool sendLinPos)
{
    static char data[19];
    //MARK: Pos
    int sizer = 0;
    memcpy(data + sizer, &t, sizeof(t));
    sizer += sizeof(t);
    qDebug() << " ";
    qDebug() << "Sending Position Data";
    PPacketType flag = PPT_NULL;
    if (send2DPos) { //if 2D position data packet is sent
        qDebug() << "sending 2d ";
        flag = PPT_2DPos;
        memcpy(data +sizer, &flag, sizeof(flag));
        sizer += sizeof(flag);
        memcpy(data + sizer, &x, sizeof(x));
        sizer += sizeof(x);
        memcpy(data + sizer, &y, sizeof(y));
        sizer += sizeof(y);
        qDebug() << "   (" << x << "," << y << ")";
    }
    if (sendLinPos && send2DPos) { //if linearization position data packet is sent
        flag = PPT_Lin;
        memcpy(data +sizer, &flag, sizeof(flag));
        sizer += sizeof(flag);
        memcpy(data + sizer, &linSeg, sizeof(linSeg));
        sizer += sizeof(linSeg);
        memcpy(data + sizer, &linPos, sizeof(linPos));
        sizer += sizeof(linPos);
    }
    flag = PPT_NULL;
    qDebug() << " sending end flag: " << flag;
    memcpy(data +sizer, &flag, sizeof(flag));
    sizer += sizeof(flag);

    memcpy(data + sizer, &cameraNum, sizeof(cameraNum));
    sizer += sizeof(cameraNum);
    sendMessage(TRODESDATATYPE_POSITION, data, sizer);
} **/

void TrodesSocketMessageHandler::sendSettleCommand() {
    sendMessage(TRODESMESSAGE_SETTLECOMMAND);
}

void TrodesSocketMessageHandler::sendEvent(uint32_t t, TrodesEventMessage event)
{
    QString eventName = event.getEventMessage();
    int evTime = event.getTime();
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << t << evTime << eventName << QString("");
    sendMessage(TRODESMESSAGE_EVENT, out);
}

void TrodesSocketMessageHandler::sendEvent(uint32_t t, TrodesEventMessage event, QString moduleName)
{
    QString eventName = event.getEventMessage();
    int evTime = event.getTime();
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << t << evTime << eventName << moduleName;
    sendMessage(TRODESMESSAGE_EVENT, out);
}

void TrodesSocketMessageHandler::sendNewEventNameRequest(QString eventName)
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName;
    //qDebug() << "NAME: " <<getModuleName();
    sendMessage(TRODESMESSAGE_EVENTNAMEREQUEST, out);
}

void TrodesSocketMessageHandler::sendEventSubscribe(int eventIndex, qint8 subModID) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventIndex; //send the event's index location
    outStream << subModID; //send the sending module's unique ID
    //qDebug() << "requesting Sub to event " << eventIndex << " for module " << subModID;
    //outStream << eventName << moduleName;
    sendMessage(TRODESMESSAGE_EVENTSUBSCRIBE, out);
}

void TrodesSocketMessageHandler::sendEventUnSubscribe(QString eventName, QString moduleName) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName << moduleName;
    sendMessage(TRODESMESSAGE_EVENTUNSUBSCRIBE, out);
}

void TrodesSocketMessageHandler::sendEventRemoveRequest(QString eventName)
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName;
    sendMessage(TRODESMESSAGE_EVENTREMOVALREQUEST, out);
}

void TrodesSocketMessageHandler::sendEventList(QStringList eventList) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    for (int i=0; i < eventList.length(); i++) {
        outStream << eventList[i];
    }

    sendMessage(TRODESMESSAGE_EVENTLIST, out);
}

void TrodesSocketMessageHandler::requestEventList() {

    sendMessage(TRODESMESSAGE_REQUESTEVENTLIST);
}

void TrodesSocketMessageHandler::sendSubscriptionList(QStringList subsList) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    for (int i=0; i < subsList.length(); i++) {
        outStream << subsList[i];
    }

    sendMessage(TRODESMESSAGE_SUBSCRIPTIONLIST, out);
}

void TrodesSocketMessageHandler::requestSubscriptionList() {
    sendMessage(TRODESMESSAGE_REQUESTSUBSCRIPTIONLIST);
}

/*
void TrodesSocketMessageHandler::sendEventNameRemoved(QString eventName)
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName;
    sendMessage(TRODESMESSAGE_EVENTREMOVED, out);
}

void TrodesSocketMessageHandler::sendNewEventCreated(QString eventName)
{
    //Sent out to all modules to notify the existence of new event
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName;
    sendMessage(TRODESMESSAGE_NEWEVENTCREATED, out);
}*/


quint8 TrodesSocketMessageHandler::getDataType(void)
{
    return dataType;
}

void TrodesSocketMessageHandler::setModuleID(qint8 ID)
{
    moduleID = ID;
}

qint8 TrodesSocketMessageHandler::getModuleID()
{
    return moduleID;
}

void TrodesSocketMessageHandler::setNTrodeId(int trodeId)
{
    nTrodeId = trodeId;
}

void TrodesSocketMessageHandler::setNTrodeIndex(int index)
{
    nTrodeIndex = index;
}

void TrodesSocketMessageHandler::setNTrodeChan(int chan)
{
    nTrodeChan = chan;
}

void TrodesSocketMessageHandler::setDecimation(quint16 dec)
{
    decimation = dec;
}

void TrodesSocketMessageHandler::setSourceSamplingRate(int rate)
{
    sourceSampRate = rate;
}

int TrodesSocketMessageHandler::getNTrodeId(void)
{
    return nTrodeId;
}

int TrodesSocketMessageHandler::getNTrodeIndex(void)
{
    return nTrodeIndex;
}
int TrodesSocketMessageHandler::getNTrodeChan(void)
{
    return nTrodeChan;
}

quint16 TrodesSocketMessageHandler::getDecimation(void)
{
    return decimation;
}


void TrodesSocketMessageHandler::turnOnDataStreaming()
{
    qDebug() << "TrodesSocketMessageHandler: turnOnDataStreaming()" <<
                "moduleID:" << this->moduleID << "nTrodeId:" << this->nTrodeId;
    sendMessage((quint8)TRODESMESSAGE_TURNONDATASTREAM);  // the cast ensures the right sendMessage is called
}

void TrodesSocketMessageHandler::turnOffDataStreaming()
{
    sendMessage((quint8)TRODESMESSAGE_TURNOFFDATASTREAM);
}



void TrodesSocketMessageHandler::sendCurrentTime(uint32_t t)
{
    qint64 tm = testTimer.restart();

    if (tm < 5) {
        qDebug() << "Short timestamp send interval: " << tm;
    }

    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << t;
    sendMessage(TRODESMESSAGE_CURRENTTIME, out);
}

void TrodesSocketMessageHandler::sendTimeRate()
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);
    //outStream << (uint32_t)hardwareConf->sourceSamplingRate;
    outStream << sourceSampRate;
    sendMessage(TRODESMESSAGE_TIMERATE, out);
}

void TrodesSocketMessageHandler::sendOpenFile(QString fileName)
{
    qDebug() << "Sending open file" << this->moduleID;
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);

    outStream << fileName;

    sendMessage(TRODESMESSAGE_OPENFILE, out);
}

void TrodesSocketMessageHandler::sendSourceConnect(QString connectionType)
{
    qDebug() << "Sending connection type";
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << connectionType;
    sendMessage(TRODESMESSAGE_SOURCECONNECT, out);
}

//Each module can identify itself with a human-readable name
void TrodesSocketMessageHandler::sendModuleName(QString name)
{
    qDebug() << "TrodesSocketMessageHandler:" << "sendModuleName(" << name << ")";
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << name;
    sendMessage(TRODESMESSAGE_MODULENAME, out);
}

//Each module can tell the server when it is fully set up. Useful for testing.
void TrodesSocketMessageHandler::sendModuleIsReady()
{

    sendMessage(TRODESMESSAGE_MODULEREADY);
}

void TrodesSocketMessageHandler::sendModuleInstance(int16_t instanceNum) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << instanceNum;
    sendMessage(TRODESMESSAGE_INSTANCENUM, out);
}

void TrodesSocketMessageHandler::sendCloseFile()
{
    sendMessage(TRODESMESSAGE_CLOSEFILE);
}

void TrodesSocketMessageHandler:: sendStartSave()
{
    sendMessage(TRODESMESSAGE_STARTSAVE);
}

void TrodesSocketMessageHandler::sendStopSave()
{
    sendMessage(TRODESMESSAGE_STOPSAVE);
}


//void TrodesSocketMessageHandler::sendConfigRequest() {
//    sendMessage(TRODESMESSAGE_REQUESTCONFIG);
//}

void TrodesSocketMessageHandler::sendCameraImage(const QImage& newImage, int cameraNum)
{
    //Sends an image from the camera. There are up to three camera sources.
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);
    newImage.save(outStream.device(), "JPEG");
    //newImage.save(&out, "JPEG"); // writes image into ba in JPEG format
    //outStream << newImage;

    switch (cameraNum) {
    case 0:
        sendMessage(TRODESMESSAGE_CAMERAIMAGE0, out);
        break;

    case 1:
        sendMessage(TRODESMESSAGE_CAMERAIMAGE1, out);
        break;

    case 2:
        sendMessage(TRODESMESSAGE_CAMERAIMAGE2, out);
        break;
    }
}

void TrodesSocketMessageHandler::sendStateScriptEvent(QString eventString)
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);

    outStream << eventString;
    sendMessage(TRODESMESSAGE_STATESCRIPTEVENT, out);
}

void TrodesSocketMessageHandler::sendQuit()
{
    sendMessage(TRODESMESSAGE_QUIT);
}

void TrodesSocketMessageHandler::closeConnection()
{
    if (tcpSocket != NULL)
        tcpSocket->close();
    // Nothing to do here for udpSockets
    emit finished();
}

void TrodesSocketMessageHandler::disconnectHandler()
{
    //tcpSocket->deleteLater();
    moduleDataStreaming = false; // if disconnected, module streaming should be stopped
    emit socketDisconnected();

    if (moduleID != TRODES_ID) {
        // emit a signal to indicate that this was a module that disconnected */
        emit moduleDisconnected(moduleID);
    }
}


void TrodesSocketMessageHandler::errorHandler(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;

    case QAbstractSocket::ConnectionRefusedError:
        emit socketErrorHappened(tr("Connection error: the connection was refused by the peer."));
        break;

    default:
        emit socketErrorHappened(QString("Connection error. %1").arg(tcpSocket->errorString()));
    }
}



//-------------------------------------------------------------------------------------

TrodesClient::TrodesClient(QObject *parent) :
    TrodesSocketMessageHandler(parent)
{
    tcpSocket = new QTcpSocket(this);
    setSocket(tcpSocket);

    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    connect(tcpSocket, SIGNAL(connected()), this, SIGNAL(connected()));
    connect(tcpSocket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
            this, &TrodesClient::clientSocketError);


    // connect(tcpSocket,SIGNAL(disconnected()),this,SIGNAL(disconnected()));
}

TrodesClient::TrodesClient(int socketType, QObject *parent) :
    TrodesSocketMessageHandler(parent)
{
    setSocketType(socketType);
    if (socketType == TRODESSOCKETTYPE_TCPIP) {
        tcpSocket = new QTcpSocket(this);
        setSocket(tcpSocket);
        connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage())); // Is this redundant? The ready read connect is in setSocket
        connect(tcpSocket, SIGNAL(connected()), this, SIGNAL(connected()));
        connect(tcpSocket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
                this, &TrodesClient::clientSocketError);

        // connect(tcpSocket,SIGNAL(disconnected()),this,SIGNAL(disconnected()));
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        udpSocket = new QUdpSocket(this);
        connect(udpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
        connect(udpSocket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
                this, &TrodesClient::clientSocketError);

    }
}

TrodesClient::TrodesClient(int socketType, QString hostname, qint16 port, bool signalReader, QObject *parent) :
    TrodesSocketMessageHandler(parent)
{
    setSocketType(socketType);
    if (socketType == TRODESSOCKETTYPE_TCPIP) {
        tcpSocket = new QTcpSocket(this);
        setSocket(tcpSocket); // The ready read connect is already in here!!!
        if (signalReader)
            connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage())); // Is this redundant?
        else
            disconnect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
        connect(tcpSocket, &QTcpSocket::connected, this, &TrodesClient::connected);
        connect(tcpSocket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
                this, &TrodesClient::clientSocketError);
        // connect(tcpSocket,SIGNAL(disconnected()),this,SIGNAL(disconnected()));
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        udpSocket = new QUdpSocket(this);
        if (signalReader)
            connect(udpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
        connect(udpSocket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
                this, &TrodesClient::clientSocketError);
    }

    setAddress(hostname);
    setPort(port);

}

void TrodesClient::setAddress(QString addressIn)
{
    address = addressIn;
}

void TrodesClient::setPort(quint16 portIn)
{
    port = portIn;
}

void TrodesClient::connectToHost()
{
    int ntries = 0;
    if (getSocketType() == TRODESSOCKETTYPE_TCPIP) {
        tcpSocket->abort(); //reset the socket in case there is still data there
        while (ntries < 10) {
            tcpSocket->connectToHost(address, port); //connect to host
            // wait for the connection to be established.
            if (tcpSocket->waitForConnected(30000) == false) {
                qDebug() << "Unable to connect trodes client socket" << address << ":" << port << "; retrying";
                QObject().thread()->usleep(1000000);
                ntries++;
            }
            else {
                // qDebug() << "Trodes Client connectToHost via TCP successful.";
                break;
            }
        }
    }
    else if (getSocketType() == TRODESSOCKETTYPE_UDP) {
        // We would like to use QUDPSocket::connectToHost, as this would let us just use the
        //   write command rather than writedatagram. But that ends up causing some problems
        //   with trodes communication, so we have to instead load the remotePort and remoteHost
        //   variables of the TrodesMessageHandler here.
        // TODO: reorganize so that this is working again! (It should be working now...)
        // udpSocket->connectToHost(QHostAddress(address), port); // note that for UDP sockets, this just specifies the default connection, so we don't need to wait

        setRemoteHost(QHostAddress(address));
        setRemotePort(port);
    }
}

void TrodesClient::disconnectFromHost()
{
    if (getSocketType() == TRODESSOCKETTYPE_TCPIP) {
        tcpSocket->disconnectFromHost();
    }
    else if (getSocketType() == TRODESSOCKETTYPE_UDP) {
        udpSocket->disconnectFromHost();
    }
    emit disconnected();
}

QStringList TrodesClient::findLocalTrodesServers()
{
    //We use a 'settings' parameter to tell other modules that a Trodes server is listening
    //Here, we check the settings to get a list of all Trodes servers.  Some of these may not be valid,
    //so we go through and check each one.  If it is not valid, we remove it from the list.  Note that this only works
    //when things are running on a single machine.  For

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("network"));
    QStringList currentTrodesServers = settings.value(QLatin1String("currentServers")).toStringList();
    QStringList checkedTrodesServers;
    //qDebug() << currentTrodesServers;

    for (int i = 0; i < currentTrodesServers.length(); i++) {
        QString currentSelection = currentTrodesServers[i];
        if (!currentSelection.isEmpty()) {
            QStringList currentItems = currentSelection.split(" ");
            if (currentItems.length() >= 2) {
                QString tmpAddress = currentItems[0];
                quint16 tmpPort = currentItems[1].toUInt();

                QTcpSocket* tmpSocket = new QTcpSocket;
                tmpSocket->connectToHost(tmpAddress, tmpPort);
                tmpSocket->waitForConnected(50);
                if (tmpSocket->state() == QAbstractSocket::ConnectedState) {
                    checkedTrodesServers.append(currentSelection);
                }

                tmpSocket->disconnect();
                tmpSocket->deleteLater();
            }
        }
    }
    settings.setValue(QLatin1String("currentServers"), checkedTrodesServers);
    settings.endGroup();

    return checkedTrodesServers;
}



QString TrodesClient::getCurrentAddress()
{
    return address;
}

quint16 TrodesClient::getCurrentPort()
{
    return port;
}

void TrodesClient::clientSocketError(QAbstractSocket::SocketError err)
{
    switch (err) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        qDebug() << "[TrodesClient] SocketError: The host was not found. Please check the host name and port settings.";
        break;
    case QAbstractSocket::ConnectionRefusedError:
        qDebug() << "[TrodesClient] SocketError: The connection was refused by the peer. "
                    "Make sure the server is running, "
                    "and check that the host name and port "
                    "settings are correct.";
        break;
    case QAbstractSocket::SocketTimeoutError :
        break;
    default:
        if (getSocketType() == TRODESSOCKETTYPE_TCPIP)
            qDebug() << "[TrodesClient] SocketError: TCP error occured: " << tcpSocket->errorString();
        else if (getSocketType() == TRODESSOCKETTYPE_UDP)
            qDebug() << "[TrodesClient] SocketError: UDP error occured: " << udpSocket->errorString();
        else
            qDebug() << "[TrodesClient] SocketError: Unknown client socket error occured";
    }
}



//--------------------------------------------------------------------------

TrodesServer::TrodesServer(QObject *parent) :
    QTcpServer(parent),
    moduleTime(NULL)
{
    address = "";
    stateScriptMessageHandler = NULL;  // initialize this so we can check it later to see if stateScript is running
    sourceTimeRate = 30000;
    // Initialize the module counter
    moduleCounter = 0;
    state_fileOpen = false;
    state_recording = false;
    state_source = "None";
    state_fileName = "";
}

TrodesServer::~TrodesServer()
{
}

void TrodesServer::setModuleTimePtr(const uint32_t *t)
{
    //used to provide a pointer to the module's current clock
    moduleTime = t;
}

void TrodesServer::setModuleTimeRate(int rate)
{
    sourceTimeRate = rate;
}

QString TrodesServer::getCurrentAddress()
{
    return serverAddress().toString();
}

quint16 TrodesServer::getCurrentPort()
{
    return serverPort();
}

QList<QString> TrodesServer::findAvailableAddresses()
{
    QString ipAddress;

    QList<QString> outList;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // find non-localhost IPv4 addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            outList.append(ipAddress);
        }
    }

    return outList;

    // if we did not find one, use IPv4 localhost
    //if (ipAddress.isEmpty())
    //    ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
}

void TrodesServer::setAddress(QString addressIn)
{
    if (!isListening()) {
        address = addressIn;
    }
}

void TrodesServer::startServer(QString identifier, quint16 port)
{
    // set up the code for a new connection
    connect(this, SIGNAL(newConnection()), this, SLOT(newConnectionRequest()));

    //Before we start the server, we need to find which addresses are available
    if (address.isEmpty()) {
        /*
        QList<QString> addressList = findAvailableAddresses();
        if (!addressList.isEmpty()) {
            address = addressList[0];
        }
        else {*/
            //if we did not find a non-localhost address, use localhost
            address = QHostAddress(QHostAddress::LocalHost).toString();
        //}
    }

    QHostAddress currentAddress(address);
    if (!listen(currentAddress, port)) {
        emit socketErrorHappened("Could not use selected address.");
        qDebug() << "Could not start trodes server on host" << address << "port" << port;
        return;
    }
    port = serverPort();

    qDebug() << "Started trodes server on host" << address << "port" << port;


    //We use a 'settings' parameter to tell other modules that a Trodes server is listening
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("network"));
    QStringList currentTrodesServers;
    //QStringList currentTrodesServers = settings.value(QLatin1String("currentServers")).toStringList();
    QString serverLabel;
    serverLabel = QString("%1 %2 %3").arg(address).arg(port).arg(identifier);

    currentTrodesServers.append(serverLabel);
    settings.setValue(QLatin1String("currentServers"), currentTrodesServers);

    settings.endGroup();
}

void TrodesServer::startLocalServer(QString identifier, quint16 port)
{
    connect(this, SIGNAL(newConnection()), this, SLOT(newLocalConnectionRequest()));

    //Before we start the server, we need to find which addresses are available
    if (address.isEmpty()) {

        //QList<QString> addressList = findAvailableAddresses();
        //if (!addressList.isEmpty()) {
        //    address = addressList[0];
        //}
        //else {
            //if we did not find a non-localhost address, use localhost
            address = QHostAddress(QHostAddress::LocalHost).toString();
        //}
    }

    QHostAddress currentAddress(address);
    if (!listen(currentAddress, port)) {
        emit socketErrorHappened("Could not use selected address.");
        qDebug() << "Could not start trodes server on host" << address << "port" << port;
        return;
    }
    port = serverPort();

    qDebug() << "[TrodesServer] Started local server " << identifier << " on host" << address << "port" << port;
}

void TrodesServer::newConnectionRequest()
{
    if (hasPendingConnections()) {
        //A new client is connecting, so create a new socket
        QTcpSocket* socket = nextPendingConnection();

        //connect(socket, SIGNAL(disconnected()),socket, SLOT(deleteLater()));


        messageHandlers.push_back(new TrodesSocketMessageHandler());
        //socket->setParent(messageHandlers.last());
        //messageHandlers.last()->setSocket(socket->socketDescriptor());
        messageHandlers.last()->setSocket(socket);
        messageHandlers.last()->setSocketType(TRODESSOCKETTYPE_TCPIP);
        messageHandlers.last()->setModuleTimePtr(moduleTime); //the moduleTime pointer points to a 32 bit value representing the module's time
        messageHandlers.last()->setSourceSamplingRate(sourceTimeRate);

        connect(messageHandlers.last(), SIGNAL(socketDisconnected()), this, SLOT(clientDisconnectHandler()));
        connect(messageHandlers.last(), SIGNAL(socketErrorHappened(QString)), this, SIGNAL(socketErrorHappened(QString))); // should this be SIGNAL???
        connect(messageHandlers.last(), SIGNAL(setDataTypeReceived(TrodesSocketMessageHandler*, quint8, qint16)), this, SLOT(setDataTypeForConnection(TrodesSocketMessageHandler*, quint8, qint16)));

        connect(messageHandlers.last(), SIGNAL(sendAllDataAvailableReceived(TrodesSocketMessageHandler*)), this, SIGNAL(doSendAllDataAvailable(TrodesSocketMessageHandler*)));
        connect(messageHandlers.last(), SIGNAL(nameReceived(TrodesSocketMessageHandler*, QString)), this, SLOT(nameReceivedFromModule(TrodesSocketMessageHandler*, QString)));
        connect(messageHandlers.last(), SIGNAL(moduleReadyRecieved(TrodesSocketMessageHandler*)), this, SLOT(readySignalReceivedFromModule(TrodesSocketMessageHandler*)));

        connect(messageHandlers.last(), SIGNAL(stateScriptCommandReceived(QString)), this, SLOT(sendStateScript(QString)));
    }
}

void TrodesServer::newLocalConnectionRequest()
{
    if (hasPendingConnections()) {
        //A new client is connecting, so create a new socket
        QTcpSocket* socket = nextPendingConnection();
        //qDebug() << "TrodesServer got local connection request";
        //connect(socket, SIGNAL(disconnected()),socket, SLOT(deleteLater()));
        qDebug() << "[TrodesServer] newLocalConnectionRequest():" << "local port:" << socket->localPort()
                 << "peer port:" << socket->peerPort();


        messageHandlers.push_back(new TrodesSocketMessageHandler());
        //socket->setParent(messageHandlers.last());
        //messageHandlers.last()->setSocket(socket->socketDescriptor());
        messageHandlers.last()->setSocket(socket);
        messageHandlers.last()->setSocketType(TRODESSOCKETTYPE_TCPIP);

        connect(messageHandlers.last(), SIGNAL(setDataTypeReceived(TrodesSocketMessageHandler*, quint8, qint16)), this, SLOT(setDataTypeForConnection(TrodesSocketMessageHandler*, quint8, qint16)));
        connect(messageHandlers.last(), SIGNAL(socketDisconnected()), this, SLOT(clientDisconnectHandler()));
        connect(messageHandlers.last(), SIGNAL(socketErrorHappened(QString)), this, SIGNAL(socketErrorHappened(QString)));
        emit clientConnected();
    }
}

void TrodesServer::sendModuleID(TrodesSocketMessageHandler *messageHandler, qint8 ID)
{
    //qDebug() << "about to send moduleID " << ID;
    // send the moduleID to the module that just connected
    QByteArray msg;
    TrodesDataStream ds(&msg, QIODevice::WriteOnly);

    ds << ID;
    messageHandler->sendMessage(TRODESMESSAGE_MODULEID, msg);
    emit moduleIDSent();
}

void TrodesServer::sendModuleBenchConfig(TrodesSocketMessageHandler *messageHandler, qint8 moduleID) {
    QByteArray msg;
    TrodesDataStream ds(&msg, QIODevice::WriteOnly);

    ds << benchConfig->isRecordingSysTime();
    ds << benchConfig->printSpikeDetect();
    ds << benchConfig->printSpikeSent();
    ds << benchConfig->printSpikeReceived();
    ds << benchConfig->printPositionStreaming();
    ds << benchConfig->printEventSys();
    ds << benchConfig->wasEditedByUser();
    ds << benchConfig->wasInitiatedFromCommandLine();

    ds << benchConfig->getFreqSpikeDetect();
    ds << benchConfig->getFreqSpikeSent();
    ds << benchConfig->getFreqSpikeReceived();
    ds << benchConfig->getFreqPositionStream();
    ds << benchConfig->getFreqEventSys();

    //qDebug() << "Frequencies S: " << qPrintable(benchConfig->getFreqStr());
    messageHandler->sendMessage(TRODESMESSAGE_BENCHMARKCONFIG, msg);

}

void TrodesServer::sendBenchConfigToModules(void) {
    QByteArray msg;
    TrodesDataStream ds(&msg, QIODevice::WriteOnly);

    ds << benchConfig->isRecordingSysTime();
    ds << benchConfig->printSpikeDetect();
    ds << benchConfig->printSpikeSent();
    ds << benchConfig->printSpikeReceived();
    ds << benchConfig->printPositionStreaming();
    ds << benchConfig->printEventSys();
    ds << benchConfig->wasEditedByUser();
    ds << benchConfig->wasInitiatedFromCommandLine();

    ds << benchConfig->getFreqSpikeDetect();
    ds << benchConfig->getFreqSpikeSent();
    ds << benchConfig->getFreqSpikeReceived();
    ds << benchConfig->getFreqPositionStream();
    ds << benchConfig->getFreqEventSys();

    //qDebug() << "Frequencies S: " << qPrintable(benchConfig->getFreqStr());
    TrodesMessage *fullMessage = new TrodesMessage(TRODESMESSAGE_BENCHMARKCONFIG, msg, this);
    sendMessageToModules(fullMessage);
}

void TrodesServer::setNamedModuleMessageHandler(TrodesSocketMessageHandler *messageHandler, QString name)
{
    // set special messageHandler names.  Currently used only for stateScript
    if (name == "stateScript") {
        stateScriptMessageHandler = messageHandler;
    }
}

void TrodesServer::sendFileOpened(QString filename)
{
    //Route to signal
    state_fileOpen = true;
    state_fileName = filename;
    emit signal_send_openfile(filename);
}

void TrodesServer::sendSourceConnect(QString source)
{
    //Route to signal
    state_source = source;
    emit signal_source_connect(source);
}

void TrodesServer::sendFileClose()
{
    state_fileOpen = false;
    emit signal_file_close();
}

void TrodesServer::sendStartRecord()
{
    state_recording = true;
    emit signal_start_record();
}

void TrodesServer::sendStopRecord()
{
    state_recording = false;
    emit signal_stop_record();
}

void TrodesServer::sendCurrentStateToModule(TrodesSocketMessageHandler *messageHandler) {
    if (state_fileOpen) {
        qDebug() << "Trodes: sending file open command to new module";
        messageHandler->sendOpenFile(state_fileName);
        if (state_recording) {
            qDebug() << "Trodes: sending start save command to new module";
            messageHandler->sendStartAquisition();
        }
    }
}

void TrodesServer::relaySettleComand() {
    emit settleCommandTriggered();
}

void TrodesServer::moduleRequstedEventList(qint8 modID) {
    //Sends the event list to the requesting module
    QStringList newList;

    /**
    for (int i=0;i < eventNames.length(); i++) {
        //The event name and the module name for that event are interleaved
        //newList << eventNames[i];
        //newList << eventOwners[i];
        QString evName = eventNames[i];
        QString evOwner = eventOwners[i];
        QString event = QString("[%1] [%2]").arg(evOwner).arg(evName);
        newList << event;
    }**/

    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    int numEvents = eventList.length();
    msg << numEvents;
    for (int i = 0; i < numEvents; i++) {
        msg << eventList.at(i).getEventName();
        msg << eventList.at(i).getParentModule();
        msg << eventList.at(i).getParentModuleID();
    }

    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_EVENTLIST, m, this);
    sendMessageToModule(modID,tm);
}

void TrodesServer::moduleRequestedSubscriptionList(qint8 modID) {
    //Sends the event subscription list to the requesting module
    QStringList newList;

    for (int i=0;i < eventNames.length(); i++) {
        for (int j = 0; j< eventListenerIDs[i].length(); j++) {
            if (eventListenerIDs[i].at(j) == modID) {
                //The event name and the module name for that event are interleaved
                //newList << eventNames[i];
                //newList << eventOwners[i];
                QString evName = eventNames[i];
                QString evOwner = eventOwners[i];
                QString event = QString("[%1] [%2]").arg(evOwner).arg(evName);
                newList << event;

                break;
            }
        }
    }

    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    msg << newList;

    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_SUBSCRIPTIONLIST, m, this);
    sendMessageToModule(modID,tm);
}

void TrodesServer::sendEventListToModules() {
    //Sends the updated event list to all modules
    QStringList newList;

    /**
    for (int i=0;i < eventNames.length(); i++) {
        //The event name and the module name for that event are interleaved
        QString evName = eventNames[i];
        QString evOwner = eventOwners[i];

        QString event = QString("[%1] [%2]").arg(evOwner).arg(evName);

        newList << event;
    } **/

    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    //qDebug() << "Sending new event list to modules";
    //msg << newList;
    int numEvents = eventList.length();
    msg << numEvents;
    for (int i = 0; i < numEvents; i++) {
        msg << eventList.at(i).getEventName();
        msg << eventList.at(i).getParentModule();
        msg << eventList.at(i).getParentModuleID();
    }



    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_EVENTLIST, m, this);
    sendMessageToModules(tm);
}

void TrodesServer::removeEventTypeFromList(TrodesEvent event)
{
    //A module has requested removal of an event
    //If the event exists in the list, remove it
    int index = -1;

    bool found = false;
    //qDebug() << "--E: '" << event.getEventName() << "' '" << event.getParentModule() << "' '" << event.getParentModuleID() << "'";
    for (int i=0;i < eventList.length(); i++) {
        if ((eventList.at(i).getEventName().compare(event.getEventName()) == 0) &&
                (eventList.at(i).getParentModule().compare(event.getParentModule())==0) &&
                (eventList.at(i).getParentModuleID() == event.getParentModuleID())) {
            index = i;
        }
    }
    if (index > -1) {
        eventNames.removeAt(index);
        eventOwners.removeAt(index);
        eventModIDs.removeAt(index);
        eventList.remove(index);
        eventListenerIDs.removeAt(index);
        sendEventListToModules();
    }
}

void TrodesServer::addEventTypeToList(TrodesEvent event)
{
    //A module has requested
    //If the event name does not already exist, add it to the list
    bool found = false;
    //qDebug() << "--E: '" << event.getEventName() << "' '" << event.getParentModule() << "' '" << event.getParentModuleID() << "'";
    for (int i=0;i < eventList.length(); i++) {
        if ((eventList.at(i).getEventName().compare(event.getEventName()) == 0) && (eventList.at(i).getParentModule().compare(event.getParentModule())==0)) {
            found = true;
        }
    }
    if (!found) {
        //qDebug() << "--E: '" << event.getEventName() << "' '" << event.getParentModule() << "' '" << event.getParentModuleID() << "'";
        eventNames << event.getEventName();
        eventOwners << event.getParentModule();
        eventModIDs.push_back(event.getParentModuleID());
        eventList.append(event);
        //QList
        QList<qint8> newListenerList;
        eventListenerIDs.append(newListenerList); //add a new list of listeners for each event
        sendEventListToModules();
    }
}
/**
void TrodesServer::addEventTypeToList(QString eventName, QString moduleName, qint8 modID)
{
    //A module has requested
    //If the event name does not already exist, add it to the list
    bool found = false;
    for (int i=0;i < eventNames.length(); i++) {
        if ((eventNames[i].compare(eventName) == 0) && (eventOwners[i].compare(moduleName)==0)) {
            found = true;
        }
    }
    if (!found) {
        qDebug() << "--E: '" << eventName << "' '" << moduleName << "' '" << modID << "'";
        eventNames << eventName;
        eventOwners << moduleName;
        eventModIDs.push_back(modID);
        sendEventListToModules();
    }
}**/

void TrodesServer::eventOccurred(uint32_t t, int evTime, TrodesEvent event)
{
    //NOTE: change Event so it either carries a small int index of what it's location in the eventList or event
    //occured is passed the index as an argument
    //qDebug() << "Event occured at " << t << ":";
    //event.printEventInfo();
    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);
    msg << t;
    msg << evTime;
    msg << event.getEventName();
    msg << event.getParentModule();
    msg << event.getParentModuleID();
    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_EVENTSENT, m, this);
    int eventInd = -1;
    for(int i = 0; i < eventList.length(); i++) {
        if ((eventList.at(i).getEventName().compare(event.getEventName()) == 0) && (eventList.at(i).getParentModule().compare(event.getParentModule())==0)) {
            eventInd = i;
            break;
        }
    }
    if (eventInd > -1) {
        for (int i = 0; i < eventListenerIDs[eventInd].length(); i++) {
            bool found = false;
            for (int j = 0; j < moduleNames.length(); j++) {
                //qDebug() << "   [" << moduleNames.at(j).moduleID << "]";
                if (moduleNames.at(j).moduleID == eventListenerIDs[eventInd].at(i)) {
                    found = true;
                }
            }
            if (found)
                sendMessageToModule(eventListenerIDs[eventInd].at(i),tm);
            else
                qDebug() << "ERROR: sending to module that does not exist (TrodesServer::eventOccurred)";

        }
    }

    //MARK: CUR
    /**
    if (index > -1) {
        QByteArray m;
        TrodesDataStream msg(&m, QIODevice::ReadWrite);


        msg << t << eventName << eventOwners[index];

        TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_EVENT, m, this);

        //Go through the list of listeners for this event, and send out updates
        for (int j=0; j<eventListenerIDs[index].length();j++) {
            sendMessageToModule(eventListenerIDs[index].at(j),tm);
        }
    } **/
}

void TrodesServer::removeModuleFromEventSys(qint8 moduleID) {

    /**
    qDebug() << "removing module[" << moduleID << "]";
    qDebug() << "******BEFORE******";
    qDebug() << "***event List before***";
    for (int i = 0; i < eventList.length(); i++) {
        qDebug() << "  --'" <<eventList.at(i).getEventName() << " [" << eventList.at(i).getParentModuleID() << "]";
    }
    qDebug() << "***event Listeners before***";
    for (int i = 0; i < eventListenerIDs.length(); i++) {
        QString header = QString("Event[%1]:").arg(i);
        QString body = "";
        for (int j = 0; j < eventListenerIDs[i].length(); j++) {
            body = QString("%1 %2").arg(body).arg(eventListenerIDs[i].at(j));
        }
        qDebug() << QString("%1%2").arg(header).arg(body);
    }**/

    //find all events registered to the specified moduleID and remove them
    for (int i = 0; i < eventList.length(); i++) {
        qint8 curModID = eventList.at(i).getParentModuleID();
        if (curModID == moduleID) {
            TrodesEvent ev(eventList.at(i).getEventName(),eventList.at(i).getParentModule(),eventList.at(i).getParentModuleID());
            removeEventTypeFromList(ev);
            i--; //tick iterater back one to account for the deletion
        }
        else if (curModID > moduleID) { //if a module was removed/disconnected, we must iterate the IDs of all modIDs > removedModID
            TrodesEvent ev = eventList.at(i);
            ev.setModuleID(curModID-1);
            eventList.replace(i,ev);
        }
    }
    //Remove the specified moduleID from the list of event listeners (essentially, unsubscribe it from all events)
    for (int i = 0; i < eventListenerIDs.length(); i++) {
        for (int j = 0; j < eventListenerIDs[i].length(); j++) {
            qint8 curModID = eventListenerIDs[i].at(j);
            if (curModID == moduleID) {
                eventListenerIDs[i].removeAt(j);
                j--;
            }
            else if (curModID > moduleID) {
                eventListenerIDs[i].replace(j,(curModID-1));
            }
        }
    }
    /**
    qDebug() << "******AFTER******";
    qDebug() << "***event List after***";
    for (int i = 0; i < eventList.length(); i++) {
        qDebug() << "  --'" <<eventList.at(i).getEventName() << " [" << eventList.at(i).getParentModuleID() << "]";
    }
    qDebug() << "***event Listeners after***";
    for (int i = 0; i < eventListenerIDs.length(); i++) {
        QString header = QString("Event[%1]:").arg(i);
        QString body = "";
        for (int j = 0; j < eventListenerIDs[i].length(); j++) {
            body = QString("%1 %2").arg(body).arg(eventListenerIDs[i].at(j));
        }
        qDebug() << QString("%1%2").arg(header).arg(body);
    }**/
}

void TrodesServer::addListenerToEvent(int eventIndex, qint8 listenerModuleID) {
    qDebug() << "Adding listener to event " << eventIndex << " --listener: " << listenerModuleID;
    bool subscribe = true;
    /**
    qDebug() << "***event Listeners before***";
    for (int i = 0; i < eventListenerIDs.length(); i++) {
        QString header = QString("Event[%1]:").arg(i);
        QString body = "";
        for (int j = 0; j < eventListenerIDs[i].length(); j++) {
            body = QString("%1 %2").arg(body).arg(eventListenerIDs[i].at(j));
        }
        qDebug() << QString("%1%2").arg(header).arg(body);
    } **/
    if (eventIndex > -1 && eventIndex < eventList.length()) {//if a vaild event was subscribed to
        for (int i = 0; i < eventListenerIDs[eventIndex].length();i++) {
            if (eventListenerIDs[eventIndex].at(i) == listenerModuleID) { //if the listenerModule is already subscribed to the event
                subscribe = false;
            }
        }
        if (subscribe) {
            eventListenerIDs[eventIndex].push_back(listenerModuleID);
        }
    }
    /**
    qDebug() << "***event Listeners after***";
    for (int i = 0; i < eventListenerIDs.length(); i++) {
        QString header = QString("Event[%1]:").arg(i);
        QString body = "";
        for (int j = 0; j < eventListenerIDs[i].length(); j++) {
            body = QString("%1 %2").arg(body).arg(eventListenerIDs[i].at(j));
        }
        qDebug() << QString("%1%2").arg(header).arg(body);
    }
    qDebug() << " "; **/
}

void TrodesServer::removeListenerFromEvent(QString eventName, QString moduleNameOfEvent, qint8 listenerModuleID) {
    int index = -1;
    for (int i=0;i < eventNames.length(); i++) {
        if ((eventNames[i].compare(eventName) == 0) && (eventOwners[i].compare(moduleNameOfEvent)==0)) {
            index = i;
        }
    }

    if (index > -1) {
        int index2 = -1;
        for (int j=0; j<eventListenerIDs[index].length();j++) {
            if (eventListenerIDs[index].at(j) == listenerModuleID) {
                index2 = j;
                break;
            }
        }
        if (index2 > -1) {
            //Found the module ID to be removed
            eventListenerIDs[index].removeAt(index2);
        }
    }
}

void TrodesServer::addEventTypeToList(QString eventName)
{
    TrodesEvent newEvent(eventName,"trodes",TRODES_ID);
    //Called from Trodes
    //addEventTypeToList(eventName,"trodes",0);
    addEventTypeToList(newEvent);
}

void TrodesServer::removeEventTypeFromList(QString eventName)
{
    //Called from Trodes
    TrodesEvent ev(eventName,"trodes",TRODES_ID);
    removeEventTypeFromList(ev);
}

void TrodesServer::eventOccurred(uint32_t t, int evTime, QString eventName)
{
    //Called from Trodes
    TrodesEvent newEvent(eventName,"trodes",TRODES_ID);
    addEventTypeToList(newEvent);
    eventOccurred(t,evTime,newEvent);
}

void TrodesServer::setDataTypeForConnection(TrodesSocketMessageHandler *messageHandler, quint8 dataType, qint16 userData)
{
    qDebug() << "[TrodesSocketMessageHandler::setDataTypeForConnection] Server on" << serverAddress() << "port" << serverPort() << "received msg. DataType = " << dataType;
    messageHandler->setModuleID(TRODES_ID);  // this will be reset below if this a messaging socket for a module
    if (dataType == TRODESDATATYPE_MESSAGING) {
        connect(messageHandler, SIGNAL(currentStateRequested(TrodesSocketMessageHandler*)),this, SLOT(sendCurrentStateToModule(TrodesSocketMessageHandler*)));

        // set up the connection to handle the distribution of the datatypes that this module provides
        connect(messageHandler, SIGNAL(dataAvailableReceived(DataTypeSpec*)), this, SIGNAL(doAddDataAvailable(DataTypeSpec*)));
        connect(messageHandler, SIGNAL(dataAvailableReceived(DataTypeSpec*)), this, SLOT(sendDataAvailableToModules(DataTypeSpec*)));

        // set up the connection to send this module the full set of DataAvailable information when it requests it.
        connect(messageHandler, SIGNAL(sendDataAvailableReceived(TrodesSocketMessageHandler*)), this, SIGNAL(doSendAllDataAvailable(TrodesSocketMessageHandler*)));

        //This socket will be used for all-purpose messaging, so we send the connecting module it's ID and then
        // create a new thread to deal with incoming and outgoing messages.
        // The module counter gets incremented each time there is a new module

        messageHandler->setModuleID(moduleCounter++);
        sendModuleID(messageHandler, messageHandler->getModuleID());
        if (benchConfig != NULL) {
            sendModuleBenchConfig(messageHandler, messageHandler->getModuleID());
        }

        // Now trigger the trodesNet structure to send the current data available.  Modules that haven't connected yet will trigger another send of their dataAvailable
        //emit tsSendAllDataAvailableToModule(messageHandler);

        // set up a signal to send out a notification to other modules when this module disconnects
        connect(messageHandler, SIGNAL(moduleDisconnected(qint8)), this, SLOT(sendModuleDisconnected(qint8)));

        // connect signals to handler file and other messages from trodes
        connect(this, SIGNAL(signal_send_openfile(QString)), messageHandler, SLOT(sendOpenFile(QString)));
        connect(this, SIGNAL(signal_source_connect(QString)), messageHandler, SLOT(sendSourceConnect(QString)));
        connect(this, SIGNAL(signal_start_record()), messageHandler, SLOT(sendStartAquisition()));
        connect(this, SIGNAL(signal_stop_record()), messageHandler, SLOT(sendStopAquisition()));
        connect(this, SIGNAL(signal_file_close()), messageHandler, SLOT(sendCloseFile()));
        connect(messageHandler, SIGNAL(moduleDataStreamOn(bool)), this, SIGNAL(moduleDataStreamOn(bool)));
        connect(messageHandler,SIGNAL(settleCommandTriggered()),this,SLOT(relaySettleComand()));


        //Event system
        //connect(messageHandler, SIGNAL(eventNameAddReqest(QString,QString,qint8)),this,SLOT(addEventTypeToList(QString,QString,qint8)));
        connect(messageHandler, SIGNAL(eventNameAddReqest(TrodesEvent)),this,SLOT(addEventTypeToList(TrodesEvent)));
        connect(messageHandler, SIGNAL(eventNameRemoveRequest(TrodesEvent)),this,SLOT(removeEventTypeFromList(TrodesEvent)));
        connect(messageHandler, SIGNAL(eventUpdatesSubscribe(int,qint8)),this,SLOT(addListenerToEvent(int,qint8)));
        connect(messageHandler, SIGNAL(eventUpdatesUnSubscribe(QString,QString,qint8)),this,SLOT(removeListenerFromEvent(QString,QString,qint8)));
        connect(messageHandler, SIGNAL(eventListReqested(qint8)),this,SLOT(moduleRequstedEventList(qint8)));
        connect(messageHandler, SIGNAL(eventSubscriptionListRequested(qint8)),this,SLOT(moduleRequestedSubscriptionList(qint8)));
        connect(messageHandler, SIGNAL(eventOccurred(uint32_t,int,TrodesEvent)),this,SLOT(eventOccurred(uint32_t,int,TrodesEvent)));

        //Otherwise set this to be a dedicated line for data
    }
    else {
        messageHandler->setDedicatedLine(dataType);
        qDebug() << "Dedicated handler. Data Type:" << dataType << ". User data:" << userData;
        emit newDataHandler(messageHandler, userData);
    }
}

void TrodesServer::readySignalReceivedFromModule(TrodesSocketMessageHandler *messageHandler) {
    modulesReady.append(messageHandler->getModuleID());
}

QList<qint8> TrodesServer::getModulesReady() {
    return modulesReady;
}


void TrodesServer::nameReceivedFromModule(TrodesSocketMessageHandler *messageHandler, QString moduleName) {

    //Go through all the exisitng module names to see if there is a repeat. If there is, then we need to assign the module an instance number
    QList<int> modCountersFound;

    for (int i=0;i<moduleNames.length();i++) {
        if (moduleNames[i].moduleName == moduleName) {
            //This module is already running
            modCountersFound.append(moduleNames[i].moduleInstance);
        }
    }

    int currentInstance = 1;
    if (modCountersFound.length() > 0) {
        //There is at least one module with this name, so we need to calculate the lowest available instance number
        bool keepGoing = true;
        while (keepGoing) {
            bool modVersionTaken = false;
            for (int i=0;i<modCountersFound.length();i++) {
                if (modCountersFound[i] == currentInstance) {
                    //This number is already taken
                    modVersionTaken = true;
                    currentInstance++;
                    break;
                }
            }
            if (!modVersionTaken) {
                keepGoing = false;
            }
        }
    }

    //Add the module name to the list
    moduleIDNameCombo mnc;
    mnc.moduleID = messageHandler->getModuleID();
    mnc.moduleName = moduleName;
    mnc.moduleInstance = currentInstance;
    moduleNames.append(mnc);

    if (currentInstance > 1) {
        moduleName = moduleName + QString("%1").arg(currentInstance);
    }
    messageHandler->sendModuleInstance(currentInstance);

    qDebug() << "[TrodesServer::nameReceivedFromModule] Server: module name " << moduleName << " Instance " << currentInstance;
    emit nameReceived(messageHandler, moduleName);

}

void TrodesServer::deleteServer()
{
    //There should be some code here to close all existing client connections
    qDebug() << "TrodesServer:" << "deleteServer()";
    close();
    emit finished();
}


void TrodesServer::sendDataAvailableToModules(DataTypeSpec *da)
{
    /* send the new data available information to all the modules */
    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    qDebug() << "TrodesServer:" << "sendDataAvaliableToModules():" << da->dataType << "port" << da->hostPort;
    msg << *da;

    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_DATATYPEAVAILABLE, m, this);
    sendMessageToModules(tm);
}



void TrodesServer::tsSendAllDataAvailableToModule(TrodesSocketMessageHandler *messageHandler)
{
    // emit a signal that causes the trodesModuleNetwork to send data to the module
    emit doSendAllDataAvailable(messageHandler);
}

void TrodesServer::tsAddDataAvailable(DataTypeSpec *da)
{
    // emit a signal that causes the trodesModuleNetwork to send data to the modules
    emit doAddDataAvailable(da);
}

void TrodesServer::sendStateScript(QString script)
{
    //qDebug() << "in sendStateScript" << (stateScriptMessageHandler == NULL) << "script:" << script;
    if (stateScriptMessageHandler != NULL) {
        QByteArray m;
        m.append(script);

        TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_STATESCRIPTCOMMAND, m, this);
        sendMessageToModule(stateScriptMessageHandler, tm);
    }
    else {
        emit trodesServerError("stateScript module not running.\nRestart with correct config file.");
    }
}





void TrodesServer::sendModuleDisconnected(qint8 ID)
{
    // send a message to the modules that another module has disconnected and then emit a signal that removes it's
    // dataAvailable element
    qDebug() << "TrodesServer:" << "sendModuleDisconnect(" << ID << ")";
    //remove module from event system
    removeModuleFromEventSys(ID);
    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    msg << ID;
    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_MODULEDISCONNECTED, m, this);
    sendMessageToModules(tm);
    emit doRemoveDataAvailable(ID);
}

quint8 TrodesServer::nConnections()
{
    return messageHandlers.length();
}

void TrodesServer::sendMessageToModule(TrodesSocketMessageHandler *messageHandler, TrodesMessage *trodesMessage)
{
    messageHandler->sendMessage(trodesMessage->messageType, trodesMessage->message);
}

void TrodesServer::sendMessageToModule(qint8 modID, TrodesMessage *trodesMessage)
{
    messageHandlers[modID]->sendMessage(trodesMessage->messageType, trodesMessage->message);
}

void TrodesServer::sendMessageToModules(TrodesMessage *trodesMessage)
{
    // go through the list of message handlers and, for each one, send a command to the attached module
    for (int i = 0; i < messageHandlers.length(); i++) {
        if (messageHandlers[i]->getDataType() == TRODESDATATYPE_MESSAGING) {
            messageHandlers[i]->sendMessage(trodesMessage->messageType, trodesMessage->message);
        }
    }
}

void TrodesServer::removeHandlerThread()
{
    //No longer being used, but KEEP just in case we need a way to create new threads
    //for messageHandlers

    QThread *tmpMessageHandlerThread = static_cast<QThread*>(sender());

    tmpMessageHandlerThread->deleteLater();
    messageHandlerThreads.takeAt(messageHandlerThreads.indexOf(tmpMessageHandlerThread));
    qDebug() << "Handler thread removed";
}

void TrodesServer::clientDisconnectHandler()
{
    qDebug() << "TrodesServer: port(" << this->getCurrentPort() << ") a module disconnected.";
    TrodesSocketMessageHandler* tmpMessageHandler = static_cast<TrodesSocketMessageHandler*>(sender());


    qint8 modID = tmpMessageHandler->getModuleID();
    tmpMessageHandler->closeConnection();
    disconnect(tmpMessageHandler);

    for (int i=0; i < moduleNames.length(); i++) {
        if (moduleNames[i].moduleID == modID) {
            moduleNames.takeAt(i);
            i--; //module removed, iterate back 1
        }
        else if (moduleNames[i].moduleID > modID) { //if the disconnected module's ID/index was less than others, iterate their IDs down to match their new positions
            moduleNames[i].moduleID--;

        }
    }
    if (state_recording && tmpMessageHandler->getModuleName() == "Camera") {
        qDebug() << "Camera module quit during recording.  Attempting to restart.";
        emit restartCrashedModule(tmpMessageHandler->getModuleName());
    }

    //Memory leak?? deleting causes crash
    messageHandlers.takeAt(messageHandlers.indexOf(tmpMessageHandler));
    for (int i = 0; i < messageHandlers.length(); i++) {
        qint8 curModID = messageHandlers[i]->getModuleID();
        if (curModID > modID) {
            sendModuleID(messageHandlers[i],(curModID-1));
            messageHandlers[i]->setModuleID((curModID-1));
        }
    }
    moduleCounter--; //module was disconnected, iterate the counter down by one

    //delete messageHandlers.takeAt(messageHandlers.indexOf(tmpMessageHandler));

    //qDebug() << "messageHandler disconnected from" << this->serverAddress().toString() << "port" << serverPort();
    emit clientDisconnected();

    //QTcpSocket* socket = static_cast<QTcpSocket*>(sender());
    //int handlerId = messageHandlers.indexOf()

    //delete buffers.take(socket);
    //delete sizes.take(socket);
    //delete dataTypes.take(socket);

    //socket->deleteLater();
}

TrodesECUClient::TrodesECUClient(QObject *parent) :
    QObject(parent)
{
    udpSocket = new QUdpSocket;
    hardwareAddress.setAddress(networkConf->hardwareAddress);
};

TrodesECUClient::~TrodesECUClient()
{
}

void TrodesECUClient::sendFunctionGoCommand(qint16 functionNum)
{
    qint64 bytesWritten;

    //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
#ifdef WIN32
    bytesWritten = udpSocket->writeDatagram((char*)&functionNum, sizeof(functionNum),
                                            QHostAddress::Broadcast, networkConf->hardwarePort);
#else
    bytesWritten = udpSocket->writeDatagram((char*)&functionNum, sizeof(functionNum),
                                            hardwareAddress, networkConf->hardwarePort);
#endif
    if (bytesWritten != sizeof(functionNum)) {
        qDebug() << "Error in writing go command to MCU";
    }
}



TrodesUDPSocket::TrodesUDPSocket(QString hostAddress, QObject *parent) :
    QObject(parent)
{
    // if the hostname is not specified, set it as best we can
    if (hostAddress == "") {
        // first check the network configuration file
        if ((networkConf->networkConfigFound) && (networkConf->trodesHost != "")) {
            hostAddress = networkConf->trodesHost;
        }
        else {
            hostAddress = "127.0.0.1";
        }
    }
    socket = new QUdpSocket(this);
    // bind the socket to the address, port 0, which means that it chooses an open port
    socket->bind(QHostAddress(hostAddress), 0);
    //qDebug() << "UDP socket created on host" << hostAddress << "port" << socket->localPort();
}

TrodesUDPSocket::~TrodesUDPSocket()
{
}

TrodesSocketMessageHandler *TrodesUDPSocket::newDataHandler(int dataType)
{
   // create a new socket and data handler for the socket and set it's data type
    TrodesSocketMessageHandler *newHandler = new TrodesSocketMessageHandler(this);
    newHandler->setDedicatedLine(dataType);
    newHandler->setSocketType(TRODESSOCKETTYPE_UDP);
    // create a udp server for this nTrode
    newHandler->udpSocket = socket;
    connect(newHandler->udpSocket, SIGNAL(readyRead()), newHandler, SLOT(readMessage()));
    return newHandler;
}

QString TrodesUDPSocket::getCurrentAddress() {
    return socket->localAddress().toString();
}

quint16 TrodesUDPSocket::getCurrentPort() {
    return socket->localPort();
}

TrodesModuleNetwork::TrodesModuleNetwork(QObject *parent) :
    QObject(parent)
{
    dataNeeded = 0;
    trodesServerHost = "\0";
    trodesServerPort = 0;
    useQTSocketsForData = true; // set this to false after you create this object if you want to use your own sockets to connect
    dataServerStarted = false;
    moduleName = "\0";
    //sysTime = new QTime();
    //qDebug() << "CT: " << QTime::currentTime().msec();
    qDebug() << "CT: " << QTime::currentTime().msecsSinceStartOfDay();
}

TrodesModuleNetwork::~TrodesModuleNetwork()
{
}

void TrodesModuleNetwork::setTcpMessageClientConnected()
{
    trodesClient->setDataType(TRODESDATATYPE_MESSAGING, 0);
}

bool TrodesModuleNetwork::trodesClientConnect()
{
    //No host address given, so we'll need to figure it out.  We also assume this can live in the same thread
    return trodesClientConnect("", 0, false);
}
bool TrodesModuleNetwork::trodesClientConnect(bool sepThread)
{
    //No host address given, so we'll need to figure it out
    return trodesClientConnect("", 0, sepThread);
}

bool TrodesModuleNetwork::trodesClientConnect(QString hostAddress, quint16 hostPort, bool sepThread)

{
    separateThread = sepThread;

    /* create the tcpClient that will connect to trodes */
    trodesClient = new TrodesClient();

    if (hostAddress.isEmpty()) { //no host address given, so we need to figure it out
        qDebug() << "Looking for Trodes server...";
        QStringList availableHosts = trodesClient->findLocalTrodesServers();
        if (availableHosts.length() > 0) {
            //Called whenever something is selected from the drop down menu of addresses
            QString pickedHost = availableHosts[0];

            QStringList currentItems = pickedHost.split(" ");
            if (currentItems.length() >= 2) {
                hostAddress = currentItems[0];
                hostPort = currentItems[1].toUInt();
            }
        }
        else if (networkConf != NULL) {
            // we are likely running on a host that is not running trodes, so we use the information in the config file
            // to set up the socket
            hostAddress = networkConf->trodesHost;
            hostPort = networkConf->trodesPort;
        }
    }

    if (hostAddress.isEmpty()) {
        return false;
    }

    //qDebug() << "trodes host and port: " << hostAddress << " " << hostPort;
    connect(trodesClient, SIGNAL(moduleIDReceived(qint8)), this, SLOT(setModuleID(qint8)));
    connect(trodesClient, SIGNAL(dataAvailableReceived(DataTypeSpec*)), this, SLOT(addDataAvailable(DataTypeSpec*)));
    connect(trodesClient, SIGNAL(moduleDisconnected(qint8)), this, SLOT(removeDataAvailable(qint8)));
    connect(trodesClient, SIGNAL(quitCommandReceived()), this, SIGNAL(quitReceived()));
    connect(trodesClient, SIGNAL(moduleDataChanUpdated(int, int)), this, SIGNAL(moduleDataChanUpdated(int, int)));


    trodesClient->setAddress(hostAddress);
    trodesClient->setPort(hostPort);
    if (separateThread) {
        QThread *trodesNetThread = new QThread;
        trodesNetThread->setObjectName("Trodes-ModNet-Client");
        //The client will be running in a separate thread.  This code is required to set that up.
        connect(trodesNetThread, SIGNAL(started()), trodesClient, SLOT(connectToHost())); //connect after the thread has started
        connect(trodesClient, SIGNAL(disconnected()), trodesNetThread, SLOT(quit()));
        connect(trodesClient, SIGNAL(disconnected()), trodesClient, SLOT(deleteLater()));
        connect(trodesNetThread, SIGNAL(finished()), trodesNetThread, SLOT(deleteLater()));

        //Function calls need to be routed via signals/slots for multi-threaded behavior
        connect(this, SIGNAL(sig_disconnectClient()), trodesClient, SLOT(disconnectFromHost()));
        connect(this, SIGNAL(sig_SetDataType(quint8, qint16)), trodesClient, SLOT(setDataType(quint8, qint16)));
        connect(this, SIGNAL(sig_sendMessage(quint8)), trodesClient, SLOT(sendMessage(quint8)));
        connect(this, SIGNAL(sig_sendMessage(quint8, const char*, quint32)), trodesClient, SLOT(sendMessage(quint8, const char*, quint32)));
        connect(this, SIGNAL(sig_sendMessage(quint8, int)), trodesClient, SLOT(sendMessage(quint8, int)));
        connect(this, SIGNAL(sig_sendMessage(quint8, QByteArray)), trodesClient, SLOT(sendMessage(quint8, QByteArray)));
        connect(this, SIGNAL(sig_sendMessage(TrodesMessage*)), trodesClient, SLOT(sendMessage(TrodesMessage*)));
        connect(this, SIGNAL(sig_sendTimeRateRequest()), trodesClient, SLOT(sendTimeRateRequest()));

        //MARK: event
        connect(this, SIGNAL(sig_sendEventListRequest()), trodesClient, SLOT(sendEventListRequest()));
        connect(this, SIGNAL(sig_sendNewEventNameRequest(QString)), trodesClient, SLOT(sendNewEventNameRequest(QString)));
        connect(this, SIGNAL(sig_sendEvent(uint32_t,TrodesEventMessage)), trodesClient, SLOT(sendEvent(uint32_t,TrodesEventMessage)));
        connect(this, SIGNAL(sig_sendEventSubscribe(int,qint8)), trodesClient, SLOT(sendEventSubscribe(int,qint8)));

        connect(this, SIGNAL(sig_sendEventRemoveRequest(QString)), trodesClient, SLOT(sendEventRemoveRequest(QString)));
        connect(trodesClient, SIGNAL(eventReceived(uint32_t,int,TrodesEvent)), this, SIGNAL(sig_eventReceived(uint32_t,int,TrodesEvent)));

        connect(this, SIGNAL(sig_sendModuleName(QString)), trodesClient, SLOT(sendModuleName(QString)));
        connect(this,SIGNAL(sig_sendModuleReady()),trodesClient,SLOT(sendModuleIsReady()));
        connect(this, SIGNAL(sig_sendCurrentStateRequest()),trodesClient, SLOT(sendCurrentStateRequest()));
        //Start the new thread
        trodesClient->moveToThread(trodesNetThread);
        trodesNetThread->start();
    }
    else {
        trodesClient->connectToHost();
    }

    //Now we wait until the client connects (check at reqular intervals for a few times before giving up)
    bool isConnected = false;
    for (int connectChecks = 0; connectChecks < 5; connectChecks++) {
        qDebug() << "Checking for connection..." << connectChecks + 1;
        if (trodesClient->isConnected()) {
            isConnected = true;
            break;
        }
        QThread::msleep(1000);
    }


    if (!isConnected) {
        qDebug() << "trodesClientConnect failed to connect to trodes server";

        return false;
    }

    if (separateThread) {
        //We now need to tell Trodes that this is a messaging socket so that it sends back our ModuleID
        //We emit a signal instead of a direct call becuase the client now lives in another thread.
        emit sig_SetDataType(TRODESDATATYPE_MESSAGING, 0);
    }
    else {
        trodesClient->setDataType(TRODESDATATYPE_MESSAGING, 0);
    }

    return true;
}

bool TrodesModuleNetwork::trodesECUClientConnect()
{
    ecuClient = new TrodesECUClient(this);
    // start a udp socket to connect to the MCU port that forwards to the ECU a
    if (!ecuClient->udpSocket->bind()) {
        qDebug() << "In trodesECUClientConnect: Error binding to ECU direct port";
        return false;
    }
    qDebug() << "ECU client bound to port" << ecuClient->udpSocket->localPort();
    return true;
}


void TrodesModuleNetwork::disconnectClient()
{
    if (separateThread) {
        emit sig_disconnectClient();
    }
    else {
        trodesClient->disconnectFromHost();
    }
}

void TrodesModuleNetwork::setModuleID(qint8 ID)
{
    moduleID = ID;

    // Now that we've received the module ID, we can check the dataProvided structure to see if we need to start
    // a local data server

    //qDebug() << "Got module ID " << moduleID << ". Now starting server? " << dataProvided.dataTypeSelected();

    // we also send the module name if it is specified
    if (moduleName.length() > 0) {
        qDebug() << "In module " << moduleID << "sending moduleName" << moduleName;
        sendModuleName(moduleName);
    }

    dataServer = NULL;
    dataProvided.moduleID = ID;
    if (networkConf != NULL && networkConf->networkConfigFound) {
        dataProvided.socketType = networkConf->dataSocketType;
    }
    else {
        dataProvided.socketType = TRODESSOCKETTYPE_TCPIP;
    }
    // This ia a bit odd, but we always create a TCP server object
    // This allows for us to put all the messageHandlers in a single list that is part of the TCPIP Server
    //qDebug() << "In module " << moduleID << "dataProvided.dataTypeSelected()" << dataProvided.dataTypeSelected();

    if (dataProvided.dataTypeSelected()) {
        // start a TCPIP server
        dataServer = new TrodesServer(this);
        if (dataProvided.socketType == TRODESSOCKETTYPE_TCPIP) {
            dataServer->startServer("module data server");
            //qDebug() << "In module " << moduleID << "started data server 1";

            dataProvided.hostName = dataServer->getCurrentAddress();
            dataProvided.hostPort = dataServer->serverPort();

        }
        else if (dataProvided.socketType == TRODESSOCKETTYPE_UDP) {
            // start a UDP server, overwriting the host and port info
            //qDebug() << "in setModuleID, localhostname" <<QHostInfo::localHostName();
            //udpDataServer = new TrodesUDPSocket(QHostInfo::localHostName(), this);
            udpDataServer = new TrodesUDPSocket("", this);
            dataProvided.hostName = udpDataServer->getCurrentAddress();
            dataProvided.hostPort = udpDataServer->getCurrentPort();
            // also add a data handler to the dataServer for each type of data provided.
            // Right now, this is only position data from the camera module.  New datatypes will require
            // additional code here to set the UDP ports in the dataProvided structure correctly
            if (dataProvided.dataType & TRODESDATATYPE_POSITION) {
                //qDebug() << "module" << ID << "starting UDP data handler on host" << dataProvided.hostName<< "port" << dataProvided.hostPort;
                TrodesSocketMessageHandler *newHandler = udpDataServer->newDataHandler(TRODESDATATYPE_POSITION);
                dataServer->messageHandlers.push_back(newHandler);
                dataProvided.positionUDPPort = dataProvided.hostPort;
            }
        }
        qDebug() << "module" << ID << "started data server on host" << dataProvided.hostName<< "port" << dataProvided.hostPort;

        QByteArray temp;
        TrodesDataStream msg(&temp, QIODevice::ReadWrite);
        msg << dataProvided;
        if (separateThread) {
            emit sig_sendMessage(TRODESMESSAGE_DATATYPEAVAILABLE, temp);
            emit sig_sendMessage(TRODESMESSAGE_SENDDATATYPEAVAILABLE);
        }
        else {
            trodesClient->sendMessage(TRODESMESSAGE_DATATYPEAVAILABLE, temp);
            trodesClient->sendMessage(TRODESMESSAGE_SENDDATATYPEAVAILABLE, temp);
        }
        dataServerStarted = true;
        emit dataServerCreated();
    }
    else {
        // get the data type Available information even if we're not providing data, so we ask for it
        //We emit a signal instead of a direct call becuase this function gets called from
        //another thread.
        if (separateThread) {
            emit sig_sendMessage(TRODESMESSAGE_SENDDATATYPEAVAILABLE);
        }
        else {
            trodesClient->sendMessage(TRODESMESSAGE_SENDDATATYPEAVAILABLE);
        }
    }
    // the module ID also defines the local range of statescript functions as follows:
    // module 0  -  fn 10 to 19
    // module 1  -  fn 20 to 29, etc.
    emit stateScriptFunctionRange((ID + 1) * 10, (ID + 1) * 10 + 9);
}

void TrodesModuleNetwork::sendModuleIsReady()
{
    if (separateThread) {
        //We emit a signal instead of a direct call becuase this function gets called from
        //another thread.

        emit sig_sendModuleReady();
    }
    else {

        trodesClient->sendModuleIsReady();
    }
}

void TrodesModuleNetwork::sendModuleName(QString name)
{
    if (separateThread) {
        //We emit a signal instead of a direct call becuase this function gets called from
        //another thread.
        qDebug() << "TrodesModuleNetwork:" << "sepThread sendModuleName(" << name << ")";
        emit sig_sendModuleName(name);
    }
    else {
        qDebug() << "TrodesModuleNetwork:" << "sendModuleName(" << name << ")";
        trodesClient->sendModuleName(name);
    }
}

void TrodesModuleNetwork::sendCurrentStateRequest()
{
    emit sig_sendCurrentStateRequest();
}

void TrodesModuleNetwork::sendTimeRateRequest()
{
    if (separateThread) {
        //We emit a signal instead of a direct call becuase this function gets called from
        //another thread.
        emit sig_sendTimeRateRequest();
    }
    else {
        trodesClient->sendTimeRateRequest();
    }
}

//MARK
void TrodesModuleNetwork::sendEventListRequest() {
    emit sig_sendEventListRequest();
}
void TrodesModuleNetwork::sendNewEventNameRequest(QString eventName) {
    emit sig_sendNewEventNameRequest(eventName);
}
void TrodesModuleNetwork::sendEvent(uint32_t curTime, TrodesEventMessage event) {
    emit sig_sendEvent(curTime,event);
    //qDebug() << "sending [" << eventName << "]";
}
void TrodesModuleNetwork::sendEventSubscribe(int eventIndex) {
    emit sig_sendEventSubscribe(eventIndex, moduleID);
}
void TrodesModuleNetwork::sendEventRemoveRequest(QString eventName) {
    emit sig_sendEventRemoveRequest(eventName);
}


void TrodesModuleNetwork::sendAllDataAvailableToModule(TrodesSocketMessageHandler *messageHandler)
{
    // Send the specified module each of the DataAvailable objects

    qDebug() << "TrodesModuleNetwork:" << "Sending all data available (n = " << dataAvailable.length() << ") structures to module " << messageHandler->getModuleID();
    for (int i = 0; i < dataAvailable.length(); i++) {
        QByteArray m;
        TrodesDataStream msg(&m, QIODevice::ReadWrite);
        msg << dataAvailable[i];
        TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_DATATYPEAVAILABLE, m, this);
        qDebug() << "TrodesModuleNetwork:" << "data avail from module" << dataAvailable[i].moduleID << "datatype" << dataAvailable[i].dataType << "socketType" << dataAvailable[i].socketType << "port" << dataAvailable[i].hostPort;
        emit messageForModule(messageHandler, tm);
    }
}



void TrodesModuleNetwork::addDataAvailable(DataTypeSpec *da)
{
    bool found = false;

    // insert this datatype spec into the main list.
    // As modules can start and stop, we need to check if this dataAvailable element exists or not and if so, we need to replace it.

    DataTypeSpec daTmp;
    for (int i = 0; i < dataAvailable.length(); i++) {
        daTmp = dataAvailable[i];
        if ((daTmp.moduleID == da->moduleID) && (daTmp.hostName == da->hostName) &&
                (daTmp.hostPort == da->hostPort)) {
            dataAvailable.replace(i, *da);
            found = true;
        }
    }
    if (!found) {
        dataAvailable.append(*da);

    }
    // Now we check to see if we need a data connection to the module that provided the DataTypeSpec object

    int currentDataType = 1;
    while (currentDataType <= TRODESDATATYPE_MAXDATATYPE) {
        if ((da->dataType & currentDataType) && (dataNeeded & currentDataType)) {
            // this source provides the currentDataType and we need to receive this datatype, so we see if any of the already connected trodesClients provides it.
            found = false;
            for (int i = 0; i < dataClient.length(); i++) {
                if (dataClient[i]->getDataType() == currentDataType) {
                    found = true;
                    qDebug() << "Found client " << i << " data type " << currentDataType;
                    break;
                }
            }
            if (!found) {
                //-------------------
                // Note - This code may not work if TrodesModuleNetwork is started in it's own thread.
                //-------------------

                qint32 dst;
                if (networkConf != NULL) {

                    dst = networkConf->dataSocketType;
                } else {

                   dst = TRODESSOCKETTYPE_TCPIP;
                }

                if (useQTSocketsForData) {
                    if ((currentDataType == TRODESDATATYPE_ANALOGIO) ||
                        (currentDataType == TRODESDATATYPE_DIGITALIO) ||
                        (currentDataType == TRODESDATATYPE_POSITION)) {
                        // create a new client to connect to the specified server.
                        TrodesClient *tc = new TrodesClient(dst);
                        //qDebug() << "dataAvailable host" << da->hostName << " " << da->hostPort;
                        tc->setAddress(da->hostName);
                        if (dst == TRODESSOCKETTYPE_TCPIP) {
                            tc->setPort(da->hostPort);
                        }
                        else if (dst == TRODESSOCKETTYPE_UDP) {
                            // set the port for the data type
                            if (currentDataType == TRODESDATATYPE_ANALOGIO) {
                                tc->setPort(da->analogIOUDPPort);
                            }
                            else if (currentDataType == TRODESDATATYPE_DIGITALIO) {
                                tc->setPort(da->digitalIOUDPPort);
                            }
                            else if (currentDataType == TRODESDATATYPE_POSITION) {
                                tc->setPort(da->positionUDPPort);
                            }
                        }

                        tc->connectToHost();
                        tc->setDataType(currentDataType, 0);
                        dataClient.append(tc);
                        emit dataClientStarted();
                        qDebug() << "added client for datatype " << currentDataType;
                    }
                    else if (currentDataType == TRODESDATATYPE_CONTINUOUS) {
                        // create one client for data from each nTrode
                        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
                           // check to see if this server provides this nTrode
                           int nTrodeIndex = da->contNTrodeIndexList.indexOf(i);
                           if (nTrodeIndex != -1) {
                                TrodesClient *tc = new TrodesClient(dst);
                                //qDebug() << "dataAvailable host" << da->hostName << " " << da->hostPort;
                                tc->setAddress(da->hostName);
                                if (dst == TRODESSOCKETTYPE_TCPIP) {
                                    tc->setPort(da->hostPort);
                                }
                                else if (dst == TRODESSOCKETTYPE_UDP) {
                                    // set the port for the data type
                                    tc->setPort(da->contNTrodeUDPPortList.at(nTrodeIndex));

                                }
                                tc->setNTrodeId(spikeConf->ntrodes[i]->nTrodeId);
                                tc->setNTrodeIndex(i);
                                tc->connectToHost();
                                tc->setDataType(currentDataType, tc->getNTrodeIndex());
                                dataClient.append(tc);
                                emit dataClientStarted();
                                qDebug() << "added client for ntrode " << i;
                            }
                        }
                    }
                    else if (currentDataType == TRODESDATATYPE_BLOCK_CONTINUOUS) {
                        // TODO - IMPLEMENT

                    }
                    else if (currentDataType == TRODESDATATYPE_SPIKES) {
                        // create one client for data from each nTrode
                        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
                           // check to see if this server provides this nTrode
                           int nTrodeIndex = da->spikeNTrodeIndexList.indexOf(i);
                           if (nTrodeIndex != -1) {
                                TrodesClient *tc = new TrodesClient(dst);
                                //qDebug() << "dataAvailable host" << da->hostName << " " << da->hostPort;
                                tc->setAddress(da->hostName);
                                if (dst == TRODESSOCKETTYPE_TCPIP) {
                                    tc->setPort(da->hostPort);
                                }
                                else if (dst == TRODESSOCKETTYPE_UDP) {
                                    // set the port for the data type
                                    tc->setPort(da->spikeNTrodeUDPPortList.at(nTrodeIndex));

                                }
                                tc->setNTrodeId(spikeConf->ntrodes[i]->nTrodeId);
                                tc->setNTrodeIndex(i);
                                tc->connectToHost();
                                tc->setDataType(currentDataType, tc->getNTrodeIndex());
                                dataClient.append(tc);
                                emit dataClientStarted();
                                qDebug() << "added client for ntrode " << i;
                            }
                        }
                    }
                }
                else {
                    // if the module needs to deal with the data clients separately, emit a signal to tell it do so
                    //qDebug() << "module" << moduleName << "in AddDataAvailable, emiting start data client for datatype " << currentDataType;
                    emit startDataClient(da, currentDataType);
                }
            }
        }
        //  move on to the next datatype
        currentDataType *= 2;
    }
}

void TrodesModuleNetwork::removeDataAvailable(qint8 ID)
{
    // remove the entry for this module */
    for (int i = 0; i < dataAvailable.length(); i++) {
        if (dataAvailable[i].moduleID == ID) {
            dataAvailable.removeAt(i);
            break;
        }
    }
}

void TrodesModuleNetwork::sendStateScript(QString *script)
{
    // send the state script on to trodes, which will forward it to the stateScript module
    //qDebug() << "TrodesModuleNetwork: Sending state script to trodes";
    TrodesMessage tm;

    tm.messageType = TRODESMESSAGE_STATESCRIPTCOMMAND;
    tm.message.append(*script);
    if (separateThread) {
        emit sig_sendMessage(&tm);
    }
    else {
        trodesClient->sendMessage(&tm);
    }
}


QProcess *TrodesModuleNetwork::startSingleModule(SingleModuleConf s, QProcess *moduleProcess)
{
    // QProcess is optional -- this way caller can set up things like stderr/stdout forwarding, if needed

    // TODO:
    // -Try to call it from FSGui, stateScript?) Paths are relative to calling App,
    // not Trodes
    // -How to handle failure? Return same process, unstarted, or destroy the process and return NULL

    // N.B. relative paths are treated as relative to the directory containing
    // the executable (or, on a Mac, the app bundle) that launched the module
    // (this is usually Trodes). This is different than the current working
    // directory of the app.

    // If the filename component of the path is a bare module name (e.g.
    // "cameraModule"), then the behavior is platform-dependent. On a Mac, if
    // the requested file does not exist we look for an app bundle by that
    // name. On Windows, we look for an executable named moduleName.exe. On
    // Linux we just use the moduleName as-is

    QString moduleAbsName; // Absolute path to use
    QFileInfo moduleAbsFileInfo;

    // Absolute path of directory containing the module, or module app bundle
    // (potentially after relative path moves)
    QString moduleWorkingDir;

    QString moduleBaseName = QFileInfo(s.moduleName).baseName();

    // Parent doesn't much matter (only used for QObject tree), process will still be killed when calling program exits, e.g.
    if (!moduleProcess)
      moduleProcess = new QProcess();

    //Get the directory containing the calling executable (usu. Trodes)
    QString callingAppDir = QCoreApplication::applicationDirPath();

#ifdef __APPLE__
    //If calling app is an app bundle, use its parent as the base directory
    if (callingAppDir.endsWith(QString(".app/Contents/MacOS")))
        callingAppDir = QDir::cleanPath(callingAppDir + "/../../../");
#endif

    // Convert moduleName to absolute path
    if (QFileInfo(s.moduleName).isRelative()){
        moduleAbsName= QDir::cleanPath(callingAppDir + "/" + s.moduleName);
        qDebug() << "[MainWindow::startModule] 'moduleName' string (" << s.moduleName << ") converted to absolute path: " << moduleAbsName;
    } else {

        // Use absolute path provided
        moduleAbsName = s.moduleName;
        qDebug() << "[MainWindow::startModule] 'moduleName' string (" << s.moduleName << ") is an absolute path (NOT RECOMMENDED); used as-is.)";

        if (!globalConf->suppressModuleAbsPathWarning && !unitTestMode){

            QMessageBox::warning(
                        0, "error", QString(
                            "Configuration file contains a hard-coded, absolute path to "
                            "a module executable: \n\nmoduleName=\""
                            + s.moduleName +
                            "\" \n\nThis is strongly discouraged since it can lead to "
                            "running old versions of modules. Instead, use a "
                            "relative path to locate modules. (Path is relative "
                            "to directory containing the launching application, in this case: '"
                            + callingAppDir  + QDir::separator() + "'). \n\n "
                            "When in doubt, use the bare moduleName in your config file: \n\n moduleName=\""
                            + moduleBaseName + "\"\n\n "
                            "(Suppress this warning with suppressModuleAbsPathWarning=\"1\" "
                            "in GlobalConfiguration)."));

        }
    }

    // Use *module's* parent directory as moduleWorkingDir
    moduleAbsFileInfo = QFileInfo(moduleAbsName);
    moduleWorkingDir = moduleAbsFileInfo.path();

#ifdef __APPLE__
    // ***************************
    // Handle Mac app bundle structure.
    //
    // The following moduleName strings:
    // 1: "/Path/to/moduleName" (where file doesn't exist, but /Path/to/moduleName.app does)
    // 2: "/Path/to/moduleName.app"
    // 3: "/Path/to/moduleName.app/Contents/MacOS/moduleName"
    // ... should launch-> "/Path/to/moduleName.app/Contents/MacOS/moduleName",
    //
    // While:
    // 4: "/Path/to/moduleName" (exists, not a bundle)
    // ... should launch -> "/Path/to/moduleName"
    //
    // In all these cases, moduleWorkingDir should be set to -> "/Path/to"

    // Case 1: User provides path to a module's base name, and an app bundle exists
    if (!moduleAbsFileInfo.exists() && QFileInfo::exists(moduleAbsFileInfo.filePath() + ".app"))
        moduleAbsName = moduleAbsName + ".app/Contents/MacOS/" + moduleBaseName;

    // Case 2: User provides absolute path to module app bundle
    else if (moduleAbsFileInfo.exists() && (moduleAbsFileInfo.suffix() == "app"))
        moduleAbsName = moduleAbsName + "/Contents/MacOS/" + moduleBaseName;

    // Case 3/4: Path to executable
    else if (moduleAbsFileInfo.exists() && !moduleAbsFileInfo.isDir()){
        // Is it part of a bundle?
        if (moduleWorkingDir.endsWith(QString(".app/Contents/MacOS"))) {
            // Case 3: executable part of a bundle, update working dir only
            moduleWorkingDir = QDir::cleanPath(moduleWorkingDir + "/../../../");
        } else {
            // Case 4: bare executable, no need to update moduleAbsName or moduleWorkingDir
        }
    }

    // ***************************


#endif
#ifdef WIN32

    // Set Windows executable paths (QProcess will launch without the '.exe'
    // suffix, but exists() and isExecutable() need the full file name.")
    if (!(moduleAbsName).endsWith(".exe")){
        moduleAbsName = moduleAbsName + ".exe";
    }

#endif
    // Update FileInfo
    moduleAbsFileInfo = QFileInfo(moduleAbsName);

    if (!moduleAbsFileInfo.exists()){
        if (!unitTestMode) {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", QString("Module \"%1\" could not be started at path: \"%2\".\n\nFile does not exist.").arg(moduleBaseName).arg(moduleAbsName));
            qDebug() << "[MainWindow::startModule] MODULE LOADING ERROR! Config file moduleName string: " << s.moduleName \
                 << ". Launching application directory: " << callingAppDir << ". Module search location (file doesn't exist): " << moduleAbsName;
        }
        return moduleProcess;
    }
    if (!moduleAbsFileInfo.isExecutable()){
        if (!unitTestMode) {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", QString("Module \"%1\" could not be started at path: \"%2\".\n\nFile is not an executable.").arg(moduleBaseName).arg(moduleAbsName));
            qDebug() << "[MainWindow::startModule] MODULE LOADING ERROR! Config file moduleName string: " << s.moduleName \
                 << ". Launching application directory: " << callingAppDir << ". Module search location (not an executable file): " << moduleAbsName;
        }
        return moduleProcess;
    }

    // launch the module with the main configuration file and the specified configuration file
    qDebug() << "[MainWindow::startModule] Launching: " << moduleAbsName;
    qDebug() << "[MainWindow::startModule] Working directory for module: " << moduleWorkingDir;

    moduleProcess->setWorkingDirectory(moduleWorkingDir);

    QStringList arglist;

    if (s.sendTrodesConfig == 1) {
        QStringList configInfo;
        // Need to pass absolute path to config file as module's working
        // directory may be different (and config file location may be relative)
        configInfo << "-trodesConfig" << QFileInfo(moduleConf->trodesConfigFileName).absoluteFilePath();
        arglist << configInfo;
    }

    if (s.sendNetworkInfo) {
        QStringList netInfo;
        netInfo << "-serverAddress" << this->tcpServer->getCurrentAddress() << "-serverPort" << QString("%1").arg(this->tcpServer->getCurrentPort());
        arglist << netInfo;
    }
    arglist << s.moduleArguments;
    qDebug().noquote() << "[MainWindow::startModule] arguments: " << arglist;
    moduleProcess->start(moduleAbsName, arglist);
    if (moduleProcess->waitForStarted(10000) == false) {
        QMessageBox messageBox;
        messageBox.critical(0, "Error", QString("%1 could not be started").arg(s.moduleName));
    }
    return moduleProcess;
}
