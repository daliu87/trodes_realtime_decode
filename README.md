# Trodes #


### An open source, cross-platform software suite for neuroscience data acquisition and experimental control. 
 
-----
### Website ###
[http://www.spikegadgets.com/software/trodes.html](http://www.spikegadgets.com/software/trodes.html)

### Wiki ###
[https://bitbucket.org/mkarlsso/trodes/wiki](https://bitbucket.org/mkarlsso/trodes/wiki)

### Screenshots ###
[OSX](https://bitbucket.org/mkarlsso/trodes/downloads/trodes_screenshot.jpg)


### Required drivers ###
[FTDI D2XX Driver](http://www.ftdichip.com/Drivers/D2XX.htm)

-----
## Overview ###
Trodes is used to monitor incoming data from hardware used in neuroscience research, with a specific focus on electrophysiology. It is also being developed to allow complex control of behavioral apparatuses and neuro-perterbation devices.  Trodes is made up of "modules", each running as a separate process and serving a specialized purpose. The modules all communicate with a central graphical interface that performs the most crucial tasks associated with visualizing and saving data. With this architecture, the trodes suite is readily extendable without needing detailed understanding of existing code. 


### Getting started using binaries ###
Currently just a Windows binary is available, and it is regularly updated. Check the downloads section.

Windows:

1) Install the FTDI D2XX driver.  We have been using the older version (release date 2014-02-21)

2) Download the latest windows binary .zip file [here](https://bitbucket.org/mkarlsso/trodes/downloads).  This contains a folder with all of the required files.

3)Unzip the archive and place the folder anywhere on your computer.

4) Double-click on 'Trodes' 




-----


### Getting started with a simulated signal ###


1)  Go to File->Workspace->Load, and select the workspace file (with a .trodesconf extension).

2) Select Connection->Source->Signal generator

3) Select Connection->Stream from source to start data streaming

4) Go to Connection->Generator controls to play with the input signal

### Basic controls ###


1) If you click on a channel, it becomes highlighted.  The audio changes to that channel, and the nTrode that the channel belongs to becomes the selected nTrode.

2) There are buttons at the top of the window.  The ‘Spikes’ button will open up a spike trigger window.  When you click on a channel, the nTrode displayed will automatically update.

3) The ‘Video’ button links to the video module.  If you click on this button, and if you have a USB webcam or Allied Vision GIGE camera connected, a window will pop up showing live video.  When you save data, this module will save video, where the frames are time stamped with the same time values as the neural recording timestamps.This module can also do online and offline rodent position tracking.

4) The ‘nTrode’ button allows you to set critical values for the selected nTrode, such as a digital reference and filter values.

5) If you save data on to .rec file, that file can later be replayed using Connection->Source->File.

6) For some workspaces, you’ll notice that there is an ‘Aux’ tab displaying the auxiliary digital and analog lines configured in the workspace file.