#ifndef SYNTHETICDATAGENERATOR_H
#define SYNTHETICDATAGENERATOR_H

#include <stdint.h>
#define SAMPLING_RATE 30000
#define DATA_LENGTH 4*SAMPLING_RATE

extern char dataBuffer[];
extern int bufferLen;


class SyntheticDataGenerator
{
public:
    SyntheticDataGenerator();

public:
    void loadNextSample();
    void setNCHAN(int value);
    void setHasECU(bool value);
    void resetTimestamp();

    int queryNCHAN();
    bool queryHasECU();


private:
    int         NCHAN;
    bool        hasECU;
    int16_t     sourceData[DATA_LENGTH];
    unsigned char        dioData[DATA_LENGTH];

    double      waveFrequency;
    double      waveAmplitude;
    double      waveModulatorFrequency;
    double      dioFrequency;

    int         dataIdx;
    uint32_t    timestamp;
};

#endif // SYNTHETICDATAGENERATOR_H
