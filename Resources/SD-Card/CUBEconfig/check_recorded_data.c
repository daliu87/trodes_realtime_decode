
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <fcntl.h>
#include <linux/fs.h>


#define MAX_FNAME_LENGTH 1000
#define INPUT_BUFFER_SIZE 1024
#define HEADER_SIZE 512


uint8_t Buff[INPUT_BUFFER_SIZE];			/* Working buffer */

uint64_t get_timestamp(uint8_t *buffer) {
  
  /* uint64_t timestamp;

  timestamp = (buffer[17]<<24 | buffer[16]<<16 | buffer[15]<<8 | buffer[14]);
  printf("timestamp: %ld [%02x %02x %02x %02x] %02x\n", timestamp, 
      buffer[14], buffer[15], buffer[16], buffer[17], buffer[0]);
  */

  return (buffer[17]<<24 | buffer[16]<<16 | buffer[15]<<8 | buffer[14]);
}

int packet_read(FILE *fp, char *buffer, long int startPacketIndex, size_t packetSize, int numPackets)
{
    size_t res;
    // position the file pointer to the start of the requested packets
    if (res = fseek(fp, HEADER_SIZE + startPacketIndex * packetSize, SEEK_SET) != 0) {
      fprintf(stderr, "Error in packet_read seeking requested data.\n");
      return -1;
    }

    // read the data packet(s). we trust that buffer has been properly allocated
    res = fread(buffer, sizeof(uint8_t) * packetSize,  numPackets, fp);
    if (res != numPackets) {
      fprintf(stderr, "Failed to read data packet(s) (rcvd=%ld, expected=%ld).\n", 
          res, numPackets);
      return -2;
    }

    return 0;
}

/*-----------------------------------------------------------------------*/
/* Main                                                                  */


int main (int argc, char *argv[])
{
  size_t res;

  uint16_t sectorSize;
  uint32_t sectorCount;
  unsigned int packetSize;
  uint64_t lastTimestamp;
  uint64_t dw; // going to be a diff in timestamps
  long unsigned int maxPackets, lastPacket, gapPacket;
  long unsigned int droppedPackets, wdp;
  unsigned int shift;
  int pd, i, j, count;
  int last_packet_start;
  int input_fd;
  FILE *input_fp;
  FILE *output_fp;
  char input_fname[MAX_FNAME_LENGTH];
  char output_fname[MAX_FNAME_LENGTH];
  
  // turn off output buffering
  setvbuf(stdout, 0, _IONBF, 0);
  printf("\n*** Packet checker 1.3 ***\n");

  if (argc < 1) {
       fprintf(stderr, "Usage: check_recorded_data <raw data file> <packet size (optional)>\n");
       return 2;
  }

  if (argc > 1) {
    strncpy(input_fname, argv[1], MAX_FNAME_LENGTH);
    if (input_fname[MAX_FNAME_LENGTH-1] != '\0') {
      fprintf(stderr, "Maximum input file name length exceeded.\n");
      return 2;
    }
    else if ( access(input_fname, F_OK ) == -1 ) {
       fprintf(stderr, "Usage: check_recorded_data <raw data file>\n");
       return 2;
    }
  }

  input_fd = open(input_fname, O_RDONLY|O_NONBLOCK);
  res = ioctl(input_fd, BLKSSZGET, &sectorSize);
  fprintf(stdout, "Read sector size: %d\n", sectorSize);

  res = ioctl(input_fd, BLKGETSIZE, &sectorCount);
  fprintf(stdout, "Read sector size: %d\n", sectorCount);

  close(input_fd);

  input_fp = fopen(input_fname, "r");
  if (input_fp == NULL) {
    fprintf(stderr, "Error opening raw data file.\n");
    return 2;
  }

  if (argc > 2) {
    packetSize = atoi(argv[2]);
    if (packetSize > 518) {
      printf("Packet size argument too big!\n");
      return 2;
    }
  } 
  else
    packetSize = 0;

  res = fread(Buff, sizeof(uint8_t), HEADER_SIZE, input_fp);

  // read the configuration sector
  if (res != HEADER_SIZE) {
    fprintf(stderr ,"Failed to read header bytes (rc=%d).\n", (int) res);
    return 2;
  }

  if (packetSize == 0) {
    // set the file pointer to the end of the HEADER
    res = fseek(input_fp, HEADER_SIZE, SEEK_SET);
    // read the first data packet(s)
    res = fread(Buff, sizeof(uint8_t), INPUT_BUFFER_SIZE, input_fp);
    if (res != INPUT_BUFFER_SIZE) {
      fprintf(stderr, "Failed to read first data packet(s) (rc=%d).\n", (int) res);
      return 2;
    }

    if (Buff[0] != 0x55) {
      fprintf(stderr, "No start packet found!\n");
      return 1;
    }
    i = 1;

    // search for the next packet header
    while (((Buff[i] != 0x55) || (Buff[i+14] != 0x01) || 
            (Buff[i+15] != 0x00) || (Buff[i+16] != 0x00) || 
            (Buff[i+17] != 0x00)) && (i++ < INPUT_BUFFER_SIZE));
    if (i == INPUT_BUFFER_SIZE) {
      fprintf(stderr, "can't find the second packet start!\n");
      return 2;
    } else {
      packetSize = i;
    }
  }

  printf("Packet size: %u bytes/packet\n", packetSize);

  maxPackets = sectorCount*512/packetSize;
  printf("Maximum packets on the disk = %lu\n", maxPackets);

  lastPacket = 1;
  shift = 0;
  while (lastPacket < maxPackets) {
    shift++;
    lastPacket = lastPacket<<1;
  }
  lastPacket = 0;
  for (i=shift; i>=0; i--) {
    lastPacket |= (1<<i);
    if (lastPacket >= maxPackets) {
      lastPacket &= ~(1<<i);
      continue;
    }
    if (res = packet_read(input_fp, Buff, lastPacket, packetSize, 1) != 0)
      return; // error in packet_read!
    
    if (Buff[0] != 0x55) {
      lastPacket &= ~(1<<i);
    }
  }
  if (lastPacket < (maxPackets-1))
    // Disk not full, don't use the last recorded packet since it might not be complete
    lastPacket--;

  printf("Packets recorded on the disk = %lu\n", lastPacket+1);
  if (res = packet_read(input_fp, Buff, lastPacket, packetSize, 1) != 0)
    return; // error in packet_read!

  lastTimestamp = get_timestamp(Buff);
  droppedPackets = lastTimestamp - lastPacket;
  if (droppedPackets) {
    printf("Dropped packets = %lu (%.2f ms)\n", droppedPackets, (float)droppedPackets/25);
    printf("Locating the gaps:\n");
    wdp = 0;
    while (wdp < droppedPackets) {
      gapPacket = 0;
      for (i=shift; i>=0; i--) {
        gapPacket |= (1<<i);
        if (gapPacket > lastPacket) {
          gapPacket &= ~(1<<i);
          continue;
        }
        if (res = packet_read(input_fp, Buff, gapPacket, packetSize, 1) != 0)
          return; // error in packet_read!
        dw = get_timestamp(Buff) - gapPacket;
        if (dw > wdp)
          gapPacket &= ~(1<<i);
      }
      if (res = packet_read(input_fp, Buff, gapPacket+1, packetSize, 1) != 0)
        return; // error in packet_read!
      dw = get_timestamp(Buff) - (gapPacket+1);
      printf("  Gap after packet %lu (%lu packets long)\n", gapPacket, dw-wdp);
      wdp = dw;
    }
  }
  else
    printf("No dropped packets\n");

  return 0;
}
  


