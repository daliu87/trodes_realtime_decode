
#include <string.h>
#include <stdio.h>
#include <windows.h>
#include "diskio.h"


BYTE Buff[32768];			/* Working buffer */



/*-----------------------------------------------------------------------*/
/* Main                                                                  */


int main (int argc, char *argv[])
{
	unsigned char res;
	WORD w;
	DWORD dw;
  unsigned int psize;
  long unsigned int maxPackets, lastPacket, gapPacket;
  long unsigned int droppedPackets, wdp;
  int pd, i;
  unsigned int shift;
  
  // turn off output buffering
  setvbuf(stdout, 0, _IONBF, 0);
	printf("\n*** Packet checker 1.3 ***\n");

  pd = 1;
  if (argc > 1)
    pd = atoi(argv[1]);
  
	if (!assign_drives(pd)) {
		printf("\nUsage: pcheck <phy drv#>\n");
		return 2;
	}

  if (argc > 2) {
    psize = atoi(argv[2]);
    if (psize > 518) {
      printf("Packet size too big!\n");
      return 2;
    }
  } else
    psize = 0;

  if (psize == 0) {
    res = disk_read(0, Buff, 1, 2);
    if (res) {
      printf("rc=%d\n", (WORD)res);
      return 2;
    }
    if (Buff[0] != 0x55) {
      printf("No start packet found!\n");
      return 1;
    }
    i = 1;
    // search for the next packet header
    while (((Buff[i] != 0x55) || (Buff[i+2] != 0x01) || 
            (Buff[i+3] != 0x00) || (Buff[i+4] != 0x00) || 
            (Buff[i+5] != 0x00)) && (i++ < 1024));
    if (i == 1024) {
      printf("can't find the second packet start!\n");
      return 2;
    } else {
      psize = i;
    }
  }

  printf("Packet size: %u bytes/packet\n", psize);

  disk_ioctl(0, GET_SECTOR_SIZE, &w);
  disk_ioctl(0, GET_SECTOR_COUNT, &dw);

  maxPackets = ((((LONGLONG)dw - 1) * w)/psize);
  printf("Maximum packets on the disk = %lu\n", maxPackets);

  lastPacket = 1;
  shift = 0;
  while (lastPacket < maxPackets) {
    shift++;
    lastPacket = lastPacket<<1;
  }
  lastPacket = 0;
  for (i=shift; i>=0; i--) {
    lastPacket |= (1<<i);
    if (lastPacket >= maxPackets) {
      lastPacket &= ~(1<<i);
      continue;
    }
    res = packet_read(0, Buff, lastPacket, psize, 1);
    if (res) {
      printf("packet_read error, rc=%d\n", (WORD)res);
      break;
    } else if (Buff[0] != 0x55) {
      lastPacket &= ~(1<<i);
    }
  }
  if (lastPacket < (maxPackets-1))
    // Disk not full, don't use the last recorded packet since it might not be complete
    lastPacket--;

  printf("Packets recorded on the disk = %lu\n", lastPacket+1);
  packet_read(0, Buff, lastPacket, psize, 1);
  droppedPackets = (Buff[5]<<24 | Buff[4]<<16 | Buff[3]<<8 | Buff[2]) - lastPacket;
  if (droppedPackets) {
    printf("Dropped packets = %lu (%.2f ms)\n", droppedPackets, (float)droppedPackets/25);
    printf("Locating the gaps:\n");
    wdp = 0;
    while (wdp < droppedPackets) {
      gapPacket = 0;
      for (i=shift; i>=0; i--) {
        gapPacket |= (1<<i);
        if (gapPacket > lastPacket) {
          gapPacket &= ~(1<<i);
          continue;
        }
        res = packet_read(0, Buff, gapPacket, psize, 1);
        if (res) {
          printf("rc=%d, gapPacket = %lu\n", (WORD)res, gapPacket);
          break;
        } else {
          dw = (Buff[5]<<24 | Buff[4]<<16 | Buff[3]<<8 | Buff[2]) - gapPacket;
          if (dw > wdp)
            gapPacket &= ~(1<<i);
        }
      }
      res = packet_read(0, Buff, gapPacket+1, psize, 1);
      if (res) {
        printf("rc=%d\n", (WORD)res);
        break;
      } else {
        dw = (Buff[5]<<24 | Buff[4]<<16 | Buff[3]<<8 | Buff[2]) - (gapPacket+1);
        printf("  Gap after packet %lu (%lu packets long)\n", gapPacket, dw-wdp);
        wdp = dw;
      }
    }
  }
  else
    printf("No dropped packets\n");
  return 0;
}
  


