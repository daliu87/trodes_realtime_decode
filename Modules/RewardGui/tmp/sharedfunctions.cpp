/* Shared functions for acquisition GUIs to interface with trodes 
* Copyright 2005 Loren M. Frank
*
* This program is part of the trodes data acquisition package.
* trodes is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* trodes is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with nspike; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "include/sharedinclude.h"

int TriggerOutput(int output)
{
    unsigned short command[FS_MAX_COMMAND_LEN];
    int len = 0;
    u32 tmptime;

    /* to trigger an output we send in a seqence of commands to the current 
     * state machine 
    // initialize the wait command
    command[len++] = DIO_S_WAIT; 
    command[len++] = DIO_S_SET_OUTPUT_HIGH | output;
    len += AddWaitToCommand(digioinfo.length[output], command+len, &tmptime);
    command[len++] = DIO_S_SET_OUTPUT_LOW | output;
    /* send the command off to the DSP 
    if (!WriteDSPDIOCommand(command, len)) { 
        sprintf(tmpstring, "Error writing DIO command to master DSP");
        DisplayErrorMessage(tmpstring);
	return 0;
    }

    digioinfo.raised[output] = 0;

    sprintf(tmpstring, "Output triggered on port %d for %f ms", output / MAX_DIO_PORTS, 
	    (float) digioinfo.length[output] / 10.0);
    DisplayStatusMessage(tmpstring); */
    return 1;
}

int TriggerOutputs(int *bit, int *length, int *delay, int *percent, int n)
    /* set up a command to trigger the specified outputs.  Returns 0 if the
     * first bit is triggered and 0 otherwise */
{
    unsigned short command[FS_MAX_COMMAND_LEN];
    int len = 0;

    int times[128];
    int bitind[128];
    int i;
    int j;
    int nelem;
    int tmp;
    int ret;
    int bitnum;
    int tdiff;
    u32 tmptime;


    /* make a list of the times when things need to happen and a parallel list of the bit
     * numbers with 0&1 for start and stop times of bit 0, 2&3 for bit 1 and so on */
    tmp = 0;
    nelem = 0;
    for (i = 0; i < n; i++) {
	/* calculate the percentage and add this if it passes */
        if ((drand48() * 100) <=  percent[i]) {
	    times[tmp] = delay[i];
	    bitind[tmp] = tmp;
	    tmp++;
	    times[tmp] = delay[i] + length[i];
	    bitind[tmp] = tmp;
	    tmp++;
	    nelem += 2;
	    if (i == 0) {
		ret = 1;
	    }
	}
    }

    /* now we need to sort the two lists in parallel */
    for (i = 0; i < nelem; i++) {
	for (j = 0; j < nelem-i-1; j++) {
	    if (times[j] > times[j+1]) {
		// swap values in both lists
		tmp = times[j];
		times[j] = times[j+1];
		times[j+1] = tmp;
		tmp = bitind[j];
		bitind[j] = bitind[j+1];
		bitind[j+1] = tmp;
	    }
	}
    }

    /* now we can create the statemachine command.  Note that we have to subtract the
     * cumulative time to get the wait lengths right 
    // initizize the wait 
    command[len++] = DIO_S_WAIT;
    tmp = 0;
    for (i = 0; i < 2*n; i++) {
	tdiff = times[i] - tmp;
	if (tdiff > 0) {
	    // add a wait to get us up to the time of the next change in state
	    len += AddWaitToCommand(tdiff, command+len, &tmptime);
	    tmp += tdiff;
	}
	// add the change of the appropriate bit 
	bitnum = bitind[i] / 2;
	/* check that this is a valid bit 
	fprintf(stderr, "tmp = %d, times[%d] = %d, bitind[%d] = %d, bit[%d] = %d\n", tmp, i, times[i], i, bitind[i], bitnum, bit[bitnum]);
	if (bit[bitnum] != -1) {
	    if ((bitind[i] % 2) == 0) {
		// turn the bit on
		command[len++] = DIO_S_SET_OUTPUT_HIGH | bit[bitnum];
		fprintf(stderr, "bit %d on\n", bit[bitnum]);
	    }
	    else {
		// turn the bit off
		command[len++] = DIO_S_SET_OUTPUT_LOW | bit[bitnum];
		fprintf(stderr, "bit %d off\n", bit[bitnum]);
	    }
	}
    }


    /* send the command off to the DSP 
    if (!WriteDSPDIOCommand(command, len)) { 
        sprintf(tmpstring, "Error writing DIO command to master DSP");
        DisplayErrorMessage(tmpstring);
	return 0;
    } */
    return ret;
}

void ChangeOutput(int output, int raise)
{
    unsigned short command[FS_MAX_COMMAND_LEN];

    /* to trigger an output we send in a seqence of commands to the current 
     * state machine 
    if (raise) {
	command[0] = DIO_S_SET_OUTPUT_HIGH | output;
    }
    else {
	command[0] = DIO_S_SET_OUTPUT_LOW | output;
    }
    /* send the command off to the DSP 
    if (!WriteDSPDIOCommand(command, 1)) { 
        sprintf(tmpstring, "Error writing digital IO command to master DSP");
        DisplayErrorMessage(tmpstring);
	return;
    }
    if (raise) {
	sprintf(tmpstring, "Output on port %d raised", output);
    }
    else {
	sprintf(tmpstring, "Output on port %d lowered", output);
    }
    digioinfo.raised[output] = raise;
    DisplayStatusMessage(tmpstring);

    /*ReadDSPData(0, DSP_SRAM, DIO_OUT_1, 10, read_data);
    fprintf(stderr, "DIO_OUT/IN data: ");
    for (i = 0; i < 10; i++) {
	fprintf(stderr, "%X ",read_data[i]);
    } */

    return;
}

