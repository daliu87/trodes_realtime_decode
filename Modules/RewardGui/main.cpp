#include "rewardControl.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    rewardControl r(a.arguments());
    r.show();

    return a.exec();
}
