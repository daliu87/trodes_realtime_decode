/* Include files that are useful for all control GUIs */

#ifndef __SHARED_INCLUDE_H_
#define __SHARED_INCLUDE_H_


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <limits.h>

//#include "sharedstruct.h"
#include "sharedfunctions.h"

#define MAX_BITS	16	// the maximum number of Digital IO bits supported by trodes
#define FS_MAX_COMMAND_LEN   32767

#define PI     3.1415
#define TWOPI  6.2830

#define TRUE    1
#define FALSE   0

#endif
