include(../../module_defaults.pri)

QT       += core gui network xml widgets

TARGET = SimpleMonitor
TEMPLATE = app

CONFIG += c++11

INCLUDEPATH += ../../../Trodes/src-config
INCLUDEPATH += ../../../Trodes/src-main

SOURCES += main.cpp\
    ../../../Trodes/src-main/trodesSocket.cpp \
    ../../../Trodes/src-config/configuration.cpp \
    ../../../Trodes/src-main/eventHandler.cpp \
    ../../../Trodes/src-main/trodesdatastructures.cpp \
    trodesDataInterface.cpp \
    mainWindow.cpp


HEADERS  += \
    ../../../Trodes/src-main/trodesSocket.h \
    ../../../Trodes/src-main/trodesSocketDefines.h \
    ../../../Trodes/src-config/configuration.h \
    ../../../Trodes/src-main/eventHandler.h \
    ../../../Trodes/src-main/trodesdatastructures.h \
    trodesDataInterface.h \
    mainWindow.h \
    ui_mainWindow.h

FORMS += \
    mainwindow.ui 

