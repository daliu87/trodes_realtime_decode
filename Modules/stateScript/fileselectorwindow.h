/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FILESELECTORWINDOW_H
#define FILESELECTORWINDOW_H
#include <QtGui>
#include <QListWidget>
#include <QFileSystemModel>

class FileSelectorWindow : public QWidget
{
    Q_OBJECT

public:
    FileSelectorWindow(QWidget *parent = 0);
    QString getCurrentStateScriptSelection();
    QString getCurrentStateScriptFolder();
    QString getCurrentLocalScriptSelection();
    QString getCurrentLocalScriptFolder();

private:

    QListView *stateScriptSelector;
    QListView *localScriptSelector;
    QString scFilePath;
    QString localFilePath;
    QString editorPath;

    QProcess*   editorProgram;
    bool        isEditorOpen;

protected:
    void resizeEvent(QResizeEvent *);

public slots:

    void changeStateScriptFolder(QString);
    void changeLocalScriptFolder(QString);
    void stateScriptDoubleClick(QModelIndex index);
    void setEditorOpen();
    void setEditorClosed(int);
    void enableLocalSelector();
    void disableLocalSelector();

signals:
     void stateScriptSelectorClicked();
     void localScriptSelectorClicked();

};

#endif // FILESELECTORWINDOW_H
