#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
//#include <chrono>
#include <QDialogButtonBox>
#include <QMessageBox>
#include "trodesDataInterface.h"
#include "eventHandler.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QStringList arguments, QWidget *parent = 0);
    ~MainWindow();

public slots:
    void changeStatusBarMessage(QString);
    void changeTimestamp(quint8 dataType, quint32 time);
    void changeContinuousMessage(QString msg);

    void changePosition(qint16 x, qint16 y, qint16 linSeg, qreal linPos);
    void changeVelocity(qreal velocity);
    void recievedTimestamp(quint32 time);
    void recievedNumPacketsSent(int num);

    void updateEventList(QVector<TrodesEvent> evList);
    void printEventList(QVector<TrodesEvent> evList);
    void pressedTestButton(void);
    void subscribeToEvent(int eventInd);
    void printEvent(TrodesEvent ev);


private:
    TrodesMessageInterface *trodesMessages;
    Ui::MainWindow *ui;
    void changeTimestampLabel(QLabel *widget, quint32 time);
    quint32 currentTime;
    int packetsRecieved;
    avgCalculator<qint32> latencyAvg;
    QVector<TrodesEvent> eventList;

    QList<QString> actionList; //method action list
    //EventDialog *eventMenu;
    //EventConnectionDialog *eventConnectionMenu;
    EventHandler *eventHandler;
    int eventCounter;

    QMenu* menuHelp;
    QAction* actionAbout;

private slots:
    void about();
    void iniActionList(void);
    void callMethod(int actionIndex, TrodesEvent event);

};

#endif // MAINWINDOW_H
