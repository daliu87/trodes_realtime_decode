include(../../module_defaults.pri)

#CONFIG += debug
#CONFIG += console
CONFIG += warn_on
QT += opengl widgets xml multimedia multimediawidgets
UI_DIR = ./ui
MOC_DIR = ./moc
OBJECTS_DIR = ./obj

#The TRODES_CODE define is necessary to allow code to be excluded for modules
#DEFINES += TRODES_CODE
DEFINES += PHOTOMETRY_CODE

INCLUDEPATH += src-main
INCLUDEPATH += src-display
INCLUDEPATH += src-config
INCLUDEPATH += src-threads

#stuff specific for mac
macx {
INCLUDEPATH += Libraries/Mac
#HEADERS += "/Applications/National Instruments/NI-DAQmx Base/includes/NIDAQmxBase.h"
ICON        = src-main/trodesIcon.icns
OTHER_FILES += \
    src-main/Info.plist
QMAKE_INFO_PLIST += src-main/Info.plist

}


unix:!macx {
INCLUDEPATH += Libraries/Linux
INCLUDEPATH += /usr/local/include  #assuming ftdi lib is in /usr/local/lib
}

win32 {
RC_ICONS += trodesIcon.ico
INCLUDEPATH += Libraries/Windows
HEADERS += "C:\Program Files (x86)\National Instruments\NI-DAQ\DAQmx ANSI C Dev\include\NIDAQmx.h"
LIBS += "C:\Program Files (x86)\National Instruments\NI-DAQ\DAQmx ANSI C Dev\lib\msvc\NIDAQmx.lib"
}


HEADERS   += \
    src-config/configuration.h \
    src-main/iirFilter.h \
    src-main/globalObjects.h\
    src-main/mainWindow.h\
    src-threads/streamProcessorThread.h \
    src-threads/recordThread.h \
    src-display/streamDisplay.h \
    src-main/sourceController.h \
    src-threads/fileSourceThread.h \
    src-display/dialogs.h \
    ../../../Source/src-main/trodesSocket.h \
    ../../../Source/src-main/sharedVariables.h \
    ../../../Source/src-main/trodesSocketDefines.h \
    src-main/abstractTrodesSource.h \
    src-threads/niusbdaqThread.h \
    src-threads/carrierThread.h


SOURCES   += \
    src-main/iirFilter.cpp \
    src-config/configuration.cpp\
    src-main/main.cpp\
    src-main/mainWindow.cpp\
    src-threads/streamProcessorThread.cpp \
    src-threads/recordThread.cpp \
    src-display/streamDisplay.cpp \
    src-main/sourceController.cpp \
    src-threads/fileSourceThread.cpp \
    src-display/dialogs.cpp \
    ../../../Source/src-main/trodesSocket.cpp \
    src-main/abstractTrodesSource.cpp \
    src-threads/niusbdaqThread.cpp \
    src-threads/carrierThread.cpp



OTHER_FILES += \
    src-main/cocoaInitializer.mm





