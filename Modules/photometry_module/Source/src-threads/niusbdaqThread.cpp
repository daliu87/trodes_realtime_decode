/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "niusbdaqThread.h"
#include "globalObjects.h"


NIUSBDAQRuntime::NIUSBDAQRuntime(QObject *parent) {

    recording = false;

#ifdef WIN32
    taskHandle = 0;
    int error;
    error = DAQmxCreateTask("",&taskHandle);
    if (error != 0) {
         qDebug() << "Error in initiating NI DAQ 1";
         emit failure();
         emit finished();
         return;
    }
    error  = DAQmxCreateAIVoltageChan(taskHandle,"cDAQ1Mod2/ai25","",DAQmx_Val_Cfg_Default,-5.0,5.0,DAQmx_Val_Volts,NULL);
    if (error != 0) {
         qDebug() << "Error in initiating NI DAQ 2";
         emit failure();
         emit finished();
         return;
    }
    error  = DAQmxCfgSampClkTiming(taskHandle,"", hardwareConf->APDsamplingRate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, 1000);
    if (error != 0) {
         qDebug() << "Error in initiating NI DAQ 3";
         emit failure();
         emit finished();
         return;
    }
#endif

}

NIUSBDAQRuntime::~NIUSBDAQRuntime() {
#ifdef WIN32
    DAQmxClearTask(taskHandle);
#endif
}

void NIUSBDAQRuntime::Run() {

    qDebug() << "NI handle input loop running....";

    int error = 0;
    int tic = 0;
    int tempMax;
    int maxAvailable = 0;
    quitNow = false;

#ifdef WIN32
    int32	read;
    float64 tempraw[hardwareConf->NFIBER*100];
    error  = DAQmxStartTask(taskHandle);
    if (error != 0) {
       qDebug() << "Error in start NI analog";
       emit failure();
       emit finished();
       return;
    }
#else
    QElapsedTimer loopTimer;
    qint64 optimalLoopTime_nS = (qint64)(1/((double) hardwareConf->APDsamplingRate)*1000000000);
    qint64 loopTimeSurplus_nS;
    int	read = 1;
#endif

    while (quitNow != true) {

#ifdef WIN32
        error = DAQmxReadAnalogF64(taskHandle, -1, 1, DAQmx_Val_GroupByScanNumber,
              tempraw, 1, &read, NULL);
#else
//        QThread::usleep(20);
        loopTimer.restart();
#endif

        if (error != 0) {
            qDebug() << "empty NI analog";
            quitNow = true;
        } else {

#ifdef WIN32
            for (int j = 0; j < read; j++){
                for (int i = 0; i < hardwareConf->NFIBER; i++) {
                    double xxxx = tempraw[j*hardwareConf->NFIBER+i]*DOUBLE2INT64_COEFFICENT;
                    rawData.data_f[rawData.writeIdx*hardwareConf->NFIBER+i] = xxxx;
                }
                if(recording){
                    currentTimeStamp++;
                }
                rawData.timestamps[rawData.writeIdx] = currentTimeStamp;
                rawData.data_f_ph[rawData.writeIdx] = rawData.outputIdx;
                rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
            }
#else
            for (int j = 0; j < read; j++){
                for (int i = 0; i < hardwareConf->NFIBER; i++) {
                    rawData.data_f[rawData.writeIdx*hardwareConf->NFIBER+i] = rawData.simulateData[rawData.outputIdx]*DOUBLE2INT64_COEFFICENT;
                }
                rawData.timestamps[rawData.writeIdx] = currentTimeStamp;
                if(recording){
                    currentTimeStamp++;
                }
                rawData.data_f_ph[rawData.writeIdx] = rawData.outputIdx;
                rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
            }
#endif
            for (int a = 0; a < rawDataAvailable.length(); a++) {
                rawDataAvailable[a]->release(read);
            }
            rawDataWritten = rawDataWritten+read;
            if ((tempMax = rawDataAvailable[0]->available()) > maxAvailable){
                maxAvailable = tempMax;
            }
            if ((++tic % 10000) == 0) {
                maxAvailable = 0;
            }

        }
#ifndef WIN32
        loopTimeSurplus_nS = optimalLoopTime_nS-loopTimer.nsecsElapsed();

        if (loopTimeSurplus_nS > 1000) {
            QThread::usleep(loopTimeSurplus_nS/1000);
        }
#endif
    }

#ifdef WIN32
    DAQmxStopTask(taskHandle);
#endif

    qDebug() << "NI input loop finished.";
    emit finished();

}

NIUSBDAQInterface::NIUSBDAQInterface(QObject *) {

  state = SOURCE_STATE_NOT_CONNECTED;
  NIUSBDataProcessor = NULL;

}

NIUSBDAQInterface::~NIUSBDAQInterface() {


}

void NIUSBDAQInterface::InitInterface() {

  connectErrorThrown = false;
  NIUSBDataProcessor = new NIUSBDAQRuntime(NULL);
  setUpThread(NIUSBDataProcessor);
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);

}

void NIUSBDAQInterface::StartAcquisition(void) {

    emit startRuntime();
    rawData.writeIdx = 0;
    NIUSBDataProcessor->aquiring = true;
    emit acquisitionStarted();
    state = SOURCE_STATE_RUNNING;
    emit stateChanged(SOURCE_STATE_RUNNING);

}

void NIUSBDAQInterface::StopAcquisition(void) {

    NIUSBDataProcessor->quitNow = true;
    QThread::msleep(100);

    if (!connectErrorThrown) {
        InitInterface();
    } else {
        CloseInterface();
    }

    emit acquisitionStopped();
    state = SOURCE_STATE_INITIALIZED;
    emit stateChanged(SOURCE_STATE_INITIALIZED);
    NIUSBDataProcessor->aquiring = false;
}

void NIUSBDAQInterface::CloseInterface(void) {

  if (state == SOURCE_STATE_INITIALIZED || connectErrorThrown) {

    if (NIUSBDataProcessor != NULL) {
        NIUSBDataProcessor->quitNow = true;
    }

    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);

    } else if (state == SOURCE_STATE_RUNNING) {
        StopAcquisition();
    }

}

void NIUSBDAQInterface::startRecord(void) {
    NIUSBDataProcessor->recording = true;
}

void NIUSBDAQInterface::stopRecord(void) {
    NIUSBDataProcessor->recording =false;
}
