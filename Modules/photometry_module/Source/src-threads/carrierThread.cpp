/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "carrierThread.h"
#include "globalObjects.h"


#ifdef WIN32
int32 CVICALLBACK SampleCallback (TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);
#endif

carrierRuntime::carrierRuntime(QObject *parent){

    loopFinished = true;

    for (int i = 0; i < photometryConf->nfibers.length(); i++){
        waveFrequency_1.push_back(photometryConf->nfibers[i]->carrierFreq[0]);
        waveFrequency_2.push_back(photometryConf->nfibers[i]->carrierFreq[1]);
    }

    generateData();

#ifdef WIN32

    taskHandle = 0;
    int error;
    error = DAQmxCreateTask("",&taskHandle);
    if (error != 0) {
         qDebug() << "Error in initiating NI DAQ 1";
         emit failure();
         emit finished();
         return;
    }

    error  = DAQmxCreateAOVoltageChan(taskHandle, "cDAQ1Mod3/ao0:1", "", -5.0, 5.0, DAQmx_Val_Volts, NULL);
    if (error != 0) {
         qDebug() << "Error in initiating NI DAQ 2";
         emit failure();
         emit finished();
         return;
    }

    error = DAQmxCfgSampClkTiming(taskHandle, "", OUTPUT_BUFFER_SIZE, DAQmx_Val_Rising, DAQmx_Val_ContSamps, OUTPUT_BUFFER_SIZE);
    if (error != 0) {
         qDebug() << "Error in initiating NI DAQ 3";
         emit failure();
         emit finished();
         return;
    }

    error = DAQmxRegisterEveryNSamplesEvent (taskHandle, DAQmx_Val_Transferred_From_Buffer, 1, 0, SampleCallback, NULL);
    if (error != 0) {
         qDebug() << "Error in DAQmxRegisterEveryNSamplesEvent";
         emit failure();
         emit finished();
         return;
    }

#endif

}

carrierRuntime::~carrierRuntime() {
#ifdef WIN32
    DAQmxClearTask(taskHandle);
#endif
}

void carrierRuntime::Run() {

    qDebug() << "NI handle output loop running....";

    quitNow = false;

#ifdef WIN32
    int error = 0;
    int32 write;
    error = DAQmxWriteAnalogF64(taskHandle, OUTPUT_BUFFER_SIZE, 0, 10.0, DAQmx_Val_GroupByScanNumber, rawData.carrierData, &write, NULL);
    if (error != 0) {
         qDebug() << "Error in starting NI DAQ output";
         emit failure();
         emit finished();
    }
    DAQmxStartTask(taskHandle);
    while (quitNow != true) {
        QThread::msleep(100);
    }
    DAQmxStopTask(taskHandle);
#else
    QElapsedTimer loopTimer;
    qint64 optimalLoopTime_nS = (qint64)(1/((double) OUTPUT_BUFFER_SIZE)*1000000000);
    qint64 loopTimeSurplus_nS;

    while (quitNow != true) {

        loopTimer.restart();

        rawData.outputIdx = (rawData.outputIdx + 1) % (OUTPUT_BUFFER_SIZE);

        loopTimeSurplus_nS = optimalLoopTime_nS-loopTimer.nsecsElapsed();

        if (loopTimeSurplus_nS > 1000) {
            QThread::usleep(loopTimeSurplus_nS/1000);
        }

    }
#endif
    qDebug() << "NI output loop finished.";
    emit finished();

}

void carrierRuntime::generateData() {

    int nf = hardwareConf->NFIBER;
    int i;
    int j;

    for(i = 0; i < OUTPUT_BUFFER_SIZE; i++){
        for (j = 0; j < nf; j++){
            rawData.data_mod[j*2+  i*2*nf] = cos((double)i*2.0*waveFrequency_1.at(j)*M_PI/OUTPUT_BUFFER_SIZE);
            rawData.data_mod[j*2+1+i*2*nf] = cos((double)i*2.0*waveFrequency_2.at(j)*M_PI/OUTPUT_BUFFER_SIZE);
        }
    }

    for(i = 0; i < OUTPUT_BUFFER_SIZE; i++){

#ifdef WIN_32
        rawData.carrierData[i*2]   = cos((double)i*2.0*waveFrequency_1.at(0)*M_PI/OUTPUT_BUFFER_SIZE)*0.1+0.3;
        rawData.carrierData[i*2+1] = cos((double)i*2.0*waveFrequency_2.at(0)*M_PI/OUTPUT_BUFFER_SIZE)*0.1+0.3;
        rawData.carrier[i] = rawData.carrierData[i*2]*
            (0.1*sin((double)i*2*2*M_PI/OUTPUT_BUFFER_SIZE)+1)
            + rawData.carrierData[i*2+1]*
            (0.1*sin((double)i*2*5*M_PI/OUTPUT_BUFFER_SIZE)+1);
#else
        rawData.carrierData[i*2]   = (cos((double)i*2.0*waveFrequency_1.at(0)*M_PI/OUTPUT_BUFFER_SIZE)*2+2.5);
//                *(0.1*sin((double)i*2*4*M_PI/OUTPUT_BUFFER_SIZE)+0.5);
        rawData.carrierData[i*2+1] = (cos((double)i*2.0*waveFrequency_2.at(0)*M_PI/OUTPUT_BUFFER_SIZE)*2+2.5);
//                *(0.1*sin((double)i*2*6*M_PI/OUTPUT_BUFFER_SIZE)+0.5);

#endif
    }

}

carrierInterface::carrierInterface(QObject *) {

    state = SOURCE_STATE_NOT_CONNECTED;
    acquisitionThread = NULL;

}

carrierInterface::~carrierInterface() {


}

void carrierInterface::InitInterface() {

    acquisitionThread = new carrierRuntime(NULL);
    setUpThread(acquisitionThread);
    state = SOURCE_STATE_INITIALIZED;

}

void carrierInterface::StartAcquisition(void) {

    emit startRuntime();
    rawData.outputIdx = 0;
    acquisitionThread->aquiring = true;
    state = SOURCE_STATE_RUNNING;

}

void carrierInterface::StopAcquisition(void) {

    acquisitionThread->quitNow = true;
    QThread::msleep(100);
    state = SOURCE_STATE_INITIALIZED;
    acquisitionThread->aquiring = false;

}

void carrierInterface::CloseInterface(void) {

    if (state == SOURCE_STATE_RUNNING)
        StopAcquisition();
    if (state == SOURCE_STATE_INITIALIZED) {
        acquisitionThread->quitNow = true;
        qDebug() << "Closed device.";
    }

}

#ifdef WIN32
int32 CVICALLBACK SampleCallback (TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData)
{
    rawData.outputIdx = (rawData.outputIdx + 1) % OUTPUT_BUFFER_SIZE;
    return 0;
}
#endif
