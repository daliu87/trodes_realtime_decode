/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "streamProcessorThread.h"
#include "globalObjects.h"
#include "../../../Source/src-main/trodesSocket.h"
#include "time.h"


StreamProcessorManager::StreamProcessorManager(QWidget *parent) :
    QObject(parent),
    neuralDataMinMax(NULL)
{

    neuralDataMinMax = new vertex2d*[hardwareConf->NCHAN]; // some might be dead
    for (int i=0; i < hardwareConf->NFIBER*4; i++)
        neuralDataMinMax[i] = new vertex2d[EEG_TIME_POINTS * 2]; // one for min, one for max
    gotSourceFailSignal = false;

    QList<int> nFiberList;
    for (int trodeCount = 0; trodeCount < photometryConf->nfibers.length(); trodeCount++) {
        nFiberList.push_back(trodeCount);
    }

    if (nFiberList.length() > 0) {
        createNewProcessorThread(nFiberList);
        qDebug() << "Starting new streamProcessorThread for nTrodes" << nFiberList;
    }  
}

StreamProcessorManager::~StreamProcessorManager()
{
    for (int i=0; i < hardwareConf->NFIBER*4; i++)
        delete [] neuralDataMinMax[i];
    delete [] neuralDataMinMax;
}

void StreamProcessorManager::sourceFail() {
    if (!gotSourceFailSignal) {
        gotSourceFailSignal = true;
        emit sourceFail_Sig();
    }
}

void StreamProcessorManager::createNewProcessorThread(QList<int> nFiberList)
{
    //Creates a new thread and streamProcessor object
    int groupNumber = streamProcessors.length();
    processorThreads.push_back(new QThread);
    processorThreads.last()->setObjectName("Trodes-Processor");
    streamProcessors.push_back(new StreamProcessor(0, groupNumber, nFiberList, this));
    connect(streamProcessors.last(), SIGNAL(sourceFail()),this,SLOT(sourceFail()));
    streamProcessors.last()->moveToThread(processorThreads.last());
    connect(processorThreads.last(), SIGNAL(started()), streamProcessors.last(), SLOT(setUp()));
    connect(streamProcessors.last(), SIGNAL(bufferOverrun()), this, SIGNAL(bufferOverrun()));
    connect(this, SIGNAL(startAllProcessorLoops()), streamProcessors.last(), SLOT(runLoop()));
    processorThreads.last()->start();
}

void StreamProcessorManager::startAcquisition()
{

    gotSourceFailSignal = false;
    qDebug() << "StreamProcessorManager::startAcquisition";
    emit startAllProcessorLoops(); //sends signal to all processor threads to start their loops

}

void StreamProcessorManager::stopAcquisition()
{
    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->quitNow = 1;
    }
}

void StreamProcessorManager::removeAllProcessors()
{
    stopAcquisition();

    for (int i = 0; i < streamProcessors.length(); i++) {
        processorThreads[i]->quit();
        processorThreads[i]->deleteLater();
        streamProcessors[i]->deleteLater();
    }
    QThread::msleep(100);


}

void StreamProcessorManager::updateDataLength(double tlength)
{
    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->newDataLength = hardwareConf->APDsamplingRate * tlength;
        streamProcessors[i]->updateDataLengthFlag = true;
    }
}

StreamProcessor::StreamProcessor(QObject*, int groupNumber, QList <int> nTList, StreamProcessorManager* managerPtr) :
    groupNum(groupNumber),
    nFiberList(nTList),
    streamManager(managerPtr)
{
    dataLength = hardwareConf->APDsamplingRate * streamConf->tLength;
    isLooping = false;
    isSetup = false;

    nChan = 0;
    for (int trode = 0; trode < nTList.length(); trode++) {
        nChan = nChan + photometryConf->nfibers[trode]->hw_chan.length();
    }

}

StreamProcessor::~StreamProcessor()
{

}

void StreamProcessor::setUp() {

    //Calculate how many data points go into one display pixel on the stream plot
    raw_increment.resize(EEG_TIME_POINTS);
    for (int j = 0; j < EEG_TIME_POINTS; j++) {
        raw_increment[j] = (int)((double)(j + 1) * (double)dataLength / (double)EEG_TIME_POINTS) -
                           (int)((double)j * (double)dataLength / (double)EEG_TIME_POINTS);
    }

    //Create a new semaphore for this group of channels.
    //if this is not the first time a workspace has been loaded, then there will already be semaphores in place.
    //Only create more if they are needed.
    rawDataAvailableMutex.lock();
    while (groupNum >= rawDataAvailable.length()) {
        rawDataAvailable.append(new QSemaphore);
    }
    rawDataAvailableMutex.unlock();

    for (int trode = 0; trode < nFiberList.length(); trode++) {
        int nt = nFiberList.at(trode);
        for (int c = 0; c < photometryConf->nfibers[nt]->hw_chan.length(); c++) {
            filtersOn.push_back(photometryConf->nfibers[nt]->filterOn[c]);
        }
    }

    rawDataAvailableIdx = groupNum;
    for (int nt = 0; nt < nFiberList.length(); nt++) {

        for (int c = 0; c < photometryConf->nfibers[nFiberList.at(nt)]->hw_chan.length(); c++) {
            int ch = photometryConf->nfibers[nFiberList.at(nt)]->hw_chan[c];
//            streamManager->neuralDataMinMax[ch].resize((int)EEG_TIME_POINTS * 2);
            for (int j = 0; j < EEG_TIME_POINTS; j++) {
//                streamManager->neuralDataMinMax[ch][2 * j].x = (double)j * (double)dataLength / (double)EEG_TIME_POINTS;
                streamManager->neuralDataMinMax[ch][2 * j].x = (double)j; // matches graphics scaling
                streamManager->neuralDataMinMax[ch][2 * j].y = 0;
//                streamManager->neuralDataMinMax[ch][(2 * j) + 1].x = (double)j * (double)dataLength / (double)EEG_TIME_POINTS;
                streamManager->neuralDataMinMax[ch][(2 * j) + 1].x = (double)j;
                streamManager->neuralDataMinMax[ch][(2 * j + 1)].y = 0;
            }
        }
    }

    rawIdx = 0;
    dataIdx = 0;

    // add that dataProvided structure to the main list

    quitNow = 0;
    updateDataLengthFlag = false;

    isSetup = true;
}

void StreamProcessor::updateDataLength(void)
{
    // Note that this is safe only because we've chosen to have a constant number of points for
    // the time base of the display. We would need to be more careful if raw_increment was going
    // to change size, in order to not have thread clashes....
    qDebug() << "Old dataLength " << dataLength;
    qDebug() << "New dataLength " << newDataLength;

    dataLength = newDataLength;
    raw_increment.resize(dataLength);

    for (int j = 0; j < EEG_TIME_POINTS; j++) {
        raw_increment[j] = (int)((double)(j + 1) * (double)dataLength / (double)EEG_TIME_POINTS) -
                           (int)((double)j * (double)dataLength / (double)EEG_TIME_POINTS);

        //There is really no need to update the xaxis on the traces, so for now we skip this step
        //This means that the xlabels are fixed to whatever values they had in the constructor,
        //and as long as we do not update them in the stream display widget, everything should work fine.
    }
    dataIdx = 0;
    qDebug() << "Set data length";
}

void StreamProcessor::runLoop()
{
    //This is where the raw data is filtered, and where the streaming display is calculated.
    //In each display bin, we draw a vertical line connecting the minumim and maximum collected values
    //in that time bin.
    //When the 'stream from source' menu is chosen, this function is executed via an emitted signal
    //from the source controller

    int samplesToCopy = 0;

    quitNow = 0;
    isLooping = true;
    bool exitLoop = false;

    int tmpDataPoint = 0;
    int tmpMaxPoint = 0;
    int tmpMinPoint = 0;
    int rdInd;
    double tmpDataPoint_d = 0;
    rawIdx = 0;
    dataIdx = 0;
    numLoopsWithNoData = 0;

    int streamIdx = 0;
    int hw_chan = 0;
    int stream_inc = 0;

    uint32_t timestamp;

    qDebug() << "Starting stream processor";

    QThread::usleep(100); //This appears to solve a race condition that sometimes occurs.  TODO: a real solution

    while (!exitLoop) {
        if (rawDataAvailable[rawDataAvailableIdx]->tryAcquire(1, 100)) {
            samplesToCopy = rawDataAvailable[rawDataAvailableIdx]->available();
            if (samplesToCopy > 15000) {
                emit bufferOverrun();
            }
            if (!rawDataAvailable[rawDataAvailableIdx]->tryAcquire(samplesToCopy))
                qDebug() << "Error acquiring available samples, group " << groupNum;

            samplesToCopy += 1; // we acquired one at the beginning

            for (int s = 0; s < samplesToCopy; s++) {
                timestamp = rawData.timestamps[rawIdx];
                streamIdx = 0;
                for (int n = 0; n < nFiberList.length(); n++) {
                    rdInd = rawIdx*hardwareConf->NFIBER + n;
                    int nt = nFiberList.at(n);
                    for (int c = 0; c < photometryConf->nfibers[nt]->hw_chan.length(); c++) {
                        hw_chan = photometryConf->nfibers[nt]->hw_chan[c];
                        tmpDataPoint = rawData.data_f[rdInd]/DOUBLE2INT64_COEFFICENT*500;

                        if (tmpDataPoint == -32768) {
                            //This is a NAN, replace with 0
                            tmpDataPoint = 0;
                        }

                        if (filtersOn[c] && linkStreamToFilters) {
                            tmpDataPoint_d = rawData.data_f[rdInd]/DOUBLE2INT64_COEFFICENT*500;
                            tmpDataPoint_d = streamConf->photometryFilters[hw_chan].addValue_hp(tmpDataPoint_d);
                            tmpDataPoint_d = rawData.carrierData[2*rawData.data_f_ph[rdInd]+photometryConf->nfibers[nt]->cidx[c]-1]
                                    *tmpDataPoint_d;
                            tmpDataPoint_d = streamConf->photometryFilters[hw_chan].addValue_lp(tmpDataPoint_d);
                            tmpDataPoint = qAbs(tmpDataPoint_d)*100;
                        }

                        tmpMaxPoint = qMin(tmpDataPoint, 5000);
                        tmpMinPoint = qMax(tmpDataPoint,-5000);

                        //Calulate the current max and min values in the current display bin
                        if (stream_inc > 0) {
                            //Old bin, so compare to existing value
                            streamManager->neuralDataMinMax[hw_chan][dataIdx * 2].y =
                                    qMax((GLfloat)tmpMaxPoint, streamManager->neuralDataMinMax[hw_chan][dataIdx * 2].y);
                            streamManager->neuralDataMinMax[hw_chan][(dataIdx * 2) + 1].y =
                                    qMin((GLfloat)tmpMinPoint, streamManager->neuralDataMinMax[hw_chan][(dataIdx * 2) + 1].y);
                        }
                        else {
                            //New bin, so reset the value
                            streamManager->neuralDataMinMax[hw_chan][dataIdx * 2].y = tmpMaxPoint;
                            streamManager->neuralDataMinMax[hw_chan][(dataIdx * 2) + 1].y = tmpMinPoint;
                        }
                        streamIdx++; // this keeps track of which row in the

                    }
                }

                rawIdx = (rawIdx + 1) % EEG_BUFFER_SIZE;
                if (++stream_inc >= raw_increment[dataIdx]) {
                    dataIdx = (dataIdx + 1) % EEG_TIME_POINTS;
                    stream_inc = 0;
                }

            }

        } else if (quitNow == 1) {
            exitLoop = true;
            isLooping = false;
        } else {
            numLoopsWithNoData++;
            if (numLoopsWithNoData > 20000) {
                numLoopsWithNoData = 0;
                qDebug() << "No data!";
            }
        }

        if (updateDataLengthFlag.testAndSetAcquire(true, false)) {
            updateDataLength();
        }

    }

    qDebug() << "Stream processor loop ended.";
}
