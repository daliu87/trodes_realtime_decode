/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IIRFILTER_H
#define IIRFILTER_H

#include <QObject>
#include "stdint.h"
#include <QHash>

class BesselFilter
{
public:
    BesselFilter();
    ~BesselFilter();
    int16_t addValue(int16_t value);
    void calcCoef();
    void setFilterRange(int lower, int upper);
    void setSamplingRate(int rate);
    void resetHistory();

private:

    QHash <QString, QHash<int, double> > aCoefHash;

    QHash <QString, QVector<double> > aCoefHashVect;
    QHash <QString, double> gainHash;

    double inputFreqHz;
    int16_t* outputBuffer;
    int16_t actualOutputValue;

    int outputBufferSize;
    int outputBufferWritePos;
    quint64 dataWritten;
    int lowerCutoffHz;
    int upperCutoffHz;

    double xv[3];
    double yv[5];

    double bcoef[3];
    double acoef[4];
    double gain;

    bool isLowPass;

};

class ButterworthFilter
{
public:
    ButterworthFilter();
    ~ButterworthFilter();
    double addValue_hp(double value);
    double addValue_lp(double value);
    void calcCoef();
    void setFilterRange(int lower, int upper);
    void setSamplingRate(int rate);
    void resetHistory();

private:

    QHash <QString, QHash<int, double> > denominatorHash;
    QHash <QString, QHash<int, double> > numeratorHash;
    QHash <QString, double> gainHash;

    int16_t currentValue;

    double inputFreqHz;
    double hpfilterInput_section[12];
    double hpfilterOutput_section[12];
    double lpfilterInput_section[12];
    double lpfilterOutput_section[12];

    double* outputBuffer;
    double actualOutputValue;

    double acof_lowpass[11];  //floating point form
    double bcof_lowpass[12];

    double acof_highpass[11];  //floating point form
    double bcof_highpass[12];


    int outputBufferSize;
    int outputBufferWritePos;
    quint64 dataWritten;

    int lowerCutoffHz;
    int upperCutoffHz;


};

#endif // IIRFILTER_H
