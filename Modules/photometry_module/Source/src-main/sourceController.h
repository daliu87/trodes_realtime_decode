/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SOURCECONTROLLER_H
#define SOURCECONTROLLER_H

#include <QtGui>
#include "dialogs.h"
#include "../../../Source/src-main/trodesSocket.h"
#include "carrierThread.h"
#include "fileSourceThread.h"
#include "niusbdaqThread.h"


#define SOURCE_STATE_NOT_CONNECTED 0
#define SOURCE_STATE_INITIALIZED   1
#define SOURCE_STATE_RUNNING       2
#define SOURCE_STATE_CONNECTERROR  3
#define SOURCE_STATE_PAUSED        4

#define SENDSTARTCOMMAND    97
#define SENDSTOPCOMMAND     98
#define SENDCHANNELCONFIGCOMMAND 99 //followed by 4 bytes designating which cards are active (up to 32)
#define SENDDIGITALOUTSTATE 10 // followed by one byte with the channel and another byte with the state (0 or 1)
#define SENDANALOGOUTSTATE  20 // followed by one byte with the channel and then two bytes with the value (16 bits)
#define SENDSTATESCRIPTCHARACTER 30 // follwed by one byte with the character to send to stateScript
#define SENDSTATESCRIPTFUNCTIONTRIGGER 31 // followed by one byte with the function number to trigger

#define EEG_BUFFER_SIZE     20000
#define OUTPUT_BUFFER_SIZE  60000

typedef struct {

  int16_t   data[EEG_BUFFER_SIZE * 1024];
  int64_t   data_f[EEG_BUFFER_SIZE * 6];
  uint16_t  data_f_ph[EEG_BUFFER_SIZE];
  uint32_t  timestamps[EEG_BUFFER_SIZE];
  int16_t   digitalInfo[EEG_BUFFER_SIZE*20];
  double    dTime[EEG_BUFFER_SIZE];
  int      writeIdx;
  int      outputIdx;

#ifdef WIN32
  float64   data_mod[OUTPUT_BUFFER_SIZE * 6 * 2];
  float64   carrierData[OUTPUT_BUFFER_SIZE * 2];
#else
  double    data_mod[OUTPUT_BUFFER_SIZE * 6 * 2];
  double    carrierData[OUTPUT_BUFFER_SIZE * 2];
  double    simulateData[OUTPUT_BUFFER_SIZE];
#endif


} eegDataBuffer;

enum DataSource {
    SourceNone, SourceFile, SourceEthernet, SourceRhythm
};
Q_DECLARE_METATYPE(DataSource)

class SourceController : public QObject {
  Q_OBJECT

public:

  SourceController(QObject *parent);
  fileSourceInterface       *fileSource; //File playback

  int               state;
  DataSource        currentSource;

private:
  int                   numConnectionTries;
  AbstractTrodesSource *currentSourceObj;
  AbstractTrodesSource *currentOutputObj;

public slots:

  //For messages going from the source back to Trodes mainwindow
  void StartAcquisition(void);
  void StopAcquisition(void);
  void PauseAcquisition(void);

  //For inbound commands going to the source
  void disconnectFromSource();
  void connectToSource();
  void pauseSource(); //file source only

  void setSource(DataSource source);
  void setSourceState(int);
  void clearBuffers();
  void waitForThreads();
  void dataError();
  void noDataComing();

  void SendTimeRequest();

  void  startRecord();
  void  stopRecord();

signals:
  void newTimeStamp(void);
  void stateChanged(int);
  void acquisitionStarted(void);
  void acquisitionStopped(void);
  void acquisitionPaused(void);
  void SDCardStatus(bool cardConnected, int numChan, bool unlocked, bool hasData);

  void  startRecording();
  void  stopRecording();


};

#endif // SOURCECONTROLLER_H
