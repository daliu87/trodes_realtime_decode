#ifndef ABSTRACTTRODESSOURCE_H
#define ABSTRACTTRODESSOURCE_H

#include <QtCore>
#include <QMessageBox>
#include "stdint.h"
#include "dialogs.h"


class AbstractSourceRuntime : public QObject {
  Q_OBJECT
public:
  AbstractSourceRuntime();
  virtual ~AbstractSourceRuntime() {}
  int quitNow;
  int aquiring;
  bool recording;

protected:
  void checkForCorrectTimeSequence();
  bool checkFrameAlignment(unsigned char*);
  void findNextSyncByte(unsigned char*);
  bool badFrameAlignment;
  unsigned int tempSyncByteLocation;
  unsigned int PACKET_SIZE;

private:
  uint32_t      lastTimeStamp;
  int           numConsecJumps;

public slots:
  virtual void Run(void) = 0;


signals:
  void NewData(void);
  void finished();
  void timeStampError();
  void failure();

};

class AbstractTrodesSource : public QObject {
  Q_OBJECT

public:
    AbstractTrodesSource();
    virtual     ~AbstractTrodesSource() {}
    void        setECUConnected(bool);
    bool        isThreadRunning();

protected:

  QThread       *workerThread;
  void          setUpThread(AbstractSourceRuntime* rtPtr);
  char          startCommandValue;
  bool          connectErrorThrown;
  bool          threadRunning;

protected slots:
  virtual void RunTimeError(void);
  void setThreadNotRunning();
  void setThreadRunning();

public slots:
  virtual void InitInterface(void) = 0;  // This gets called after constructor
  virtual void StartAcquisition(void) = 0;
  virtual void StopAcquisition(void) = 0;
  virtual void CloseInterface(void) = 0;
  virtual void SendSettleCommand(void);
  virtual void SendSDCardUnlock(void);
  virtual void ConnectToSDCard(void);
  virtual void ReconfigureSDCard(int numChannels);

signals:
  void stateChanged(int);
  void startRuntime(void);
  void acquisitionStarted(void);
  void acquisitionStopped(void);
  void acquisitionPaused(void);
  void timeStampError();
  void SDCardStatus(bool cardConnected,int numChan, bool unlocked, bool hasData);

  void newTimeStamp();



};


#endif // ABSTRACTTRODESSOURCE_H
