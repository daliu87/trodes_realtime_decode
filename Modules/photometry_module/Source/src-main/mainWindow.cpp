/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "mainWindow.h"

QGLFormat qglFormat;
bool linkStreamToFilters;
bool linkChangesBool;
bool exportMode;

MainWindow::MainWindow(QStringList arguments)
{

  quitting = false;

  if (objectName().isEmpty())
    setObjectName(QString("Main"));

  resize(800, 600);
  exportMode = false; //Whether or not data displays should be updated
  hardwareConf = new HardwareConfiguration(NULL); // this is requried for creating the audio generator, but this object

  setAutoFillBackground(true);
  soundDialogOpen = false;
  fileInitiated = false;
  recordFileOpen = false;
  dataStreaming = false;
  recording = false;
  timerTick = 0;
  channelsConfigured = false;
  visibleTime = 0;
  eventTabWasChanged = false;
  for (int i = 0; i < 64; i++) {
    eventTabsInitialized[i] = false;
  }
  eventTabsInitialized[0] = true;
  currentTrodeSelected = 0;
  singleTriggerWindowOpen = false;

  //qglFormat.setVersion(3,2);
  qglFormat.setProfile(QGLFormat::CoreProfile);
  qglFormat.setDoubleBuffer(true);

  statusbar = new QStatusBar(this);
  setStatusBar(statusbar);
  statusbar->showMessage(tr("Not connected to device"));
  //------------------------------------------


  //File menu--------------------------------
  menuFile = new QMenu;
  menuFile->setTitle("File");
  menuBar()->addAction(menuFile->menuAction());

  menuConfig = new QMenu;
  menuFile->addAction(menuConfig->menuAction());
  actionLoadConfig = new QAction(this);
  menuConfig->addAction(actionLoadConfig);
  actionCloseConfig = new QAction(this);
  actionCloseConfig->setEnabled(false);
  menuConfig->addAction(actionCloseConfig);

  actionCloseFile = new QAction(this);
  actionCloseFile->setEnabled(false);
  menuFile->addAction(actionCloseFile);
  menuFile->addSeparator();
  actionRecord = new QAction(this);
  actionRecord->setEnabled(false);
  menuFile->addAction(actionRecord);
  actionPause = new QAction(this);
  actionPause->setEnabled(false);
  menuFile->addAction(actionPause);
  actionPlay = new QAction(this);
  actionPlay->setEnabled(false);
  menuFile->addAction(actionPlay);
  menuFile->addSeparator();
  actionQuit = new QAction(this);
  actionQuit->setShortcut(Qt::CTRL + Qt::Key_Q);
  menuFile->addAction(actionQuit);


  connect(actionLoadConfig, SIGNAL(triggered()), this, SLOT(loadConfig()));
  connect(actionCloseConfig, SIGNAL(triggered()), this, SLOT(closeConfig()));
  QObject::connect(actionCloseFile, SIGNAL(triggered()), this, SLOT(closeFile()));
  connect(actionRecord,SIGNAL(triggered()),this,SLOT(actionRecordSelected()));
  connect(actionPause,SIGNAL(triggered()),this,SLOT(actionPauseSelected()));
  connect(actionPlay,SIGNAL(triggered()),this,SLOT(actionPlaySelected()));
  connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));


  //Connection menu--------------------------------
  menuSystem = new QMenu;
  menuSystem->setTitle("Connection");
  menuBar()->addAction(menuSystem->menuAction());
  sourceMenu = new QMenu;
  menuSystem->addAction(sourceMenu->menuAction());
  menuSystem->addSeparator();
  menuSimulationSource = new QMenu;
  menuSpikeGadgetsSource = new QMenu;
  actionSourceNone = new QAction(this);
  actionSourceFile = new QAction(this);
  actionSourceEthernet = new QAction(this);
  menuSpikeGadgetsSource->addAction(actionSourceEthernet);
  actionSourceNone->setCheckable(true);
  actionSourceNone->setChecked(true);
  actionSourceFile->setCheckable(true);
  actionSourceFile->setChecked(false);
  actionSourceEthernet->setCheckable(true);
  actionSourceEthernet->setChecked(false);
  sourceMenu->addAction(actionSourceNone);
  sourceMenu->addAction(actionSourceFile);
  sourceMenu->addAction(menuSimulationSource->menuAction());
  sourceMenu->addAction(menuSpikeGadgetsSource->menuAction());
  actionSourceNone->setData(QVariant::fromValue(SourceNone));
  actionSourceFile->setData(QVariant::fromValue(SourceFile));
  actionSourceEthernet->setData(QVariant::fromValue(SourceEthernet));
  actionConnect = new QAction(this);
  menuSystem->addAction(actionConnect);
  actionConnect->setEnabled(false);
  actionDisconnect = new QAction(this);
  menuSystem->addAction(actionDisconnect);
  actionDisconnect->setEnabled(false);
  actionClearBuffers = new QAction(this);
  menuSystem->addAction(actionClearBuffers);
  actionClearBuffers->setEnabled(false);
  menuSystem->addSeparator();

  QObject::connect(actionSourceNone,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFile,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceEthernet,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceEthernet,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionConnect, SIGNAL(triggered()), this, SLOT(connectToSource()));
  QObject::connect(actionDisconnect, SIGNAL(triggered()), this, SLOT(disconnectFromSource()));



  //Display menu-------------------------------------------
  menuDisplay = new QMenu;
  menuDisplay->setTitle("View");
  menuBar()->addAction(menuDisplay->menuAction());

  menuEEGDisplay = new QMenu;
  menuDisplay->addAction(menuEEGDisplay->menuAction());
  menuSetTLength = new QMenu(this);
  actionSetTLength0_2 = new QAction(this);
  actionSetTLength0_2->setData(0.2);
  actionSetTLength0_2->setCheckable(true);
  actionSetTLength0_2->setChecked(false);
  actionSetTLength0_5 = new QAction(this);
  actionSetTLength0_5->setData(0.5);
  actionSetTLength0_5->setCheckable(true);
  actionSetTLength0_5->setChecked(false);
  actionSetTLength1_0 = new QAction(this);
  actionSetTLength1_0->setData(1.0);
  actionSetTLength1_0->setCheckable(true);
  actionSetTLength1_0->setChecked(true);
  actionSetTLength2_0 = new QAction(this);
  actionSetTLength2_0->setData(2.0);
  actionSetTLength2_0->setCheckable(true);
  actionSetTLength2_0->setChecked(false);
  actionSetTLength5_0 = new QAction(this);
  actionSetTLength5_0->setData(5.0);
  actionSetTLength5_0->setCheckable(true);
  actionSetTLength5_0->setChecked(false);
  menuSetTLength->addAction(actionSetTLength0_2);
  menuSetTLength->addAction(actionSetTLength0_5);
  menuSetTLength->addAction(actionSetTLength1_0);
  menuSetTLength->addAction(actionSetTLength2_0);
  menuSetTLength->addAction(actionSetTLength5_0);
  menuEEGDisplay->addMenu(menuSetTLength);
  streamFilterMenu = new QMenu;
  menuEEGDisplay->addAction(streamFilterMenu->menuAction());
  streamFilterLink = new QAction(this);
  streamFilterLink->setCheckable(true);
  streamFilterLink->setChecked(true);
  streamFilterUnLink = new QAction(this);
  streamFilterUnLink->setCheckable(true);
  streamFilterUnLink->setChecked(false);
  streamFilterMenu->addAction(streamFilterLink);
  streamFilterMenu->addAction(streamFilterUnLink);
  linkStreamToFilters = true;

  connect(actionSetTLength0_2,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength0_5,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength1_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength2_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength5_0,SIGNAL(triggered()),this,SLOT(setTLength()));

  QObject::connect(streamFilterLink, SIGNAL(triggered()), this, SLOT(linkFilters()));
  QObject::connect(streamFilterUnLink, SIGNAL(triggered()), this, SLOT(unLinkFilters()));

  //Help menu-----------------------------------------
  actionAboutQT = new QAction(this);
  actionAboutQT->setMenuRole(QAction::AboutQtRole);
  menuHelp = new QMenu;
  menuBar()->addAction(menuHelp->menuAction());
  menuHelp->addAction(actionAboutQT);

  QObject::connect(actionAboutQT, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

  //Layouts and tabs-----------------------------------------
  mainLayout =  new QGridLayout();
  mainLayout->setContentsMargins(QMargins(10,0,10,0));
  mainLayout->setVerticalSpacing(3);
  tabs = new QTabWidget(this);
  tabs->setStyleSheet("QTabWidget::pane {margin: 0px}") ;
  tabs->setTabPosition(QTabWidget::West);
  mainLayout->addWidget(tabs,2,0);
  //---------------------------------------------------------

  //Top control panel setup----------------------------------
  headerLayout = new QGridLayout();
  headerLayout->setContentsMargins(QMargins(1,1,1,1));
  headerLayout->setHorizontalSpacing(3);


  int totalRightItems = 7;
  int totalLeftItems = 3;

  //Time display
  QTime mainClock(0,0,0,0);
  QFont labelFont;
  labelFont.setPixelSize(20);
  labelFont.setFamily("Console");
  timeLabel  = new QLabel;
  timeLabel->setText(mainClock.toString("hh:mm:ss.z"));
  timeLabel->setFont(labelFont);
  timeLabel->setMinimumWidth(100);
  timeLabel->setAlignment(Qt::AlignLeft);
  pullTimer = new QTimer(this);
  connect(pullTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
  pullTimer->start(100); //update timer every 100 ms
  headerLayout->addWidget(timeLabel,0,totalRightItems+totalLeftItems);

  //QString path = QCoreApplication::applicationFilePath();
  QString path = QApplication::applicationDirPath();
  QString slash("\\");

#if defined (__linux) || defined (__APPLE__)
  slash = "/";
#endif

    //Record, pause, and play buttons
    recordButton = new TrodesButton;
    pauseButton = new TrodesButton;
    playButton = new TrodesButton;
    QPixmap playPixmap(path + slash + "playImage.png");
    QPixmap pausePixmap(path + slash + "pauseImage.png");
    QPixmap recordPixmap(path + slash + "recordImage.png");
    QIcon recordButtonIcon(recordPixmap);
    QIcon pauseButtonIcon(pausePixmap);
    QIcon playButtonIcon(playPixmap);
    recordButton->setIcon(recordButtonIcon);
    recordButton->setRedDown(true);
    pauseButton->setIcon(pauseButtonIcon);
    playButton->setIcon(playButtonIcon);
    recordButton->setIconSize(QSize(15, 15));
    pauseButton->setIconSize(QSize(10, 10));
    playButton->setIconSize(QSize(15, 15));
    recordButton->setFixedSize(50, 20);
    pauseButton->setFixedSize(50, 20);
    playButton->setFixedSize(50, 20);
    recordButton->setToolTip(tr("Record"));
    pauseButton->setToolTip(tr("Pause"));
    playButton->setToolTip(tr("Play file"));
    recordButton->setEnabled(false);
    pauseButton->setEnabled(false);
    playButton->setEnabled(false);

    headerLayout->addWidget(recordButton, 0, 0);
    headerLayout->addWidget(pauseButton, 0, 1);
    headerLayout->addWidget(playButton, 0, 2);
    connect(recordButton, SIGNAL(pressed()), this, SLOT(recordButtonPressed()));
    connect(pauseButton, SIGNAL(pressed()), this, SLOT(pauseButtonPressed()));
    connect(playButton, SIGNAL(pressed()), this, SLOT(playButtonPressed()));
    connect(recordButton, SIGNAL(released()), this, SLOT(recordButtonReleased()));
    connect(pauseButton, SIGNAL(released()), this, SLOT(pauseButtonReleased()));
    connect(playButton, SIGNAL(released()), this, SLOT(playButtonReleased()));

    //File name label
    fileLabel = new QLabel;
    fileLabel->setMinimumWidth(200);
    QString FileLabelColor("gray");
    QString FileLabelText("No file open");
    QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
    fileString = fileLabelTextTemplate.arg(FileLabelColor, FileLabelText);
    fileLabel->setText(fileString);
    mainLayout->addWidget(fileLabel, 1, 0);
    headerLayout->setColumnStretch(totalLeftItems, 1);
    mainLayout->addLayout(headerLayout, 0, 0);
    //---------------------------------------------

    //when the state of the source stream changes, the menus need to reflect that
    sourceControl = new SourceController(NULL);
    connect(sourceControl, SIGNAL(stateChanged(int)), this, SLOT(setSourceMenuState(int)));

    QWidget *window = new QWidget();
    window->setLayout(mainLayout);
    setCentralWidget(window);
    QMetaObject::connectSlotsByName(this);
    retranslateUi();


    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("position")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    //Module
    filePositionSlider = new MySlider();
    filePositionSlider->setFocusPolicy(Qt::NoFocus);
    filePositionSlider->setOrientation(Qt::Horizontal);
    filePositionSlider->setFixedHeight(20);
    filePositionSlider->setMinimum(0);
    filePositionSlider->setMaximum(TIMESLIDERSTEPS);
    filePositionSlider->setSingleStep(1);
//    connect(filePositionSlider,SIGNAL(sliderMoved(int)),this,SLOT(playbackSliderPositionChanged(int)));
//    connect(filePositionSlider,SIGNAL(sliderPressed()),this,SLOT(playbackSliderPressed()));
//    connect(filePositionSlider,SIGNAL(sliderReleased()),this,SLOT(playbackSliderReleased()));
//    fileControlLayout->addWidget(filePositionSlider,0,1);

    clockRate = 30000;
    inputFileOpen = false;
    playbackMode = false;
    inputFileName = "";
    fileStartTime = 0;
    fileEndTime = 0;
    videoWasPlayingBeforeSliderPress = false;
    isVideoFilePlaying = false;
    isVideoSliderPressed = false;
    justSeekedFlag = false;
    loggingOn = false;
    fileOpen = false;

    int optionInd = 1;
    qDebug() << "arguments:" << arguments;
#ifndef WIN32
    arguments << "-trodesConfig" << "/Users/ziyingc/Dropbox/Lab/Trodes/Workspaces/Photometry/32_Singles_ECU_Fiber.trodesconf";
#else
    arguments << "-trodesConfig" << "C:/Users/Kemere_Lab_PC/Dropbox/Lab/Trodes/Workspaces/Photometry/32_Singles_ECU_Fiber.trodesconf";
#endif
    arguments << "-serverAddress"<< "127.0.0.1" << "-serverPort" << "56574";
    while (optionInd < arguments.length()) {
        if ((arguments.at(optionInd).compare("-trodesConfig",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            trodesConfigFile = arguments.at(optionInd+1);
            loadConfig(trodesConfigFile);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverAddress",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverAddress = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverPort",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverPortValue= arguments.at(optionInd+1);
            optionInd++;
        }
        optionInd++;
    }

    qDebug() << "Photometry module started";
    moduleNet = new TrodesModuleNetwork();
    moduleNet->dataProvided.dataType = TRODESDATATYPE_POSITION;
    bool connectionSuccess;
    if ((!serverAddress.isEmpty()) &&  (!serverPortValue.isEmpty())) {
        connectionSuccess = moduleNet->trodesClientConnect(serverAddress,serverPortValue.toUInt(), true);
    } else {
        connectionSuccess = moduleNet->trodesClientConnect(true);
    }

    if (!connectionSuccess) {
        currentOperationMode = PHOTOMETRYMODULE_STANDALONEMODE | PHOTOMETRYMODULE_FILEPLAYBACKMODE;
    } else {
        currentOperationMode = PHOTOMETRYMODULE_SLAVEMODE | PHOTOMETRYMODULE_CAMERASTREAMINGMODE;
    }

    if (connectionSuccess) {

        connect(moduleNet->trodesClient,SIGNAL(openFileEventReceived(QString)),     this,SLOT(openRecordDialog(QString)));
        connect(moduleNet->trodesClient,SIGNAL(sourceConnectEventRecieved(QString)),this,SLOT(setPhotometrySource(QString)));
        connect(moduleNet->trodesClient,SIGNAL(currentTimeReceived(quint32)),   this,SLOT(setTime(quint32)));
        connect(moduleNet->trodesClient,SIGNAL(timeRateReceived(quint32)),      this,SLOT(setTimeRate(quint32)));

//        connect(moduleNet->trodesClient,SIGNAL(startAquisitionEventReceived()), this,SLOT(startRecording()));
        connect(moduleNet->trodesClient,SIGNAL(startAquisitionEventReceived()), moduleNet->trodesClient,SLOT(sendTimeRequest()));

        connect(moduleNet->trodesClient,SIGNAL(stopAquisitionEventReceived()),  this,SLOT(stopRecording()));
        connect(moduleNet->trodesClient,SIGNAL(closeFileEventReceived()),       this,SLOT(closePFile()));
        connect(moduleNet->trodesClient,SIGNAL(quitCommandReceived()),          this,SLOT(close()));
        connect(moduleNet->trodesClient,SIGNAL(instanceReveivedFromServer(int)),this,SLOT(setModuleInstance(int)));

        connect(this, SIGNAL(recordingStarted()), sourceControl, SLOT(startRecord()));
        connect(this, SIGNAL(recordingStopped()), sourceControl, SLOT(stopRecord()));
//        connect(this, SIGNAL(recordingStarted()), moduleNet->trodesClient, SLOT(sendTimeRequest()));


        moduleNet->sendModuleName("Photometry");
        moduleNet->sendTimeRateRequest();
        moduleNet->sendCurrentStateRequest();

        qDebug() << "Starting in slave mode";

    }else{
        qDebug() << "Starting in standalone mode";
    }

}

MainWindow::~MainWindow()
{

}

void MainWindow::closeConfig()
{
    if (channelsConfigured) {

        disconnectFromSource();

        //this if statement prevents an infinite loop from occuring if switching from playback source
        if (!playbackFileOpen) {
            setSource(SourceEthernet);
        }
        emit endAllThreads();

        //remove each display tab
        while (tabs->count() > 0) {
            tabs->removeTab(0);
        }
        delete eegDisp;

        streamManager->removeAllProcessors();
        delete streamManager;

        delete nTrodeTable;
        delete streamConf;
        delete spikeConf;
        delete headerConf;
        delete moduleConf;
        delete networkConf;
        delete photometryConf;

        channelsConfigured = false;
        actionCloseConfig->setEnabled(false);
        actionLoadConfig->setEnabled(true);
        actionConnect->setEnabled(false);
        actionDisconnect->setEnabled(false);
        currentConfigFileName = "";
    }
}

void MainWindow::loadConfig()
{
    //Used the saved system settings from the last session as the default folder
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("paths"));
    QString tempPath = settings.value(QLatin1String("configPath")).toString();

    settings.endGroup();

    QString fileName = QFileDialog::getOpenFileName(0, QString("Open configuration file"), tempPath, "Trodes config files (*.trodesconf *.xml)");
    if (!fileName.isEmpty()) {
        //Save the folder in system setting for the next session
        QFileInfo fi(fileName);
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("configPath"), fi.absoluteFilePath());
        settings.endGroup();
        loadConfig(fileName);
    }
}

int MainWindow::loadConfig(QString fileName)
{
    // This is used to load a configuration and create all the control/display widgets
    // according to the settings in the config file

    loadedConfigFile = fileName;
    if (!channelsConfigured) {
        //Read the config .xml file; the second argument specifies that this is being called within trodes
        int parseCode = nsParseTrodesConfig(fileName);

        if (parseCode < 0) {
            return parseCode;
        }
        currentConfigFileName = fileName;

        //Set up the stream controller
        streamManager = new StreamProcessorManager(0);
        connect(streamManager, SIGNAL(bufferOverrun()), this, SLOT(bufferOverrunHandler())); //In case data rates are too fast, use an emergency stop signal
        connect(streamManager, SIGNAL(sourceFail_Sig()), sourceControl, SLOT(noDataComing()));
        connect(this, SIGNAL(newTraceLength(double)), streamManager, SLOT(updateDataLength(double)));

        connect(photometryConf, SIGNAL(changeAllMaxDisp(int)), this, SLOT(setAllMaxDisp(int)));
        connect(sourceControl, SIGNAL(acquisitionStarted()), streamManager, SLOT(startAcquisition()));
        connect(sourceControl, SIGNAL(acquisitionStopped()), streamManager, SLOT(stopAcquisition()));
        qDebug() << "Set up stream manager";

        eegDisp = new StreamDisplayManager(0, streamManager);
        int columnCount = 0;
        for (int tnum = 0; tnum < eegDisp->eegDisplayWidgets.length(); tnum++) {
            int columnsOnPage = eegDisp->columnsPerPage[tnum];
            tabs->addTab(eegDisp->eegDisplayWidgets[tnum], QString("Tr ") + QString("%1").arg(eegDisp->nTrodeIDs[columnCount].first()) + "-" + QString("%1").arg(eegDisp->nTrodeIDs[columnCount + columnsOnPage - 1].last()));
            columnCount += columnsOnPage;
        }
        qDebug() << "Set up stream display";

        //As of qt5.1.1, you have to switch between the tabs in order for the mousePressEvent callback in the glWidgets to work.
        tabs->setCurrentIndex(eegDisp->eegDisplayWidgets.length() - 1);
        tabs->setCurrentIndex(0);
        connect(eegDisp, SIGNAL(trodeSelected(int)), this, SLOT(selectTrode(int)));


        //Record thread setup---------------------------
        recordOut = new RecordThread();
        connect(this, SIGNAL(endAllThreads()), recordOut, SLOT(endRecordThread()));
        // update the list of channels to save once a configuration file has been loaded
        connect(this, SIGNAL(configFileLoaded()), recordOut, SLOT(setupSaveDisplayedChan()));
        connect(recordOut,SIGNAL(writeError()),this,SLOT(errorSaving()));

        //----------------------------------------------

        channelsConfigured = true;
        actionLoadConfig->setEnabled(false);
        actionCloseConfig->setEnabled(true);
        if (sourceControl->currentSource > 0) {
            actionConnect->setEnabled(true);
        }
        actionDisconnect->setEnabled(false);

        // set up the network connections with the modules if modules are defined
        // set myID to -1 to indicate that this is trodes
        moduleConf->myID = TRODES_ID;
        emit configFileLoaded();

        return parseCode;
    }
    else {
        return -1;
    }
}

void MainWindow::restartCrashedModule(QString modName) {

}

void MainWindow::selectTrode(int nTrode)
{
    if (nTrode != currentTrodeSelected) {
        currentTrodeSelected = nTrode;
    }
}

int MainWindow::findEndOfConfigSection(QString configFileName) {


    QFile file;
    int filePos = -1;

    if (!configFileName.isEmpty()) {
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            return -1;
        }

        QFileInfo fi(configFileName);
        QString ext = fi.suffix();
        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header

            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (file.pos() < 1000000) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
            }
        }
        file.close();
        return filePos;
    }
}

void MainWindow::openPlaybackFile(const QString fileName)
{
    qDebug() << "Opening playback file" << fileName;

    if ((!playbackFileOpen) && (!recordFileOpen)) {
        int filePos;
        bool usingExternalWorkspace = false;

        QFileInfo fI(fileName);

        QString baseName = fI.baseName();
        QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";

        QFileInfo workspaceFile(workspaceCheckName);
        if (workspaceFile.exists()) {
            qDebug() << "Using the following workspace file: " << workspaceFile.fileName();
            usingExternalWorkspace = true;
            filePos = findEndOfConfigSection(fileName);
        }


        //Open up the configuration settings used when the
        //data were recorded. x current workspace.
        if (channelsConfigured) {
            closeConfig();
        }

        if (usingExternalWorkspace) {
            loadConfig(workspaceFile.absoluteFilePath());
        } else {
           //No external workspace with the same name found, so try to load the settings
           //embedded in the recording file
           filePos = loadConfig(fileName);
        }

        if (filePos < 0) {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "The configuration settings associated with this recording could not be parsed.");

            actionSourceNone->setChecked(true);
            sourceControl->setSource(SourceNone);
            return;
        }



        playbackFileOpen = true;
        playbackFile = fileName;
        fileString = fI.fileName();
        fileLabel->setText(fileString);

        fileDataPos = filePos;
        actionSourceFile->setChecked(true);
        sourceControl->setSource(SourceFile);
        actionPlay->setEnabled(true);
        playButton->setEnabled(true);
        pauseButton->setEnabled(true);

        actionSourceNone->setChecked(false);
        actionSourceEthernet->setChecked(false);

    }
}

void MainWindow::threadError(QString errorString) {
    // show a box with the error message
    QMessageBox::warning(this, "Error", errorString);
}

void MainWindow::openRecordDialog(QString filename)
{
    //dialog to create a new record file
    if (!fileInitiated) {
        QFileInfo fi(filename);
        QString fileBaseName = fi.baseName();
        fileName = fi.absolutePath()+QString(QDir::separator())+fileBaseName;

        int numChecks = moduleInstance;
        QFileInfo fileCheck(fileName + QString(".%1.rec").arg(moduleInstance));
        while (fileCheck.exists()) {
            numChecks++;
            fileCheck.setFile(fileName + QString(".%1").arg(numChecks) + ".rec");
        }
        if (numChecks > 0) {
            fileName = fileName + QString(".ph.%1").arg(numChecks)+ ".rec";
        }
        fileInitiated = true;

    }

    if (!(fileName == "")) {
        if (!fileName.endsWith(".rec")) {
            fileName += ".rec";
        }
        int fileOpenStatus = recordOut->openFile(fileName);
        if (fileOpenStatus == -1) {
            QMessageBox::information(0, "error", tr("File already exists. Please rename existing file first."));
            return;
        }
        if (fileOpenStatus == -2) {
            QMessageBox::information(0, "error", tr("Error: File could not be created."));
            return;
        }

        recordFileOpen = true;
        recordFileName = fileName;

        QFileInfo fI(fileName);
        fileString = fI.fileName();
        fileLabel->setText(fileString + tr("   (0 MB)"));
        actionCloseFile->setEnabled(true);
        actionCloseConfig->setEnabled(false);

        if (dataStreaming) {
            actionRecord->setEnabled(true);
            recordButton->setEnabled(true);
            pauseButton->setEnabled(true);
            pauseButton->setDown(true);
        }


    }

}

void MainWindow::bufferOverrunHandler()
{
    if (!exportMode) {
        qDebug() << "Buffer overrun!";
    }
    else {
        sourceControl->waitForThreads();
    }
}

void MainWindow::showErrorMessage(QString msg) {
    //Multi-purpose error dialog
    QMessageBox messageBox;
    messageBox.critical(0,"Error",msg);
    messageBox.setFixedSize(500,200);
}

void MainWindow::errorSaving() {
    pauseButtonPressed();
    showErrorMessage(QString("Can not write to disk.  Disk may be full or not avialble for writing."));
}

void MainWindow::closeFile()
{
    recordOut->closeFile();
    recordFileOpen = false;
    QString FileLabelColor("gray");
    QString FileLabelText("No file open");
    QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
    fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));
    actionCloseFile->setEnabled(false);
    actionRecord->setEnabled(false);
    recordButton->setEnabled(false);
    actionPause->setEnabled(false);
    pauseButton->setEnabled(false);

    if (channelsConfigured) {
        actionCloseConfig->setEnabled(true);
    }

    emit recordFileClosed();
}

void MainWindow::recordButtonPressed()
{
    pauseButton->setDown(false);
    actionPause->setEnabled(true);
    actionRecord->setEnabled(false);
    recordOut->startRecord();
    actionCloseFile->setEnabled(false);
    recording = true;
    actionDisconnect->setEnabled(false);
    statusbar->showMessage(tr("Started recording at ") + calcTimeString());
//    emit recordingStarted();
}
void MainWindow::recordButtonReleased()
{
    recordButton->setDown(true);
}
void MainWindow::actionRecordSelected()
{
    recordButton->setDown(true);
    recordButtonPressed();
}

void MainWindow::pauseButtonPressed()
{
    if (recordFileOpen) {
        recordButton->setDown(false);
        actionPause->setEnabled(false);
        actionDisconnect->setEnabled(true);
        actionCloseFile->setEnabled(true);
        recordOut->pauseRecord();
        recording = false;
        actionRecord->setEnabled(true);
        statusbar->showMessage(tr("Paused recording at ") + calcTimeString());
//        emit recordingStopped();
    }
    else if (playbackFileOpen) {
        actionPause->setEnabled(false);
        actionPlay->setEnabled(true);
        playButton->setDown(false);
        sourceControl->pauseSource();
    }
}
void MainWindow::pauseButtonReleased()
{
    pauseButton->setDown(true);
}
void MainWindow::actionPauseSelected()
{
    pauseButton->setDown(true);
    pauseButtonPressed();
}

void MainWindow::playButtonPressed()
{
    pauseButton->setDown(false);
    actionPause->setEnabled(true);
    actionPlay->setEnabled(false);
    filePlaybackSpeed = 1; //Normal speed
    exportMode = false;

    connectToSource();
}
void MainWindow::playButtonReleased()
{
    playButton->setDown(true);
}
void MainWindow::actionPlaySelected()
{
    playButton->setDown(true);
    playButtonPressed();
}

void MainWindow::disconnectFromSource()
{

    if (recordFileOpen) {
        if (recordOut->getBytesWritten() > 0) {
            //There is a record file open with data in it
            //This needs to be closed
            QMessageBox messageBox;
            int answer = messageBox.question(0, "File is open", "The record file will be closed. Proceed?");
            //messageBox.setFixedSize(500,200);
            if (answer == 1) {
                closeFile();
            }
            else {
                return;
            }
        }
    }

    if (playbackFileOpen) {
        actionPlay->setEnabled(true);
        actionPause->setEnabled(false);
        pauseButton->setDown(true);
        playButton->setDown(false);
    }

    //disconnect from source
    sourceControl->disconnectFromSource();
}

void MainWindow::connectToSource()
{
    //Pointless routing right now, but probably good to retain this step in case we
    //want menus to change, etc.

    sourceControl->connectToSource();
}

void MainWindow::setSource()
{
    //source selected with menu ( wrapper for setSource(int) )
    QAction* action = (QAction*)sender();

    setSource(action->data().value<DataSource>());
}

void MainWindow::setSource(DataSource source)
{
    //changes the source of the data stream

    if (source != sourceControl->currentSource) {
        //set the menu state
        actionSourceNone->setChecked(false);
        actionSourceFile->setChecked(false);
        actionSourceEthernet->setChecked(false);

        bool needToClosePlaybackConfig = false;
        if (playbackFileOpen && (source != SourceFile)) {
            needToClosePlaybackConfig = true;

        }

        //Used the saved system settings from the last session as the default folder
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        QString tempPath = settings.value(QLatin1String("playbackPath")).toString();
        settings.endGroup();
        QString pFileName;

        switch (source) {
        case SourceNone:

            actionSourceNone->setChecked(true);
            sourceControl->setSource(source);
            actionSourceNone->setEnabled(true);
            actionSourceFile->setEnabled(true);
            actionSourceEthernet->setEnabled(true);
            break;

        case SourceFile:

            pFileName = QFileDialog::getOpenFileName(0, tr("Open file for playback"), tempPath, tr("Rec files (*.rec)"));
            if (!pFileName.isEmpty()) {
                //Save the folder in system setting for the next session
                QFileInfo fi(pFileName);
                QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
                settings.beginGroup(QLatin1String("paths"));
                settings.setValue(QLatin1String("playbackPath"), fi.absoluteFilePath());
                settings.endGroup();
                openPlaybackFile(pFileName);
            }

            break;

        case SourceEthernet:
            actionSourceEthernet->setChecked(true);
            sourceControl->setSource(source);
            break;

        case SourceRhythm:
            actionSourceEthernet->setChecked(true);
            sourceControl->setSource(source);
            break;
        }


        if (needToClosePlaybackConfig) {

            qDebug() << "Closing playback config.";
            closeConfig(); //if the source was a file, we close the associated configuration

            QString FileLabelColor("gray");
            QString FileLabelText("No file open");
            QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
            fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));

            actionPlay->setEnabled(false);
            actionPause->setEnabled(false);
            playButton->setEnabled(false);
            pauseButton->setEnabled(false);
        }
    }
}

void MainWindow::setTLength()
{
    QAction* action = (QAction*)sender();

    actionSetTLength0_2->setChecked(false);
    actionSetTLength0_5->setChecked(false);
    actionSetTLength1_0->setChecked(false);
    actionSetTLength2_0->setChecked(false);
    actionSetTLength5_0->setChecked(false);
    action->setChecked(true);
    double value = action->data().toDouble();

    eegDisp->freezeDisplay(true);

    //send signal to streamProcessor to change trace length
    emit newTraceLength(value);

    //There is really no need to update the xaxis on the traces, so for now we skip this step
    /*
       for (int i = 0; i < eegDisp->glStreamWidgets.length(); i++) {
          eegDisp->glStreamWidgets[i]->setTLength(value);
       }*/

    eegDisp->freezeDisplay(false);
}

void MainWindow::removeFromOpenNtrodeList(int nTrodeNum)
{
    //TODO:  if multiple nTrode windows are open, we will use the nTrodeNum input

    if (singleTriggerWindowOpen) {
        singleTriggerWindowOpen = false;
    }
}

void MainWindow::setSourceMenuState(int state)
{
    //When the state of the source changes, the state is emitted and this function is called
    //to set the menus
    if (state == SOURCE_STATE_NOT_CONNECTED) {
        if (channelsConfigured && (sourceControl->currentSource > 0)) {
            actionConnect->setEnabled(true);
            actionCloseConfig->setEnabled(true);
        }
        else {
            actionConnect->setEnabled(false);
        }
        actionDisconnect->setEnabled(false);
        statusbar->showMessage(tr("Not connected to device"));
        dataStreaming = false;
        emit sourceConnected("None");
    }
    else if (state == SOURCE_STATE_CONNECTERROR) {
        //setSource(1);
        qDebug() << "Menu controller got error sig";
        QMessageBox messageBox;
        messageBox.critical(0, "Error", "Connection to source failed.");
        messageBox.setFixedSize(500, 200);
        setSource(SourceNone);
    }
    else if (state == SOURCE_STATE_INITIALIZED) {
        if (channelsConfigured) {
            actionConnect->setEnabled(true);
            actionCloseConfig->setEnabled(true);

        }
        else {
            actionConnect->setEnabled(false);
        }
        if (playbackFileOpen) {
            actionPause->setEnabled(false);
            actionPlay->setEnabled(true);
            playButton->setDown(false);
            pauseButton->setDown(true);
        }
        actionDisconnect->setEnabled(false);

        statusbar->showMessage(tr("Connected to device. Currently not streaming."));
        dataStreaming = false;

        actionSourceNone->setEnabled(true);
        actionSourceFile->setEnabled(true);
        actionSourceEthernet->setEnabled(true);

        actionRecord->setEnabled(false);
        recordButton->setEnabled(false);

        //Emit connection signal
        //Listeners:
        //TrodesServer -> modules
        switch (sourceControl->currentSource) {
            case SourceFile:
                emit sourceConnected(QString("File: ") + playbackFile);
                break;

            case SourceEthernet:
                emit sourceConnected("Ethernet");
                break;
        }

        emit closeWaveformDialog();

        if (quitting) {
            closeEvent(new QCloseEvent());
            return;
        }
    }
    else if (state == SOURCE_STATE_RUNNING) {
        actionLoadConfig->setEnabled(false);
        actionCloseConfig->setEnabled(false);
        actionConnect->setEnabled(false);
        actionDisconnect->setEnabled(true);

        actionSourceNone->setEnabled(false);
        actionSourceFile->setEnabled(false);
        actionSourceEthernet->setEnabled(false);
        statusbar->showMessage(tr("Data currently streaming from device"));
        dataStreaming = true;

        if (recordFileOpen) {
            actionRecord->setEnabled(true);
            recordButton->setEnabled(true);
            pauseButton->setEnabled(true);
            pauseButton->setDown(true);
        }
        if (playbackFileOpen) {
            actionPause->setEnabled(true);
            actionPlay->setEnabled(false);
            playButton->setDown(true);
            pauseButton->setDown(false);
        }
    }
    else if (state == SOURCE_STATE_PAUSED) {
        dataStreaming = false;
    }
}

void MainWindow::updateTime()
{
    visibleTime++;
    if (visibleTime > 5) {
        if (eventTabWasChanged) {
            eventTabWasChanged = false;
            update();
        }
    }

    if (!exportMode) {
        //QTime currentTime;
        QString currentTimeString("");
        uint32_t tmpTimeStamp = currentTimeStamp;
        int hoursPassed = floor(tmpTimeStamp / (hardwareConf->APDsamplingRate * 60 * 60));
        tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * hardwareConf->APDsamplingRate);
        int minutesPassed = floor(tmpTimeStamp / (hardwareConf->APDsamplingRate * 60));
        tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * hardwareConf->APDsamplingRate);
        int secondsPassed = floor(tmpTimeStamp / (hardwareConf->APDsamplingRate));
        tmpTimeStamp = tmpTimeStamp - (secondsPassed * hardwareConf->APDsamplingRate);

        if (hoursPassed < 10)
            currentTimeString.append("0");

        currentTimeString.append(QString::number(hoursPassed));
        currentTimeString.append(":");
        if (minutesPassed < 10)
            currentTimeString.append("0");

        currentTimeString.append(QString::number(minutesPassed));
        currentTimeString.append(":");
        if (secondsPassed < 10)
            currentTimeString.append("0");

        currentTimeString.append(QString::number(secondsPassed));
        timeLabel->setText(currentTimeString);

        timerTick = (timerTick + 1) % 5;
        if ((recordFileOpen) && (timerTick == 0)) {
            fileLabel->setText(fileString + QString("   (%1 MB    Available: %2 MB)").arg(recordOut->getBytesWritten() / 1000000).arg(recordOut->getBytesFree() / 1000000));
        }

    }
}

void MainWindow::linkFilters()
{
    linkStreamToFilters = true;
    streamFilterLink->setChecked(true);
    streamFilterUnLink->setChecked(false);
}

void MainWindow::unLinkFilters()
{
    linkStreamToFilters = false;
    streamFilterUnLink->setChecked(true);
    streamFilterLink->setChecked(false);
}

void MainWindow::linkChanges()
{
    linkChangesBool = true;
}

void MainWindow::unLinkChanges()
{
    linkChangesBool = false;
}

void MainWindow::setAllMaxDisp(int newMaxDisp)
{
    linkChangesBool = false;
    for (int i = 0; i < photometryConf->nfibers.length(); i++) {
        photometryConf->setMaxDisp(i, newMaxDisp);
    }
    linkChangesBool = true;
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    //Quit all threads and close all windows before accepting the close event
    qDebug() << "closing";
    if (dataStreaming || playbackFileOpen) {

        disconnectFromSource();

        QThread::msleep(250);
    }

    sourceControl->setSource(SourceNone); //closes and deletes all source threads
    QThread::msleep(250);

    if (channelsConfigured) {
        closeConfig();
    }


    emit endAllThreads();
     QThread::msleep(250);

    emit closeAllWindows();
    event->accept();
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    emit closeSoundDialog();
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::moveEvent(QMoveEvent *)
{
    emit closeSoundDialog();
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    if (!eventTabWasChanged) {
        event->accept();
    }
    else {
        QPainter painter;
        painter.begin(this);
        render(&painter);
        painter.end();
    }
}

void MainWindow::eventTabChanged(int newTab)
{
    if (eventTabsInitialized[newTab] == false) {
        eventTabWasChanged = true;
        visibleTime = 0;
        eventTabsInitialized[newTab] = true;
    }
}

QString MainWindow::calcTimeString()
{
    QString currentTimeString("");
    uint32_t tmpTimeStamp = currentTimeStamp;
    int hoursPassed = floor(tmpTimeStamp / (hardwareConf->APDsamplingRate * 60 * 60));

    tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * hardwareConf->APDsamplingRate);
    int minutesPassed = floor(tmpTimeStamp / (hardwareConf->APDsamplingRate * 60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * hardwareConf->APDsamplingRate);
    int secondsPassed = floor(tmpTimeStamp / (hardwareConf->APDsamplingRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed * hardwareConf->APDsamplingRate);
    int tenthsPassed = floor(((tmpTimeStamp * 10) / hardwareConf->APDsamplingRate));

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));
    currentTimeString.append(".");
    currentTimeString.append(QString::number(tenthsPassed));

    return currentTimeString;
}


//Module
//-----------------------------------------------------------------------------
void MainWindow::setPhotometrySource(QString source) {

    if (!dataStreaming) {
        qDebug()<<source;
        DataSource phtsource = SourceEthernet;
        setSource(phtsource);
        connectToSource();
    }else{

    }

}

void MainWindow::setTime(quint32 t) {

    currentTimeStamp = t;
    startRecording();

}

void MainWindow::setTimeRate(quint32 inTimeRate) {
    qDebug() << "Time rate: " << inTimeRate;
    clockRate = inTimeRate;
}

void MainWindow::startRecording() {

    emit recordingStarted();
    recordOut->startRecord();

    pauseButton->setDown(false);
    actionPause->setEnabled(true);
    actionRecord->setEnabled(false);
    actionCloseFile->setEnabled(false);
    recording = true;
    actionDisconnect->setEnabled(false);
    statusbar->showMessage(tr("Started recording"));


}

void MainWindow::stopRecording() {
    if (recordFileOpen) {
        emit recordingStopped();
        recordOut->pauseRecord();
        recordButton->setDown(false);
        actionPause->setEnabled(false);
        actionDisconnect->setEnabled(true);
        actionCloseFile->setEnabled(true);
        recording = false;
        actionRecord->setEnabled(true);
        statusbar->showMessage(tr("Paused recording at ") + calcTimeString());
    }
    else if (playbackFileOpen) {
        actionPause->setEnabled(false);
        actionPlay->setEnabled(true);
        playButton->setDown(false);
        sourceControl->pauseSource();
    }
}

void MainWindow::closePFile() {

    recordOut->closeFile();
    recordFileOpen = false;
    QString FileLabelColor("gray");
    QString FileLabelText("No file open");
    QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
    fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));
    actionCloseFile->setEnabled(false);
    actionRecord->setEnabled(false);
    recordButton->setEnabled(false);
    actionPause->setEnabled(false);
    pauseButton->setEnabled(false);

    if (channelsConfigured) {
        actionCloseConfig->setEnabled(true);
    }


}

void MainWindow::setModuleInstance(int instanceNum) {
    moduleInstance = instanceNum;
    qDebug() << "Photometry Module instance " << moduleInstance;
    setWindowTitle(QString("Photometry %1").arg(moduleInstance));
}

void MainWindow::setFileClosedStatus() {
    statusbar->showMessage("Status: file closed");
    fileInitiated = false;
    fileOpen = false;
}


//Slider
//-----------------------------------------------------------------------------
MySlider::MySlider(QWidget *parent):QSlider(parent) {
    draggingStart = false;
    draggingEnd = false;
    draggingKnob = false;
    startBorder = 0;
    endBorder = TIMESLIDERSTEPS;
    for (int i=0; i<TIMESLIDERSTEPS; i++) {
        logMarker[i] = false;
    }
}

void MySlider::markCurrentLocation() {
    logMarker[value()] = true;
}

void MySlider::clearLogMarks() {
    for (int i=0; i<TIMESLIDERSTEPS; i++) {
        logMarker[i] = false;
    }
    update();
}

void MySlider::clearLogMarksAfterCurrentPos() {
    for (int i=value()+1; i<TIMESLIDERSTEPS; i++) {
        logMarker[i] = false;
    }
    update();
}

void MySlider::mousePressEvent ( QMouseEvent * event ) {

    QStyleOptionSlider opt;
      initStyleOption(&opt);
      QRect sr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);
      int clickVal = minimum() + ((maximum()-minimum()) * (event->x()-5)) / (width()-10);

      /*
      if (event->button() == Qt::LeftButton && sr.contains(event->pos()) == true) {
          if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
            QSlider::mousePressEvent(event);
          }
      }*/

      int allowedClickError = TIMESLIDERSTEPS/50;
      if (event->button() == Qt::LeftButton) {

        //If the user clicked the main knob, and the click was closer to the main knob then the others...
        if ((abs(clickVal-value()) < allowedClickError) && (abs(clickVal-value()) < abs(clickVal-startBorder)) && (abs(clickVal-value()) < abs(clickVal-endBorder)) ){
              setValue(clickVal);
              draggingKnob = true;
              update();
              emit sliderPressed();
              event->accept();
        //If the user slicked the start knob, and the main knob was not nearby...
        } else if ((abs(clickVal-startBorder) < allowedClickError) && (abs(value()-startBorder)>(allowedClickError/4)) ){
            startBorder = clickVal;
            draggingStart = true;
            update();
            event->accept();
        //If the user slicked the end knob, and the main knob was not nearby...
        } else if ((abs(clickVal-endBorder) < allowedClickError) && (abs(value()-endBorder)>(allowedClickError/4)) ){
            endBorder = clickVal;
            draggingEnd = true;
            update();
            event->accept();
        //Otherwise, put the main knob where the user clicked.
        } else if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
            setValue(clickVal);
            draggingKnob = true;
            update();
            emit sliderMoved(clickVal);
            emit sliderPressed();

            event->accept();

            //event->accept();
            //QSlider::mousePressEvent(event);
        }

      }

}

void MySlider::mouseReleaseEvent(QMouseEvent *event) {
    if (draggingStart && startBorder > value()) {
        setValue(startBorder);
        emit sliderReleased();
    } else if (draggingEnd && endBorder < value()) {
        setValue(endBorder);
        emit sliderReleased();
    } else if (draggingKnob){
        emit sliderReleased();
        //QSlider::mouseReleaseEvent(event);
    }

    draggingStart = false;
    draggingEnd = false;
    draggingKnob = false;
}

void MySlider::mouseMoveEvent(QMouseEvent *event) {
    int clickVal = minimum() + ((maximum()-minimum()) * (event->x()-5)) / (width()-10);
    if (draggingStart) {
        //Dragging the start border slider
        if ((clickVal >= 0) && (clickVal <= endBorder)) {
            startBorder = clickVal;
            emit newRange(startBorder,endBorder);
            update();
        }
    } else if (draggingEnd) {
        //Dragging the end border slider
        if ((clickVal >= startBorder) && (clickVal <= TIMESLIDERSTEPS)) {
            endBorder = clickVal;
            emit newRange(startBorder,endBorder);
            update();
        }
    } else if (draggingKnob) {
        //Dragging the main position slider
        if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
            setValue(clickVal);
            emit sliderMoved(clickVal);
            update();
        }
    }
    /*
    else {
        if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
          QSlider::mouseMoveEvent(event);
        }

    }*/

}

void MySlider::paintEvent(QPaintEvent *event) {

    QStylePainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    double pixPerUnit;
    if ((maximum()-minimum()) > 0) {
        pixPerUnit = (width()-10)/(double)(maximum()-minimum()) ;
    } else {
        pixPerUnit = 0;
    }

    double startSliderPos = (startBorder*pixPerUnit)+5;
    double endSliderPos = (endBorder*pixPerUnit)+5;
    double knobPos = (value()*pixPerUnit)+5;

    double pixPerLogMarkerStep = (double)(width()-10)/TIMESLIDERSTEPS;

    QPen linePen;

    linePen.setWidth(2);
    linePen.setBrush(Qt::red);
    painter.setPen(linePen);
    for (int i=0; i < TIMESLIDERSTEPS; i++) {
        if (logMarker[i]) {
            painter.drawPoint((i*pixPerLogMarkerStep)+5,2);
        }
    }


    linePen.setWidth(2);
    linePen.setBrush(Qt::lightGray);
    painter.setPen(linePen);

    //Draw the groove line
    QPainterPath rangePath;
    rangePath.moveTo(5, height()/2);
    rangePath.lineTo(width()-5, height()/2);
    painter.drawPath(rangePath);


    //Draw the range line
    linePen.setWidth(3);
    linePen.setBrush(Qt::gray);
    painter.setPen(linePen);
    QPainterPath groovePath;
    groovePath.moveTo(startSliderPos, height()/2);
    groovePath.lineTo(endSliderPos, height()/2);
    painter.drawPath(groovePath);


    //Draw the start range slider knob
    //qDebug() << value() << width() << pixPerUnit;

    QPainterPath startSlider;


    startSlider.addRoundedRect(startSliderPos-2.5,height()/4,5.0,height()/2,5,5.0);
    painter.fillPath(startSlider,QBrush(Qt::gray));

    linePen.setBrush(Qt::black);
    linePen.setWidth(1);
    painter.setPen(linePen);
    painter.drawPath(startSlider);


    //Draw the start range slider knob
    QPainterPath endSlider;
    endSlider.addRoundedRect(endSliderPos-2.5,height()/4,5.0,height()/2,5,5.0);
    painter.fillPath(endSlider,QBrush(Qt::gray));
    painter.drawPath(endSlider);


    //Draw the main slider knob
    QPainterPath mainSliderKnob;
    //mainSliderKnob.addRoundedRect(knobPos-3,0,6.0,height(),5,5.0);

    mainSliderKnob.addRoundedRect(knobPos-4,(height()/2)-4.0,8.0,8.0,5.0,5.0);
    painter.fillPath(mainSliderKnob,QBrush(QColor(150,150,200)));
    //painter.fillPath(mainSliderKnob,QBrush(Qt::gray));
    painter.drawPath(mainSliderKnob);

    painter.end();
}

void MySlider::setStart(int newStartBorder) {

    if ((newStartBorder >= 0) && (newStartBorder <= endBorder)) {
        startBorder = newStartBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newStartBorder > endBorder) {
        //The start value is greater than the end value, set use the end value
        startBorder = endBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newStartBorder < 0) {
        startBorder = 0;
        //emit newRange(startBorder,endBorder);
        update();
    }
    if (startBorder > value()) {
        setValue(startBorder);
        emit sliderReleased();
    } else if (endBorder < value()) {
        setValue(endBorder);
        emit sliderReleased();
    }

}

void MySlider::setEnd(int newEndBorder) {

    if ((newEndBorder <= TIMESLIDERSTEPS) && (newEndBorder >= startBorder)) {
        endBorder = newEndBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newEndBorder < startBorder) {
        //The end value is less than the start value, set use the start value
        endBorder = startBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newEndBorder > TIMESLIDERSTEPS) {
        endBorder = TIMESLIDERSTEPS;
        //emit newRange(startBorder,endBorder);
        update();
    }
    if (startBorder > value()) {
        setValue(startBorder);
        //emit sliderReleased();
    } else if (endBorder < value()) {
        setValue(endBorder);
        //emit sliderReleased();
    }

}


