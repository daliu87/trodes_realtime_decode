TEMPLATE = subdirs


# Compiles faster if commented out. There are no actual dependencies
#CONFIG += ordered


SUBDIRS = cameraModule stateScript SimpleCommunicator SimpleMonitor/Source/SimpleMonitor.pro

# Add platform-specific modules below
unix {
SUBDIRS += FSGui
}

win32 {
}
