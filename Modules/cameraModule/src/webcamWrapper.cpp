#include "webcamWrapper.h"

WebcamWrapper::WebcamWrapper()
    : cameraInput(NULL) {

    surface = new WebcamDisplaySurface(this);
    numFramesRecieved = 0;

    connect(surface,SIGNAL(newFrame(QImage*, uint32_t, bool)),this,SLOT(sendFrameSignals(QImage*,uint32_t, bool)));
    connect(surface,SIGNAL(activeChanged(bool)),this,SLOT(cameraStateChanged(bool)));
    //connect(surface,SIGNAL(newFrame(QImage*, bool)),this,SIGNAL(newFrame(QImage*,bool)));
    //connect(surface,SIGNAL(newFrame()),this,SIGNAL(newFrame()));

    isCameraOpen = false;
    isCameraAcquiring = false;

}

WebcamWrapper::~WebcamWrapper() {

}

QStringList WebcamWrapper::availableCameras() {
    QStringList out;
    //QList<QCameraInfo> availCameras = QCameraInfo::availableCameras();
    availCameras = QCameraInfo::availableCameras();
    qDebug() << "Webcams:" << availCameras;
    for (int i = 0; i < availCameras.length(); i++) {
        out.append(availCameras[i].description());
    }
    return out;
}

bool WebcamWrapper::open(int cameraID) {

    //QList<QCameraInfo> availCameras = QCameraInfo::availableCameras();
    //if (availableCameras.length() > cameraID) {

        if (cameraInput != NULL) {
            cameraInput->unload();
            cameraInput->deleteLater();
            cameraInput = NULL;
        }
        cameraInput = new QCamera(availCameras[cameraID]);
        cameraInput->setViewfinder(videoSurface());
        isCameraOpen = true;
        numFramesRecieved = 0;
        return true;
    /*} else {
        qDebug() << "Error in opening webcam";
        return false;
    }*/


}

void WebcamWrapper::close() {

    stop();

    if (cameraInput != NULL) {
        cameraInput->unload();
        cameraInput->deleteLater();
        cameraInput = NULL;
    }
    isCameraOpen = false;
}

bool WebcamWrapper::start() {

    if (isCameraOpen) {
         cameraInput->start(); // to start the viewfinder
         return true;
    } else {
        return false;
    }

}

void WebcamWrapper::stop() {
    if (isCameraAcquiring) {
        cameraInput->stop();
        isCameraAcquiring = false;
    }

}

void WebcamWrapper::cameraStateChanged(bool active) {

    if (active) {
        isCameraAcquiring = true;
        qDebug() << "Video format is: " << (QImage::Format)surface->getFormat();
        //Log the video format
        switch (surface->getFormat()) {
        case QImage::Format_RGB32:
            qDebug() << "RGB32 video format";
            setFormat(AbstractCamera::Fmt_RGB32);
            break;
        case QImage::Format_RGB888:
            qDebug() << "RGB24 video format";
            setFormat(AbstractCamera::Fmt_RGB24);
            break;
        case QImage::Format_ARGB32:
            qDebug() << "ARGB32 video format";
            setFormat(AbstractCamera::Fmt_ARGB32);
            break;
        default:
            qDebug() << "Video format not supported";
            setFormat(AbstractCamera::Fmt_Invalid);
            break;

        }
    } else {
        isCameraAcquiring = false;
    }
}


//----------------------------------------------------




WebcamDisplaySurface::WebcamDisplaySurface(QObject *parent)
    : QAbstractVideoSurface(parent)
    , imageFormat(QImage::Format_Invalid) {

    frameTimer.start();
}

QList<QVideoFrame::PixelFormat> WebcamDisplaySurface::supportedPixelFormats(
        QAbstractVideoBuffer::HandleType handleType) const {

    if (handleType == QAbstractVideoBuffer::NoHandle) {
        return QList<QVideoFrame::PixelFormat>()
                << QVideoFrame::Format_RGB32
                << QVideoFrame::Format_ARGB32
                << QVideoFrame::Format_ARGB32_Premultiplied
                << QVideoFrame::Format_RGB565
                << QVideoFrame::Format_RGB555;
    } else {
        return QList<QVideoFrame::PixelFormat>();
    }
}

QImage::Format WebcamDisplaySurface::getFormat() {
    return imageFormat;
}

bool WebcamDisplaySurface::isFormatSupported(
        const QVideoSurfaceFormat &format, QVideoSurfaceFormat *similar) const
{
    Q_UNUSED(similar);

    const QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
    const QSize size = format.frameSize();

    return imageFormat != QImage::Format_Invalid
            && !size.isEmpty()
            && format.handleType() == QAbstractVideoBuffer::NoHandle;
}

bool WebcamDisplaySurface::start(const QVideoSurfaceFormat &format)
{
    const QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
    const QSize size = format.frameSize();

    if (imageFormat != QImage::Format_Invalid && !size.isEmpty()) {
        this->imageFormat = imageFormat;

        imageSize = size;
        sourceRect = format.viewport();

        QAbstractVideoSurface::start(format);

        //widget->updateGeometry();
        //updateVideoRect();

        return true;
    } else {
        return false;
    }
}

void WebcamDisplaySurface::stop()
{
    currentFrame = QVideoFrame();
    targetRect = QRect();

    QAbstractVideoSurface::stop();

    //widget->update();
}

bool WebcamDisplaySurface::present(const QVideoFrame &frame) {

    if (surfaceFormat().pixelFormat() != frame.pixelFormat()
            || surfaceFormat().frameSize() != frame.size()) {
        setError(IncorrectFormatError);
        stop();

        return false;
    } else {


        qint64 et = frameTimer.restart();
        if (et < 5) {
            qDebug() << "Received frame interval: " << et;
        }

        currentFrame = frame;
        //widget->update();

        if (currentFrame.map(QAbstractVideoBuffer::ReadOnly)) {
            QImage *sendImage = new QImage(currentFrame.bits(),
                             currentFrame.width(),
                             currentFrame.height(),
                             currentFrame.bytesPerLine(),
                             imageFormat);


            //widget->newImage(*sendImage);



#ifdef WIN32
               emit newFrame(sendImage,0, true); //send image to image processor, flip frame
#else
               emit newFrame(sendImage,0, false); //send image to image processor
#endif

            //emit newFrame(); //for time request via tcp server
            currentFrame.unmap();
        }

        return true;
    }
}

/*
void WebcamDisplaySurface::updateVideoRect()
{
    QSize size = surfaceFormat().sizeHint();
    size.scale(widget->size().boundedTo(size), Qt::KeepAspectRatio);

    targetRect = QRect(QPoint(0, 0), size);
    targetRect.moveCenter(widget->rect().center());
}*/

/*
void WebcamDisplaySurface::paint(QPainter *painter) {
    if (currentFrame.map(QAbstractVideoBuffer::ReadOnly)) {
        const QTransform oldTransform = painter->transform();

        if (surfaceFormat().scanLineDirection() == QVideoSurfaceFormat::BottomToTop) {
           painter->scale(1, -1);
           painter->translate(0, -widget->height());
        }

        QImage image(
                currentFrame.bits(),
                currentFrame.width(),
                currentFrame.height(),
                currentFrame.bytesPerLine(),
                imageFormat);


        //Here we can make modifications to the image before rendering.
        QImage showImage(image);

        QRgb *line;

            for(int y = 0; y<image.height(); y++){
                line = (QRgb *)image.scanLine(y);

                for(int x = 0; x<image.width(); x++){
                    int average = (qRed(line[x]) + qGreen(line[x]) + qRed(line[x]))/3;
                    if (average > 200) {
                       showImage.setPixel(x,y, qRgb(255, 0, 0));
                    }
                }

            }
        painter->drawImage(targetRect, showImage, sourceRect);

        painter->setTransform(oldTransform);

        currentFrame.unmap();
    }
}*/
