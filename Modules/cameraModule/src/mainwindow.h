/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "videoDisplay.h"
#include <QMainWindow>
#include "videoEncoder.h"
#include "trodesSocket.h"
#include "configuration.h"
#include "dialogs.h"
#include "eventHandler.h"

#ifdef PYTHONEMBEDDED
#include "pythoncamerainterface.h"
#include "PythonQt_QtAll.h"
#include "gui/PythonQtScriptingConsole.h"

inline void setPyPath(const wchar_t* upath) {
#ifdef __linux__
    Py_SetPath(upath);
#endif
} //end setPyPath()
#endif

extern NetworkConfiguration *networkConf;

class Style_tweaks : public QProxyStyle
{
    public:

        void drawPrimitive(PrimitiveElement element, const QStyleOption *option,
                           QPainter *painter, const QWidget *widget) const
        {
            /* do not draw focus rectangles - this permits modern styling */
            if (element == QStyle::PE_FrameFocusRect)
                return;

            QProxyStyle::drawPrimitive(element, option, painter, widget);
        }
};

class MySlider : public QSlider {
//A custom slider that inherits the QSlider.  It has two extra sliders (besides the main one
//that QSlider provides).  These are used to restrict the range of the slider to a region of interest.

Q_OBJECT

public:
    MySlider(QWidget *parent = 0);

private:
    int startBorder;
    int endBorder;
    bool draggingStart;
    bool draggingEnd;
    bool draggingKnob;
    //bool logMarker[TIMESLIDERSTEPS];
    QBitArray logMarker;

public slots:
    void setStart(int newStartBorder); //set the start of the time range
    void setEnd(int newEndBorder); //set the end of the time range
    void markCurrentLocation();
    void clearLogMarks();
    void clearLogMarksAfterCurrentPos();


protected:
    void mousePressEvent (QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

signals:

    void newRange(int start,int end);
};

class MyAction : public QAction {
    Q_OBJECT

public:
     MyAction(QWidget *parent = 0);

     inline void setOption(OptionFlag Flag) { flg = Flag; };

private:
     OptionFlag flg;

public slots:
     void sendOption(void);

signals:
    void optionSig(OptionFlag);
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QStringList options, QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);
    void keyPressEvent(QKeyEvent *);
    bool event(QEvent *) Q_DECL_OVERRIDE;
    //bool eventFilter(QObject *obj, QEvent *ev);

private:

#ifdef PYTHONEMBEDDED
    PythonQtObjectPtr  mainContext;
    PythonQtScriptingConsole *pyConsole;
    //PythonCameraInterface pyInterface;
    QtToPythonCircularBuffer posData_toPython;

#endif

    MyAction* tester;


    QMenu*      menuFile;
    QMenu*      menuGeometry;
    QMenu*      menuLoadGeo;
    QMenu*      menuSaveGeo;
    QMenu*      menuLoadGeoOptions;
    QMenu*      menuSaveGeoOptions;
    QMenu*      menuHelp;
    //QMenu*      menuLogFiles;
    // ****** Adding new action buttons ******
    // create new action buttons as a MyAction class, then implement them in the
    // MainWindow constructor under the 'sub menu options' section in the manner
    // shown.  Make sure to set the proper tool flag using setOption (see
    // OptionFlag enumeration in videoDisplay.h), and create the proper connections
    // under the 'action connections' section.
    // You must also create an 'geometryExists' signal in videoPlayer.h to detect
    // existance of specific geometries.  Make a corresponding slot in
    // mainwindow.ccp/.h to set save action buttons to enabled (see 'setEnableXGeo
    // metry' slot functions.  Note that the function 'setEnableAllGeometrySave'
    // must also be updated to keep track of the 'save all' button's enabled bool
    MyAction*    actionLoadAllGeoTrack;
    MyAction*    actionLoadLinearGeoTrack;
    MyAction*    actionLoadRangeGeoTrack;

    MyAction*    actionSaveAllGeoTrack;
    MyAction*    actionSaveLinearGeoTrack;
    MyAction*    actionSaveRangeGeoTrack;


    QAction*   actionAbout;

    QMenu*      menuEdit;
    QAction*    actionSetPythonDir;
    QSplitter*  panelSplitter;



    QTimer                      closeTimer;
    bool                        programIsClosing;
    bool                        sourceDialogOpen;
    bool                        toolsDialogOpen;
    int                         currentTool;
    TrackingSettings            trackSettings;


    VideoDisplayController      *videoController;
    VideoDisplayWindow          *displayWindow;
    GraphicsWindow              *graphicsWindow;
    QGridLayout                 *mainLayout;
    TrodesModuleNetwork         *moduleNet;

    QStatusBar                  *statusbar;

    TrodesButton                 *settingsButton;
    TrodesButton                 *sourceButton;
    TrodesButton                 *pauseButton;
    TrackingButton               *playButton;
    TrackingButton               *trackButton;
    TrodesButton                 *toolsButton;
    TrodesButton                 *eventButton;

    EventHandler                 *eventHandler;
    QList<QString> actionList;
    //TrodesButton                 *offlineTrackButton;


    //QLabel                     *timeLabel;
    QLineEdit                    *timeLabel;

    QLineEdit                   *fileStartTimeLabel;
    QLineEdit                   *fileEndTimeLabel;
    MySlider                    *filePositionSlider;


    qint8 moduleID;
    uint32_t currentTime;
    quint8 cameraNum;

    uint32_t clockRate;

    int resX;
    int resY;
    QString serverAddress;
    QString serverPortValue;
    //int currentThresh;
    //bool trackDark;
    bool videoStreaming;
    bool fileInitiated;
    bool fileOpen;
    bool recording;
    bool justSeekedFlag;
    bool lockEnterButtonLinkToPlay;

    bool inputFileOpen;
    bool playbackMode;
    QString inputFileName;
    quint32 fileStartTime;
    quint32 fileEndTime;
    quint32 startSliderTime;
    quint32 endSliderTime;
    bool videoWasPlayingBeforeSliderPress;
    bool isVideoFilePlaying;
    bool isVideoSliderPressed;

    QString fileName;
    QString baseFileName;
    unsigned char currentOperationMode;
    bool trackingOn;
    bool loggingOn;

    int moduleInstance;

    QString pythonDir;

    bool convertTimeString(QString tString, quint32 &t);
    void setSourceOn(bool on);

    QVector<dataPacket> positionData;

    quint32 currentTime2;
    int numOfSentPackets;

    void iniActionList(void);

private slots:
    void about();
    void closeAfterDelay();
    void setTime(quint32);
    void setTimeRate(quint32);
    void setVideoTimeRange(quint32, quint32);
    void setSliderTimeRange(quint32 start, quint32 end);

    void newSettings(TrackingSettings t);
    //void setThresh(int newThresh, bool trackDark);
    //void setRing(int ringSize, bool ringOn);
    void settingsButtonPressed();
    void sourceButtonPressed();
    void toolsButtonPressed();
    void eventButtonPressed();
    void setSourceMenuClosed();
    void setToolsMenuClosed();

    void saveLinearGeometry(OptionFlag flg);
    void loadLinearGeometry(OptionFlag flg);
    void setEnableAllGeometrySave(void);
    void setEnableLinearGeometrySave(bool on);
    void setEnableRangeGeometrySave(bool on);
    void setPythonDir();

    void trackingButtonPressed();
    void trackingButtonReleased();
    void setTracking(bool);


    void videoStarted(int resolutionY, int resolutionX);
    void startRecording();
    void stopRecording();

    //void createFile();
    void closeFile();
    void sendNewAnimalPosition(quint32 time);
    void turnOnPlayback(QString);
    void turnOffPlayback();
    void activatePlaybackControls();
    void playbackSliderPositionChanged(int position);
    void playbackSliderPressed();
    void playbackSliderReleased();
    void setFilePlaybackStopped();

    void pauseButtonPressed();
    void playButtonPressed();
    void pauseButtonReleased();
    void playButtonReleased();

    void setRecordStatusOn();
    void setRecordStatusOff();
    void setFileOpenedStatus();
    void setFileClosedStatus();

    void setCurrentTool(int toolNum);
    void connectProcessor();

    void getUserInput();

    void newStartTimeEntered();
    void newEndTimeEntered();
    void enableTimeEdit();
    void disableTimeEdit();
    void newTimeEntered();

    void setFullTimeRange();
    void rewindFileToStartRange();

    void setModuleInstance(int instanceNum);

    //MARK: Event System
    void broadcastEvent(TrodesEventMessage event);
    void broadcastNewEventReq(QString newEventReq);
    void broadcastRemoveEventReq(QString event);
    void printEventList(QVector<TrodesEvent> evList);

    void callMethod(int actionIndex, TrodesEvent ev);
    void addPoint(void);

    //void addDataPacket(dataPacket packetToAdd);


//    void setTcpClientConnected();
//    void setTcpClientDisconnected();
//    void tcpConnect(QString addressIn,quint16 portIn);
//    void setModuleID(qint8 ID);


public slots:
    void addDataPacket(dataPacket packetToAdd);
    void sendVelocity(double velocity);
    void setupFile(QString filename);
    void nextFileRequested();
    void setFileOpen();
    void testSlot(OptionFlag);

signals:
    void signal_startRecording();
    void signal_stopRecording();
    void signal_createFile(QString file_name);
    void signal_closeFile();
    void signal_startPlayback();
    void signal_startFastPlayback();
    void signal_pausePlayback();
    void signal_stepFrameForward();
    void signal_stepFrameBackward();
    void signal_createPlaybackLogFile(QString file_name);
    void signal_closePlaybackLogFile();
    void signal_newSettings(TrackingSettings t);

    void closeDialogs();

};


#endif // MAINWINDOW_H
