#include "avtWrapper.h"
#include <QtGui>

// Frame completed callback executes on seperate driver thread.
// One callback thread per camera. If a frame callback function has not
// completed, and the next frame returns, the next frame's callback function is queued.
// This situation is best avoided (camera running faster than host can process frames).
// Spend as little time in this thread as possible and offload processing
// to other threads or save processing until later.
//
// Note: If a camera is unplugged, this callback will not get called until PvCaptureQueueClear.
// i.e. callback with pFrame->Status = ePvErrUnplugged doesn't happen -- so don't rely
// on this as a test for a missing camera.


quint64 numFramesHandled;
QAtomicInteger<quint64> numFramesIn;
void _STDCALL FrameDoneCB(tPvFrame* pFrame)
{
    numFramesIn++;
}

AvtCameraController::AvtCameraController() {

    isCameraOpen = false;
    initialized = false;
    isCameraAcquiring = false;
    isBlackAndWhite= false;

    hasReset = false;

    for (int i=0; i<256; i++) {
        colorTable.append(qRgb(i,i,i));
    }

    tPvCameraInfo   cameraList[MAX_CAMERA_LIST];
    unsigned long   cameraNum = 0;
    //unsigned long   cameraRle; // UNUSED
    QStringList cameraNames;

    //connect(&frameTriggerTimer,SIGNAL(timeout()),this,SLOT(getFrame()));
    connect(&frameTriggerTimer,SIGNAL(timeout()),this,SLOT(getAvailableFrames()));

    blinkTimer.setSingleShot(true);
    connect(&blinkTimer,SIGNAL(timeout()),this,SLOT(endBlink()));
    startBlinkTimer.setSingleShot(true);
    connect(&startBlinkTimer,SIGNAL(timeout()),this,SLOT(startBlink()));


    int maxTries = 10;
    int currentTry = 0;
    bool success = false;

    while (!success && currentTry < maxTries) {
        detectedCameras.clear();

        success = true;
        // initialize PvAPI
        if(PvInitialize() != ePvErrSuccess) {
            qDebug() << "Failed to initialize the PvAPI";
            return;
        }

        // wait for all cameras to initialize before getting list of cameras
        QThread::msleep(1000);
        cameraNum = PvCameraCount();

        if (PvCameraList(cameraList,MAX_CAMERA_LIST,NULL) > 0) {
            qDebug() << "PvAPI detected GIGE camera(s)";
            tPvIpSettings Conf;
            tPvErr lErr;

            // display info
            for(unsigned long i=0;i<cameraNum;i++) {

                // get the camera's IP configuration
                if((lErr = PvCameraIpSettingsGet(cameraList[i].UniqueId,&Conf)) == ePvErrSuccess) {

                    if (cameraList[i].PermittedAccess & ePvAccessMaster) {
                        CameraIPName newCam;
                        newCam.ipAddress = Conf.CurrentIpAddress;
                        newCam.cameraName = QString(cameraList[i].DisplayName);
                        qDebug() << newCam.cameraName;
                        detectedCameras.push_back(newCam);
                    } else {
                        qDebug() << "Camera did not permit access";
                        success = false;
                        QThread::msleep(500);
                    }
                } else {
                    qDebug() << "Error getting IP settings for camera.";
                    success = false;
                    QThread::msleep(500);
                }
            }
        } else {
            qDebug() << "PvAPI detected no GIGE cameras";
        }

        PvUnInitialize();
        currentTry++;
    }

    PvInitializeNoDiscovery();
    initialized = true;



}

AvtCameraController::~AvtCameraController() {
    // uninit the API
    if (isCameraOpen) {
        close();
    }

    PvUnInitialize();
}

void AvtCameraController::resetSystem() {
    qDebug() << "Reset PvAPI";

    PvUnInitialize();

    tPvCameraInfo   cameraList[MAX_CAMERA_LIST];
    unsigned long   cameraNum = 0;
    QStringList cameraNames;


    int maxTries = 10;
    int currentTry = 0;
    bool success = false;

    while (!success && currentTry < maxTries) {
        detectedCameras.clear();

        success = true;
        // initialize PvAPI
        if(PvInitialize() != ePvErrSuccess) {
            qDebug() << "Failed to initialize the PvAPI";
            return;
        }

        // wait for all cameras to initialize before getting list of cameras
        QThread::msleep(1000);
        cameraNum = PvCameraCount();

        if (PvCameraList(cameraList,MAX_CAMERA_LIST,NULL) > 0) {
            qDebug() << "PvAPI detected GIGE camera(s)";
            tPvIpSettings Conf;
            tPvErr lErr;

            // display info
            for(unsigned long i=0;i<cameraNum;i++) {

                // get the camera's IP configuration
                if((lErr = PvCameraIpSettingsGet(cameraList[i].UniqueId,&Conf)) == ePvErrSuccess) {

                    if (cameraList[i].PermittedAccess & ePvAccessMaster) {
                        CameraIPName newCam;
                        newCam.ipAddress = Conf.CurrentIpAddress;
                        newCam.cameraName = QString(cameraList[i].DisplayName);
                        qDebug() << newCam.cameraName;
                        detectedCameras.push_back(newCam);
                    } else {
                        qDebug() << "Camera did not permit access";
                        success = false;
                        QThread::msleep(500);
                    }
                } else {
                    qDebug() << "Error getting IP settings for camera.";
                    success = false;
                    QThread::msleep(500);
                }
            }
        } else {
            qDebug() << "PvAPI detected no GIGE cameras";
        }

        PvUnInitialize();
        currentTry++;
    }

    PvInitializeNoDiscovery();
    initialized = true;
}

QStringList AvtCameraController::availableCameras() {

    QStringList cameraNames;


    /*
    tPvCameraInfo   cameraList[MAX_CAMERA_LIST];
    unsigned long   cameraNum = 0;
    unsigned long   cameraRle;



    if (initialized) {


        int numTries = 0;
        // first, get list of reachable cameras.
        while (!cameraNum && numTries < 5) {
            cameraNum = PvCameraList(cameraList,MAX_CAMERA_LIST,NULL);
            QThread::msleep(100);
            numTries++;
        }

        if(cameraNum) {
            struct in_addr addr;
            tPvIpSettings Conf;
            tPvErr lErr;

            // display info
            for(unsigned long i=0;i<cameraNum;i++) {

                // get the camera's IP configuration
                if((lErr = PvCameraIpSettingsGet(cameraList[i].UniqueId,&Conf)) == ePvErrSuccess) {

                    addr.s_addr = Conf.CurrentIpAddress;
                    if (cameraList[i].PermittedAccess & ePvAccessMaster) {
                        //camera is available for use
                        cameraNames << cameraList[0].DisplayName;
                    }

                } else {

                }

            }
        }else {
            qDebug() << "No AVT camera detected ...";
        }
    }
    */

    for (int i=0; i<detectedCameras.length();i++) {
        cameraNames << detectedCameras[i].cameraName;
    }

    return cameraNames;
}

bool AvtCameraController::open(int cameraID) {
    //tPvCameraInfo   cameraList[MAX_CAMERA_LIST];
    //unsigned long   cameraNum = 0;
    //unsigned long   cameraRle;
    //QStringList cameraNames;

    tPvErr errCode;
    unsigned long FrameSize = 0;


    //IMPORTANT: Initialize camera structure. See tPvFrame in PvApi.h for more info.
    memset(&cameraInfo,0,sizeof(tCamera));

    /*
    int numTries = 0;
    // first, get list of reachable cameras.
    while (!cameraNum && numTries < 5) {
        cameraNum = PvCameraList(cameraList,MAX_CAMERA_LIST,NULL);
        QThread::msleep(100);
        numTries++;
    }*/

    //if(cameraNum > cameraID) {
    if(detectedCameras.length() > cameraID) {
        //struct in_addr addr;
        //tPvIpSettings Conf;
        //tPvErr lErr;

        //Get the unique ID of the chosen camera
        //cameraInfo.UID = cameraList[cameraID].UniqueId;

        //open camera
        //if ((errCode = PvCameraOpen(cameraInfo.UID,ePvAccessMaster,&(cameraInfo.Handle))) != ePvErrSuccess) {
        if ((errCode = PvCameraOpenByAddr(detectedCameras[cameraID].ipAddress,ePvAccessMaster,&(cameraInfo.Handle))) != ePvErrSuccess) {

            if (errCode == ePvErrAccessDenied) {
                qDebug() << "PvCameraOpen returned ePvErrAccessDenied.";       
            } else if (errCode == ePvErrNotFound){
                qDebug() << "PvCameraOpen returned ePvErrNotFound.";
            } else {
                qDebug() << "PvCameraOpen error";         
            }
            resetSystem();

            qDebug() << "Trying again...";
            if(detectedCameras.length() > cameraID) {
                if ((errCode = PvCameraOpenByAddr(detectedCameras[cameraID].ipAddress,ePvAccessMaster,&(cameraInfo.Handle))) != ePvErrSuccess) {
                    resetSystem();
                    qDebug() << "Failed again, giving up.";
                    return false;
                }
            }
        }

        /*
        //List available pixel formats
        char enumSet[1000];
        if (PvAttrRangeEnum(cameraInfo.Handle, "PixelFormat",
                            enumSet, sizeof(enumSet), NULL) == ePvErrSuccess)
        {
        char* member = strtok(enumSet, ",");
        // strtok isn't always thread safe!
        while (member != NULL)
        {
        printf("Mode %s\n", member);
        member = strtok(NULL, ",");
        }
        }*/

        //We are currently hardcoding the format to RGB24, but we might want to add options here
         if(PvAttrEnumSet(cameraInfo.Handle,"PixelFormat","Rgb24") == ePvErrSuccess) {
            setFormat(AbstractCamera::Fmt_RGB24);
            isBlackAndWhite = false;
        } else if (PvAttrEnumSet(cameraInfo.Handle,"PixelFormat","Mono8") == ePvErrSuccess) {
            setFormat(AbstractCamera::Fmt_RGB24);
            isBlackAndWhite = true;
        } else {
            qDebug() << "Could not set camera to a supported format";
            return false;
        }


        // Calculate frame buffer size
        if((errCode = PvAttrUint32Get(cameraInfo.Handle,"TotalBytesPerFrame",&FrameSize)) != ePvErrSuccess) {

            qDebug() << "CameraSetup: Get TotalBytesPerFrame error";
            return false;
        }

        // allocate the frame buffers
        for(int i=0;i<FRAMESCOUNT;i++) {

            cameraInfo.Frames[i].ImageBuffer = new char[FrameSize];
            if(cameraInfo.Frames[i].ImageBuffer) {

                cameraInfo.Frames[i].ImageBufferSize = FrameSize;
            } else {

                qDebug() << "CameraSetup: Failed to allocate buffers";
                return false;
            }
        }

        // allocate image buffer
        /*
        cameraInfo.Frame.ImageBuffer = new char[FrameSize+1000];
        if(!cameraInfo.Frame.ImageBuffer) {

            qDebug() << "CameraSetup: Failed to allocate buffers";
            return false;
        }
        cameraInfo.Frame.ImageBufferSize = FrameSize;
        */
        isCameraOpen = true;
        currentCameraID = cameraID;

        //tPvImageFormat pixelFormat; // UNUSED
        char String[256];
        tPvInt64 infoVal;

        PvAttrEnumGet(cameraInfo.Handle,"PixelFormat",String,256,NULL);
        qDebug() << "Image format: " << String;


        //This sets the gain to auto, but we probably don't want this hardcoded
        /*PvAttrEnumSet(cameraInfo.Handle,"GainMode","Auto");

        PvAttrEnumGet(cameraInfo.Handle,"GainMode",String,256,NULL);
        qDebug() << "Gain Mode: " << String;*/


        tPvInt64 minVal;
        tPvInt64 maxXVal;
        tPvInt64 maxYVal;
        PvAttrRangeInt64(cameraInfo.Handle,"Height",&minVal,&maxYVal);
        qDebug() << "Y resolution range: " << (int64_t)minVal << "to" << (int64_t)maxYVal;
        PvAttrRangeInt64(cameraInfo.Handle,"Width",&minVal,&maxXVal);
        qDebug() << "X resolution range: " << (int64_t)minVal << "to" << (int64_t)maxXVal;


        //Try to use maximum resolution
        PvAttrInt64Set(cameraInfo.Handle,"Height",maxYVal);
        PvAttrInt64Set(cameraInfo.Handle,"Width",maxXVal);


        PvAttrInt64Get(cameraInfo.Handle,"Height",&infoVal);
        qDebug() << "Y resolution: " << (int64_t)infoVal;

        PvAttrInt64Get(cameraInfo.Handle,"Width",&infoVal);
        qDebug() << "X resolution: " << (int64_t)infoVal;






        //Use this to list the camera properties to set
        /*tPvAttrListPtr listPtr;
        unsigned long listLength;

        if (PvAttrList(cameraInfo.Handle, &listPtr, &listLength) == ePvErrSuccess)
        {
           for (int i = 0; i < listLength; i++)
           {
             const char* attributeName = listPtr[i];
             printf("Attribute %s\n", attributeName);
           }
        }*/


        return true;
    } else {
        qDebug() << "Bad camera ID selection...";
        return false;
    }

}

void AvtCameraController::close() {
    tPvErr errCode;
    if (isCameraAcquiring) {
        stop();
    }
    if (isCameraOpen) {
        if((errCode = PvCameraClose(cameraInfo.Handle)) != ePvErrSuccess) {
            qDebug() << "Error closing camera";
        } else {
            qDebug() << "Camera closed.";
        }

        // free image buffers

        for(int i=0;i<FRAMESCOUNT;i++)
            delete [] (char*)cameraInfo.Frames[i].ImageBuffer;

        //delete [] (char*)cameraInfo.Frame.ImageBuffer;
    }
}

bool AvtCameraController::start() {
    //Starts acquisition
    //If frameRate > 0, then the camera is put into software trigger mode, where a timer
    //is used to trigger frames at the desired frame rate.  If frameRate is < 1, then the camera is
    //put into hardware trigger mode.

    tPvErr errCode;
    int frameRate = 30;
    frameIndex = 0;

    numFramesIn = 0;
    numFramesHandled = 0;

    // NOTE: This call sets camera PacketSize to largest sized test packet, up to 8228, that doesn't fail
    // on network card. Some MS VISTA network card drivers become unresponsive if test packet fails.
    // Use PvUint32Set(handle, "PacketSize", MaxAllowablePacketSize) instead. See network card properties
    // for max allowable PacketSize/MTU/JumboFrameSize.
    if((errCode = PvCaptureAdjustPacketSize(cameraInfo.Handle,8228)) != ePvErrSuccess) {
        qDebug() << "CameraStart: PvCaptureAdjustPacketSize error";
        return false;
    }

    // start driver capture stream
    if((errCode = PvCaptureStart(cameraInfo.Handle)) != ePvErrSuccess) {
        qDebug() << "CameraStart: PvCaptureStart error";
        return false;
    }

    // queue frames.
    for(int i=0;i<FRAMESCOUNT;i++) {

        if((errCode = PvCaptureQueueFrame(cameraInfo.Handle,&(cameraInfo.Frames[i]),FrameDoneCB)) != ePvErrSuccess) {
            qDebug() << "CameraStart: PvCaptureQueueFrame error";
            PvCaptureEnd(cameraInfo.Handle);
            return false;
        }

        /*if((errCode = PvCaptureQueueFrame(cameraInfo.Handle,&(cameraInfo.Frames[i]),NULL)) != ePvErrSuccess) {
            qDebug() << "CameraStart: PvCaptureQueueFrame error";
            PvCaptureEnd(cameraInfo.Handle);
            return false;
        }*/
    }

    // queue frame
    /*if((errCode = PvCaptureQueueFrame(cameraInfo.Handle,&(cameraInfo.Frame),NULL)) != ePvErrSuccess) {
        qDebug() << "CameraStart: PvCaptureQueueFrame error";
        // stop driver capture stream
        PvCaptureEnd(cameraInfo.Handle);
        return false;
    }*/



    if (frameRate > 0) {

        /*
        tPvFloat32 minRange;
        tPvFloat32 maxRange;

        tPvErr errorCode;
        errorCode = PvAttrRangeFloat32(cameraInfo.Handle, "FrameRate", &minRange, &maxRange);

        if (errorCode == ePvErrSuccess) {
            qDebug() << "fps range: " << minRange << maxRange;
        } else if (errorCode == ePvErrNotFound) {
            qDebug() << "Frame rate attribute not found";

        }*/

        /*
        if (PvAttrFloat32Set(cameraInfo.Handle, "FrameRate", 29.0) != ePvErrSuccess) {

                qDebug() << "Could not set frame rate on camera.";
        }*/

        if((PvAttrEnumSet(cameraInfo.Handle,"FrameStartTriggerMode","Freerun") != ePvErrSuccess) ||
                (PvAttrEnumSet(cameraInfo.Handle,"AcquisitionMode","Continuous") != ePvErrSuccess) ||
                (PvCommandRun(cameraInfo.Handle,"AcquisitionStart") != ePvErrSuccess))
        {
            qDebug() << "CameraStart: failed to set camera attributes";
            // clear queued frame
            PvCaptureQueueClear(cameraInfo.Handle);
            // stop driver capture stream
            PvCaptureEnd(cameraInfo.Handle);
            return false;
        }
        //Set up trigger timer
        frameTriggerTimer.start(10); //Timer to trigger if any frames have come in.
    }


    /*
    if (frameRate > 0) {
        // set the camera in software trigger, continuous mode, and start camera receiving triggers
        if((PvAttrEnumSet(cameraInfo.Handle,"FrameStartTriggerMode","Software") != ePvErrSuccess) ||
                (PvAttrEnumSet(cameraInfo.Handle,"AcquisitionMode","Continuous") != ePvErrSuccess) ||
                (PvCommandRun(cameraInfo.Handle,"AcquisitionStart") != ePvErrSuccess)) {

            qDebug() << "CameraStart: failed to set camera attributes";
            // clear queued frame
            PvCaptureQueueClear(cameraInfo.Handle);
            // stop driver capture stream
            PvCaptureEnd(cameraInfo.Handle);
            return false;
        }
        //Set up trigger timer
        frameTriggerTimer.start(30);

    }*/
    isCameraAcquiring = true;

    return true;
}

void AvtCameraController::stop() {
    frameTriggerTimer.stop();
    tPvErr errCode;

    //stop camera receiving triggers
    if ((errCode = PvCommandRun(cameraInfo.Handle,"AcquisitionStop")) != ePvErrSuccess) {
        qDebug() << "AcquisitionStop command error";
    } else {
        qDebug() << "Camera stopped";
    }

    //clear queued frames. will block until all frames dequeued
    if ((errCode = PvCaptureQueueClear(cameraInfo.Handle)) != ePvErrSuccess) {
        qDebug() << "PvCaptureQueueClear error";
    } else {
        qDebug() << "Queue cleared";
    }

    //stop driver stream
    if ((errCode = PvCaptureEnd(cameraInfo.Handle)) != ePvErrSuccess) {
        qDebug() << "PvCaptureEnd error";
    } else {
        qDebug() << "Driver stream stopped";
    }
    isCameraAcquiring = false;
}

void AvtCameraController::blinkAcquire() {

    startBlinkTimer.start(250); //The landmark pause starts 1/4 second after record start to make sure a few frames come in.
    /*
    if (isCameraAcquiring) {
        qDebug() << "Creating brief pause in video acquisition";
        stop();
        blinkTimer.start(500);
    }*/
}

void AvtCameraController::resetCurrentCamera() {
    int camID = currentCameraID;

    close();
    PvUnInitialize();
    PvInitializeNoDiscovery();
    if (!open(camID)) {
        qDebug() << "Error during camera reset.  Open procedure failed.";
    }
    if (!start()) {
        qDebug() << "Error during camera reset.  Start procedure failed.";
    }
    QThread::msleep(500);

}

void AvtCameraController::startBlink() {

    if (isCameraAcquiring) {
        qDebug() << "Creating brief pause in video acquisition";
        stop();
        blinkTimer.start(500);
    }
}

void AvtCameraController::endBlink() {

    if (isCameraOpen) {
        start();
    }
}

void AvtCameraController::getFrame() {
    tPvErr errCode;


    /*
    if (PvCaptureWaitForFrameDone(cameraInfo.Handle,&(cameraInfo.Frames[frameIndex]),10) == ePvErrTimeout) {

        return; //If no frame is found within the timout period, abort
    }*/

    while (PvCaptureWaitForFrameDone(cameraInfo.Handle,&(cameraInfo.Frames[frameIndex]),1) != ePvErrTimeout) {
        //check returned Frame.Status
        if(cameraInfo.Frames[frameIndex].Status == ePvErrSuccess) {



            //qDebug() << cameraInfo.Frames[frameIndex].FrameCount;


            QImage* sendImagePtr;

            //convert to QImage
            if (!isBlackAndWhite) {
                QImage imageConvert((uchar*)cameraInfo.Frames[frameIndex].ImageBuffer,
                                    (int)cameraInfo.Frames[frameIndex].Width,
                                    (int)cameraInfo.Frames[frameIndex].Height,cameraInfo.Frames[frameIndex].Width*3,
                                    QImage::Format_RGB888);

                sendImagePtr = new QImage(imageConvert.copy()); //this pointer will be added to a buffer and deleted later once it has been processed and saved.
            } else {
                QImage imageConvert((uchar*)cameraInfo.Frames[frameIndex].ImageBuffer,
                                    (int)cameraInfo.Frames[frameIndex].Width,
                                    (int)cameraInfo.Frames[frameIndex].Height,cameraInfo.Frames[frameIndex].Width,
                                    QImage::Format_Indexed8);
                imageConvert.setColorTable(colorTable);

                sendImagePtr = new QImage(imageConvert.convertToFormat(QImage::Format_RGB888)); //this pointer will be added to a buffer and deleted later once it has been processed and saved.

            }

            /*
        QImage *imageConvert = new QImage((uchar*)cameraInfo.Frame.ImageBuffer,
                         (int)cameraInfo.Frame.Width,
                         (int)cameraInfo.Frame.Height,cameraInfo.Frame.Width*3,
                         QImage::Format_RGB888);
        */
            emit newFrame(sendImagePtr, cameraInfo.Frames[frameIndex].FrameCount, false);
            emit newFrame();

            //sendFrameSignals(sendImagePtr, cameraInfo.Frames[frameIndex].FrameCount, false);


        } else {
            if (cameraInfo.Frames[frameIndex].Status == ePvErrDataMissing) {
                //qDebug() << "Dropped packets. Possible improper network card settings. See GigE Installation Guide.";
            } else {
                qDebug() << "Frame.Status error";
            }
        }

        //Requeue frame
        //if ((errCode = PvCaptureQueueFrame(cameraInfo.Handle,&cameraInfo.Frames[frameIndex],NULL)) != ePvErrSuccess) {
        //    qDebug() << "PvCaptureQueueFrame error";
        //}

        //Requeue frame
        if ((errCode = PvCaptureQueueFrame(cameraInfo.Handle,&cameraInfo.Frames[frameIndex],NULL)) != ePvErrSuccess) {
            if (errCode == ePvErrBadSequence) {
                qDebug() << "PvCaptureQueueFrame error. Can't queue frame until camera stream has started.";
            } else if (errCode == ePvErrQueueFull) {
                qDebug() << "PvCaptureQueueFrame error. Frame queue full.";
            } else if (errCode == ePvErrUnplugged) {
                qDebug() << "PvCaptureQueueFrame error. Camera unplugged or not responsive.";
            }
            //resetCurrentCamera();

        }

        //Incement the circular frame buffer index
        frameIndex = (frameIndex + 1) % FRAMESCOUNT;
    }


}

void AvtCameraController::getAvailableFrames() {
    tPvErr errCode;
    quint64 fIn = numFramesIn;

    while (numFramesHandled < fIn) {
        //check returned Frame.Status
        if(cameraInfo.Frames[frameIndex].Status == ePvErrSuccess) {



            //qDebug() << cameraInfo.Frames[frameIndex].FrameCount;


            QImage* sendImagePtr;

            //convert to QImage
            if (!isBlackAndWhite) {
                QImage imageConvert((uchar*)cameraInfo.Frames[frameIndex].ImageBuffer,
                                    (int)cameraInfo.Frames[frameIndex].Width,
                                    (int)cameraInfo.Frames[frameIndex].Height,cameraInfo.Frames[frameIndex].Width*3,
                                    QImage::Format_RGB888);

                sendImagePtr = new QImage(imageConvert.copy()); //this pointer will be added to a buffer and deleted later once it has been processed and saved.
            } else {
                QImage imageConvert((uchar*)cameraInfo.Frames[frameIndex].ImageBuffer,
                                    (int)cameraInfo.Frames[frameIndex].Width,
                                    (int)cameraInfo.Frames[frameIndex].Height,cameraInfo.Frames[frameIndex].Width,
                                    QImage::Format_Indexed8);
                imageConvert.setColorTable(colorTable);

                sendImagePtr = new QImage(imageConvert.convertToFormat(QImage::Format_RGB888)); //this pointer will be added to a buffer and deleted later once it has been processed and saved.

            }



            /*if (cameraInfo.Frames[frameIndex].FrameCount-oldFrameCount > 1) {
                qDebug() << "Source missing frames:" << QThread::currentThreadId();
            }*/
            oldFrameCount = cameraInfo.Frames[frameIndex].FrameCount;
            emit newFrame(sendImagePtr, cameraInfo.Frames[frameIndex].FrameCount, false);
            emit newFrame();




        } else {
            if (cameraInfo.Frames[frameIndex].Status == ePvErrDataMissing) {
                //qDebug() << "Dropped packets. Possible improper network card settings. See GigE Installation Guide.";
            } else {
                qDebug() << "Frame.Status error";
            }
        }

        //Requeue frame

        if ((errCode = PvCaptureQueueFrame(cameraInfo.Handle,&cameraInfo.Frames[frameIndex],FrameDoneCB)) != ePvErrSuccess) {
            if (errCode == ePvErrBadSequence) {

                qDebug() << "PvCaptureQueueFrame error. Can't queue frame until camera stream has started.";
            } else if (errCode == ePvErrQueueFull) {
                qDebug() << "PvCaptureQueueFrame error. Frame queue full.";
            } else if (errCode == ePvErrUnplugged) {
                qDebug() << "PvCaptureQueueFrame error. Camera unplugged or not responsive.";
            }
            resetCurrentCamera();
            break;

        }

        //Add to the circular frame buffer index
        frameIndex = (frameIndex + 1) % FRAMESCOUNT;

        numFramesHandled++;


    }

    //Used to test the camera reset procedure
    /*
    if (numFramesHandled > 200 && !hasReset) {
        hasReset = true;
        qDebug() << "Test reset.";
        resetCurrentCamera();
    }*/


}
