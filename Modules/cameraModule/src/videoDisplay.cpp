/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "videoDisplay.h"
#include <QTime>


//VideoDisplayController::VideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient, VideoWriter *writer):

#ifdef ARAVIS
typedef struct {
	VideoDisplayWindow* display;
	GigECameraInterface* camera;
} GigEVideoObj;

void test_callback(GigECameraInterface* camera, VideoDisplaySurface* surf) {
	qDebug() << ("callback!");
}

void GigEVideoDisplayController::new_frame_callback(ArvStream *stream, void* callback_data) {
	GigEVideoObj* gige_video = reinterpret_cast<GigEVideoObj*>(callback_data);
	uchar* raw_frame = gige_video->camera->get_current_frame();

	int height = gige_video->camera->get_current_height();
	int width = gige_video->camera->get_current_width();

	QImage* frame_img = new QImage(raw_frame, width, height, QImage::Format_Mono);
	//gige_video->display->newImage(frame_img);
	//gige_video->display->videoSurface()->present(frame);
}

GigEVideoDisplayController::GigEVideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient, VideoEncoder *encoder) : tcpClient(tcpClient) {
    
	imageProcessor = new VideoImageProcessor(this,encoder);

	_arv = AravisWrapper::get_instance();	
	// Try opening first camera
	_cur_camera = _arv->open_camera(0);
	GigEVideoObj* gige_video = (GigEVideoObj*) g_malloc(sizeof(*gige_video));

	gige_video->display = displayWidget;
	gige_video->camera = _cur_camera;

	int height = gige_video->camera->get_current_height();
	int width = gige_video->camera->get_current_width();

	//connect(this, SIGNAL(dummy()), displayWidget, SLOT(dumber()));
	
	_cur_camera->set_new_frame_callback((void*)&GigEVideoDisplayController::new_frame_callback, gige_video);
	
	qDebug() << "starting aquisition";
	_cur_camera->start_aquisition();
}

GigEVideoDisplayController::~GigEVideoDisplayController() {
}
#endif

VideoDisplayController::VideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient):
	tcpClient(tcpClient) {
    if (DEBUG_MODE) { int fun = 1;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    //cameraInput = NULL;
    liveCameraMode = false;
    decoder = NULL;
    currentCameraType = -1;
    currentCameraNum = -1;

}

void VideoDisplayController::resetCamera() {
    if (DEBUG_MODE) { int fun = 2;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (currentCameraType > -1) {
        cameraControllers[currentCameraType]->resetCurrentCamera();
    }

}

void VideoDisplayController::startController() {
    if (DEBUG_MODE) { int fun = 3;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //This is called once the thread has started

    imageProcessor = new VideoImageProcessor(this);
    connect(this,SIGNAL(signal_startRecording()),imageProcessor,SLOT(startRecording()));
    connect(this,SIGNAL(signal_stopRecording()),imageProcessor,SLOT(stopRecording()));
    connect(this,SIGNAL(signal_createFile(QString)),imageProcessor,SLOT(createFile(QString)));
    connect(this,SIGNAL(signal_closeFile()),imageProcessor,SLOT(closeFile()));
    connect(this,SIGNAL(sig_UserInput1(QPoint)),imageProcessor,SLOT(userInput1(QPoint)));
    connect(this,SIGNAL(sig_UserInput2(QPoint)),imageProcessor,SLOT(userInput2(QPoint)));
    connect(this,SIGNAL(signal_createPlaybackLogFile(QString)),imageProcessor,SLOT(createPlaybackLogFile(QString)));
    connect(this,SIGNAL(signal_closePlaybackLogFile()),imageProcessor,SLOT(closePlaybackLogFile()));
    connect(this,SIGNAL(signal_linearGeometryExists(bool)),imageProcessor,SLOT(linearGeometryExists(bool)));
    connect(this,SIGNAL(signal_newLinearGeometry(QVector<QPointF>,QVector<LineNodeIndex>)),imageProcessor,SLOT(newLinearGeometry(QVector<QPointF>,QVector<LineNodeIndex>)));

    connect(this,SIGNAL(signal_newSettings(TrackingSettings)),imageProcessor,SLOT(newTrackingSettings(TrackingSettings)));
    //connect(imageProcessor,SIGNAL(newImage_signal(QImage)),this,SIGNAL(newFrame(QImage))); //To paint the image on the screen
    connect(imageProcessor,SIGNAL(newAnimalLocation(quint32)),this,SIGNAL(newAnimalPos(quint32)));
    connect(imageProcessor,SIGNAL(sendNewDataPacket(dataPacket)),this,SIGNAL(sendDataPacket(dataPacket)));
    connect(imageProcessor,SIGNAL(badLocation()),this,SLOT(pauseForBadLoc()));
    connect(this, SIGNAL(sig_seekFrame(qint32)),this,SLOT(seekFrame(qint32)));
    connect(this,SIGNAL(newVideoTimeStamp(quint32)),imageProcessor,SLOT(newTimestamp(quint32)));
    connect(imageProcessor,SIGNAL(streamStarted(int,int)),this,SIGNAL(videoStreamStart(int,int)));
    connect(imageProcessor,SIGNAL(watchdogAlarm()),this,SLOT(resetCamera()));



    cameraControllers.push_back(new WebcamWrapper());

#ifdef AVT_GIGE
    cameraControllers.push_back(new AvtCameraController());
#endif

    connect(tcpClient,SIGNAL(currentTimeReceived(quint32)),imageProcessor,SLOT(newTimestamp(quint32)));


    //connect the frame signals for each camera controller
    for (int i = 0;i < cameraControllers.length();i++) {
        connect(cameraControllers[i],SIGNAL(newFrame(QImage*,quint32, bool)),imageProcessor,SLOT(newImage(QImage*,quint32, bool)),Qt::QueuedConnection);
        connect(cameraControllers[i],SIGNAL(formatSet(AbstractCamera::videoFmt)),imageProcessor,SLOT(setVideoFormat(AbstractCamera::videoFmt)));
        //connect(cameraControllers[i],SIGNAL(debugSig()), imageProcessor,SLOT(debugSlot()));
        if (tcpClient->isConnected()) {
            connect(cameraControllers[i],SIGNAL(newFrame()),tcpClient,SLOT(sendTimeRequest()));
        }
    }

    //Move image processor to new thread

    imageProcessorThread = new QThread;
    imageProcessorThread->setObjectName("ImageProcessor");
    imageProcessor->moveToThread(imageProcessorThread);
    connect(imageProcessor, SIGNAL(finished()), imageProcessorThread, SLOT(quit()));
    connect(imageProcessor, SIGNAL(finished()), imageProcessor, SLOT(deleteLater()));
    connect(imageProcessor,SIGNAL(streamStarted(int,int)),this,SIGNAL(videoStreamStart(int,int)));
    connect(imageProcessorThread, SIGNAL(started()), imageProcessor, SLOT(initialize()));
    connect(imageProcessorThread, SIGNAL(finished()), imageProcessorThread, SLOT(deleteLater()));
    connect(this,SIGNAL(signal_endProcessor()),imageProcessor,SLOT(endProcessing()));
    imageProcessorThread->start();




    //imageProcessor->initialize();

    if (tcpClient->isConnected()) {
        //connect(surface,SIGNAL(newFrame()),tcpClient,SLOT(sendTimeRequest()));
    }

    connect(&displayFirstFrameTimer,SIGNAL(timeout()),this,SLOT(displayFirstFrameFromFile()));
    connect(this,SIGNAL(signal_startFirstFrameTimer()),this,SLOT(startFirstFrameTimer()));



    frameReadTimer = new QTimer(this);
    connect(frameReadTimer,SIGNAL(timeout()),this,SLOT(readNextFrameFromFile()));
    //if (liveCameraMode) {
    //    startFirstCameraFound();
    //}
    emit processorCreated();
}

bool VideoDisplayController::inputFileSelected(QString fileName) {
    if (DEBUG_MODE) { int fun = 4;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    if (decoder) {
        //delete the encoder if it exists
        closePlaybackFile();
    }

    decoder = new H264_Decoder();

    if( !decoder->load(fileName,25.0)) {

        qDebug() << QString("File %1 not found").arg(fileName);
        return false;
    }

    int tries = 0;
    bool readsuccess = false;
    while (tries < 5) {
        if (!decoder->readFrame()) {
            tries++;
            qDebug() << "Could not read frame.";
        } else {
            readsuccess = true;
            break;
        }
    }

    if (!readsuccess) {
        qDebug() << "File load failed.  Could not read a frame";
        //Need an error dialog to pop up
        return false;
    }

    playbackTimeStamps.clear();
    currentFrameNum = 0;
    QFileInfo fileInfo(fileName);

    QFile timeStampFile;

    timeStampFile.setFileName(fileInfo.absolutePath()+"/"+fileInfo.completeBaseName()+".videoTimeStamps");
    qDebug() << "Opening file: " << timeStampFile.fileName();
    if (timeStampFile.exists()) {
        if (!timeStampFile.open(QIODevice::ReadOnly)) {
            qDebug() << "Error: cannot open time stamp file";
        } else {
            QString line;

            line += timeStampFile.readLine();
            if (line.contains("<Start settings>")) {

                bool endOfHeaderFound = false;
                while (!timeStampFile.atEnd() && !endOfHeaderFound) {
                    line.clear();
                    line += timeStampFile.readLine();
                    if (line.contains("Clock rate:")) {
                        QString clockRateString = line.remove(0,12);
                        clockRateString.chop(1);
                        bool ok = false;
                        int readClockRate;
                        readClockRate = clockRateString.toInt(&ok);
                        if (ok) {
                          emit videoFileTimeRate((quint32)readClockRate);
                        } else {
                            emit videoFileTimeRate(30000);
                        }

                    } else if (line.contains("<End settings>")) {
                        endOfHeaderFound = true;
                    }
                }
            } else {
                //No header exists
                timeStampFile.seek(0);
            }
            //char timeStampData[4];
            quint32 timeStampData;
            quint32* timeStampDataPtr = &timeStampData;

            while (!timeStampFile.atEnd()) {

                if (timeStampFile.read((char*)timeStampDataPtr,4) < 4) {
                    qDebug() << "Not enough data";
                    break;
                }
                playbackTimeStamps.append(timeStampData);
            }
            timeStampFile.close();
            sliderStart = 0;
            sliderEnd = playbackTimeStamps.length();
            emit videoFileTimeRange(playbackTimeStamps[0],playbackTimeStamps.last());
            //emit videoFileTimeRate(30000);



        }
    } else {
        qDebug() << "No time stamp file found!";
    }

    //QFile file;
    //file.setFileName(fileName);
    //QVideoDecoder decoder;



    closeCurrentCamera();


    /*
    if (cameraInput != NULL) {
        cameraInput->unload();
        cameraInput->deleteLater();
        cameraInput = NULL;

    }*/

    //Display the first frame
    displayFirstFrameFromFile();



    //emit signal_startFirstFrameTimer();
    //displayFirstFrameTimer.start(250);

    return true;
}

void VideoDisplayController::startFirstFrameTimer() {
    if (DEBUG_MODE) { int fun = 5;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    displayFirstFrameTimer.start(250);
}

void VideoDisplayController::stepForward() {
    if (DEBUG_MODE) { int fun = 6;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    readNextFrameFromFile();
}

void VideoDisplayController::stepBackward() {
    if (DEBUG_MODE) { int fun = 7;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    qint32 desiredFrame = currentFrameNum - 1;
    seekFrame(desiredFrame);
}

void VideoDisplayController::seekFrame(qint32 desiredframe) {
    if (DEBUG_MODE) { int fun = 8;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    bool frameTimerRunning = frameReadTimer->isActive();
    frameReadTimer->stop();
    if (!decoder->seekFrame(desiredframe)) {
        qDebug() << "failed to seek";
        return;
    }
    currentFrameNum = desiredframe;
    QImage *newFrame;
    decoder->createQImageFromFrame(newFrame);


    //Should be a signal (separate thread)
    imageProcessor->newImage(newFrame, 0, false);


    if (!playbackTimeStamps.isEmpty() && playbackTimeStamps.length() > currentFrameNum) {

        imageProcessor->seekPositionFile(playbackTimeStamps[currentFrameNum]); //should be a signal
        emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
    }
    if (frameTimerRunning) {
        frameReadTimer->start(1000/25);
    }

}

void VideoDisplayController::setLiveCameraMode(bool on) {
    if (DEBUG_MODE) { int fun = 9;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    liveCameraMode = on;
}

void VideoDisplayController::seekToTime(quint32 t) {
    if (DEBUG_MODE) { int fun = 10;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    qint32 desiredframe = 0;
    if (!playbackTimeStamps.isEmpty()) {
        while ((playbackTimeStamps.at(desiredframe) < t) && (desiredframe < playbackTimeStamps.length())) {
            desiredframe++;
        }
    } else {
        return;
    }

    //We emit a signal instead of a direct call becuase this function gets called from
    //another thread.
    emit sig_seekFrame(desiredframe);
    //seekFrame(desiredframe);
}

void VideoDisplayController::seekRelativeFrame(int position) {
    if (DEBUG_MODE) { int fun = 11;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //position is an integer between 0 and TIMESLIDERSTEPS, with TIMESLIDERSTEPS being the end of the file.

    qint32 desiredframe;
    if (!playbackTimeStamps.isEmpty()) {
         //emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
        double framesPerUnit = (double)(playbackTimeStamps.length()-1)/TIMESLIDERSTEPS;
        desiredframe = position*framesPerUnit;
    } else {
        return;
    }

    //We emit a signal instead of a direct call becuase this function gets called from
    //another thread.
    emit sig_seekFrame(desiredframe);
    //seekFrame(desiredframe);

}

void VideoDisplayController::newSliderRange(int start, int end) {
    if (DEBUG_MODE) { int fun = 12;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    quint32 startPos;
    quint32 endPos;
    qint32 desiredframe;
    if (!playbackTimeStamps.isEmpty()) {
         //emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
        double framesPerUnit = (double)(playbackTimeStamps.length()-1)/TIMESLIDERSTEPS;
        desiredframe = start*framesPerUnit;
        if (desiredframe >= playbackTimeStamps.length()) {
            desiredframe = playbackTimeStamps.length()-1;
        }
        startPos = playbackTimeStamps[desiredframe];
        sliderStart = desiredframe;

        desiredframe = end*framesPerUnit;
        if (desiredframe >= playbackTimeStamps.length()) {
            desiredframe = playbackTimeStamps.length()-1;
        }
        endPos = playbackTimeStamps[desiredframe];
        sliderEnd = desiredframe;

        emit sig_newSliderRange(startPos,endPos);
    }
}

void VideoDisplayController::newTimeRange(quint32 start, quint32 end) {
    //This is called when the time range was given via string input (instead of with the slider)
    if (DEBUG_MODE) { int fun = 13;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    qint32 desiredStartframe = 0;
    qint32 desiredEndframe = 0;
    if (!playbackTimeStamps.isEmpty()) {
         //emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
        while ((playbackTimeStamps.at(desiredStartframe) < start) && (desiredStartframe < playbackTimeStamps.length())) {
            desiredStartframe++;
        }

        while ((playbackTimeStamps.at(desiredEndframe) < end) && (desiredEndframe < playbackTimeStamps.length())) {
            desiredEndframe++;
        }

        sliderStart = desiredStartframe;
        sliderEnd = desiredEndframe;

    }
}

void VideoDisplayController::startPlayback() {
    //seekFrame(1);
    if (DEBUG_MODE) { int fun = 14;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    fastPlayback = false;
    imageProcessor->setFastPlayback(false);
    frameReadTimer->start(1000/25);

}

void VideoDisplayController::startFastPlayback() {
    if (DEBUG_MODE) { int fun = 15;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    fastPlayback = true;
    imageProcessor->setFastPlayback(true);
    frameReadTimer->start(100/25);
}

void VideoDisplayController::pausePlayback() {
    if (DEBUG_MODE) { int fun = 16;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    frameReadTimer->stop();
}

void VideoDisplayController::closePlaybackFile() {
    if (DEBUG_MODE) { int fun = 17;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    frameReadTimer->stop();
    if (decoder) {
        delete decoder;
        decoder = NULL;
    }
    emit filePlaybackClosed();
}

void VideoDisplayController::pauseForBadLoc() {
    if (DEBUG_MODE) { int fun = 18;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (!liveCameraMode) {
        pausePlayback();
        emit getUserTrackingInput();
        emit pausedForBadLoc();
    }
}

void VideoDisplayController::userInput1(QPoint loc) {
    if (DEBUG_MODE) { int fun = 19;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (!liveCameraMode && !frameReadTimer->isActive()) {
        emit sig_UserInput1(loc);
    }
}

void VideoDisplayController::userInput2(QPoint loc) {
    if (DEBUG_MODE) { int fun = 20;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (!liveCameraMode && !frameReadTimer->isActive()) {
        emit sig_UserInput2(loc);
    }
}

void VideoDisplayController::linearGeometryExists(bool on) {
    if (DEBUG_MODE) { int fun = 21;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    emit signal_linearGeometryExists(on);
}

void VideoDisplayController::newLinearGeometry(QVector<QPointF> nodes, QVector<LineNodeIndex> lines) {
    if (DEBUG_MODE) { int fun = 22;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    emit signal_newLinearGeometry(nodes, lines);
}

void VideoDisplayController::displayFirstFrameFromFile() {
    if (DEBUG_MODE) { int fun = 23;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Display the first frame of the file

    QImage *newFrame;
    //decoder->getFrame(newFrame);
    decoder->createQImageFromFrame(newFrame);
    imageProcessor->newImage(newFrame, 0, false);
    displayFirstFrameTimer.stop();
    if (!playbackTimeStamps.isEmpty() && playbackTimeStamps.length() > currentFrameNum) {
         emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
    }

    emit filePlaybackReady();

}

void VideoDisplayController::readNextFrameFromFile() {
    if (DEBUG_MODE) { int fun = 24;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //This function is called regularly by the frameReadTimer when the
    //play button is pressed
    if (currentFrameNum >= sliderEnd) {
        frameReadTimer->stop();
        emit filePlaybackStopped();
        return;
    }

    if (decoder != NULL) {
        if (!decoder->readFrame()) {
            qDebug() << "End of file reached";
            qDebug() << "Number of expected frames: " << playbackTimeStamps.length();
            frameReadTimer->stop();
            emit filePlaybackStopped();
            //seekFrame(0);
            return;
        }

        QImage *newFrame;
        decoder->createQImageFromFrame(newFrame);
        //decoder->getFrame(newFrame);

        imageProcessor->newImage(newFrame, 0, false);

        currentFrameNum++;
        if (!playbackTimeStamps.isEmpty() && playbackTimeStamps.length() > currentFrameNum) {
            emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
        }
    }
}

QStringList VideoDisplayController::availableCameras() {
    if (DEBUG_MODE) { int fun = 25;
    qDebug() << "       Function Call " << fun; } //debugging mode call

   /* QStringList cameraNames;
    for (int i = 0; i < cameraControllers.length(); i++) {
        QStringList tempCamList = cameraControllers[i]->availableCameras();
        cameraNames.append(tempCamList);
    }*/

    cameraNames.clear();
    cameraType.clear();
    cameraNum.clear();

    for (int i = 0; i < cameraControllers.length(); i++) {

        QStringList tempCamList = cameraControllers[i]->availableCameras();
        cameraNames.append(tempCamList);
        for (int j = 0; j < tempCamList.length(); j++) {
            cameraType.append(i);
            cameraNum.append(j);
        }
    }

    return cameraNames;
}

void VideoDisplayController::closeCurrentCamera() {
    if (DEBUG_MODE) { int fun = 26;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (currentCameraType > -1) {
        cameraControllers[currentCameraType]->stop();
        cameraControllers[currentCameraType]->close();       
    }
    currentCameraType = -1;
    currentCameraNum = -1;
}

void VideoDisplayController::startFirstCameraFound() {
    if (DEBUG_MODE) { int fun = 27;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    int lastCameraOpen = 0;
    QString lastCameraOpenName = "";
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("camera"));
    lastCameraOpen = settings.value(QLatin1String("lastCameraNum")).toInt();
    lastCameraOpenName = settings.value(QLatin1String("lastCameraName")).toString();
    settings.endGroup();

    /*QVector<int> cameraType; //unique for each driver type
    QVector<int> cameraNum; //for each driver, count the number of cameras available
    QStringList cameraNames;
    for (int i = 0; i < cameraControllers.length(); i++) {
        QStringList tempCamList = cameraControllers[i]->availableCameras();
        cameraNames.append(tempCamList);
        for (int j = 0; j < tempCamList.length(); j++) {
            cameraType.append(i);
            cameraNum.append(j);
        }
    }*/

    //The last open camera may still be available, so start that
    if ((lastCameraOpen > -1) && (cameraType.length() > lastCameraOpen)&&(lastCameraOpenName == cameraNames[lastCameraOpen])) {
        newCameraSelected(lastCameraOpen);
    } else if (cameraType.length() > 0) {
        newCameraSelected(0);
    }
}

void VideoDisplayController::newCameraSelected(int cameraID) {

    if (DEBUG_MODE) { int fun = 28;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (decoder) {
        //delete the encoder if it exists
        closePlaybackFile();
    }

    bool fileInitiated = false;
    if (imageProcessor->isFileCreated()) {
        //A file for saving video is open.  We need to start a new one for this camera.
        fileInitiated = true;
        //imageProcessor->closeFile();
        //emit signal_closeFile();
    }

    closeCurrentCamera();

    //QVector<int> cameraType; //unique for each driver type
    //QVector<int> cameraNum; //for each driver, count the number of cameras available
    //QStringList cameraNames;


    /*for (int i = 0; i < cameraControllers.length(); i++) {

        QStringList tempCamList = cameraControllers[i]->availableCameras();
        cameraNames.append(tempCamList);
        for (int j = 0; j < tempCamList.length(); j++) {
            cameraType.append(i);
            cameraNum.append(j);
        }
    }*/

    if (cameraType.length() > cameraID) {

        if (!cameraControllers[cameraType[cameraID]]->open(cameraNum[cameraID])) {
            qDebug() << "Error opening camera";
            currentCameraType = -1;
            currentFrameNum = -1;
            return;
        }

        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
        settings.beginGroup(QLatin1String("camera"));
        settings.setValue(QLatin1String("lastCameraNum"), cameraID);
        settings.setValue(QLatin1String("lastCameraName"), cameraNames[cameraID]);
        settings.endGroup();

        qDebug() << "Camera opened";
        if (!cameraControllers[cameraType[cameraID]]->start()) {
            qDebug() << "Error starting camera";
            closeCurrentCamera();
            return;
        }

        qDebug() << "Camera started";
        currentCameraType = cameraType[cameraID];
        currentFrameNum = cameraNum[cameraID];


        if (fileInitiated) {
            emit signal_nextFileNeeded();

        }

    } else {
        qDebug() << "Error in camera selection";


    }

}

VideoDisplayController::~VideoDisplayController() {
    if (DEBUG_MODE) { int fun = 29;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    for (int i = 0; i < cameraControllers.length(); i++) {
        delete cameraControllers[i];
    }
}

void VideoDisplayController::closeDown() {
    if (DEBUG_MODE) { int fun = 30;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    emit finished();
}

/*
void VideoDisplayController::setThresh(int thresh, bool trackDarkPix) {

    imageProcessor->newThreshold(thresh, trackDarkPix);

}*/

void VideoDisplayController::endProcessor() {
    if (DEBUG_MODE) { int fun = 31;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    /*
    if (cameraInput != NULL) {
        cameraInput->unload();
        cameraInput->deleteLater();
        cameraInput = NULL;
    }*/

    //imageProcessor->endProcessing();

    emit signal_endProcessor();
}

void VideoDisplayController::startRecording() {
    if (DEBUG_MODE) { int fun = 32;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    cameraControllers[currentCameraType]->blinkAcquire(); //Used to temporarily pause frame acqisition to make timestamping easier
    emit signal_startRecording();
}

void VideoDisplayController::stopRecording() {
    if (DEBUG_MODE) { int fun = 33;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    emit signal_stopRecording();
}

//------------------------------------------------------------------
RubberBandPolygonNode::RubberBandPolygonNode(int nodeNum, QGraphicsItem *parent):
    //This object is a movable node in a user-drawn polygon. It is a child
    //of a RubberBandPolygon
    QGraphicsRectItem(parent),
    nodeNum(nodeNum)
    {
    if (DEBUG_MODE) { int fun = 35;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    setAcceptHoverEvents(true);
    dragging = false;
    fill = false;


}


void RubberBandPolygonNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    //Set composition mode to be the inverse of the background
    //painter->setCompositionMode(QPainter::CompositionMode_Difference);
    //painter->setCompositionMode(QPainter::RasterOp_SourceAndNotDestination);
    if (DEBUG_MODE) { int fun = 34;
    qDebug() << "       Function Call " << fun; } //debugging mode call


    painter->setOpacity(1);
    QPen pen;
    if (fill) {
        //QBrush b;
        //b.setColor(Qt::white);
        //QPen pen(Qt::red,1);
        pen.setColor(Qt::red);
        pen.setWidth(2);
        painter->setBrush(Qt::red);
    } else {
        pen.setColor(Qt::white);
        pen.setWidth(1);
        painter->setBrush(Qt::NoBrush);
    }
    painter->setPen(pen);
    painter->drawRect(rect());
}

void RubberBandPolygonNode::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
    if (DEBUG_MODE) { int fun = 36;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    fill = true;
    update();
}

void RubberBandPolygonNode::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    if (DEBUG_MODE) { int fun = 37;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    fill = false;
    update();
}

void RubberBandPolygonNode::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    //The node is being dragged
    if (DEBUG_MODE) { int fun = 38;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    QGraphicsItem::mouseMoveEvent(event);
    dragging = true;
    emit nodeMoved(nodeNum);
}

void RubberBandPolygonNode::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 39;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (event->button() == Qt::RightButton) {
        //Node right-clicked

        emit nodeRightClicked(nodeNum, event->pos());
    }
}

void RubberBandPolygonNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 40;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    QGraphicsItem::mouseReleaseEvent(event);
    if (dragging) {
        emit nodeMoveFinished(); //this is emitted whenever you release click on a node in a lineObject
    }
    dragging = false;
}
//----------------------------------------------------------
RubberBandPolygon::RubberBandPolygon(QGraphicsItem *parent)
    :QGraphicsPolygonItem(parent) {
    if (DEBUG_MODE) { int fun = 41;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    //This is a user-drawn polygon to define ROI's
    dragging = false;
    setFlag(QGraphicsItem::ItemIsMovable,true);
    //setFlag(QGraphicsItem::ItemIsSelectable,true);
    id = -1;
    inside = false;
    opacity = 0.2;
}

RubberBandPolygon::~RubberBandPolygon() {
    if (DEBUG_MODE) { int fun = 42;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Not sure if this is actually needed
    while (points.length() > 0) {
        removeLastPoint();
    }
}

void RubberBandPolygon::removeLastPoint() {
    if (DEBUG_MODE) { int fun = 43;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (points.length()>0) {
        points.removeLast();
        relativePoints.removeLast();
        setPolygon(QPolygonF(points));
        curPoly = polygon();
        delete nodes.takeLast();

    }
}

void RubberBandPolygon::moveLastPoint(QPointF newLoc) {
    if (DEBUG_MODE) { int fun = 44;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //move the last point in the polygon to the new location
    //Used as the polygon is being drawn by the user
    if (points.length()>0) {
        points.last().setX(newLoc.x());
        points.last().setY(newLoc.y());
        QPointF newRelativePoint;
        newRelativePoint.setX(newLoc.x()/this->scene()->width());
        newRelativePoint.setY(newLoc.y()/this->scene()->height());
        relativePoints.last().setX(newRelativePoint.x());
        relativePoints.last().setY(newRelativePoint.y());

        setPolygon(QPolygonF(points));
        curPoly = polygon();
        nodes.last()->setPos(newLoc.x()-3,newLoc.y()-3);
    }
}

void RubberBandPolygon::addPoint(QPointF newPoint) {
    //Add a new point to the polygon
    if (DEBUG_MODE) { int fun = 45;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    points.append(newPoint);
    //We need to calculate the point's relative location on
    //the drawing area (between 0 and 1 for both x and y
    //dimensions. This is important for resizing and for
    //calculating what data falls inside the polygon.
    QPointF newRelativePoint;
    newRelativePoint.setX(newPoint.x()/this->scene()->width());
    newRelativePoint.setY(newPoint.y()/this->scene()->height());
    relativePoints.append(newRelativePoint);


    setPolygon(QPolygonF(points));
    curPoly = polygon();

    //We also create a node that can be dragged by the user.
    RubberBandPolygonNode *tmpNode = new RubberBandPolygonNode(points.length()-1,this);
    nodes.append(tmpNode);
    tmpNode->setRect(0,0,6,6);
    tmpNode->setPos(newPoint.x()-3,newPoint.y()-3);
    tmpNode->setFlag(QGraphicsItem::ItemIsMovable,true);
    tmpNode->setVisible(false);
    connect(tmpNode,SIGNAL(nodeMoved(int)),this,SLOT(childNodeMoved(int)));
    connect(tmpNode,SIGNAL(nodeMoveFinished()),this,SLOT(childNodeMoved()));

}

void RubberBandPolygon::childNodeMoved() {
    if (DEBUG_MODE) { int fun = 46;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //this is called after any of the nodes has finished moving
    emit shapeChanged();
}

void RubberBandPolygon::childNodeMoved(int nodeNum) {
    if (DEBUG_MODE) { int fun = 47;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //this is called as the node is moving-- update polygon shape

    points[nodeNum].setX(nodes[nodeNum]->x()+3);
    points[nodeNum].setY(nodes[nodeNum]->y()+3);
    prepareGeometryChange();
    setPolygon(QPolygonF(points));

    QPolygonF scenePoly = mapToScene(polygon());
    curPoly = scenePoly;
    //Also, recalculate the new relative locations
    for (int i=0; i < points.length(); i++) {

        relativePoints[i].setX(scenePoly[i].x()/this->scene()->width());
        relativePoints[i].setY(scenePoly[i].y()/this->scene()->height());
    }

}

void RubberBandPolygon::updateSize() {
    if (DEBUG_MODE) { int fun = 48;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Called after a resize event
    QPointF tmpPoint;
    setPos(0,0);
    for (int i=0; i < points.length(); i++) {
        tmpPoint.setX(relativePoints[i].x()*this->scene()->width());
        tmpPoint.setY(relativePoints[i].y()*this->scene()->height());
        points[i].setX(tmpPoint.x());
        points[i].setY(tmpPoint.y());
        nodes[i]->setX(tmpPoint.x()-3);
        nodes[i]->setY(tmpPoint.y()-3);

    }
    setPolygon(QPolygonF(points));
    curPoly = polygon();
}

void RubberBandPolygon::highlight() {
    if (DEBUG_MODE) { int fun = 49;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Make the draggable nodes visible when the polygon is clicked
    //with the edit tool
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(true);
    }
}

void RubberBandPolygon::removeHighlight() {
    //Hide the draggable nodes
    if (DEBUG_MODE) { int fun = 50;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(false);
    }
}

bool RubberBandPolygon::isIncludeType() {
    if (DEBUG_MODE) { int fun = 51;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (type == 0) {
        return true;
    } else {
        return false;
    }
}

bool RubberBandPolygon::isExcludeType() {
    if (DEBUG_MODE) { int fun = 52;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (type == 1) {
        return true;
    } else {
        return false;
    }
}

bool RubberBandPolygon::isZoneType() {
    if (DEBUG_MODE) { int fun = 53;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (type == 2) {
        return true;
    } else {
        return false;
    }
}

void RubberBandPolygon::setIncludeType() {
    if (DEBUG_MODE) { int fun = 54;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    type = 0;
    setColor(Qt::green);
    //brush = Qt::green;
    //setBrush( Qt::green );

}

void RubberBandPolygon::setExcludeType() {
    if (DEBUG_MODE) { int fun = 55;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    type = 1;
    setColor(Qt::red);
    //brush = Qt::red;
    //setBrush( Qt::red );

}

void RubberBandPolygon::setZoneType() {
    if (DEBUG_MODE) { int fun = 56;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    type = 2;
    setColor(Qt::color0,0.0);
}

void RubberBandPolygon::setColor(const QBrush &br, double opac) {
    if (opac >= 0.0 && opac <= 1.0 )
        opacity = opac;
    brush = br;
    setBrush(brush);
}

void RubberBandPolygon::calculateIncludedPoints(bool *inside, int imageWidth, int imageHeight) {
    //Calculate whether or not each pixel in included.
    //The output depends on the type of polygon (include or exclude polygon)
    if (DEBUG_MODE) { int fun = 57;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Create a new polygon in the shape of this polygon and scale
    //it to the pixel locations of the underlying image
    QVector<QPointF> absPoints;
    absPoints.resize(points.length());
    for (int i=0; i < points.length(); i++) {
        absPoints[i].setX(relativePoints[i].x()*imageWidth);
        absPoints[i].setY(relativePoints[i].y()*imageHeight);
    }
    QPolygonF absPolygon(absPoints);

    //Now we decide if each pixel in the image is included, where 'included'
    //means inside the polygon for include polygons and outside
    //the polygon for exclude polygons
    uint32_t pixnum = 0;
    if (isIncludeType()) {
        for (int h = 0; h < imageHeight; h++) {
            for (int w = 0; w < imageWidth; w++) {
                if (!absPolygon.containsPoint(QPointF(w,h),Qt::OddEvenFill)) {
                    *(inside+pixnum) = false;
                }
                pixnum++;
            }
        }
    } else if (isExcludeType()) {
        for (int h = 0; h < imageHeight; h++) {
            for (int w = 0; w < imageWidth; w++) {
                if (absPolygon.containsPoint(QPointF(w,h),Qt::OddEvenFill)) {
                    *(inside+pixnum) = false;
                }
                pixnum++;
            }
        }
    }
}

void RubberBandPolygon::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    //Paint the polygon
    if (DEBUG_MODE) { int fun = 58;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Set the composition mode so that the fill is see-through, and the color
    //depends on the polygon type
    painter->setCompositionMode(QPainter::CompositionMode_SourceOver);

    QBrush br;
    if (type==0) {
        br = Qt::green;
        opacity = 0.2;
    } else if (type == 1) {
        br = Qt::red;
        opacity = 0.2;
    } else {
        br = brush;
    }

    painter->setOpacity(opacity);
    painter->setBrush(br);

    if (points.length() > 2) {
        painter->drawPolygon(polygon());
    }

    //Set the composition mode so that the polygon's lines are the inverse of the background.
    //painter->setCompositionMode(QPainter::CompositionMode_Difference);
    //painter->setCompositionMode(QPainter::RasterOp_SourceAndNotDestination);
    painter->setOpacity(1);

    QPen pen(Qt::white,1);
    //pen.setWidth(2);
    //pen.setColor(Qt::white);
    painter->setPen(pen);
    painter->setBrush(Qt::NoBrush);

    if (points.length() > 2) {
        painter->drawPolygon(polygon());
    } else if (points.length() == 2) {
        painter->drawLine(QLineF(points[0],points[1]));
    }
}

void RubberBandPolygon::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    //The polygon was clicked
    if (DEBUG_MODE) { int fun = 59;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    emit hasHighlight();
    highlight();
}

void RubberBandPolygon::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 60;
    qDebug() << "       Function Call " << fun; } //debugging mode call
  //The polygon is being dragged.  We need to recalculate
  //the new relative points
  QGraphicsItem::mouseMoveEvent(event);
  QPolygonF scenePoly = mapToScene(polygon());
  curPoly = scenePoly;

  int numPoints = points.length();
  //points.clear();
  //setPolygon(scenePoly);
  for (int i=0; i < numPoints; i++) {
      /**
      QPointF newPoint;
      newPoint.setX(scenePoly[i].x());
      newPoint.setY(scenePoly[i].y());
      points.append(newPoint); **/
      relativePoints[i].setX(scenePoly[i].x()/this->scene()->width());
      relativePoints[i].setY(scenePoly[i].y()/this->scene()->height());
  }

  dragging = true;
}

void RubberBandPolygon::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 61;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    QGraphicsItem::mouseReleaseEvent(event);
    if (dragging) {
        emit shapeChanged();

        /**
        qDebug() << "Finished dragging shape";
        qDebug() << "Points:";
        for (int i=0; i < points.length(); i++) {

            qDebug() << " x: " << points[i].x() << " - y: " << points[i].y();
        }
        setPolygon(QPolygonF(points));
        **/
    }
    dragging = false;
}







//----------------------------------------------------------
RubberBandNodeShape::RubberBandNodeShape(QGraphicsItem *parent)
    :QGraphicsPathItem(parent) {
    if (DEBUG_MODE) { int fun = 62;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    //This is a user-drawn line shape
    dragging = false;
    currentLineInd = -1;
    lineHighlight = -1;
    currentLocationMarker.setX(-1);
    currentLocationMarker.setY(-1);

    setFlag(QGraphicsItem::ItemIsMovable,true);
    //setFlag(QGraphicsItem::ItemIsSelectable,true);

}

RubberBandNodeShape::~RubberBandNodeShape() {
    if (DEBUG_MODE) { int fun = 63;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Not sure if this is actually needed
    /*
    while (points.length() > 0) {
        removeLastPoint();
    }*/
}





void RubberBandNodeShape::setLastMouseClickLoc(const QPointF &point) {
    if (DEBUG_MODE) { int fun = 64;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    lastMouseClickLoc = point;
    pointsRelativeToLastMouseCLick.clear();
    for (int i=0; i < points.length(); i++) {
        pointsRelativeToLastMouseCLick.push_back(points[i]-point);
    }
}

bool RubberBandNodeShape::contains(const QPointF &point) const {
    if (DEBUG_MODE) { int fun = 65;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Calculate the shortest distance to each segment
    qreal tmpDist;
    bool gotHit = false;
    for (int i = 0; i < lines.length(); i++) {
        tmpDist = distToLine(point,lines[i].line);
        if (tmpDist < 20) {
            gotHit = true;
            break;
        }
    }

    return gotHit;
}

qreal RubberBandNodeShape::distToLine(const QPointF &p, const QLineF &line) const {
    if (DEBUG_MODE) { int fun = 66;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Create a line from the point to the first point of the input line
    QLineF lineToPoint;
    lineToPoint.setP1(line.p1());
    lineToPoint.setP2(p);

    //Calculate the angle between the two lines
    qreal angleToPoint = 2*M_PI*(line.angleTo(lineToPoint)/360);

    qreal projectionOntoSegment = qCos(angleToPoint)*lineToPoint.length();
    qreal distanceToSegment;
    if (projectionOntoSegment > 0 && projectionOntoSegment <= line.length()) {
       //The projected point falls on the line
       distanceToSegment = abs(qSin(angleToPoint)*lineToPoint.length());
    } else if (projectionOntoSegment < 0) {
        //The projected point is less than the first point of the line.  Use the distance to p1
        distanceToSegment = lineToPoint.length();
        projectionOntoSegment = 0;
    } else {
        //The projected point is greater than the 2nd point on the line. Use the distance to p2
        QLineF lineToPoint2;
        lineToPoint2.setP1(line.p2());
        lineToPoint2.setP2(p);
        distanceToSegment = lineToPoint2.length();
        projectionOntoSegment = line.length(); //The projection is the total length of the line

    }

    return distanceToSegment;
}

void RubberBandNodeShape::removeLastPoint() {
    if (DEBUG_MODE) { int fun = 67;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (points.length()>0) {
        bool needsPruning = true;

        while (needsPruning) {
            for (int l=0; l < lines.length(); l++) {
                if (lines[l].endNodeIndex == (points.length()-1)) {
                    lines.takeAt(l);
                    break;
                }
                if (l == (lines.length()-1)) {
                    needsPruning = false;
                }
            }
        }
        points.removeLast();
        relativePoints.removeLast();
        setPainterPath();
        //setPolygon(QPolygonF(points));
        delete nodes.takeLast();



    }
}

void RubberBandNodeShape::moveLastPoint(QPointF newLoc) {
    //move the last point in the polygon to the new location
    //Used as the polygon is being drawn by the user
    if (DEBUG_MODE) { int fun = 68;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (points.length()>0) {

        points.last().setX(newLoc.x());
        points.last().setY(newLoc.y());
        //update the lines
        for (int l=0; l < lines.length(); l++) {
            if (lines[l].startNodeIndex == (points.length()-1)) {
                lines[l].line.setP1(points.last());
            }
            if (lines[l].endNodeIndex == (points.length()-1)) {
                lines[l].line.setP2(points.last());
            }

        }
        QPointF newRelativePoint;
        newRelativePoint.setX(newLoc.x()/this->scene()->width());
        newRelativePoint.setY(newLoc.y()/this->scene()->height());
        relativePoints.last().setX(newRelativePoint.x());
        relativePoints.last().setY(newRelativePoint.y());

        //setPolygon(QPolygonF(points));
        setPainterPath();
        nodes.last()->setPos(newLoc.x()-3,newLoc.y()-3);
    }
    update();
}

void RubberBandNodeShape::setPainterPath() {
    QPainterPath path;
    if (DEBUG_MODE) { int fun = 69;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    for (int i=0;i<lines.length();i++) {
        path.moveTo(lines[i].line.p1());
        path.lineTo(lines[i].line.p2());
    }

    this->setPath(path);
}

void RubberBandNodeShape::dragTo(const QPointF &point) {
    if (DEBUG_MODE) { int fun = 70;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    for (int i=0; i<points.length(); i++) {
        points[i] = point+pointsRelativeToLastMouseCLick[i];
        relativePoints[i].setX(points[i].x()/this->scene()->width());
        relativePoints[i].setY(points[i].y()/this->scene()->height());
        nodes[i]->setPos(points[i].x()-3,points[i].y()-3);
    }

    for (int i=0; i<lines.length(); i++) {
        lines[i].line.setP1(points[lines[i].startNodeIndex]);
        lines[i].line.setP2(points[lines[i].endNodeIndex]);
    }

    setPainterPath();

}

void RubberBandNodeShape::setLineZone(int lineInd, int zone) {
    if (DEBUG_MODE) { int fun = 71;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    lines[lineInd].zone = zone;
}

void RubberBandNodeShape::setShape(const QVector<QPointF> &inputNodes, QVector<LineNodeIndex> &inputLines) {
    if (DEBUG_MODE) { int fun = 72;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    points.clear();
    lines.clear();
    relativePoints.clear();
    while (!nodes.isEmpty()) {
        delete nodes.takeLast();
    }

    points = inputNodes;
    lines = inputLines;


    for (int i=0; i < points.length(); i++) {

        //We need to calculate the point's relative location on
        //the drawing area (between 0 and 1 for both x and y
        //dimensions. This is important for resizing and for
        //calculating what data falls inside the polygon.
        QPointF newRelativePoint;
        newRelativePoint.setX(points[i].x()/this->scene()->width());
        newRelativePoint.setY(points[i].y()/this->scene()->height());
        relativePoints.append(newRelativePoint);

        //We also create a node that can be dragged by the user.
        RubberBandPolygonNode *tmpNode = new RubberBandPolygonNode(i,this);

        nodes.append(tmpNode);
        tmpNode->setRect(0,0,6,6);
        tmpNode->setPos(points[i].x()-3,points[i].y()-3);
        tmpNode->setFlag(QGraphicsItem::ItemIsMovable,true);
        tmpNode->setVisible(false);
        connect(tmpNode,SIGNAL(nodeMoved(int)),this,SLOT(childNodeMoved(int)));
        connect(tmpNode,SIGNAL(nodeMoveFinished()),this,SLOT(childNodeMoved()));

        connect(tmpNode,SIGNAL(nodeRightClicked(int,QPointF)),this,SIGNAL(nodeRightClicked(int,QPointF)));
    }


    setPainterPath();


}

QVector<QPointF> RubberBandNodeShape::getNodes() {
    if (DEBUG_MODE) { int fun = 73;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    return points;
}

QVector<LineNodeIndex> RubberBandNodeShape::getLines() {
    if (DEBUG_MODE) { int fun = 74;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    return lines;
}

void RubberBandNodeShape::setLineHighlight(int lineIndex) {
    if (DEBUG_MODE) { int fun = 75;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    lineHighlight = lineIndex;
    update();
}

int RubberBandNodeShape::clickedNearLine(const QPointF &point) {
    if (DEBUG_MODE) { int fun = 76;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Returns the line index if the user clicked near it
    qreal tmpDist;

    for (int i = 0; i < lines.length(); i++) {
        tmpDist = distToLine(point,lines[i].line);
        if (tmpDist < 20) {
            return i;
        }
    }

    return -1; //if not near a line
}

int RubberBandNodeShape::clickedNearNode(const QPointF &newPoint) {
    if (DEBUG_MODE) { int fun = 77;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Returns the node index if the user clicked near it

    for (int i=0; i < points.length(); i++) {
        QPointF pointDiff = points[i]-newPoint;
        if (pointDiff.manhattanLength() < 10) {
            return i;
        }
    }
    return -1; //if not near a node
}

void RubberBandNodeShape::connectLastLineToNode(int nodeIndex) {
    if (DEBUG_MODE) { int fun = 78;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    points.removeLast();
    relativePoints.removeLast();
    delete nodes.takeLast();
    lines.last().endNodeIndex = nodeIndex;
    lines.last().line.setP2(points[nodeIndex]);
    setPainterPath();


}

void RubberBandNodeShape::addBranch(int nodeIndex) {
    if (DEBUG_MODE) { int fun = 79;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    highlight();
    emit hasHighlight();

    if (points.length()>nodeIndex) { //just to be safe
        LineNodeIndex tmpLineNodeIndex;
        tmpLineNodeIndex.startNodeIndex = nodeIndex;
        tmpLineNodeIndex.line.setP1(points.at(nodeIndex));

        points.append(points.at(nodeIndex));
        tmpLineNodeIndex.endNodeIndex = points.length()-1;
        tmpLineNodeIndex.line.setP2(points.last());

        lines.append(tmpLineNodeIndex);
    }

    //We need to calculate the point's relative location on
    //the drawing area (between 0 and 1 for both x and y
    //dimensions. This is important for resizing and for
    //calculating what data falls inside the polygon.
    QPointF newPoint = points.at(nodeIndex);
    QPointF newRelativePoint;
    newRelativePoint.setX(newPoint.x()/this->scene()->width());
    newRelativePoint.setY(newPoint.y()/this->scene()->height());
    relativePoints.append(newRelativePoint);

        //lines.append(tmpLineNodeIndex);
    setPainterPath();

    //setPolygon(QPolygonF(points));

    //We also create a node that can be dragged by the user.
    RubberBandPolygonNode *tmpNode = new RubberBandPolygonNode(points.length()-1,this);
    nodes.append(tmpNode);
    tmpNode->setRect(0,0,6,6);
    tmpNode->setPos(newPoint.x()-3,newPoint.y()-3);
    tmpNode->setFlag(QGraphicsItem::ItemIsMovable,true);
    tmpNode->setVisible(false);
    connect(tmpNode,SIGNAL(nodeMoved(int)),this,SLOT(childNodeMoved(int)));
    connect(tmpNode,SIGNAL(nodeMoveFinished()),this,SLOT(childNodeMoved()));
    connect(tmpNode,SIGNAL(nodeRightClicked(int,QPointF)),this,SIGNAL(nodeRightClicked(int,QPointF)));


}

void RubberBandNodeShape::addPoint(QPointF newPoint) {
    if (DEBUG_MODE) { int fun = 80;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Add a new point to the polygon

    highlight();
    emit hasHighlight();



    if (points.length() > 0) {

        LineNodeIndex tmpLineNodeIndex;
        tmpLineNodeIndex.startNodeIndex = points.length()-1;
        tmpLineNodeIndex.line.setP1(points.last());

        points.append(newPoint);

        tmpLineNodeIndex.endNodeIndex = points.length()-1;
        tmpLineNodeIndex.line.setP2(points.last());

        lines.append(tmpLineNodeIndex);
        //lines.append(new QGraphicsLineItem(this,))

    } else {
        points.append(newPoint);

    }

    //We need to calculate the point's relative location on
    //the drawing area (between 0 and 1 for both x and y
    //dimensions. This is important for resizing and for
    //calculating what data falls inside the polygon.
    QPointF newRelativePoint;
    newRelativePoint.setX(newPoint.x()/this->scene()->width());
    newRelativePoint.setY(newPoint.y()/this->scene()->height());
    relativePoints.append(newRelativePoint);

        //lines.append(tmpLineNodeIndex);
    setPainterPath();

    //setPolygon(QPolygonF(points));

    //We also create a node that can be dragged by the user.
    RubberBandPolygonNode *tmpNode = new RubberBandPolygonNode(points.length()-1,this);
    nodes.append(tmpNode);
    tmpNode->setRect(0,0,6,6);
    tmpNode->setPos(newPoint.x()-3,newPoint.y()-3);
    tmpNode->setFlag(QGraphicsItem::ItemIsMovable,true);
    tmpNode->setVisible(false);
    connect(tmpNode,SIGNAL(nodeMoved(int)),this,SLOT(childNodeMoved(int)));
    connect(tmpNode,SIGNAL(nodeMoveFinished()),this,SLOT(childNodeMoved()));
    connect(tmpNode,SIGNAL(nodeRightClicked(int,QPointF)),this,SIGNAL(nodeRightClicked(int,QPointF)));

}

void RubberBandNodeShape::childNodeMoved() {
    if (DEBUG_MODE) { int fun = 81;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //this is called after any of the nodes has finished moving
    emit shapeChanged();
}

void RubberBandNodeShape::childNodeMoved(int nodeNum) {
    if (DEBUG_MODE) { int fun = 82;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //this is called as the node is moving-- update polygon shape

    points[nodeNum].setX(nodes[nodeNum]->x()+3);
    points[nodeNum].setY(nodes[nodeNum]->y()+3);
    prepareGeometryChange();

    setPainterPath();
    //setPolygon(QPolygonF(points));


    //QPolygonF scenePoly = mapToScene(polygon());
    //Also, recalculate the new relative locations
    for (int i=0; i < points.length(); i++) {
        relativePoints[i].setX(mapToScene(points[i]).x() /this->scene()->width());
        relativePoints[i].setY(mapToScene(points[i]).y()/this->scene()->height());

        //update the lines
        for (int l=0; l < lines.length(); l++) {
            if (lines[l].startNodeIndex == i) {
                lines[l].line.setP1(points[i]);
            }
            if (lines[l].endNodeIndex == i) {
                lines[l].line.setP2(points[i]);
            }

        }

        //relativePoints[i].setX(scenePoly[i].x()/this->scene()->width());
        //relativePoints[i].setY(scenePoly[i].y()/this->scene()->height());
    }

}

void RubberBandNodeShape::updateSize() {
    if (DEBUG_MODE) { int fun = 83;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Called after a resize event
    QPointF tmpPoint;
    setPos(0,0);
    for (int i=0; i < points.length(); i++) {
        tmpPoint.setX(relativePoints[i].x()*this->scene()->width());
        tmpPoint.setY(relativePoints[i].y()*this->scene()->height());
        points[i].setX(tmpPoint.x());
        points[i].setY(tmpPoint.y());
        nodes[i]->setX(tmpPoint.x()-3);
        nodes[i]->setY(tmpPoint.y()-3);
        //update the lines
        for (int l=0; l < lines.length(); l++) {
            if (lines[l].startNodeIndex == i) {
                lines[l].line.setP1(points[i]);
            }
            if (lines[l].endNodeIndex == i) {
                lines[l].line.setP2(points[i]);
            }
        }

    }
    setPainterPath();
    //setPolygon(QPolygonF(points));
}

void RubberBandNodeShape::highlight() {
    if (DEBUG_MODE) { int fun = 84;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Make the draggable nodes visible when the polygon is clicked
    //with the edit tool
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(true);
    }
}

void RubberBandNodeShape::removeHighlight() {
    if (DEBUG_MODE) { int fun = 85;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Hide the draggable nodes
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(false);
    }
}

void RubberBandNodeShape::setCurrentLocation(QPointF loc) {
    if (DEBUG_MODE) { int fun = 86;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    currentLocationMarker = loc;
    update();
}



void RubberBandNodeShape::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    //Paint the polygon
    if (DEBUG_MODE) { int fun = 87;
    qDebug() << "       Function Call " << fun; } //debugging mode call


    //Set the composition mode so that the fill is see-through, and the color
    //depends on the polygon type
    /*
    painter->setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter->setOpacity(.2);
    if (type==0) {
        painter->setBrush( Qt::green );
    } else if (type == 1) {
        painter->setBrush( Qt::red );
    } else if (type == 1) {
        painter->setBrush( Qt::yellow );
    }
    if (points.length() > 2) {
        //painter->drawPolygon(polygon());
    }*/

    //Set the composition mode so that the polygon's lines are the inverse of the background.
    //painter->setCompositionMode(QPainter::CompositionMode_Difference);

    //painter->setCompositionMode(QPainter::RasterOp_SourceAndNotDestination);
    painter->setOpacity(1);

    QPen pen(Qt::white,1);
    painter->setPen(pen);
    painter->setBrush(Qt::NoBrush);

    painter->drawPath(path());
    if (lineHighlight != -1) {
        //painter->setCompositionMode(QPainter::CompositionMode_Source);
        pen.setColor(Qt::red);
        pen.setWidth(2);
        painter->setPen(pen);
        painter->drawLine(lines[lineHighlight].line);
    }
    //Draw a circle where the location was linearized to
    if (currentLocationMarker.x() > -1) {
        painter->drawEllipse(currentLocationMarker,5,5);
    }
}

void RubberBandNodeShape::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    //The polygon was clicked
    if (DEBUG_MODE) { int fun = 88;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    emit hasHighlight();
    highlight();

}


void RubberBandNodeShape::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 89;
    qDebug() << "       Function Call " << fun; } //debugging mode call
  //The polygon is being dragged.  We need to recalculate
  //the new relative points
  QGraphicsItem::mouseMoveEvent(event);

  /*
  QPolygonF scenePoly = mapToScene(polygon());

  for (int i=0; i < points.length(); i++) {

      relativePoints[i].setX(scenePoly[i].x()/this->scene()->width());
      relativePoints[i].setY(scenePoly[i].y()/this->scene()->height());
  }*/
  dragging = true;

}

void RubberBandNodeShape::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 90;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    QGraphicsItem::mouseReleaseEvent(event);
    if (dragging) {
        emit shapeChanged();
    }
    dragging = false;
    qDebug() << "DONE!";
}



//The GraphicsWindow contains the polygons and the underlying video image
GraphicsWindow::GraphicsWindow(QWidget *parent):
    QGraphicsView(parent) {
    if (DEBUG_MODE) { int fun = 91;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    dispWin = new VideoDisplayWindow(NULL);
    scene = new CustomScene;
    connect(scene,SIGNAL(emptySpaceClicked()),this,SLOT(spaceClicked()));
    connect(dispWin,SIGNAL(resolutionChanged()),this,SLOT(calculateConsideredPixels()));
    connect(this,SIGNAL(pixelScaleChanged(double)),dispWin,SLOT(setPixelScale(double)));

    //MARK: event
    connect(dispWin,SIGNAL(isObjAt(QPoint)),this,SLOT(checkPosForObj(QPoint)));
    connect(dispWin,SIGNAL(sig_newImage()),this,SLOT(tickPolygonEventTimers()));
    connect(this,SIGNAL(isObjAt(bool,toolFlag)),dispWin,SLOT(setBool(bool,toolFlag))); //sets FRL variable    

    scene->addWidget(dispWin,Qt::Widget);
    this->setScene(scene);

    pixlScale = 1; //defaults to 1 pix/cm
    userRealDistanceInput = 10;
    currentlyDrawing = false;
    currentlyDrawingLineShape = false;
    linShapeIsDragging = false;
    currentlySelectedPolygon = -1;
    currentlySelectedLineShape = -1;
    currentIncludePolygon = -1;  //there can only be one or zero include polygons
    currentLinearizationShape = -1;
    rangeLineShape = -1;
    rangeLineNodeNum = -1;
    rangeLineAnchorNode = -1;

    trackSettings.ringOn = false;
    showTwoLeds = false;

    //addIncludePolygon();

    dispWin->show();

    medianLocMarker = new QGraphicsEllipseItem();
    LED1Marker = new QGraphicsEllipseItem();
    LED2Marker = new QGraphicsEllipseItem();
    ringMarker = new QGraphicsEllipseItem();
    directionMarker = new QGraphicsLineItem();
    directionArrow = new QGraphicsPathItem();

    QPen markerPen;
    markerPen.setWidth(1);
    markerPen.setColor(Qt::green);

    LED1Marker->setPen(markerPen);
    LED1Marker->setRect(1,1,6,6);
    LED2Marker->setPen(markerPen);
    LED2Marker->setRect(1,1,6,6);
    medianLocMarker->setPen(markerPen);
    medianLocMarker->setRect(1,1,6,6);

    directionMarker->setPen(markerPen);
    directionArrow->setPen(markerPen);


    ringMarker->setPen(markerPen);
    ringMarker->setRect(1,1,20,20);
    scene->addItem(medianLocMarker);
    scene->addItem(ringMarker);
    scene->addItem(LED1Marker);
    scene->addItem(LED2Marker);
    scene->addItem(directionMarker);
    scene->addItem(directionArrow);



    //setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DirectRendering)));
    setViewport(new QGLWidget(QGLFormat(QGL::DoubleBuffer| QGL::DirectRendering)));
    //setViewport(new QGLWidget(QGLFormat(QGL::DoubleBuffer)));
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);


    //setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);

    QPointF iniP(-1,-1);
    curTrackedLoc = iniP;
    polygonIdCounter = 0;

}

void GraphicsWindow::initializeEvents() {
    //NOTE: for some reason these events don't have the correct module name appended on them...
    emit broadcastNewEventReq("Geometry Saved");
    emit broadcastNewEventReq("Geometry Loaded");
    emit broadcastNewEventReq("Zone created!");
    emit broadcastNewEventReq("Linearization Track created!");
    emit broadcastNewEventReq("Range Line created!");
}

QString GraphicsWindow::getLineContaining(QString str, QTextStream *fullText, bool startAtBegin) {
    QString line;
    if (DEBUG_MODE) { int fun = 92;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (startAtBegin) {
        fullText->seek(0); //return to beginning of stream
    }

    do {
        line = fullText->readLine();
    } while(!line.isNull() && !line.contains(str));
    return(line);
}

//debugging functino
void GraphicsWindow::printTextStream(QTextStream *stream, int streamPos) {
    if (DEBUG_MODE) { int fun = 93;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    stream->seek(0);
    QString line;
    qDebug() << "**** Print Stream ****";
    do {
        line = stream->readLine();
        qDebug() << "* " << line << " *";
    } while(!line.isNull());
    stream->seek(streamPos); //return to streamPos defined by user.  0 by default
}

void GraphicsWindow::loadGeometry(QString filename, OptionFlag flg) {
    if (DEBUG_MODE) { int fun = 94;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (flg == O_NULL) {
        qDebug() << "Error: invalid load flag recieved. (GraphicsWindow::loadGeometry)";
        return;
    }

    QFile myfile;

    myfile.setFileName(filename);

    if (!myfile.open(QIODevice::ReadOnly)) {
        qDebug() << "Error: unable to open file '" << filename << "' (GraphicsWindow::loadGeometry)";
        return;
    }

    currentlySelectedPolygon = -1;
    currentlySelectedLineShape = -1;
    currentlyDrawingLineShape = false;
    setNoHighlight();

    if (flg == O_ALL || flg == O_LINEAR)
        loadLinearGeometry(filename, &myfile);

    if (flg == O_ALL || flg == O_RANGE)
        loadRangeLineGeometry(filename, &myfile);

    myfile.close();
    emit broadcastEvent(TrodesEventMessage("Geometry Loaded"));
}

void GraphicsWindow::loadLinearGeometry(QString filename, QFile *inputFile) {
    if (DEBUG_MODE) { int fun = 95;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    QTextStream textStream(inputFile);

    QString line = getLineContaining("<Linearization Object>", &textStream);

    if (line.contains("1")) {
        qDebug() << "Loading linearization geometry from " << filename;
    }
    else {
        qDebug() << "No linearization geometry";
        return;
    }

    if (currentLinearizationShape != -1) {
        deleteLineShape(currentLinearizationShape);
    }

    QVector<QPointF> nodes;
    QVector<LineNodeIndex> geoLines;
    QVector<int> nodes_x;
    QVector<int> nodes_y;


    QDataStream inputStream(inputFile);
    inputStream.setByteOrder(QDataStream::LittleEndian);



    QString inputLine;
    qint64 startOfLine = 0;
    qint64 startOfBinary = 0;
    qint64 endOfSeg = 0;


    while (true) {
        startOfLine = textStream.pos();
        inputLine = textStream.readLine();
        if (inputLine.contains("<End settings>")) {
            startOfBinary = startOfLine+15;

            if (!nodes_x.isEmpty() && !nodes_y.isEmpty() && (nodes_x.length() == nodes_y.length())) {
                for (int i=0; i< (nodes_x.length()-1);i++) {
                    QPointF newNode;
                    //qDebug() << "i: " << i << " | x: " << nodes_x[i] << " - y: " << nodes_y[i];
                    newNode.setX(nodes_x[i]);
                    newNode.setY(nodes_y[i]);
                    nodes.push_back(newNode);
                }
            } else {
                qDebug() << "Something wrong with geometry file";
                return;
            }
            while (!textStream.atEnd() && !inputLine.contains("<End binary>")) {
                inputLine = textStream.readLine();
            }
            endOfSeg = textStream.pos()-14; //14 is the length in chars of the string "\n<End Binary>\n". Kinda janky code, but it works :/

            inputFile->seek(startOfBinary);
            while (inputFile->pos() <= endOfSeg) {
                int16_t tmpStart;
                int16_t tmpEnd;
                int16_t tmpZone;
                inputStream >> tmpStart >> tmpEnd >> tmpZone;
                LineNodeIndex newLine;
                newLine.startNodeIndex = tmpStart;
                newLine.endNodeIndex = tmpEnd;
                newLine.zone = tmpZone;
                QLineF tmpLine;
                tmpLine.setP1(nodes[tmpStart]);
                tmpLine.setP2(nodes[tmpEnd]);
                //qDebug() << " --P1(" << tmpLine.p1().x() << "," << tmpLine.p1().y() << ")" << " --P2(" << tmpLine.p2().x() << "," << tmpLine.p2().y() << ")";
                newLine.line = tmpLine;
                geoLines.push_back(newLine);
            }
            geoLines.pop_back(); //this is necessary b/c the current binary reading function always has a blank space at the end of it giving a bad node index that shouldn't actually exist
            break;

        } else if (inputLine.contains("nodes_x:")) {
            inputLine.remove(0,9);
            QStringList stringArray = inputLine.split(" ");
            for (int i=0; i<stringArray.length();i++) {
                bool ok = true;
                if (ok) {
                    nodes_x.push_back(stringArray[i].toInt());
                }
            }
        } else if (inputLine.contains("nodes_y:")) {
            inputLine.remove(0,9);
            QStringList stringArray = inputLine.split(" ");
            for (int i=0; i<stringArray.length();i++) {
                bool ok = true;
                if (ok) {
                    nodes_y.push_back(stringArray[i].toInt());
                }
            }
        } else if (inputLine.contains("anchorNodeInd:")) {
            inputLine.remove(0,15);
            bool ok;
            int tmpNum = inputLine.toInt(&ok);
            if (!ok) {
                qDebug() << "Can not read anchorNodeInd in geometry file";
                return;
            }

            linearizationShapeAnchorNode = tmpNum;
        }
        if (inputLine.isNull() || inputLine.contains("<End Settings>")) {
            break;
        }

    }

    addTrackGeometry();
    currentLinearizationShape = lineShapes.length()-1;



    emit newLinearGeometry(nodes, geoLines);
    emit linearGeometryExists(true);
    emit linearGeometryAnchorNodeSet(linearizationShapeAnchorNode);

    //MARK HERE
    QSize currentRes = dispWin->getResolution();
    QPointF tmpRelativePoint;
    for (int i=0; i<nodes.length(); i++) {
        tmpRelativePoint.setX(nodes[i].x()/currentRes.width());
        tmpRelativePoint.setY(nodes[i].y()/currentRes.height());
        nodes[i].setX(tmpRelativePoint.x()*this->scene->width());
        nodes[i].setY(tmpRelativePoint.y()*this->scene->height());

    }

    int lengthModifier = 0;
    QVector<dataSend> dataToSend;
    for (int i=0; i<geoLines.length(); i++) {

        QPointF tmpLinePoint, p1, p2;

        tmpRelativePoint.setX(geoLines[i].line.p1().x()/currentRes.width());
        tmpRelativePoint.setY(geoLines[i].line.p1().y()/currentRes.height());
        p1 = tmpRelativePoint;
        tmpLinePoint.setX(tmpRelativePoint.x()*this->scene->width());
        tmpLinePoint.setY(tmpRelativePoint.y()*this->scene->height());
        geoLines[i].line.setP1(tmpLinePoint);

        tmpRelativePoint.setX(geoLines[i].line.p2().x()/currentRes.width());
        tmpRelativePoint.setY(geoLines[i].line.p2().y()/currentRes.height());
        p2 = tmpRelativePoint;
        tmpLinePoint.setX(tmpRelativePoint.x()*this->scene->width());
        tmpLinePoint.setY(tmpRelativePoint.y()*this->scene->height());
        geoLines[i].line.setP2(tmpLinePoint);

        dataSend linSegNum(DT_int16_t, i);
        dataSend x1(DT_qreal, p1.x());
        dataSend y1(DT_qreal, p1.y());
        dataSend x2(DT_qreal, p2.x());
        dataSend y2(DT_qreal, p2.y());

        if ((p1.x() != p2.x()) || (p1.y() != p2.y())) { //exclude non-existant lines
            dataToSend.append(linSegNum);
            dataToSend.append(x1);
            dataToSend.append(y1);
            dataToSend.append(x2);
            dataToSend.append(y2);
        }
        else {
            lengthModifier++;
        }
        geoLines[i].zone = 0;

    }

    dataPacket linTrack(PPT_LinTrack);
    dataSend numLins(DT_int16_t, geoLines.length()-lengthModifier);
    linTrack.insert(numLins);
    for (int i = 0; i < dataToSend.length(); i++) {
        linTrack.insert(dataToSend.at(i));
    }
    emit sendNewDataPacket(linTrack);

    //linearGeometryShapeChanged();
    //MARK: Follow up, may have to add in a re-calculator here
    lineShapes.last()->setShape(nodes, geoLines);
    connect(lineShapes.last(),SIGNAL(nodeRightClicked(int,QPointF)),this,SLOT(showLinearGeometryNodeContextMenu(int,QPointF)));


}

void GraphicsWindow::loadRangeLineGeometry(QString filename, QFile *inputFile) {
    if (DEBUG_MODE) { int fun = 96;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    QTextStream textStream(inputFile);
    QString line = getLineContaining("<Rangeline Object>", &textStream);

    if (line.contains("1")) {
        qDebug() << "Loading range line geometry from " << filename;
    }
    else {
        qDebug() << "No range line geometry";
        return;
    }

    if (rangeLineShape != -1) {
        deleteLineShape(rangeLineShape);
    }

    currentlyDrawingLineShape = false;
    QVector<QPointF> nodes;
    QVector<LineNodeIndex> geoLines;
    QVector<int> nodes_x;
    QVector<int> nodes_y;


    while (!line.contains("<End settings>") && !line.isNull()) {

        if (line.contains("userRealDistanceInput:")) {
            line.remove(0,23);
            userRealDistanceInput = line.toDouble();
        }
        if (line.contains("nodes_x:")) {
            line.remove(0,9);
            QStringList stringArray = line.split(" ");
            for (int i=0; i<stringArray.length();i++) {
                bool ok = true;
                if (ok) {
                    nodes_x.push_back(stringArray[i].toInt());
                }
            }
        }
        else if (line.contains("nodes_y:")) {
            line.remove(0,9);
            QStringList stringArray = line.split(" ");
            for (int i=0; i<stringArray.length();i++) {
                bool ok = true;
                if (ok) {
                    nodes_y.push_back(stringArray[i].toInt());
                }
            }
        }
        else if (line.contains("anchorNodeInd:")) {
            line.remove(0,15);
            rangeLineAnchorNode = line.toInt();
        }
        line = textStream.readLine();
    }//end while()

    if (!nodes_x.isEmpty() && !nodes_y.isEmpty() && (nodes_x.length() == nodes_y.length())) {
        for (int i=0; i< (nodes_x.length()-1);i++) {
            QPointF newNode;
            newNode.setX(nodes_x[i]);
            newNode.setY(nodes_y[i]);
            nodes.push_back(newNode);
        }
    } else {
        qDebug() << "Something wrong with geometry file";
        return;
    }

    addTrackGeometry();
    rangeLineNodeNum = 1;
    rangeLineShape = lineShapes.length()-1;

    QSize currentRes = dispWin->getResolution();
    QPointF tmpRelativePoint;
    for (int i=0; i<nodes.length(); i++) {
        tmpRelativePoint.setX(nodes[i].x()/currentRes.width());
        tmpRelativePoint.setY(nodes[i].y()/currentRes.height());
        //nodes[i].setX(tmpRelativePoint.x()*this->scene->width());
        //nodes[i].setY(tmpRelativePoint.y()*this->scene->height());

        //NOTE: this scaling calculation returns a non-int float answer.  THis causes micro inacuracy (< 1 pixel) when loading in geometry.
        //Possibly round up to fix this problem.
        //MAY BE BAD SOLUTION -- MARK: followup
        qreal x, y;
        x = tmpRelativePoint.x()*this->scene->width();
        y = tmpRelativePoint.y()*this->scene->height();
        if (x > (int)x)
            x = (int)x + 1;
        if (y > (int)y)
            y = (int)y + 1;
        nodes[i].setX(x);
        nodes[i].setY(y);

    }

    QPointF first,last;
    first = nodes.at(0);
    last = nodes.at(1);

    lineShapes.last()->addPoint(first); //add first pt
    lineShapes.last()->addPoint(first);
    lineShapes.at(rangeLineShape)->moveLastPoint(last);
    lineShapes[rangeLineShape]->setLineZone(0,0);

    int nodeFound = lineShapes[rangeLineShape]->clickedNearNode(last);
    lineShapes.at(rangeLineShape)->addPoint(last);
    lineShapes.at(rangeLineShape)->connectLastLineToNode(nodeFound);
    geoLines = lineShapes[rangeLineShape]->getLines();

    lineShapes.last()->setShape(nodes,geoLines);

    for(int i = 0; i < lineShapes[rangeLineShape]->getNodes().length(); i++)
        connect(lineShapes[rangeLineShape]->accessNode(i),SIGNAL(nodeMoveFinished()),this,SLOT(calculatePixelScale()));
    calculatePixelScale(false);
    emit rangeGeometryExists(true);
}

void GraphicsWindow::showLinearGeometryLineContextMenu(int lineInd, QPointF position) {
    if (DEBUG_MODE) { int fun = 97;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //QVector<QPointF> nodesInShape = lineShapes[currentLinearizationShape]->getNodes();

    lineShapes[currentLinearizationShape]->setLineHighlight(lineInd);
    QVector<LineNodeIndex> linesInShape = lineShapes[currentLinearizationShape]->getLines();

    QPoint globalPos = this->mapToGlobal(position.toPoint());

    QMenu optionsMenu;
    QMenu zoneMenu;
    zoneMenu.setTitle("Set zone number");

    //channelMenu.addAction("nTrode settings...");

    optionsMenu.addMenu(&zoneMenu);

    for (int i=0; i<10; i++) {
        zoneMenu.addAction(QString().number(i));
        if (linesInShape[lineInd].zone == i) {
            zoneMenu.actions().last()->setCheckable(true);
            zoneMenu.actions().last()->setChecked(true);
        }
    }
    //channelMenu.addAction("Change background color...");
    QAction* selectedItem = optionsMenu.exec(globalPos);
    if (selectedItem) {
       // something was chosen, do stuff
       for (int i=0; i<10; i++) {
           if (selectedItem->text() == QString().number(i)) {
               lineShapes[currentLinearizationShape]->setLineZone(lineInd,i);
               emit linearGeometryLineZoneSet(lineInd, i);
               break;
           }
       }

    } else {
      // nothing was chosen
    }
    lineShapes[currentLinearizationShape]->setLineHighlight(-1);
}

void GraphicsWindow::showLinearGeometryNodeContextMenu(int nodeInd, QPointF position) {
    if (DEBUG_MODE) { int fun = 98;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    QVector<QPointF> nodesInShape = lineShapes[currentlySelectedLineShape]->getNodes();


    //QPoint globalPos = this->mapToGlobal(position.toPoint());
    QPoint globalPos = this->mapToGlobal(nodesInShape[nodeInd].toPoint());


    QPointF tmpRelativePoint;
    QSize currentRes = dispWin->getResolution();

    tmpRelativePoint.setX(position.x()/this->scene->width());
    tmpRelativePoint.setY(position.y()/this->scene->height());
    tmpRelativePoint.setX(tmpRelativePoint.x()*currentRes.width());
    tmpRelativePoint.setY(tmpRelativePoint.y()*currentRes.height());



    QMenu optionsMenu;

    //channelMenu.addAction("nTrode settings...");

    optionsMenu.addAction("Set linearization anchor");
    //channelMenu.addAction("Change background color...");
    QAction* selectedItem = optionsMenu.exec(globalPos);
    if (selectedItem) {
       // something was chosen, do stuff
       if (selectedItem->text() == "Set linearization anchor") {
            //Set this node as 0 for linearization
           if (currentlySelectedLineShape == currentLinearizationShape) {
               linearizationShapeAnchorNode = nodeInd;
           }
           else if (currentlySelectedLineShape == rangeLineShape) {
               rangeLineAnchorNode = nodeInd;
           }
           emit linearGeometryAnchorNodeSet(nodeInd);


       } else if (selectedItem->text() == "Change nTrode color...") {
           //showNtrodeColorSelector(channel);
       }  else if (selectedItem->text() == "Change background color...") {
           //showBackgroundColorSelector();
       }

    } else {
      // nothing was chosen
    }
}

void GraphicsWindow::saveGeometry(QString filename, OptionFlag flg) {
    QFile myfile;
    if (DEBUG_MODE) { int fun = 99;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //create file template
    qDebug() << "saving geometry to test file";

    myfile.setFileName(filename);
    if (!myfile.open(QIODevice::WriteOnly)) {
        qDebug() << "Error: could not create file (GraphicsWindow::saveGeometry)";
    }

    QDataStream outputStream(&myfile);
    outputStream.setByteOrder(QDataStream::LittleEndian);

    myfile.write("<Linearization Object>");
    if (flg == O_ALL || flg == O_LINEAR)
        saveCurrentLinearGeometry(filename, &myfile);
    myfile.write("\n");

    myfile.write("<Rangeline Object>");
    if (flg == O_ALL || flg == O_RANGE)
        saveCurrentRangeGeometry(filename, &myfile);
    myfile.write("\n");


    myfile.flush();
    myfile.close();

    emit broadcastEvent(TrodesEventMessage("Geometry Saved"));
}

void GraphicsWindow::saveCurrentLinearGeometry(QString filename, QFile *savefile) {
    if (DEBUG_MODE) { int fun = 100;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (currentLinearizationShape == -1) {
        savefile->write("\n");
        return;
    }
    savefile->write(" 1\n");

    qDebug() << "Saving linear geometry to " << filename;

    QString infoLine;
    QString fieldLine;


    QVector<QPointF> nodes = lineShapes[currentLinearizationShape]->getNodes();
    QVector<LineNodeIndex> geoLines = lineShapes[currentLinearizationShape]->getLines();

    QSize currentRes = dispWin->getResolution();
    QPointF tmpRelativePoint;
    for (int i=0; i<nodes.length(); i++) {
        tmpRelativePoint.setX(nodes[i].x()/this->scene->width());
        tmpRelativePoint.setY(nodes[i].y()/this->scene->height());
        nodes[i].setX(tmpRelativePoint.x()*currentRes.width());
        nodes[i].setY(tmpRelativePoint.y()*currentRes.height());
    }

    for (int i=0; i<geoLines.length(); i++) {

        QPointF tmpLinePoint;

        tmpRelativePoint.setX(geoLines[i].line.p1().x()/this->scene->width());
        tmpRelativePoint.setY(geoLines[i].line.p1().y()/this->scene->height());
        tmpLinePoint.setX(tmpRelativePoint.x()*currentRes.width());
        tmpLinePoint.setY(tmpRelativePoint.y()*currentRes.height());
        geoLines[i].line.setP1(tmpLinePoint);


        tmpRelativePoint.setX(geoLines[i].line.p2().x()/this->scene->width());
        tmpRelativePoint.setY(geoLines[i].line.p2().y()/this->scene->height());
        tmpLinePoint.setX(tmpRelativePoint.x()*currentRes.width());
        tmpLinePoint.setY(tmpRelativePoint.y()*currentRes.height());
        geoLines[i].line.setP2(tmpLinePoint);

    }


    //Create an output file for current linear geometry
    //*****************************************


    QDataStream outputStream(savefile);
    outputStream.setByteOrder(QDataStream::LittleEndian);

    savefile->write("<Start settings>\n");
    infoLine = QString("Description: Linear track geometry\n");
    savefile->write(infoLine.toLocal8Bit());
    infoLine = QString("Byte_order: little endian\n");
    savefile->write(infoLine.toLocal8Bit());


    //The node locations are written in text form
    infoLine = QString("nodes_x: ");
    for (int i=0; i<nodes.length(); i++) {
        infoLine += QString().setNum((int)nodes[i].x());
        infoLine += " ";
    }
    infoLine += "\n";
    savefile->write(infoLine.toLocal8Bit());

    infoLine = QString("nodes_y: ");
    for (int i=0; i<nodes.length(); i++) {
        infoLine += QString().setNum((int)nodes[i].y());
        infoLine += " ";
    }
    infoLine += "\n";
    savefile->write(infoLine.toLocal8Bit());

    infoLine = QString("anchorNodeInd: ");
    infoLine += QString().setNum(linearizationShapeAnchorNode);
    infoLine += "\n";
    savefile->write(infoLine.toLocal8Bit());

    //The lines (containing the node indeces) are written in binary form
    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<line 2*int16><lineZone int16>";
    fieldLine += "\n";
    savefile->write(fieldLine.toLocal8Bit());
    savefile->write("<End settings>\n");
    savefile->flush();


    for (int i=0; i < geoLines.length(); i++) {
        outputStream << (int16_t)geoLines[i].startNodeIndex << (int16_t)geoLines[i].endNodeIndex << (int16_t)geoLines[i].zone;
    }

    savefile->write("\n<End binary>\n");
    savefile->flush();
   // outputFile.close();

}

void GraphicsWindow::saveCurrentRangeGeometry(QString filename, QFile *savefile) {
    if (DEBUG_MODE) { int fun = 101;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (rangeLineShape == -1) {
        savefile->write("\n");
        return;
    }

    savefile->write(" 1\n");

    qDebug() << "Saving Range geometry to " << filename;

    QString infoLine, infoLine2;
    //extract nodes and lines from our rangeLineShape
    QVector<QPointF> nodes = lineShapes[rangeLineShape]->getNodes();
    QVector<LineNodeIndex> goeLines = lineShapes[rangeLineShape]->getLines();

    QSize currentRes = dispWin->getResolution();
    QPointF tmpRelativePoint;
    for (int i=0; i<nodes.length(); i++) {
        tmpRelativePoint.setX(nodes[i].x()/this->scene->width());
        tmpRelativePoint.setY(nodes[i].y()/this->scene->height());
        nodes[i].setX(tmpRelativePoint.x()*currentRes.width());
        nodes[i].setY(tmpRelativePoint.y()*currentRes.height());
    }

    savefile->write("<Start settings>\n");
    savefile->write("Description: Range line geometry\n");

    infoLine = "userRealDistanceInput: ";
    infoLine += QString::number(userRealDistanceInput);
    infoLine += "\n";
    savefile->write(infoLine.toLocal8Bit());

    infoLine = QString("nodes_x: ");
    infoLine2 = QString("nodes_y: ");
    for (int i=0; i<nodes.length(); i++) {
        infoLine += QString().setNum((int)nodes[i].x());
        infoLine += " ";
        infoLine2 += QString().setNum((int)nodes[i].y());
        infoLine2 += " ";
    }
    infoLine += "\n";
    infoLine2 += "\n";
    savefile->write(infoLine.toLocal8Bit());
    savefile->write(infoLine2.toLocal8Bit());

    infoLine.clear();
    infoLine = "anchorNodeInd: ";
    infoLine += QString().setNum(rangeLineAnchorNode);
    infoLine += "\n";
    savefile->write(infoLine.toLocal8Bit());

    savefile->write("<End settings>\n");
    savefile->flush();
}

void GraphicsWindow::locMarkerOn(bool on) {
    if (DEBUG_MODE) { int fun = 102;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    isTrackingOn = on;
    /*
    if (showTwoLeds) {
        LED1Marker->setVisible(isTrackingOn);
        LED2Marker->setVisible(isTrackingOn);
        medianLocMarker->setVisible(isTrackingOn);
    } else {
        LED1Marker->setVisible(false);
        LED2Marker->setVisible(false);
        medianLocMarker->setVisible(isTrackingOn);
    }
    ringMarker->setVisible(isTrackingOn && trackSettings.ringOn);
    */
    newSettings(trackSettings);
}

void GraphicsWindow::newSettings(TrackingSettings s) {
    if (DEBUG_MODE) { int fun = 103;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    //trackSettings.ringOn = s.ringOn;
    trackSettings = s;
    QSize res = dispWin->getResolution();

    ringSize.setWidth((float)s.currentRingSize/res.width());
    ringSize.setHeight((float)s.currentRingSize/res.height());

    ringMarker->setRect(1,1,2*scene->width()*ringSize.width(),2*scene->height()*ringSize.height());
    ringMarker->setVisible(isTrackingOn && trackSettings.ringOn);

    medianLocMarker->setVisible(isTrackingOn);

    LED1Marker->setVisible(isTrackingOn && trackSettings.twoLEDs && !trackSettings.trackDark);
    LED2Marker->setVisible(isTrackingOn && trackSettings.twoLEDs && !trackSettings.trackDark);
    directionMarker->setVisible(isTrackingOn && trackSettings.twoLEDs && !trackSettings.trackDark);
    directionArrow->setVisible(isTrackingOn && trackSettings.twoLEDs && !trackSettings.trackDark);

    if (trackSettings.trackDark) {
        //The marker should be light if tracking dark pixels, and dark if tracking light
        QPen markerPen;
        markerPen.setWidth(1);
        markerPen.setColor(Qt::green);
        medianLocMarker->setPen(markerPen);
    } else {
        if (!trackSettings.twoLEDs) {
            QPen markerPen;
            markerPen.setWidth(1);
            markerPen.setColor(Qt::blue);
            LED1Marker->setPen(markerPen);
            LED2Marker->setPen(markerPen);
            directionMarker->setPen(markerPen);
            directionArrow->setPen(markerPen);
            medianLocMarker->setPen(markerPen);
        } else {
            QPen markerPen1;
            QPen markerPen2;
            QPen markerPen3;
            markerPen1.setWidth(1);
            markerPen2.setWidth(1);
            markerPen3.setWidth(1);
            markerPen1.setColor(Qt::green);
            markerPen2.setColor(Qt::red);
            markerPen3.setColor(Qt::blue);

            LED1Marker->setPen(markerPen1);
            LED2Marker->setPen(markerPen2);
            directionMarker->setPen(markerPen3);
            directionArrow->setPen(markerPen3);
            medianLocMarker->setPen(markerPen3);
        }
    }


}

void GraphicsWindow::newLinearLocation(QPoint p) {
    if (DEBUG_MODE) { int fun = 104;
    qDebug() << "       Function Call " << fun; } //debugging mode call


    if (currentLinearizationShape > -1) {
        QPointF linLoc;
        QSize res = dispWin->getResolution();
        linLoc.setX((float)p.x()/res.width());
        linLoc.setY((float)p.y()/res.height());

        qreal x = scene->width()*linLoc.x();
        qreal y = scene->height()*linLoc.y();

        lineShapes[currentLinearizationShape]->setCurrentLocation(QPointF(x,y));
    }
}

void GraphicsWindow::newLocation(QPoint p) {
    if (DEBUG_MODE) { int fun = 105;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //A single point is being tracked

    /*
    if (showTwoLeds) {
        showTwoLeds = false;
        LED1Marker->setVisible(false);
        LED2Marker->setVisible(false);
        medianLocMarker->setVisible(isTrackingOn);
    }*/
    QSize res = dispWin->getResolution();
    medianLoc.setX((float)p.x()/res.width());
    medianLoc.setY((float)p.y()/res.height());

    qreal x = scene->width()*medianLoc.x();
    qreal y = scene->height()*medianLoc.y();

    QPointF gridP(x,y);

    medianLocMarker->setX(x-3);
    medianLocMarker->setY(y-3);
    ringMarker->setX(x-(scene->width()*ringSize.width()));
    ringMarker->setY(y-(scene->height()*ringSize.height()));

    curTrackedLoc = gridP;
    checkPtPolyOverlap(gridP);
}

void GraphicsWindow::newLocation(QPoint p1, QPoint p2, QPoint midPoint) {
    if (DEBUG_MODE) { int fun = 106;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //Two LED locations are being tracked
    //qDebug() << "p1: " << p1.x() << " - " << p1.y();
    //qDebug() << "p2: " << p2.x() << " - " << p2.y();
    //qDebug() << "m: " << midPoint.x() << " - " << midPoint.y();
    //qDebug() << " ";
    /*
    if (!showTwoLeds) {
        //qDebug() << "Got here" << p1 << p2 << midPoint;
        showTwoLeds = true;
        LED1Marker->setVisible(isTrackingOn);
        LED2Marker->setVisible(isTrackingOn);
        directionMarker->setVisible(isTrackingOn);
        //medianLocMarker->setVisible(false);
    }*/

    QSize res = dispWin->getResolution();
    LED1Loc.setX((float)p1.x()/res.width());
    LED1Loc.setY((float)p1.y()/res.height());
    LED2Loc.setX((float)p2.x()/res.width());
    LED2Loc.setY((float)p2.y()/res.height());

    //qDebug() << " *" << LED1Loc.x() << " -- " << LED1Loc.y() << " || " << p1.x() << " -- " << p1.y();

    qreal x1 = scene->width()*LED1Loc.x();
    qreal y1 = scene->height()*LED1Loc.y();
    qreal x2 = scene->width()*LED2Loc.x();
    qreal y2 = scene->height()*LED2Loc.y();

    medianLoc.setX((float)midPoint.x()/res.width());
    medianLoc.setY((float)midPoint.y()/res.height());

    qreal midx = scene->width()*medianLoc.x();
    qreal midy = scene->height()*medianLoc.y();

    QPointF gridMidPoint(midx,midy);

    LED1Marker->setX(x1-3);
    LED1Marker->setY(y1-3);
    LED2Marker->setX(x2-3);
    LED2Marker->setY(y2-3);
    ringMarker->setX(midx-(scene->width()*ringSize.width()));
    ringMarker->setY(midy-(scene->height()*ringSize.height()));
    medianLocMarker->setX(midx-3);
    medianLocMarker->setY(midy-3);

    directionMarker->setLine(x1,y1,x2,y2);
    QPainterPath arrowPath;
    arrowPath.moveTo(x1,y1);
    arrowPath.lineTo(directionMarker->line().normalVector().pointAt(0.2)+QPointF(directionMarker->line().dx(),directionMarker->line().dy()));
    arrowPath.lineTo(directionMarker->line().p1()-(directionMarker->line().normalVector().pointAt(0.2)-directionMarker->line().p1()) +QPointF(directionMarker->line().dx(),directionMarker->line().dy()));
    arrowPath.lineTo(directionMarker->line().p1());
    directionArrow->setPath(arrowPath);

    curTrackedLoc = gridMidPoint;
    checkPtPolyOverlap(gridMidPoint);
}

//MARK: Event
void GraphicsWindow::checkPosForObj(const QPoint pos) {
    if (DEBUG_MODE) { int fun = 107;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //bool retval = false;
    if (rangeLineShape != -1) { //if rangeLine exists

        if (!lineShapes[rangeLineShape]->clickedNearLine(pos)) {
            //qDebug() << "hovering over line...";
            emit isObjAt(true,T_RANGE); //rangeLine found
        }
        else {
            emit isObjAt(false,T_RANGE);
        }
    }
    else {
         emit isObjAt(false,T_RANGE);
    }
}

//this function calculates the pixel/distance ratio of the rangeLine
void GraphicsWindow::calculatePixelScale(bool getInput) {
    if (DEBUG_MODE) { int fun = 108;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (rangeLineShape != -1 && (currentlySelectedLineShape == rangeLineShape)) { //if range line exists
        bool ok;
        if (getInput) {
            QString uInput = QInputDialog::getText(this, tr("Input real distance in cm"), tr("Line length (cm):"),QLineEdit::Normal,"10", &ok);
            userRealDistanceInput = uInput.toDouble();
        }
        //first retreve pix length of line
        double pixelLen = lineShapes[currentlySelectedLineShape]->getLines()[0].line.length();
        pixlScale = pixelLen/userRealDistanceInput;
        emit pixelScaleChanged(pixlScale);
        //qDebug() << "pixlScale: " << pixlScale << " Distance: " << userRealDistanceInput;
    }
}

void GraphicsWindow::checkPtPolyOverlap(QPointF p) {
    //qDebug() << "poitn: " << p.x() << " - " << p.y();
    for (int i = 0; i < polygons.length(); i++) {
        RubberBandPolygon *curPoly = polygons.at(i);
        int timerDelay = 10; //turn to 0 to make event transitions instant
        if (curPoly->polyContainsPoint(p)) { //if the point is inside the polygon
            //qDebug() << "I'm in a Polygon!";
            if (!curPoly->isSomethingInside() && curPoly->getEventTimer(0) <= 0 && curPoly->getEventTimer(1) <= 0) { //if there was previously nothing inside the polygon
                curPoly->setInside(true);
                if (curPoly->getEventsLength() > 0) {
                    QString event = curPoly->getEvent(0);
                    if (event.compare(curPoly->getPrevEvent()) != 0) {
                    curPoly->setEventTimer(0, timerDelay);
                    curPoly->setColor(Qt::blue, 0.2);
                    curPoly->setPrevEvent(event);
                    emit broadcastEvent(TrodesEventMessage(event)); //the event at location 0 will always be an enter event for zones
                    }
                }

                //curPoly->setBlue();
                //curPoly->type = 1;

                //curPoly->setBrush(QBrush(QColor(255,0,0,255)));
            }
        }
        else { //the point is not in the polygon
            if(curPoly->isSomethingInside() && curPoly->getEventTimer(0) <= 0 && curPoly->getEventTimer(1) <= 0) { //if something was previously insidethe polygon, it isn't any longer
                curPoly->setInside(false);
                if (curPoly->getEventsLength() > 1) {
                    QString event = curPoly->getEvent(1);
                    if (event.compare(curPoly->getPrevEvent()) != 0) {
                    curPoly->setEventTimer(1, timerDelay);
                    curPoly->setPrevEvent(event);
                    emit broadcastEvent(TrodesEventMessage(event));
                    curPoly->setColor(Qt::color1, 0.0);
                    }
                }

            }

        }
    }
}



void GraphicsWindow::tickPolygonEventTimers() {
    for (int i = 0; i < polygons.length(); i++) {//for all polygons
        //RubberBandPolygon *curPoly = polygons.at(i);
        polygons.at(i)->tickEventTimers();
    }
}

void GraphicsWindow::plotCurTrackedLoc() {
    if (isTrackingOn) {
        plotPoint(curTrackedLoc);
    }
}

void GraphicsWindow::plotPoint(QPointF pt) {
    if (pt.x() >= 0 && pt.y() >= 0) {
        int rad = 2;
        plottedPoints.append(scene->addEllipse(pt.x()-rad, pt.y()-rad, rad*2.0, rad*2.0, QPen(Qt::green), QBrush(Qt::SolidPattern)));
    }
}

void GraphicsWindow::sendZoneData(qint8 zoneID) {
    if (zoneID >= 0 && zoneID < polygons.length()) {
        RubberBandPolygon *zonePoly = polygons.at(zoneID);

        dataPacket zoneData(PPT_Zone);
        dataSend zoneID(DT_uint8_t, zonePoly->getId());
        dataSend numLines(DT_int16_t, zonePoly->getRPoints().length());
        zoneData.insert(zoneID);
        zoneData.insert(numLines);
        for (int i = 0; i < zonePoly->getRPoints().length(); i++) {
            dataSend x(DT_qreal, zonePoly->getRPoints().at(i).x());
            dataSend y(DT_qreal, zonePoly->getRPoints().at(i).y());
            zoneData.insert(x);
            zoneData.insert(y);
        }
        emit sendNewDataPacket(zoneData);

    }
    else {
        qDebug() << "Error: bad zoneID sent[" << zoneID << "] (GraphicsWindow::sendZoneData)";
    }
}

void GraphicsWindow::addExcludePolygon() {
    if (DEBUG_MODE) { int fun = 109;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setExcludeType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged()),this,SLOT(calculateConsideredPixels()));

    polygons.append(newPoly);
    scene->addItem(newPoly);
}

void GraphicsWindow::addZonePolygon() {
    if (DEBUG_MODE) { int fun = 110;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setZoneType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));

    polygons.append(newPoly);
    scene->addItem(newPoly);
}

void GraphicsWindow::addIncludePolygon() {
    if (DEBUG_MODE) { int fun = 111;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setIncludeType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged()),this,SLOT(calculateConsideredPixels()));

    polygons.append(newPoly);
    scene->addItem(newPoly);

}

void GraphicsWindow::addLineGeometry(void) {
    if (DEBUG_MODE) { int fun = 112;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    RubberBandNodeShape *newLineShape = new RubberBandNodeShape();

    connect(newLineShape,SIGNAL(hasHighlight()),this,SLOT(lineShapeHighlighted()));

    lineShapes.append(newLineShape);
    scene->addItem(newLineShape);
}

void GraphicsWindow::addTrackGeometry() {
    if (DEBUG_MODE) { int fun = 113;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    RubberBandNodeShape *newLineShape = new RubberBandNodeShape();
    //newPoly->setIncludeType();

    connect(newLineShape,SIGNAL(hasHighlight()),this,SLOT(lineShapeHighlighted()));
    connect(newLineShape,SIGNAL(shapeChanged()),this,SLOT(linearGeometryShapeChanged()));

    lineShapes.append(newLineShape);
    scene->addItem(newLineShape);
}

void GraphicsWindow::spaceClicked() {
    if (DEBUG_MODE) { int fun = 114;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //the background was clicked, so turn off all highlights
    if (currentlySelectedLineShape == -1)
        setNoHighlight();
}

void GraphicsWindow::setNoHighlight() {
    if (DEBUG_MODE) { int fun = 115;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->removeHighlight();
    }
    for (int i=0; i<lineShapes.length(); i++) {
        lineShapes[i]->removeHighlight();
    }
    currentlySelectedPolygon = -1;
    currentlySelectedLineShape = -1;
}

void GraphicsWindow::lineShapeHighlighted() {
    if (DEBUG_MODE) { int fun = 116;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    RubberBandNodeShape* highlightedPoly = dynamic_cast<RubberBandNodeShape*>(sender());

    for (int i=0; i<lineShapes.length(); i++) {
        if (lineShapes[i] == highlightedPoly) {
            currentlySelectedLineShape = i;
        } else {
            lineShapes[i]->removeHighlight();
        }
    }
    for (int i=0; i<polygons.length(); i++) {
         polygons[i]->removeHighlight();
    }
    currentlySelectedPolygon = -1;

}

void GraphicsWindow::polygonHighlighted() {
    if (DEBUG_MODE) { int fun = 117;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    RubberBandPolygon* highlightedPoly = dynamic_cast<RubberBandPolygon*>(sender());

    for (int i=0; i<polygons.length(); i++) {
        if (polygons[i] == highlightedPoly) {
            currentlySelectedPolygon = i;
        } else {
            polygons[i]->removeHighlight();
        }
    }
    for (int i=0; i<lineShapes.length(); i++) {
        lineShapes[i]->removeHighlight();
    }
    currentlySelectedLineShape = -1;
}

void GraphicsWindow::setTool(int toolNum) {
    if (DEBUG_MODE) { int fun = 118;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    currentTool = toolNum;

    if (currentTool != EDITTOOL_ID) {
        //We only allow highlighting when the edit tool is active
        //setNoHighlight();
    }
}

void GraphicsWindow::linearGeometryShapeChanged() {
    if (DEBUG_MODE) { int fun = 119;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //qDebug() << "**  CSLS: " << currentlySelectedLineShape;
    if (currentlySelectedLineShape != -1) {
        QVector<QPointF> nodes = lineShapes.at(currentlySelectedLineShape)->getNodes();
        QVector<LineNodeIndex> geoLines = lineShapes.at(currentlySelectedLineShape)->getLines();

        QSize currentRes = dispWin->getResolution();
        QPointF tmpRelativePoint;
        for (int i=0; i<nodes.length(); i++) {

            tmpRelativePoint.setX(nodes[i].x()/this->scene->width());
            tmpRelativePoint.setY(nodes[i].y()/this->scene->height());
            //qDebug() << " -Node: " << i << " (" << tmpRelativePoint.x() << "," << tmpRelativePoint.y() << ")";
            nodes[i].setX(tmpRelativePoint.x()*currentRes.width());
            nodes[i].setY(tmpRelativePoint.y()*currentRes.height());
        }

        int lengthModifier = 0;
        QVector<dataSend> dataToSend;
        for (int i=0; i<geoLines.length(); i++) {

            QPointF tmpLinePoint, p1, p2;

            tmpRelativePoint.setX(geoLines[i].line.p1().x()/this->scene->width());
            tmpRelativePoint.setY(geoLines[i].line.p1().y()/this->scene->height());
            p1 = tmpRelativePoint;
            tmpLinePoint.setX(tmpRelativePoint.x()*currentRes.width());
            tmpLinePoint.setY(tmpRelativePoint.y()*currentRes.height());
            geoLines[i].line.setP1(tmpLinePoint);


            tmpRelativePoint.setX(geoLines[i].line.p2().x()/this->scene->width());
            tmpRelativePoint.setY(geoLines[i].line.p2().y()/this->scene->height());
            p2 = tmpRelativePoint;
            tmpLinePoint.setX(tmpRelativePoint.x()*currentRes.width());
            tmpLinePoint.setY(tmpRelativePoint.y()*currentRes.height());
            geoLines[i].line.setP2(tmpLinePoint);

            dataSend linSegNum(DT_int16_t, i);
            dataSend x1(DT_qreal, p1.x());
            dataSend y1(DT_qreal, p1.y());
            dataSend x2(DT_qreal, p2.x());
            dataSend y2(DT_qreal, p2.y());

            if ((p1.x() != p2.x()) || (p1.y() != p2.y())) { //exclude non-existant lines
                dataToSend.append(linSegNum);
                dataToSend.append(x1);
                dataToSend.append(y1);
                dataToSend.append(x2);
                dataToSend.append(y2);
            }
            else {
                lengthModifier++;
            }
            //qDebug() << " -- P1(" << p1.x() << "," << p1.y() << ") || P2(" << p2.x() << "," << p2.y() << ")";
        }

        dataPacket linTrack(PPT_LinTrack);
        dataSend numLins(DT_int16_t, geoLines.length()-lengthModifier);
        linTrack.insert(numLins);
        for (int i = 0; i < dataToSend.length(); i++) {
            linTrack.insert(dataToSend.at(i));
        }


        if (currentlySelectedLineShape == currentLinearizationShape) {
            emit sendNewDataPacket(linTrack);
            emit newLinearGeometry(nodes,geoLines);
        }

        //MARK: Followup
        int anchorNodeIndex;
        if (currentlySelectedLineShape == currentLinearizationShape) {
            anchorNodeIndex = linearizationShapeAnchorNode;
        }
        else if (currentlySelectedLineShape == rangeLineShape) {
            anchorNodeIndex = rangeLineAnchorNode;
        }

        emit linearGeometryAnchorNodeSet(anchorNodeIndex);
    }//end if
    else
        qDebug() << "Error: invalid CSLS code 1 -- " << currentlySelectedLineShape;
}

void GraphicsWindow::calculateConsideredPixels() {
    if (DEBUG_MODE) { int fun = 120;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //This function is called every time a polygon
    //is changed or whenever we need to recalculate
    //which pixels are included in the current set of
    //polygons.

    QSize currentRes = dispWin->getResolution();
    quint32 totalPix = currentRes.width()*currentRes.height();
    QVector<bool> isIncluded;

    isIncluded.resize(totalPix);
    //start by assuming that every pixel is included
    for (int i=0; i<isIncluded.length(); i++) {
        isIncluded[i] = true;
    }
    //qDebug() << "   SIZE OF THE VECTOR : " << sizeof(isIncluded);


    if (currentRes.height() > 0) {
        //Calculate which pixels to include, going through each polygon one by one
        for (int i=0; i < polygons.length(); i++) {
            //qDebug() << "   SIZE OF THE Poly : " << sizeof(polygons[i]);
            polygons[i]->calculateIncludedPoints(isIncluded.data(),currentRes.width(),currentRes.height());
        }
        emit newIncludeCalculation(isIncluded);
    }
}

void GraphicsWindow::mousePressEvent(QMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 121;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    qDebug() << " --press at (" << event->localPos().x() << "," << event->localPos().y() << ")";


    emit(broadcastEvent(TrodesEventMessage("Button pressed!")));


    //When the window is clicked, the behavior depends on
    //which tool is currently selected.
    if (currentlyDrawing) {
        polygons.last()->addPoint(event->localPos());
        return;
    }

    if (currentlyDrawingLineShape) {
        int nodeFound = lineShapes[currentlySelectedLineShape]->clickedNearNode(event->localPos());

        QVector<QPointF> tmpNodes = lineShapes[currentlySelectedLineShape]->getNodes();
        if (nodeFound > -1 && nodeFound < (tmpNodes.length()-1)) {
            //Clicked on an existing node (but not the last one).
            qDebug() << "Connecting to existing node";
            lineShapes.at(currentlySelectedLineShape)->connectLastLineToNode(nodeFound);
            currentlyDrawingLineShape = false;
            //lineShapes.length();
            linearGeometryShapeChanged();
        } else {

            lineShapes.at(currentlySelectedLineShape)->addPoint(event->localPos());
            lineShapes[currentlySelectedLineShape]->setLineZone(0,0);

            if (currentlySelectedLineShape == rangeLineShape) { //we only want rangeLine to connect once, so exit out of the line builder
                lineShapes.at(currentlySelectedLineShape)->connectLastLineToNode(nodeFound);
                currentlyDrawingLineShape = false;

                for(int i = 0; i < lineShapes[rangeLineShape]->getNodes().length(); i++ )
                    connect(lineShapes[rangeLineShape]->accessNode(i),SIGNAL(nodeMoveFinished()),this,SLOT(calculatePixelScale()));

                calculatePixelScale(true); //prompts user for real distance and calculates pixel/distance ratio

            }
        }
        return;
    }
    if (currentTool == POINTTOOL_ID) {
        //qDebug() << "POINTTOOL_ID found";

        setNoHighlight();
        QSize currentRes = dispWin->getResolution();
        QPointF newRelativePoint;
        newRelativePoint.setX(event->localPos().x()/this->scene->width());
        newRelativePoint.setY(event->localPos().y()/this->scene->height());
        //qDebug() << newRelativePoint;
        QPoint pixelPoint;
        pixelPoint.setX(newRelativePoint.x()*currentRes.width());
        pixelPoint.setY(newRelativePoint.y()*currentRes.height());
        //For two-point tracking we use the left and right mouse buttons to
        //set each point
        if (event->buttons() == Qt::RightButton) {
            emit userInput2(pixelPoint);
        } else if (event->buttons() == Qt::LeftButton) {
            emit userInput1(pixelPoint);
        }
        //newLocation(pixelPoint);
    } else if (currentTool == INCLUDETOOL_ID) {
        setNoHighlight();
        if (currentIncludePolygon != -1) {
            delete polygons.takeAt(currentIncludePolygon);
        }
        addIncludePolygon();
        currentIncludePolygon = polygons.length()-1;

        currentlyDrawing = true;
        polygons.last()->addPoint(event->localPos());
        polygons.last()->addPoint(event->localPos());

    } else if (currentTool == EXCLUDETOOL_ID) {

        setNoHighlight();
        addExcludePolygon();

        currentlyDrawing = true;
        polygons.last()->addPoint(event->localPos());
        polygons.last()->addPoint(event->localPos());
    } else if (currentTool == ZONETOOL_ID) {

        //Mark: cur
        setNoHighlight();
        addZonePolygon();

        currentlyDrawing = true;
        polygons.last()->addPoint(event->localPos());
        polygons.last()->addPoint(event->localPos());

    } else if ((currentTool == EDITTOOL_ID)&&(itemAt(event->pos()))) {
        // Clicked on polygon, pass on event to children

        //if (((currentLinearizationShape != -1)&&(lineShapes[currentLinearizationShape]->contains(event->localPos())))) {
        //loop detects if any line shapes are present, then acts accordingly
        if (currentLinearizationShape != -1 && lineShapes[currentLinearizationShape]->contains(event->localPos())) {//found linearizationShape
            setNoHighlight();
            currentlySelectedLineShape = currentLinearizationShape;
        }
        else if (rangeLineShape != -1 && lineShapes[rangeLineShape]->contains(event->localPos())) { //found rangeLineShape
            setNoHighlight();
            currentlySelectedLineShape = rangeLineShape;
        }
        else {
            currentlySelectedLineShape = -1; //no lineshape found
        }

        //qDebug() << "   : CSLS " << currentlySelectedLineShape << " -- CLS: " << currentLinearizationShape;

        if (currentlySelectedLineShape != -1) { //if a lineshape was found
            lineShapes[currentlySelectedLineShape]->highlight();

            if (lineShapes[currentlySelectedLineShape]->clickedNearNode(event->localPos()) == -1) {
                if (event->buttons() == Qt::RightButton) {
                    //The the right button was clicked AND not near a node AND near a line, show line context menu
                    int lineClicked = lineShapes[currentlySelectedLineShape]->clickedNearLine(event->localPos());
                    if (lineClicked != -1) {
                        showLinearGeometryLineContextMenu(lineClicked, event->localPos());
                    }
                }
                else {
                    //If the left button was pressed AND not near a node, start dragging entire shape
                        lineShapes[currentlySelectedLineShape]->setLastMouseClickLoc(event->localPos());
                        linShapeIsDragging = true;
                }
            }
        }//end if a lineShape was found

        QGraphicsView::mousePressEvent(event);

    } else if (currentTool == LINEARIZETOOL_ID) {
        //used to define the geometry of a linear track
        if (currentLinearizationShape != -1) {

            //First we check if any of the existing nodes were clicked.  If so, we need
            //to add a branch to that shape instead of starting a new shape
            int shapeClicked = -1;
            int nodeFound = -1;
            //for (int i=0; i<lineShapes.length();i++) {
            nodeFound = lineShapes[currentLinearizationShape]->clickedNearNode(event->localPos());
            if (nodeFound > -1) {

                shapeClicked = currentLinearizationShape;

            }
            //}
            if (shapeClicked > -1) {
                //Start a new branch in an existing shape
                currentlyDrawingLineShape = true;
                lineShapes[shapeClicked]->addBranch(nodeFound);               
                lineShapes[currentLinearizationShape]->setLineZone(lineShapes[currentLinearizationShape]->getLines().length()-1,0);


            } else {
                //Start a new shape by deleting the current one
                deleteLineShape(currentLinearizationShape);
                //create new geometry
                addTrackGeometry();
                currentLinearizationShape = lineShapes.length()-1;
                linearizationShapeAnchorNode = 0;


                connect(lineShapes.last(),SIGNAL(nodeRightClicked(int,QPointF)),this,SLOT(showLinearGeometryNodeContextMenu(int,QPointF)));
                currentlyDrawingLineShape = true;
                lineShapes.last()->addPoint(event->localPos());
                lineShapes.last()->addPoint(event->localPos());
                lineShapes[currentLinearizationShape]->setLineZone(0,0);
            }
        } else {
            //No other linearization shapes exist, so start a new shape
            setNoHighlight();
            addTrackGeometry();
            currentLinearizationShape = lineShapes.length()-1;
            linearizationShapeAnchorNode = 0;

            connect(lineShapes.last(),SIGNAL(nodeRightClicked(int,QPointF)),this,SLOT(showLinearGeometryNodeContextMenu(int,QPointF)));

            currentlyDrawingLineShape = true;
            lineShapes.last()->addPoint(event->localPos());
            lineShapes.last()->addPoint(event->localPos());
            lineShapes[currentLinearizationShape]->setLineZone(0,0);
            //emit newLinearGeometry(lineShapes.last()->getNodes(),lineShapes.last()->getLines());
            emit linearGeometryExists(true);
        }
    }
    else if (currentTool == RANGETOOL_ID) {
        //RangeTool is used to define a pixel/(real distance) ratio -- i.e. 50pix/cm
        if (rangeLineShape != -1) { //if rangeLine already exists...
            if (rangeLineNodeNum >= 1) { //if there are more 2 nodes...
                int nodeClicked = -1;
                nodeClicked = lineShapes[rangeLineShape]->clickedNearNode(event->localPos());

                if (nodeClicked != -1) { //if node was double clicked, move the node

                }
                else { //if not, invalid click, delete obj
                    deleteLineShape(rangeLineShape);
                    rangeLineNodeNum = -1;
                    rangeLineShape = -1;
                    rangeLineAnchorNode = -1;
                }
            }
            else { //if there are less than 2 nodes, simply add one node to the count
                rangeLineNodeNum++;

            }
        } //end if (rangeLine already exists)

        if (rangeLineShape == -1){  //if rangeLine does not already exist..
            //make new rangeLine
            setNoHighlight();
            addTrackGeometry(); //adds a new rubberBand shape to RangeLineShape (at the end of vector LL)
            rangeLineShape = lineShapes.length()-1; //set our range lineShape's index in lineShape array
            rangeLineNodeNum = 0; //set num of nodes to 0
            rangeLineAnchorNode = 0;

            currentlyDrawingLineShape = true;
            lineShapes.last()->addPoint(event->localPos()); //create point at mouse pos
            lineShapes.last()->addPoint(event->localPos());

            lineShapes[rangeLineShape]->setLineZone(0,0);
            emit rangeGeometryExists(true);
        } //end if (create rangeLine)
    }

}

void GraphicsWindow::mouseMoveEvent(QMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 122;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (currentlyDrawing) {
        polygons.last()->moveLastPoint(event->localPos());
        //dispWin->update();
    } else if (currentlyDrawingLineShape) {
        lineShapes.at(currentlySelectedLineShape)->moveLastPoint(event->localPos());
    } else if (linShapeIsDragging) {
        if (currentlySelectedLineShape != -1) {
            lineShapes[currentlySelectedLineShape]->dragTo(event->localPos());
        }
        else {
            qDebug() << "Error: invalid CSLS code 2 -- " << currentlySelectedLineShape;
        }
    }
    else {
        QGraphicsView::mouseMoveEvent(event);
    }


}

void GraphicsWindow::mouseReleaseEvent(QMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 123;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if(linShapeIsDragging && currentlySelectedLineShape != -1) {
        linearGeometryShapeChanged();
    }
    linShapeIsDragging = false;

    if (currentTool == EDITTOOL_ID) {
        if (currentlySelectedPolygon != -1) {
            if (polygons.at(currentlySelectedPolygon)->getType() == 2) {
                sendZoneData(currentlySelectedPolygon);
            }
        }
    }

    QGraphicsView::mouseReleaseEvent(event);
}

void GraphicsWindow::mouseDoubleClickEvent(QMouseEvent *event) {
    if (DEBUG_MODE) { int fun = 124;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (currentlyDrawing) {
        currentlyDrawing = false;
        //qDebug() << "shape finished";
        polygons.last()->removeLastPoint();
        calculateConsideredPixels();

        //mark: event system
        RubberBandPolygon *newPoly = polygons.last();
        newPoly->setId(polygonIdCounter);
        polygonIdCounter++;
        switch (currentTool) {
        case ZONETOOL_ID: {

            emit broadcastEvent(TrodesEventMessage("Zone created!"));
            QString enterEv = QString("Zone %1 entered!").arg(newPoly->getId());
            QString exitEv = QString("Zone %1 exited!").arg(newPoly->getId());
            //qDebug() << "Zone Created:";
            sendZoneData(newPoly->getId());
            newPoly->addEvent(enterEv);
            newPoly->addEvent(exitEv);
            break;
        }
        default:
            break;
        }
        for (int i = 0; i < newPoly->getEventsLength(); i++)
            emit broadcastNewEventReq(newPoly->getEvent(i));

    } else if (currentlyDrawingLineShape) {
        currentlyDrawingLineShape = false;

        //if you double clicked without drawing a line first, then delete the line shape
        if (lineShapes.at(currentlySelectedLineShape)->getNodes().length() == 2) {
            deleteLineShape(currentlySelectedLineShape);
        }
        else //otherwise, remove the final point (which is a double point b/c of double click)
            lineShapes.at(currentlySelectedLineShape)->removeLastPoint();

        if (currentLinearizationShape == currentlySelectedLineShape && currentLinearizationShape != -1) {
            //Just finished drawing a linear track geometry
            linearGeometryShapeChanged();
            emit linearGeometryExists(true);
        }

        switch (currentTool) {
        case LINEARIZETOOL_ID: {
            emit broadcastEvent(TrodesEventMessage("Linearization Track created!"));
            break;
        }
        case RANGETOOL_ID: {
            emit broadcastEvent(TrodesEventMessage("Range Line created!"));
            break;
        }
        default: {
            break;
        }
        }
    }
    else if (currentTool == EDITTOOL_ID) {
        if (currentlySelectedLineShape == rangeLineShape)
            calculatePixelScale(true);
    }
}

void GraphicsWindow::passKeyEvent(QKeyEvent *event) {
    if (DEBUG_MODE) { int fun = 125;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    keyPressEvent(event);
}

void GraphicsWindow::deleteLineShape(int index) {
    if (DEBUG_MODE) { int fun = 126;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (index < lineShapes.length() && index != -1 ) {
        //NOTE: if you add another lineShape tool, then this code WILL NOT WORK
        //also it won't work if you add more than one lin shape and one range shape
        if (index == currentLinearizationShape) { //MARK: delete
            if (currentLinearizationShape < rangeLineShape) {
                rangeLineShape = currentLinearizationShape; //if lin was before range, we have to switch range's loc
            }
            currentLinearizationShape = -1;
            emit linearGeometryExists(false);
        }
        else if (index == rangeLineShape) {
            if (rangeLineShape < currentLinearizationShape) {
                currentLinearizationShape = rangeLineShape; //if range was before line, we have to switch their loc's
            }
            rangeLineShape = -1;
            emit rangeGeometryExists(false);
        }

        delete lineShapes.takeAt(index);
        setNoHighlight();
        //emit linearGeometryExists(false);
    }
    else {
        qDebug() << "Error: invalid index passed (deleteLineShape) " << index;
    }
}

void GraphicsWindow::keyPressEvent(QKeyEvent *event) {
    if (DEBUG_MODE) { int fun = 127;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    if ((event->key() == Qt::Key_Delete)||(event->key() == Qt::Key_Backspace)) {
        //delete the selected polygon
        if (currentlySelectedPolygon != -1) {
            //if an include polygon, note that there is no inlcude polygon anymore
            if (currentlySelectedPolygon == currentIncludePolygon) {
                currentIncludePolygon = -1;
            }
            RubberBandPolygon *rmvPoly = polygons.at(currentlySelectedPolygon);
            //delete all polygon specific events
            for(int i = 0; i < rmvPoly->getEventsLength(); i++) {
                emit broadcastRemoveEventReq(rmvPoly->getEvent(i));
            }

            delete polygons.takeAt(currentlySelectedPolygon);
            setNoHighlight();
            calculateConsideredPixels();
        } else if (currentlySelectedLineShape != -1) {
            deleteLineShape(currentlySelectedLineShape);
        }
    } else if (event->key() == Qt::Key_Escape) {
        if (currentlyDrawing) {
            currentlyDrawing = false;
            polygons.last()->removeLastPoint();
            calculateConsideredPixels();
        } else if (currentlyDrawingLineShape) {
             //Just finished drawing a linear track geometry
            currentlyDrawingLineShape = false;
            lineShapes.at(currentlySelectedLineShape)->removeLastPoint();               
            //emit newLinearGeometry(lineShapes.at(currentlySelectedLineShape)->getNodes(),lineShapes.at(currentlySelectedLineShape)->getLines());
            if (currentlySelectedLineShape == -1)
                qDebug() << "OH NOOOOOO!3";
            linearGeometryShapeChanged();
            emit linearGeometryExists(true);
        }
    }

    if (event->key() == Qt::Key_P) { //MARK: Debug function
        //emit(broadcastRemoveEventReq("Zone created!"));
        for (int i = 0; i < plottedPoints.length(); i++)
            scene->removeItem(plottedPoints.at(i));
        //scene->clear();

        /**
        if (rangeLineShape != -1) {
            qDebug() << "** pt1: " << lineShapes[rangeLineShape]->getNodes().at(0) << " pt2: " << lineShapes[rangeLineShape]->getNodes().at(1);
        }**/
    }
}

void GraphicsWindow::resizeEvent(QResizeEvent *event) {
    if (DEBUG_MODE) { int fun = 128;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    dispWin->resize(event->size().width(),event->size().height());
    scene->setSceneRect(0,0,event->size().width(),event->size().height());

    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->updateSize();
    }
    for (int i=0; i<lineShapes.length(); i++) {
        lineShapes[i]->updateSize();
    }

    qreal x = scene->width()*medianLoc.x();
    qreal y = scene->height()*medianLoc.y();

    medianLocMarker->setX(x-3);
    medianLocMarker->setY(y-3);
    ringMarker->setRect(1,1,2*scene->width()*ringSize.width(),2*scene->height()*ringSize.height());

    ringMarker->setX(x-(scene->width()*ringSize.width()));
    ringMarker->setY(y-(scene->height()*ringSize.height()));


    qreal x1 = scene->width()*LED1Loc.x();
    qreal y1 = scene->height()*LED1Loc.y();
    LED1Marker->setX(x1-3);
    LED1Marker->setY(y1-3);

    qreal x2 = scene->width()*LED2Loc.x();
    qreal y2 = scene->height()*LED2Loc.y();
    LED2Marker->setX(x2-3);
    LED2Marker->setY(y2-3);

    directionMarker->setLine(x1,y1,x2,y2);
    QPainterPath arrowPath;
    arrowPath.moveTo(x1,y1);
    arrowPath.lineTo(directionMarker->line().normalVector().pointAt(0.2)+QPointF(directionMarker->line().dx(),directionMarker->line().dy()));
    arrowPath.lineTo(directionMarker->line().p1()-(directionMarker->line().normalVector().pointAt(0.2)-directionMarker->line().p1()) +QPointF(directionMarker->line().dx(),directionMarker->line().dy()));
    arrowPath.lineTo(directionMarker->line().p1());
    directionArrow->setPath(arrowPath);

    calculatePixelScale();
}

//------------------------------------------------

VideoDisplayWindow::VideoDisplayWindow(QWidget *parent):
    QWidget(parent) {
    if (DEBUG_MODE) { int fun = 129;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    setAutoFillBackground(false);
    //setAttribute(Qt::WA_NoSystemBackground, true);

    QPalette palette = this->palette();
    palette.setColor(QPalette::Background, Qt::black);
    setPalette(palette);
    //MARK: event
    foundRangeLine = false;
    pixelScale = 0;

}

VideoDisplayWindow::~VideoDisplayWindow() {
    if (DEBUG_MODE) { int fun = 130;
    qDebug() << "       Function Call " << fun; } //debugging mode call
}

QSize VideoDisplayWindow::getResolution() {
    if (DEBUG_MODE) { int fun = 131;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    return currentImage.size();
}

void VideoDisplayWindow::newImage(QImage image) {
    if (DEBUG_MODE) { int fun = 132;
    qDebug() << "       Function Call " << fun; } //debugging mode call

    //A new image has come in.  Copy it and delete the original.  This is a bit wasteful, so we
    //might want to use the pointer and delete it when the next frame comes it.
    //currentImage = image->copy();
    //delete image;
    //qDebug() << "new image! vidDisplay";
    currentImage = image; //shallow copy
    QSize newSize = getResolution();

    if ((newSize.height() != currentSize.height())||(newSize.width() != currentSize.width())) {
        currentSize = newSize;
        emit resolutionChanged();
    }

    emit sig_newImage();
    update();
}

void VideoDisplayWindow::setBool(bool check, toolFlag flg) {
    if (DEBUG_MODE) { int fun = 133;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    if (flg == T_RANGE)
      foundRangeLine = check;  //set foundRangeLine bool

    //add other flags here when you want to expand functionality
}

void VideoDisplayWindow::enterEvent(QEvent *event) {
    if (DEBUG_MODE) { int fun = 134;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //mouse entered video display area-- change cursor to cross-hair
    this->setCursor(Qt::CrossCursor);
}

void VideoDisplayWindow::leaveEvent(QEvent *event) {
    if (DEBUG_MODE) { int fun = 135;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //mouse left video display area-- change back to arrow
    this->setCursor(Qt::ArrowCursor);
}

void VideoDisplayWindow::paintEvent(QPaintEvent *event) {
    if (DEBUG_MODE) { int fun = 136;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    QPainter painter(this);
     //Set the painter to use a smooth scaling algorithm.
     //painter.setRenderHint(QPainter::SmoothPixmapTransform, 1);
     painter.setRenderHint(QPainter::HighQualityAntialiasing, 1);

     /*
     QPixmap tempPixMap;
     tempPixMap.convertFromImage(currentImage);
     painter.drawPixmap(this->rect(),tempPixMap);
     */

     painter.drawImage(this->rect(), currentImage);

 }


 void VideoDisplayWindow::resizeEvent(QResizeEvent *event) {
     if (DEBUG_MODE) { int fun = 137;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     QWidget::resizeEvent(event);
 }

 //MARK: event
 //tooltip event manager
 bool VideoDisplayWindow::event(QEvent *event) {
     if (DEBUG_MODE) { int fun = 138;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     if (event->type() == QEvent::ToolTip) {
         QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
         emit isObjAt(helpEvent->pos()); //tell GraphicsWindow to check if an object is under the curser and edit the proper boolians if it is
         if (foundRangeLine) {
            QString s = QString::number(pixelScale);
            QToolTip::showText(helpEvent->globalPos(), "Pixel Scale: " + s + " pix/cm");
         }
         //Add boolian checks here to show other event texts in the same manner as the prev 4 lines


         //QWidget::toolTip();
         return(true);

        //if ()
        //qDebug() << "TootTip Event at (" << helpEvent->pos().x() << "," << helpEvent->pos().y() << ")";
     }
     return(QWidget::event(event));
 }


 //------------------------------------------------
 //FrameBundle is a container for a Qimage frame along with the system timestamp
 FrameBundle::FrameBundle():
    imagePtr(NULL),
    timestamp(0),
    hwFrameCount(0),
    xloc(0),
    yloc(0),
    xloc2(0),
    yloc2(0),
    videoFieldFilled(false),
    timeFieldFilled(false),
    locationFieldFilled(false)
 {

 }

 void FrameBundle::setImage(QImage *imagePtrIn) {
     if (DEBUG_MODE) { int fun = 139;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     imagePtr = imagePtrIn;
     videoFieldFilled = true;
 }

 void FrameBundle::setTime(quint32 timestampIn) {
     if (DEBUG_MODE) { int fun = 140;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     timestamp = timestampIn;
     timeFieldFilled = true;
 }

 void FrameBundle::setLocation(qint16 x1, qint16 y1, qint16 x2, qint16 y2) {
     if (DEBUG_MODE) { int fun = 141;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     xloc = x1;
     yloc = y1;
     xloc2 = x2;
     yloc2 = y2;
     locationFieldFilled = true;
 }


 void FrameBundle::deleteFrame() {
     if (DEBUG_MODE) { int fun = 142;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     delete imagePtr;

     imagePtr = NULL;
     timestamp = 0;
     xloc = 0;
     yloc = 0;
     videoFieldFilled = false;
     timeFieldFilled = false;
     locationFieldFilled = false;
 }

 bool FrameBundle::bothFieldsFilled() {
     if (DEBUG_MODE) { int fun = 143;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     if (videoFieldFilled && timeFieldFilled) {
         return true;
     } else {
         qDebug() << "Video: " << videoFieldFilled << " time: " << timeFieldFilled;
         return false;
     }
 }

 //------------------------------------------------------------------------
 VideoImageBuffer::VideoImageBuffer(QObject *parent):
     bufferSize(100),
     imageWriteHead(0),
     timestampWriteHead(0),
     frameReadHead(0),
     lastFrameReadHead(0)
 {
     if (DEBUG_MODE) { int fun = 144;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     frameBundleBuffer = new FrameBundle[bufferSize];

 }

 VideoImageBuffer::~VideoImageBuffer() {
     if (DEBUG_MODE) { int fun = 145;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     for (int i=0; i < bufferSize; i++) {
         frameBundleBuffer[i].deleteFrame();
     }
     delete [] frameBundleBuffer;
 }

 void VideoImageBuffer::addImage(QImage *imagePtr, uint32_t hwFrameCount) {
     if (DEBUG_MODE) { int fun = 146;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     if (frameBundleBuffer[imageWriteHead].imagePtr) {
         frameBundleBuffer[imageWriteHead].deleteFrame();
     }
     frameBundleBuffer[imageWriteHead].hwFrameCount = hwFrameCount;
     frameBundleBuffer[imageWriteHead].setImage(imagePtr);
     //frameBundleBuffer[imageWriteHead].setLocation(xloc, yloc);
     imageWriteHead = (imageWriteHead+1)%bufferSize;
 }

 void VideoImageBuffer::addImage(QImage *imagePtr, uint32_t hwFrameCount, QPoint loc1, QPoint loc2) {
     if (DEBUG_MODE) { int fun = 147;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     if (frameBundleBuffer[imageWriteHead].imagePtr) {
         frameBundleBuffer[imageWriteHead].deleteFrame();
     }
     frameBundleBuffer[imageWriteHead].hwFrameCount = hwFrameCount;
     frameBundleBuffer[imageWriteHead].setImage(imagePtr);
     //qDebug() << " -- " << loc1.x() << " " << loc1.y() << " _- " << loc2.x() << " " << loc2.y();
     frameBundleBuffer[imageWriteHead].setLocation(loc1.x(),loc1.y(),loc2.x(),loc2.y());
     //frameBundleBuffer[imageWriteHead].setLocation(xloc, yloc);
     imageWriteHead = (imageWriteHead+1)%bufferSize;
 }


 void VideoImageBuffer::addTimestamp(quint32 timestamp) {
     if (DEBUG_MODE) { int fun = 148;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     frameBundleBuffer[timestampWriteHead].setTime(timestamp);
     timestampWriteHead = (timestampWriteHead+1)%bufferSize;
 }

 FrameBundle VideoImageBuffer::getNextFrame() {
     if (DEBUG_MODE) { int fun = 149;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     FrameBundle bundleOut;

     //Don't return a frame unless both time and the image were logged;
     if (frameBundleBuffer[frameReadHead].bothFieldsFilled()) {

        bundleOut = frameBundleBuffer[frameReadHead];

        lastFrameReadHead = frameReadHead;
        frameReadHead = (frameReadHead+1)%bufferSize;
     }

     return bundleOut;
 }

 void VideoImageBuffer::removeLastFrame() {
     if (DEBUG_MODE) { int fun = 150;
     qDebug() << "       Function Call " << fun; } //debugging mode call
    frameBundleBuffer[lastFrameReadHead].deleteFrame();
 }

 //------------------------------------------------

 VideoImageProcessor::VideoImageProcessor(QObject *parent):
     clockRate(0),
     streamActive(false),
     recording(false),
     frameHeight(0),
     frameWidth(0),
     currentVideoFormat(AbstractCamera::Fmt_Invalid),
     fileCreated(false),
     createFileAfterCameraLoad(false),
     lastFrame(NULL),
     lastHWFrameCount(0),
     trackingOn(false),
     skipPosCalc(false),
     positionFile(NULL),
     positionFileOpen(false),
     fastPlayback(false)

 {
     if (DEBUG_MODE) { int fun = 151;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     lastLocationSegment = -1;
     logging = false;
     trackLinearPosition = false;
     linearTrackExists = false;
     currentTrackedLinPosition = -1;
     fpsTimer = new QElapsedTimer();
     changeSegTimer = 0;
     currentTime = -1;
 }


 VideoImageProcessor::~VideoImageProcessor() {
     if (DEBUG_MODE) { int fun = 152;
     qDebug() << "       Function Call " << fun; } //debugging mode call
 }

 void VideoImageProcessor::setVideoFormat(AbstractCamera::videoFmt format) {
     if (DEBUG_MODE) { int fun = 153;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     currentVideoFormat = format;
 }

 void VideoImageProcessor::initialize() {
     if (DEBUG_MODE) { int fun = 154;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     frameBuffer = new VideoImageBuffer(this);
     watchdogTimer = new QTimer();
     connect(watchdogTimer,SIGNAL(timeout()),this,SLOT(watchdogTimout()));
     watchdogTimer->start(2000);
 }

 void VideoImageProcessor::setFastPlayback(bool on) {
     if (DEBUG_MODE) { int fun = 155;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     fastPlayback = on;
 }

 void VideoImageProcessor::setIncludedPixels(QVector<bool> includedPixelsIn) {
     if (DEBUG_MODE) { int fun = 156;
     qDebug() << "       Function Call " << fun; } //debugging mode call

     includedPixels.clear();
     includedPixels = includedPixelsIn;
     if (currentSettings.currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE) {

         replotLastFrame();
     }

 }

 void VideoImageProcessor::linearGeometryLineZoneSet(int lineInd, int zone) {
     if (DEBUG_MODE) { int fun = 157;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     qDebug() << "LINE ZONE!!!!" << lineInd << zone;
     linearTrackLines[lineInd].zone = zone;
 }

 void VideoImageProcessor::linearGeometryAnchorNodeSet(int nodeInd) {
     if (DEBUG_MODE) { int fun = 158;
     qDebug() << "       Function Call " << fun; } //debugging mode call
    qDebug() << "Processor got new linear anchor";
    linearGeometryAnchorNodeInd = nodeInd;
 }

 void VideoImageProcessor::linearGeometryExists(bool on) {
     if (DEBUG_MODE) { int fun = 159;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     trackLinearPosition = on;
 }

 void VideoImageProcessor::newLinearGeometry(QVector<QPointF> nodes, QVector<LineNodeIndex> lines) {
     if (DEBUG_MODE) { int fun = 160;
     qDebug() << "       Function Call " << fun; } //debugging mode call
    linearTrackNodes = nodes;
    linearTrackLines = lines;


    qDebug() << "Processor got new geometry.";

    //Calculate which nodes in the geometry represent end points of arms.
    //These nodes will be used to track the animal's unique linear location in the
    //environment.

    endNodes.clear();
    distToEndNodes.clear();
    for (int n=0; n<linearTrackNodes.length(); n++) {
        int useCount = 0;
        //determine if node is used by only one line segment
        for (int l=0; l < linearTrackLines.length(); l++) {
            if (linearTrackLines[l].endNodeIndex == n || linearTrackLines[l].startNodeIndex == n) {
                useCount++;
            }
        }
        if (useCount == 1) {
            endNodes.push_back(n);
            distToEndNodes.push_back(0.0);
        }
    }


    //Next we want to create a distance matrix stating the minimum linear travel distance between any pairs
    //of nodes.  This algorithm is based on the idea that the nth power of an adjacency matrix tells
    //you which nodes are n segments apart. However, instead of using 0's (not connected) and 1's (connected),
    //we use e^(-distance).  Here 0 means infinite distance (which is the same as not connected).  After all the
    //mulitplication steps, a simple log of the values returns the summed distance.

    qreal distaceDivisor = 1000;
    QVector<QVector<qreal> > adjacentDistanceMatrix; //all 1st order connected nodes
    QVector<QVector<qreal> > distanceMatrix; //used to keep track of nth order distances
    QVector<QVector<qreal> > tmpDistanceMatrix; //tmp storage for matrix multiplication
    QVector<QVector<bool> > doneMatrix; //keeps track of which pairs of nodes have an entry already
    adjacentDistanceMatrix.resize(linearTrackNodes.length());
    tmpDistanceMatrix.resize(linearTrackNodes.length());
    distanceMatrix.resize(linearTrackNodes.length());
    shortestDistanceMatrix.clear();
    shortestDistanceMatrix.resize(linearTrackNodes.length());
    doneMatrix.resize(linearTrackNodes.length());
    int numEntriesNeeded = linearTrackNodes.length()*linearTrackNodes.length();
    int currentEntries = 0;


    for (int n=0;n<linearTrackNodes.length();n++) {
        adjacentDistanceMatrix[n].resize(linearTrackNodes.length());
        distanceMatrix[n].resize(linearTrackNodes.length());
        tmpDistanceMatrix[n].resize(linearTrackNodes.length());
        shortestDistanceMatrix[n].resize(linearTrackNodes.length());
        doneMatrix[n].resize(linearTrackNodes.length());

        for (int n2=0;n2<linearTrackNodes.length();n2++) {
            if (n == n2) {
                doneMatrix[n][n2] = true;
                currentEntries++;
            } else {
                doneMatrix[n][n2] = false;
            }
            adjacentDistanceMatrix[n][n2] = 0; //infinite length
            distanceMatrix[n][n2] = 0;
            shortestDistanceMatrix[n][n2] = 0;
            for (int l=0; l < linearTrackLines.length(); l++) {
                if ( (linearTrackLines[l].endNodeIndex == n && linearTrackLines[l].startNodeIndex == n2) ||
                     (linearTrackLines[l].startNodeIndex == n && linearTrackLines[l].endNodeIndex == n2) )  {
                    adjacentDistanceMatrix[n][n2] = qExp(-linearTrackLines[l].line.length()/distaceDivisor);
                    distanceMatrix[n][n2] = adjacentDistanceMatrix[n][n2]; //currently 1st order
                    shortestDistanceMatrix[n][n2] = linearTrackLines[l].line.length(); //fill in 1st order entries

                    doneMatrix[n][n2] = true;
                    currentEntries++;

                }
            }
        }
    }

    int prev = -1;
    while (currentEntries < numEntriesNeeded) {
        //Matrix multiplication (distanceMatrix * adjacentDistanceMatrix')
        //We do this until every entry in shortestDistanceMatrix has been filled
        for (int n=0;n<linearTrackNodes.length();n++) {
            for (int n2=0;n2<linearTrackNodes.length();n2++) {
                tmpDistanceMatrix[n][n2] = 0;
                if (!doneMatrix[n][n2]) {
                    //We only calculate this entry if a distance between these nodes has not already been
                    //found
                    for (int multInd = 0; multInd < adjacentDistanceMatrix.length(); multInd++) {
                        tmpDistanceMatrix[n][n2] += distanceMatrix[n][multInd]*adjacentDistanceMatrix[multInd][n2];
                    }
                    if (tmpDistanceMatrix[n][n2] > 0) {
                        doneMatrix[n][n2] = true;
                        currentEntries++;
                        shortestDistanceMatrix[n][n2] = -qLn(tmpDistanceMatrix[n][n2])*distaceDivisor;
                    }
                }
            }
        }

        //Update the nth order distance matrix with the matrix multiplication result
        for (int n=0;n<linearTrackNodes.length();n++) {
            for (int n2=0;n2<linearTrackNodes.length();n2++) {
                distanceMatrix[n][n2] = tmpDistanceMatrix[n][n2]; //new nth order distance matrix
            }
        }
        //qDebug() <<"yeah while loop! curEntries: " << currentEntries << "  numEntriesNeeded: " << numEntriesNeeded;
        if (prev == currentEntries) {
            qDebug() << "Error: infinite loop detected, breaking out (VideoImageProcessor::newLinearGeometry)";
            break;
        }
        else {
            prev = currentEntries;
        }
    }
    linearGeometryAnchorNodeInd = 0;
    linearTrackExists = true; //this prevents tracking from occuring before a linear track exists
 }



 void VideoImageProcessor::findLinearPosition(QPoint currentLoc) {
     if (DEBUG_MODE) { int fun = 161;
     qDebug() << "       Function Call " << fun; } //debugging mode call

    QPointF currentLocF = (QPointF)currentLoc;

    QVector<qreal> segmentProjections;
    QVector<qreal> distancesToSegments;
    distancesToSegments.resize(linearTrackLines.length());
    segmentProjections.resize(linearTrackLines.length());

    //Calculate the shortest distance to each segment, then pick the segment
    //that is closest as long as it does not cause a jump in linear position
    qreal minDist = 100000;
    int closestSegment = 0;
    for (int i = 0; i < linearTrackLines.length(); i++) {
        //QLineF normalLine = linearTrackLines[i].line.normalVector();
        //normalLine.translate(currentLocF-normalLine.p1());
        distancesToSegments[i] = distToLine(currentLocF,linearTrackLines[i].line, segmentProjections.data()+i);
        if (distancesToSegments[i] < minDist) {
            minDist = distancesToSegments[i];
            closestSegment = i;
        }
    }

    changeSegTimer++;
    if (lastLocationSegment > -1 && closestSegment != lastLocationSegment) {

        //This segment is different than the last one, so we need to make sure we aren't
        //jumping to the wrong place
        //Figure out which nodes of the last segment and the new segment are closest to each other in the graphF
        qreal jumpDistance;

        //*

        if (shortestDistanceMatrix[linearTrackLines[lastLocationSegment].startNodeIndex][linearTrackLines[closestSegment].startNodeIndex] < shortestDistanceMatrix[linearTrackLines[lastLocationSegment].startNodeIndex][linearTrackLines[closestSegment].endNodeIndex]) {
            if (shortestDistanceMatrix[linearTrackLines[lastLocationSegment].startNodeIndex][linearTrackLines[closestSegment].startNodeIndex] < shortestDistanceMatrix[linearTrackLines[lastLocationSegment].endNodeIndex][linearTrackLines[closestSegment].startNodeIndex]) {
                //start to start
                jumpDistance = segmentProjections[lastLocationSegment] + segmentProjections[closestSegment] + shortestDistanceMatrix[linearTrackLines[lastLocationSegment].startNodeIndex][linearTrackLines[closestSegment].startNodeIndex];
            } else {
                //end to start
                jumpDistance = (linearTrackLines[lastLocationSegment].line.length()-segmentProjections[lastLocationSegment]) + segmentProjections[closestSegment] + shortestDistanceMatrix[linearTrackLines[lastLocationSegment].endNodeIndex][linearTrackLines[closestSegment].startNodeIndex];

            }

        } else {
            if (shortestDistanceMatrix[linearTrackLines[lastLocationSegment].startNodeIndex][linearTrackLines[closestSegment].endNodeIndex] < shortestDistanceMatrix[linearTrackLines[lastLocationSegment].endNodeIndex][linearTrackLines[closestSegment].endNodeIndex]) {
                //start to end
                jumpDistance = segmentProjections[lastLocationSegment] + (linearTrackLines[closestSegment].line.length()-segmentProjections[closestSegment]) + shortestDistanceMatrix[linearTrackLines[lastLocationSegment].startNodeIndex][linearTrackLines[closestSegment].endNodeIndex];

            } else {
                //end to end (impossible?)
                jumpDistance = (linearTrackLines[lastLocationSegment].line.length()-segmentProjections[lastLocationSegment]) + (linearTrackLines[closestSegment].line.length()-segmentProjections[closestSegment]) + shortestDistanceMatrix[linearTrackLines[lastLocationSegment].endNodeIndex][linearTrackLines[closestSegment].endNodeIndex];

            }
        }
        int changeTime = LINEAR_JUMP_TIMEOUT;
        if (jumpDistance > (distancesToSegments[lastLocationSegment]*2)) {
            //The jump along the linear track to the nearst segment is longer than 2 times the direct distance to the old segment, so we keep the old segment
            if (changeSegTimer < changeTime)
                closestSegment = lastLocationSegment;
        }
        else if (changeSegTimer >= changeTime ||
                 ((linearTrackLines[lastLocationSegment].startNodeIndex == linearTrackLines[closestSegment].startNodeIndex) ||
                  (linearTrackLines[lastLocationSegment].startNodeIndex == linearTrackLines[closestSegment].endNodeIndex) ||
                   (linearTrackLines[lastLocationSegment].endNodeIndex == linearTrackLines[closestSegment].startNodeIndex) ||
                    (linearTrackLines[lastLocationSegment].endNodeIndex == linearTrackLines[closestSegment].endNodeIndex)))
        {
            //change tracks
            changeSegTimer = 0;
        }
        else { //iterate timer and don't change tracks
            closestSegment = lastLocationSegment;
        }

    }
    else if (lastLocationSegment > -1 && closestSegment == lastLocationSegment) {
        changeSegTimer = 0; //on same seg, reset timer
    }

    lastLocationSegment = closestSegment;

    //determin the relative tracked linear position
    if (lastLocationSegment != -1) {
        qreal linLen = linearTrackLines[lastLocationSegment].line.length();
        qreal relativeLocationOnLin = segmentProjections.at(lastLocationSegment); //location from node0 to point on line
        currentTrackedLinPosition = relativeLocationOnLin/linLen; //set the current linear position (relative positioning 0-1 scale)
        dataPacket linData(PPT_Lin);
        dataSend linSeg(DT_int16_t, lastLocationSegment);
        dataSend linPos(DT_qreal, currentTrackedLinPosition);
        linData.insert(linSeg);
        linData.insert(linPos);
        emit sendNewDataPacket(linData);
    }


    //Calculate the distance from each node at the end of an arm to the current location. The combination
    //of these distances represents a unique location in the linear environment
    qreal propAlongSegment = segmentProjections[closestSegment]/linearTrackLines[closestSegment].line.length();
    emit newLinearLocation(linearTrackLines[closestSegment].line.pointAt(propAlongSegment).toPoint());

    //MARK: Pos
    qreal distFromAnchorNode;
    //To calculate the shortest distance from the anchor node to the animal, we need to decide if it is shorter to go
    //though the current segment's start node or end node
    if (shortestDistanceMatrix[linearGeometryAnchorNodeInd][linearTrackLines[closestSegment].startNodeIndex]+segmentProjections[closestSegment] < shortestDistanceMatrix[linearGeometryAnchorNodeInd][linearTrackLines[closestSegment].endNodeIndex]+linearTrackLines[closestSegment].line.length()-segmentProjections[closestSegment]) {
        distFromAnchorNode = shortestDistanceMatrix[linearGeometryAnchorNodeInd][linearTrackLines[closestSegment].startNodeIndex] + segmentProjections[closestSegment];
    } else {
        distFromAnchorNode = shortestDistanceMatrix[linearGeometryAnchorNodeInd][linearTrackLines[closestSegment].endNodeIndex] +linearTrackLines[closestSegment].line.length()-segmentProjections[closestSegment];
    }
    //qDebug() << distFromAnchorNode << linearTrackLines[closestSegment].zone;

    //qDebug() << distToEndNodes;

 }

 qreal VideoImageProcessor::distToLine(const QPointF &p, const QLineF &line, qreal *lineProjection) {
     if (DEBUG_MODE) { int fun = 162;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     //Create a line from the point to the first point of the input line
     QLineF lineToPoint;
     lineToPoint.setP1(line.p1());
     lineToPoint.setP2(p);
     //Calculate the angle between the two lines
     qreal angleToPoint = 2*M_PI*(line.angleTo(lineToPoint)/360);

     qreal projectionOntoSegment = qCos(angleToPoint)*lineToPoint.length();
     qreal distanceToSegment;
     if (projectionOntoSegment > 0 && projectionOntoSegment <= line.length()) {
        //The projected point falls on the line
        distanceToSegment = abs(qSin(angleToPoint)*lineToPoint.length());
     } else if (projectionOntoSegment < 0) {
         //The projected point is less than the first point of the line.  Use the distance to p1
         distanceToSegment = lineToPoint.length();
         projectionOntoSegment = 0;
     } else {
         //The projected point is greater than the 2nd point on the line. Use the distance to p2
         QLineF lineToPoint2;
         lineToPoint2.setP1(line.p2());
         lineToPoint2.setP2(p);
         distanceToSegment = lineToPoint2.length();
         projectionOntoSegment = line.length(); //The projection is the total length of the line

     }
     *lineProjection = projectionOntoSegment;
     return distanceToSegment;


   // transform to loocal coordinates system (0,0) - (lx, ly)

   /*
   QPointF p1 = line.p1();
   QPointF p2 = line.p2();
   qreal x = p.x() - p1.x();
   qreal y = p.y() - p1.y();
   qreal x2 = p2.x() - p1.x();
   qreal y2 = p2.y() - p1.y();

   // if line is a point (nodes are the same) =>
   // just return distance between point and one line node
   qreal norm = sqrt(x2*x2 + y2*y2);
   if (norm <= std::numeric_limits<int>::epsilon())
     return sqrt(x*x + y*y);

   // distance
   return fabs(x*y2 - y*x2) / norm;
   */
 }

 //pixel threshold (dark or light)
 void VideoImageProcessor::newTrackingSettings(TrackingSettings t) {
     if (DEBUG_MODE) { int fun = 163;
     qDebug() << "       Function Call " << fun; } //debugging mode call

     currentSettings = t;
     if (currentSettings.currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE) {

         replotLastFrame();
     }
 }

 void VideoImageProcessor::userInput1(QPoint loc) {
     if (DEBUG_MODE) { int fun = 164;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     if (trackingOn) {

         lastLocationSegment = -1; //Allow jumps to any linear geometry segment

         //Figure out which tracking algorithm to use
         if (!currentSettings.twoLEDs || currentSettings.trackDark) {
             xMedian = loc.x();
             yMedian = loc.y();
         } else {

             LED1Loc.setX(loc.x());
             LED1Loc.setY(loc.y());
             midPointLoc.setX((LED1Loc.x()+LED2Loc.x())/2);
             midPointLoc.setY((LED1Loc.y()+LED2Loc.y())/2);
         }

         skipPosCalc = true;

         replotLastFrame();
         skipPosCalc = false;
     }
 }

 void VideoImageProcessor::userInput2(QPoint loc) {
     if (DEBUG_MODE) { int fun = 165;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     if (trackingOn) {
         lastLocationSegment = -1; //Allow jumps to any linear geometry segment

         if (currentSettings.twoLEDs) {
             LED2Loc.setX(loc.x());
             LED2Loc.setY(loc.y());
             midPointLoc.setX((LED1Loc.x()+LED2Loc.x())/2);
             midPointLoc.setY((LED1Loc.y()+LED2Loc.y())/2);
         }
         skipPosCalc = true;

         replotLastFrame();
         skipPosCalc = false;
     }
 }

 /*
 void VideoImageProcessor::newThreshold(int value, bool trackDarkPix) {
     brightPixThreshold = value;
     trackDark = trackDarkPix;
     replotLastFrame();
 }

 void VideoImageProcessor::newRing(int newRingSize, bool newRingOn) {
     ringSize = newRingSize;
     ringOn = newRingOn;
     replotLastFrame();
 }*/

 //toggle position tracking
 void VideoImageProcessor::setTracking(bool track) {
     if (DEBUG_MODE) { int fun = 166;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     trackingOn = track;
     if (trackingOn && fileCreated && !positionFileOpen) {
         //live camera mode -- create a file
         createPositionFile(baseFileName+".videoPositionTracking");
     }
     if (currentSettings.currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE) {
         //offline tracking from file
         /*
         QFileInfo fi;
         fi.setFile(baseFileName+".videoPositionTracking");
         if (!fi.exists()) {
             qDebug() << "Creating new tracking file.";
             createPositionFile(baseFileName+".videoPositionTracking");
         } else {
             qDebug() << "Opening previous tracking file.";
         }*/


         replotLastFrame();
     }
 }


 void VideoImageProcessor::replotLastFrame() {
     if (DEBUG_MODE) { int fun = 167;
     qDebug() << "       Function Call " << fun; } //debugging mode call

    if (lastFrame != NULL) {


        //QImage* displayImage = new QImage(lastFrame->copy());
        QImage displayImage = lastFrame->copy();




        if (trackingOn) {
            //Figure out which tracking algorithm to use
            if (!currentSettings.twoLEDs || currentSettings.trackDark) {
                processImage(displayImage);
            } else if (currentSettings.LEDColorPair == LED_COLOR_RED_GREEN){
                processImage_2LED_REDGREEN(displayImage);
            } else if (currentSettings.LEDColorPair == LED_COLOR_WHITE_WHITE) {
                processImage(displayImage); //will change
            }
        }

        emit newImage_signal(displayImage); //Signal to display the image. Not safe across threads!!


        //delete displayImage; //Not safe across threads!!


     }
 }

 void VideoImageProcessor::processImage(QImage &displayImage) {
     if (DEBUG_MODE) { int fun = 168;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     bool useIncludeFilter = false;
     if (includedPixels.length() > 0) {
         useIncludeFilter = true;
     }
     //If tracking is on, we find all the bright (or dark) pixels, based on the current threshold.
     //We count the number of pixels along the image columns and rows.
     uint16_t ycounts[displayImage.height()];
     uint16_t xcounts[displayImage.width()];
     for(int y = 0; y<displayImage.height(); y++){
         ycounts[y] = 0;
     }
     for(int x = 0; x<displayImage.width(); x++){
         xcounts[x] = 0;
     }

     int totalCount = 0;
     //QRgb * line;
     uint32_t currentPixNum = 0;

     int circMinX = 0;
     int circMaxX = 0;
     int circMinY = 0;
     int circMaxY = 0;

     //Calculate the rectangle around the exclude ring
     if (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) {
         circMinX = xMedian-currentSettings.currentRingSize;
         circMaxX = xMedian+currentSettings.currentRingSize;
         circMinY = yMedian-currentSettings.currentRingSize;
         circMaxY = yMedian+currentSettings.currentRingSize;
     }

     int bytesPerPixel = 3;
     int pixelOffset = 0;


     if (displayImage.format() == QImage::Format_RGB888) {
         bytesPerPixel = 3;
     } else if (displayImage.format() == QImage::Format_RGB32) {
         bytesPerPixel = 4;
         pixelOffset = 1;
     } else if (displayImage.format() == QImage::Format_ARGB32) {
         bytesPerPixel = 4;
         pixelOffset = 0;
     }

     for(int y = 0; y<displayImage.height(); y++){
         //QRgb * line = (QRgb *)img->scanLine(y);
         uchar* line = displayImage.scanLine(y);



        if ((useIncludeFilter) && (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) && ((y < circMinY)||(y > circMaxY))) {
            currentPixNum = currentPixNum+displayImage.width();
            continue;
        }

         for(int x = 0; x<displayImage.width(); x++){
             QRgb currentPixel = displayImage.pixel(x,y);
             int average = (qRed(currentPixel) + qGreen(currentPixel) + qRed(currentPixel))/3;

             //int average = (qRed(line[x]) + qGreen(line[x]) + qRed(line[x]))/3;
             //considerThisPix = true;
             if (useIncludeFilter) {
                 if (!includedPixels[currentPixNum]) {
                     currentPixNum++;

                     continue;
                     //considerThisPix = false;
                 }
                 if (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) {
                     if ((x < circMinX)||(x > circMaxX)||(y < circMinY)||(y > circMaxY)) {
                         currentPixNum++;
                         continue;
                     } else {


                         double dist = sqrt(pow((double) x-xMedian,2) + pow((double) y-yMedian,2));
                         if (dist > currentSettings.currentRingSize) {
                         currentPixNum++;
                         continue;
                         }
                     }
                 }
             }
             if ((!currentSettings.trackDark)&&(average > currentSettings.currentThresh)) {
                 ycounts[y]++;
                 xcounts[x]++;
                 totalCount++;
                 //If the pixel was bright, change it to red
                 //displayImage.setPixel(x,y, qRgb(255, 0, 0));


                 int cp = (bytesPerPixel*x)+pixelOffset;
                 if (displayImage.format() == QImage::Format_ARGB32) {
                     line[cp] = 0;
                     line[cp+1] = 0;
                     line[cp+2] = 255;
                 } else if (displayImage.format() == QImage::Format_RGB32){

                    line[cp-1] = 0;
                    line[cp] = 0;
                    line[cp+1] = 255;
                    line[cp+2] = 0;
                 } else if (displayImage.format() == QImage::Format_RGB888){

                     line[cp] = 255;
                     line[cp+1] = 0;
                     line[cp+2] = 0;
                 }
             } else if ((currentSettings.trackDark)&&(average < currentSettings.currentThresh)) {

                 ycounts[y]++;
                 xcounts[x]++;
                 totalCount++;
                 //If the pixel was dark, change it to blue
                 //displayImage.setPixel(x,y, qRgb(0, 0, 255));
                 int cp = (bytesPerPixel*x)+pixelOffset;
                 if (displayImage.format() == QImage::Format_ARGB32) {
                     line[cp] = 255;
                     line[cp+1] = 0;
                     line[cp+2] = 0;
                 } else if (displayImage.format() == QImage::Format_RGB32){
                     line[cp-1] = 255;
                     line[cp] = 0;
                     line[cp+1] = 0;
                     line[cp+2] = 255;
                 } else if (displayImage.format() == QImage::Format_RGB888){


                    line[cp] = 0;
                    line[cp+1] = 0;
                    line[cp+2] = 255;
                 }


             }
             currentPixNum++;
         }
     }

     if (!skipPosCalc) {
         //Now that we have the bright pixel counts, we can calculate the
         //median x and y location of all the bright pixels
         int halfCount = totalCount/2;
         totalCount = 0;
         for(int y = 0; y<displayImage.height(); y++){
             totalCount+=ycounts[y];
             if (totalCount > halfCount) {
                 yMedian = y;
                 break;
             }
         }

         totalCount = 0;
         for(int x = 0; x<displayImage.width(); x++){
             totalCount+=xcounts[x];
             if (totalCount > halfCount) {
                 xMedian = x;
                 break;
             }
         }
     }

     //Paint on a green circle where the median bright pixel location is
     //int circleRadius = displayImage.height()/100;
     /*
     QPainter p;
     p.begin(displayImage);
     QPen pen;
     pen.setColor(QColor(0,255,0));
     pen.setWidth(2);
     p.setPen(pen);
     p.drawEllipse(QPoint(xMedian,yMedian),circleRadius,circleRadius);




     if (currentSettings.ringOn) {
         p.drawEllipse(QPoint(xMedian,yMedian),currentSettings.currentRingSize,currentSettings.currentRingSize);
     }

     p.end();
     */
     QPoint currLoc = QPoint(xMedian,yMedian);
     if (trackLinearPosition && linearTrackExists) {
        findLinearPosition(currLoc); //MARK: pos
     }

     emit newLocation(currLoc);
 }


 void VideoImageProcessor::processImage_2LED_REDGREEN(QImage &displayImage) {
     if (DEBUG_MODE) { int fun = 169;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     bool useIncludeFilter = false;
     if (includedPixels.length() > 0) {
         useIncludeFilter = true;
     }
     //If tracking is on, we find all the bright (or dark) pixels, based on the current threshold.
     //We count the number of pixels along the image columns and rows.
     uint16_t ycounts1[displayImage.height()];
     uint16_t  xcounts1[displayImage.width()];

     uint16_t  ycounts2[displayImage.height()];
     uint16_t  xcounts2[displayImage.width()];

     for(int y = 0; y<displayImage.height(); y++){
         ycounts1[y] = 0;
         ycounts2[y] = 0;
     }
     for(int x = 0; x<displayImage.width(); x++){
         xcounts1[x] = 0;
         xcounts2[x] = 0;
     }

     int totalCount1 = 0;
     int totalCount2 = 0;
     //QRgb * line;
     uint32_t currentPixNum = 0;

     int circMinX = 0;
     int circMaxX = 0;
     int circMinY = 0;
     int circMaxY = 0;

     //Calculate the rectangle around the exclude ring
     if (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) {
         circMinX = midPointLoc.x()-currentSettings.currentRingSize;
         circMaxX = midPointLoc.x()+currentSettings.currentRingSize;
         circMinY = midPointLoc.y()-currentSettings.currentRingSize;
         circMaxY = midPointLoc.y()+currentSettings.currentRingSize;
     }
     /*
     unsigned char* buffer_;
     int w = displayImage.width();
     int h = displayImage.height();

     buffer_ = new unsigned char[3 * w * h];
     //...
     displayImage.data_ptr();

     for(int i = 0; i < h; i++){
      for(int j = 0; j < w; j++){

       unsigned char r, g, b;
       //...

       buffer_[4 * (i * w + j)    ] = r;
       buffer_[4 * (i * w + j) + 1] = g;
       buffer_[4 * (i * w + j) + 2] = b;
      }
     }*/

     int bytesPerPixel = 3;
     int pixelOffset = 0;

     if (displayImage.format() == QImage::Format_RGB888) {
         bytesPerPixel = 3;
     } else if (displayImage.format() == QImage::Format_RGB32) {
         bytesPerPixel = 4;
         pixelOffset = 1;
     } else if (displayImage.format() == QImage::Format_ARGB32) {
         bytesPerPixel = 4;
         pixelOffset = 0;
     }

     for(int y = 0; y<displayImage.height(); y++){
         uchar* line = displayImage.scanLine(y);

         if ((useIncludeFilter) && (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) && ((y < circMinY)||(y > circMaxY))) {
             currentPixNum = currentPixNum+displayImage.width();
             continue;
         }

         for(int x = 0; x<displayImage.width(); x++){

             //int average = (qRed(currentPixel) + qGreen(currentPixel) + qRed(currentPixel))/3;

             //int average = (qRed(line[x]) + qGreen(line[x]) + qRed(line[x]))/3;
             //considerThisPix = true;
             if (useIncludeFilter) {
                 if (!includedPixels[currentPixNum]) {
                     currentPixNum++;

                     continue;
                     //considerThisPix = false;
                 }
                 if (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) {
                     if ((x < circMinX)||(x > circMaxX)||(y < circMinY)||(y > circMaxY)) {
                         currentPixNum++;
                         continue;
                     } else {


                         double dist = sqrt(pow((double) x-midPointLoc.x(),2) + pow((double) y-midPointLoc.y(),2));
                         if (dist > currentSettings.currentRingSize) {
                             currentPixNum++;
                             continue;
                         }
                     }
                 }
             }


             if (currentSettings.LEDColorPair == LED_COLOR_RED_GREEN) {
                 //int bluethresh = 255-currentSettings.currentThresh;
                 int cp = (bytesPerPixel*x)+pixelOffset;

                 if (displayImage.format() == QImage::Format_ARGB32) {

                     if ((line[cp+1] > currentSettings.currentThresh)&&(line[cp] < currentSettings.currentThresh) && (line[cp+1] > line[cp+2]) ) {
                         //The pixel is more green than the other two colors
                         ycounts1[y]++;
                         xcounts1[x]++;
                         totalCount1++;
                         //Change pixel to pure green
                         //displayImage.setPixel(x,y, qRgb(0, 255, 0));

                         line[cp] = 0;
                         line[cp+1] = 255;
                         line[cp+2] = 0;

                     } else if ((line[cp+2] > currentSettings.currentThresh)&&(line[cp+1] < currentSettings.currentThresh) && (line[cp+2] > line[cp]) ) {
                         //The pixel is more red than the other two colors
                         ycounts2[y]++;
                         xcounts2[x]++;
                         totalCount2++;
                         //Change pixel to pure red
                         //displayImage.setPixel(x,y, qRgb(255, 0, 0));


                         line[cp] = 0;
                         line[cp+1] = 0;
                         line[cp+2] = 255;

                     }
                 } else if (displayImage.format() == QImage::Format_RGB32){
                     QRgb currentPixel = displayImage.pixel(x,y);
                     if ((qGreen(currentPixel) > currentSettings.currentThresh)&&(qRed(currentPixel) < currentSettings.currentThresh) && (qGreen(currentPixel) > qBlue(currentPixel)) ) {
                         //The pixel is more green than the other two colors
                         ycounts1[y]++;
                         xcounts1[x]++;
                         totalCount1++;
                         //Change pixel to pure green

                         line[cp] = 255;
                         line[cp+1] = 0;
                         line[cp+2] = 0;

                     } else if ((qRed(currentPixel) > currentSettings.currentThresh)&&(qGreen(currentPixel) < currentSettings.currentThresh) && (qRed(currentPixel) > qBlue(currentPixel)) ) {
                         //The pixel is more red than the other two colors
                         ycounts2[y]++;
                         xcounts2[x]++;
                         totalCount2++;
                         //Change pixel to pure red

                         line[cp-1] = 0;
                         line[cp] = 0;
                         line[cp+1] = 255;
                         line[cp+2] = 0;

                     }
                   } else if (displayImage.format() == QImage::Format_RGB888){
                     if ((line[cp+1] > currentSettings.currentThresh)&&(line[cp] < currentSettings.currentThresh) && (line[cp+1] > line[cp+2]) ) {
                         //The pixel is more green than the other two colors
                         ycounts1[y]++;
                         xcounts1[x]++;
                         totalCount1++;
                         //Change pixel to pure green
                         //displayImage.setPixel(x,y, qRgb(0, 255, 0));

                         line[cp] = 0;
                         line[cp+1] = 255;
                         line[cp+2] = 0;

                     } else if ((line[cp] > currentSettings.currentThresh)&&(line[cp+1] < currentSettings.currentThresh) && (line[cp] > line[cp+2]) ) {
                         //The pixel is more red than the other two colors
                         ycounts2[y]++;
                         xcounts2[x]++;
                         totalCount2++;
                         //Change pixel to pure red
                         //displayImage.setPixel(x,y, qRgb(255, 0, 0));


                         line[cp] = 255;
                         line[cp+1] = 0;
                         line[cp+2] = 0;

                     }

                 }
                /*
                 if ((qGreen(currentPixel) > currentSettings.currentThresh)&&(qRed(currentPixel) < currentSettings.currentThresh) && (qGreen(currentPixel) > qBlue(currentPixel)) ) {
                     //The pixel is more green than the other two colors
                     ycounts1[y]++;
                     xcounts1[x]++;
                     totalCount1++;
                     //Change pixel to pure green
                     //displayImage.setPixel(x,y, qRgb(0, 255, 0));

                     int cp = (bytesPerPixel*x)+pixelOffset;
                     if (displayImage.format() == QImage::Format_ARGB32) {
                         line[cp] = 0;
                         line[cp+1] = 0;
                         line[cp+2] = 255;
                     } else {

                        line[cp] = 255;
                        line[cp+1] = 0;
                        line[cp+2] = 0;
                     }

                     //line[cp] = 255;
                     //line[cp+1] = 0;
                     //line[cp+2] = 0;

                 } else if ((qRed(currentPixel) > currentSettings.currentThresh)&&(qGreen(currentPixel) < currentSettings.currentThresh) && (qRed(currentPixel) > qBlue(currentPixel)) ) {
                     //The pixel is more red than the other two colors
                     ycounts2[y]++;
                     xcounts2[x]++;
                     totalCount2++;
                     //Change pixel to pure red
                     //displayImage.setPixel(x,y, qRgb(255, 0, 0));

                     int cp = (bytesPerPixel*x)+pixelOffset;
                     if (displayImage.format() == QImage::Format_ARGB32) {
                         line[cp] = 0;
                         line[cp+1] = 0;
                         line[cp+2] = 255;
                     } else {

                        line[cp] = 0;
                        line[cp+1] = 0;
                        line[cp+2] = 255;
                     }
                     //line[cp] = 0;
                     //line[cp+1] = 255;
                     //line[cp+2] = 0;

                 }*/
             }

             currentPixNum++;
         }
     }

     if (!skipPosCalc) {
         //Now that we have the bright pixel counts, we can calculate the
         //median x and y location of the bright pixels
         int halfCount1 = totalCount1/2;
         int halfCount2 = totalCount2/2;

         //Calculate median location of LED1
         totalCount1 = 0;
         for(int y = 0; y<displayImage.height(); y++){
             totalCount1+=ycounts1[y];
             if (totalCount1 > halfCount1) {
                 LED1Loc.setY(y);
                 break;
             }
         }
         totalCount1 = 0;
         for(int x = 0; x<displayImage.width(); x++){
             totalCount1+=xcounts1[x];
             if (totalCount1 > halfCount1) {
                 LED1Loc.setX(x);
                 break;
             }
         }

         //Calculate median location of LED2
         totalCount2 = 0;
         for(int y = 0; y<displayImage.height(); y++){
             totalCount2+=ycounts2[y];
             if (totalCount2 > halfCount2) {
                 LED2Loc.setY(y);
                 break;
             }
         }
         totalCount2 = 0;
         for(int x = 0; x<displayImage.width(); x++){
             totalCount2+=xcounts2[x];
             if (totalCount2 > halfCount2) {
                 LED2Loc.setX(x);
                 break;
             }
         }

         midPointLoc.setX((LED1Loc.x()+LED2Loc.x())/2);
         midPointLoc.setY((LED1Loc.y()+LED2Loc.y())/2);



         //Now we determine if we need to ask the user for input
         double distBtwLEDs = sqrt(pow((double) LED1Loc.x()-LED2Loc.x(),2) + pow((double) LED1Loc.y()-LED2Loc.y(),2));
         if (distBtwLEDs > 300) {
             //The distance between the LEDs is too high

             //qDebug() << "Problem!" << distBtwLEDs;
             emit badLocation();
         } else if (totalCount1 == 0 || totalCount2 == 0) {
             //No pixels found for at least one LED
             emit badLocation();
         }
     }

     if (trackLinearPosition && linearTrackExists) {
        findLinearPosition(midPointLoc);
     }
    //MARK: pos
     emit newLocation(LED1Loc,LED2Loc,midPointLoc);

 }


 void VideoImageProcessor::newImage(QImage *img, quint32 hardwareFrameCount, bool flip) {
     if (DEBUG_MODE) { int fun = 170;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     /*qDebug() << fpsTimer->elapsed();
     fpsTimer->restart();*/
     framesStillComing = true;
     if ((hardwareFrameCount > lastHWFrameCount) && (hardwareFrameCount-lastHWFrameCount > 1) ) {
         qDebug() << "Frames dropped: " << hardwareFrameCount-lastHWFrameCount-1;
     }


     //qDebug() << "Processor got image, " << QThread::currentThreadId();
     if (flip) {
        *img = img->mirrored(false,true);
    }

    lastFrame = img;
    //The image to be displayed is not always the same as the image to save, so we need to make a deep copy.

    //QImage* displayImage = new QImage(img->copy());
    QImage displayImage = img->copy();


    //qint16 xMedian = 0;
    //qint16 yMedian = 0;


    if (trackingOn) {
        //Figure out which tracking algorithm to use
        if (!currentSettings.twoLEDs || currentSettings.trackDark) {
            processImage(displayImage);
        } else if (currentSettings.LEDColorPair == LED_COLOR_RED_GREEN){
            processImage_2LED_REDGREEN(displayImage);
        } else if (currentSettings.LEDColorPair == LED_COLOR_WHITE_WHITE) {
            processImage(displayImage); //will change
        }
    }

    if (!fastPlayback) {

        emit newImage_signal(displayImage); //Signal to display the image.
    } else {
        emit newImage_signal(displayImage); //Signal to display the image.
        //delete displayImage;
    }

    frameHeight = img->height();
    frameWidth = img->width();

    if (!streamActive) {
            streamActive = true;
            emit streamStarted(img->height(),img->width());
            if (createFileAfterCameraLoad) {
                //This means that we are waiting for the camera to initialize before open up a
                //file.  So we call createFile().
                createFileAfterCameraLoad = false;
                createFile(nextFileName);
            }
    }
     //Add the frame to the buffer (when all info for the frame is filled, it can be saved)
     if (trackingOn || logging) {
         if (!currentSettings.twoLEDs) {
            frameBuffer->addImage(img,hardwareFrameCount,QPoint(xMedian,yMedian),QPoint(0,0));
         } else {
             frameBuffer->addImage(img,hardwareFrameCount,LED1Loc,LED2Loc);
         }
     } else {
        frameBuffer->addImage(img,hardwareFrameCount);
     }
     lastHWFrameCount = hardwareFrameCount;
     //delete displayImage; //Not safe across threads!!

     //encoder->encodeImage(img);
 }

 void VideoImageProcessor::createFile(QString filename) {
     if (DEBUG_MODE) { int fun = 171;
     qDebug() << "       Function Call " << fun; } //debugging mode call
    //Create all files (video, timestamps, position)
    qDebug() << "Creating video file.";

     if (fileCreated) {
         //If a file already exists, the user has switched cameras while a file was open.  This means that we need to close the current
         //file.  Then, we wait for the camera to get initialized, at which point this function gets called again by
         //newImage(...)

         qDebug() << "Setting up next file to open after camera initates.";
         closeFile();
         nextFileName = filename;
         createFileAfterCameraLoad = true;
         streamActive = false;
         return;
     }
     if (streamActive) {

        int fps = 25;
        videoEncoder = new X264VideoEncoder(NULL);
        connect(this,SIGNAL(encodeImage(QImage)),videoEncoder,SLOT(encodeImage(QImage)));
        connect(this,SIGNAL(writeTimeStamp(quint32,quint32)),videoEncoder,SLOT(writeTimestamp(quint32,quint32)));

        //Turning off this thread for now, it may not be needed.
        //Added note-- putting this in separate thread appears to cause a race condition and crash.
        //This needs some investigating.

        QThread* encoderThread = new QThread();
        encoderThread->setObjectName("EncoderThread");
        //connect(encoderThread ,SIGNAL(started()),videoEncoder,SLOT(startAudio()));
        connect(videoEncoder, SIGNAL(finished()), encoderThread , SLOT(quit()));
        connect(videoEncoder, SIGNAL(finished()), videoEncoder, SLOT(deleteLater()));
        connect(encoderThread , SIGNAL(finished()), encoderThread , SLOT(deleteLater()));
        videoEncoder->moveToThread(encoderThread);
        encoderThread->start();

        bool ok = videoEncoder->createFile(filename+".h264",frameWidth,frameHeight,currentVideoFormat,fps);

        if (ok) {
            qDebug() << "Video file created";

        } else {
            qDebug() << "Error creating video file";            
            return;
        }

        ok = videoEncoder->createTimestampFile(filename+".videoTimeStamps", clockRate);
        if (ok) {
            qDebug() << "Timestamp file created";
            fileCreated = true;

        } else {
            qDebug() << "Error creating timestamp file";
            closeFile();
            return;
        }

        if (trackingOn) {
            if (!createPositionFile(filename+".videoPositionTracking")) {
                qDebug() << "Error creating tracking file";
                closeFile();
                return;
            }
        }

        baseFileName = filename;

        emit fileOpened();
        if (recording) {
            emit recordingStarted();
        }

     }
 }

 void VideoImageProcessor::createPlaybackLogFile(QString filename) {
     if (DEBUG_MODE) { int fun = 172;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     qDebug() << "Creating log file: " << filename;
     if (!createPositionFile(filename)) {
         //TODO: Code to open pop-up error window
         return;
     }
     logging = true;

 }

 void VideoImageProcessor::closePlaybackLogFile() {
     if (DEBUG_MODE) { int fun = 173;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     qDebug() << "Closing log file";
     closePositionFile();
 }

 bool VideoImageProcessor::isFileCreated() {
     if (DEBUG_MODE) { int fun = 174;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     return fileCreated;
 }

 /*
 void VideoImageProcessor::createFileAfterCameraLoaded(QString filename) {
     baseFileName = filename;
     createFileAfterCameraLoad = true;
     streamActive = false;
 }*/

 bool VideoImageProcessor::createPositionFile(QString filenameIn) {
     if (DEBUG_MODE) { int fun = 175;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     //Create a new position file

     if (!positionFileOpen) {

         qDebug() << "Creating position file: " << filenameIn;
         positionFile = new QFile;
         positionFile->setFileName(filenameIn);

         //Create file
         if (!positionFile->open(QIODevice::ReadWrite)) {
             return false;
         }

         positionFileOpen = true;

         //Write the current settings to file
         positionFile->write("<Start settings>\n");
         QString threshLine = QString("threshold: %1\n").arg(currentSettings.currentThresh);
         positionFile->write(threshLine.toLocal8Bit());
         QString trackDarkLine = QString("dark: %1\n").arg((int) currentSettings.trackDark);
         positionFile->write(trackDarkLine.toLocal8Bit());
         QString clockrateLine = QString("clockrate: %1\n").arg((int) clockRate);
         positionFile->write(clockrateLine.toLocal8Bit());
         QString fieldLine;
         fieldLine += "Fields: ";
         fieldLine += "<time uint32>";
         fieldLine += "<xloc uint16>";
         fieldLine += "<yloc uint16>";
         fieldLine += "<xloc2 uint16>";
         fieldLine += "<yloc2 uint16>";
         fieldLine += "\n";
         positionFile->write(fieldLine.toLocal8Bit());

         positionFile->write("<End settings>\n");
         positionfileStartOfData = positionFile->pos();
         return true;

     } else {
         return false;
     }

 }

 void VideoImageProcessor::closePositionFile() {
     if (DEBUG_MODE) { int fun = 176;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     if (positionFileOpen) {
        positionFile->close();
        delete positionFile;
        positionFileOpen = false;
        logging = false;
     }
 }

 void VideoImageProcessor::writePosition(const FrameBundle &fb) {
     if (DEBUG_MODE) { int fun = 177;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     if (positionFileOpen) {

         QDataStream outStream(positionFile); //link outStream to the file
         outStream.setByteOrder(QDataStream::LittleEndian);
         //TODO:  put the following line in an if statement,
         //depending on what info should be included in the file
         outStream << fb.timestamp << fb.xloc << fb.yloc << fb.xloc2 << fb.yloc2;
         positionFile->flush();
     }
 }

 void VideoImageProcessor::seekPositionFile(qint32 t) {
     if (DEBUG_MODE) { int fun = 178;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     //Seek to a specific position in the position log file, based on the timestamp.
     //Erase everything after that position
     if (positionFileOpen) {

        qint32 timestamp;
        qint16 xloc, yloc, xloc2, yloc2;
        timestamp = -1;
        positionFile->seek(positionfileStartOfData);
        QDataStream inStream(positionFile); //link inStream to the file
        inStream.setByteOrder(QDataStream::LittleEndian);
        qint32 lastPos;
        while ((timestamp < t)&&(!positionFile->atEnd())) {
            lastPos = positionFile->pos();
            inStream >> timestamp >> xloc >> yloc >> xloc2 >> yloc2; //Read in one record
        }
        positionFile->resize(lastPos); //Erases everything after
        positionFile->seek(lastPos);
     }
 }

void VideoImageProcessor::watchdogTimout() {
    if (DEBUG_MODE) { int fun = 179;
    qDebug() << "       Function Call " << fun; } //debugging mode call
    //qDebug() << "Checking for frames..." << framesStillComing;

    //Used to make sure frames are still coming in during recording.  If not, reset hardware to get things working again.
    if (recording && !framesStillComing) {
        //Frames have stopped coming in during a recording.  We need to reset the camera.
        qDebug() << "Frame stream distrupted during recording.  Attempting to reconnect...";
        emit  watchdogAlarm();

    }
    framesStillComing = false;  //set to false, and when a new frame comes in it will be set to true again.
}


 void VideoImageProcessor::startRecording() {
     if (DEBUG_MODE) { int fun = 180;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     qDebug() << "Got record signal" << streamActive << fileCreated;
     recording = true;

     if (streamActive && fileCreated) {
        emit recordingStarted();
     }


 }

 void VideoImageProcessor::stopRecording() {
     if (DEBUG_MODE) { int fun = 181;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     recording = false;

 }

 void VideoImageProcessor::closeFile() {
     if (DEBUG_MODE) { int fun = 182;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     videoEncoder->close();
     //videoEncoder->endThread(); //This is used if the encoder lived ina separate thread
     delete videoEncoder; //This is used if the encoder lives in this thread
     fileCreated = false;
     closePositionFile();


 }

 void VideoImageProcessor::toggleLogging(bool on) {
     if (DEBUG_MODE) { int fun = 183;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     logging = on;

 }

 void VideoImageProcessor::newTimestamp(quint32 t) {
     if (DEBUG_MODE) { int fun = 184;
     qDebug() << "       Function Call " << fun; } //debugging mode call
    //The requested timestamp was received from the master module (ie., Trodes).
     //Now, we can save the image and the timestamp to file.

     //qDebug() << "Time stamp received: " << t;
     frameBuffer->addTimestamp(t);

     FrameBundle completedFrame = frameBuffer->getNextFrame();
     //qDebug() << "Next frame: " << completedFrame.timestamp;

     if (completedFrame.imagePtr != NULL) {
         //MARK: pos data

         emit newAnimalLocation(completedFrame.timestamp);

         dataPacket twoDimCoordinates(PPT_2DPos);
         dataSend x1(DT_int16_t, completedFrame.xloc);
         dataSend y1(DT_int16_t, completedFrame.yloc);
         //qDebug() << " --T: " << QTime::currentTime().msecsSinceStartOfDay();
         int newTime = QTime::currentTime().msecsSinceStartOfDay();
         //qreal newTime2 = ((qreal)clock()/CLOCKS_PER_SEC)*1000; //current sys prgm clock in ms;
         //qDebug() << " --Sending timestamp: " << newTime2;
         currentTime = newTime;

         dataSend time(DT_int, currentTime);
         //dataSend time2(DT_qreal, newTime2);
         twoDimCoordinates.insert(time); //add this to the decoder side, then we can compare the latencies
         //twoDimCoordinates.insert(time2);
         twoDimCoordinates.insert(x1);
         twoDimCoordinates.insert(y1);
         emit sendNewDataPacket(twoDimCoordinates);


         if (recording) {
            //qDebug() << "Writing to file";
            emit encodeImage(*completedFrame.imagePtr);
            emit writeTimeStamp(completedFrame.timestamp, completedFrame.hwFrameCount);

            //videoEncoder->encodeImage(*completedFrame.imagePtr);
            //videoEncoder->writeTimestamp(completedFrame.timestamp);

            if (trackingOn) {
                if (positionFileOpen) {
                    writePosition(completedFrame);
                } else {
                    qDebug() << "No position file created!!";
                }

            }

            //encoder->encodeImage(*completedFrame.imagePtr);
            //encoder->writeTimestamp(completedFrame.timestamp);
         } else if ((positionFileOpen) && (logging)) {
             writePosition(completedFrame);
         }

         //if the frame existed, we remove it from the buffer
         //frameBuffer->removeLastFrame();
     }

 }

 void VideoImageProcessor::endProcessing() {
     if (DEBUG_MODE) { int fun = 185;
     qDebug() << "       Function Call " << fun; } //debugging mode call
     watchdogTimer->stop();
     emit finished();
 }

