#ifndef PYTHONCAMERAINTERFACE_H
#define PYTHONCAMERAINTERFACE_H

#include "PythonQt.h"
#include <QObject>
#include <QtWidgets>
#include <QtCore>

/*template <class T> class PythonDataInterface {

public:
    PythonDataInterface();

    void writeDataPoint(const T &p);
    T readDataPoint();
    bool isNextReadContinuous();
    int dataAvailable();

private:

    int bufferSize;
    QVector<T> buffer;
    int writeHead;
    int readHead;

    quint64 totalItemsWritten;
    quint64 totalItemsRead;
    bool continuous;



};*/



class PythonDataInterface : public QObject {

Q_OBJECT

public:
    PythonDataInterface(PythonQtObjectPtr *mainContextInput);

    void writeDataPoint(const QVariant &p);



public slots:

    /*QPoint readNextPoint();
    QPoint readLatestPoint();
    qreal readReal();
    bool isNextReadContinuous();
    int dataAvailable();*/
    void setNewDataCallback(const QString &constructorCall);
    void setSpeed(qreal s);
    QPoint currentPos();



private:
    PythonQtObjectPtr *mainContext;
    QPoint _currentPos;
    QElapsedTimer timer;
    qreal averageFramePeriod;

    /*int bufferSize;
    QVector<QVariant> buffer;
    int writeHead;
    int readHead;

    quint64 totalItemsWritten;
    quint64 totalItemsRead;
    bool continuous;

    QVariant readDataPoint();
    void updateReadHead();*/

    PythonQtObjectPtr newDataCallbackTag;
    bool hasCallback;

signals:
    void newVelocity(double velocity);


};


class QtToPythonCircularBuffer : public QObject {

Q_OBJECT

public:
    QtToPythonCircularBuffer();

    void setContext(PythonQtObjectPtr *mainContextInput);
    PythonDataInterface *dataBuffer;

public slots:

    void writePoint(QPoint p);
    void writePoint(QPoint pf, QPoint pb, QPoint mid);


private:

signals:
    void sendVelocity(double velocity);


};





#endif // PYTHONCAMERAINTERFACE_H
