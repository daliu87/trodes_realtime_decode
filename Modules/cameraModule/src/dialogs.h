/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIALOGS_H
#define DIALOGS_H

#include <QToolTip>
#include <QDialog>
#include <QtWidgets>
#include <QCamera>
#include "sharedtrodesstyles.h"
//#include <QCameraInfo>

//Tool button definitions
#define TOOL_BUTTON_SIZE 50
#define TOOL_NUM_BUTTONS 7
#define TOOL_COLUMNS 2
#define TOOL_ROWS ((TOOL_NUM_BUTTONS/TOOL_COLUMNS)+(TOOL_NUM_BUTTONS%2))
#define TOOL_PANEL_PAD_W 4
#define TOOL_PANEL_PAD_H 6
    //tool IDs
#define POINTTOOL_ID 1
#define EXCLUDETOOL_ID 2
#define INCLUDETOOL_ID 3
#define ZONETOOL_ID 4
#define EDITTOOL_ID 7
#define LINEARIZETOOL_ID 6
#define RANGETOOL_ID 5

#define LED_COLOR_WHITE_WHITE 0
#define LED_COLOR_RED_GREEN 1

//A four-bit value descibes the module's current operation mode:

#define CAMERAMODULE_STANDALONEMODE 1
#define CAMERAMODULE_SLAVEMODE 2

#define CAMERAMODULE_CAMERASTREAMINGMODE 8
#define CAMERAMODULE_FILEPLAYBACKMODE 16


class TrackingButton : public TrodesButton {

Q_OBJECT

public:
    TrackingButton(QWidget *parent = 0);
    void setRedDown(bool yes);
    void setBlinkOn();
    void setBlinkOff();
    bool hitButton(const QPoint &pos) const {
        return(QPushButton::hitButton(pos));
    }
private:
    QTimer blinkTimer;

private slots:

    void toggleBlink();

};


//SocketDialog is used to connect to a TCP server

class ToolsDialog : public QFrame {

Q_OBJECT

public:
    ToolsDialog(int currentTool, QWidget *parent = 0);
    ~ToolsDialog();

private:
    int currentSelectedTool;
    //tool dialogue menu buttons
    TrodesButton *pointToolButton;
    TrodesButton *excludeToolButton;
    TrodesButton *includeToolButton;
    TrodesButton *zoneToolButton;
    TrodesButton *editToolButton;
    TrodesButton *linearizeToolButton;
    TrodesButton *rangeToolButton;
    TrodesButton **toolButtons;

    int isButtonAt(const QPoint &pos) const;


private slots:
    void toolButtonPressed();


public slots:


protected:
    void closeEvent(QCloseEvent* event);
    bool event(QEvent *) Q_DECL_OVERRIDE;

signals:

    void windowOpenState(bool);
    void windowClosed();
    void toolActivated(int);

};

class TrackingSettings {
public:
    TrackingSettings();
    int currentThresh;
    bool trackDark;
    unsigned char currentOperationMode;

    int currentRingSize;
    bool ringOn;
    bool onlyConsiderPixelsInsideRing;
    int LEDColorPair;
    bool twoLEDs;

};


//SocketDialog is used to connect to a TCP server
class SettingsDialog : public QFrame {

Q_OBJECT

public:
    SettingsDialog(TrackingSettings settings, QWidget *parent = 0);

private:

    QSlider *threshSlider;
    QSlider *ringSizeSlider;
    QLabel  *threshDisplay;
    QLabel  *ringSizeDisplay;
    TrodesButton *flipButton;
    QGroupBox *threshBox;
    QGroupBox *ringBox;
    QGroupBox *LEDBox;
    TrodesButton *colorOneButton;

    TrodesButton *advancedButton;
    //bool trackDark;
    //bool ringOn;
    //int currentThresh;
    //int currentRingSize;
    TrackingSettings currentSettings;

private slots:

    void updateValues(int);
    void updateRingSize(int);
    void updateColorButtonLabels();
    void flipButtonPressed();
    void toggleRing(bool);
    void toggleTwoLEDs(bool);
    void colorButton1Pressed();

public slots:


protected:
    void closeEvent(QCloseEvent* event);

signals:

    void settingsChanged(TrackingSettings t);
    //void newThresh(int newThresh, bool trackDark);
    //void newRing(int ringSize, bool ringOn);
    void windowOpenState(bool);
    void ringToggled(bool);
    void windowClosed();


};


class SourceDialog : public QFrame {

Q_OBJECT

public:
    SourceDialog(QStringList availableCameras, QWidget *parent = 0);


private:

    QMenu *sourceListMenu;
    QList<QAction*> cameraListActions;
    QMenu *cameraMenuItem;
    QAction *fileMenuAction;
    //QSlider *threshSlider;
    //QLabel  *threshDisplay;
    //QPushButton *flipButton;
    //bool trackDark;
    //int currentThresh;

private slots:

    //void updateValues(int);
    //void flipButtonPressed();
    void newCameraItemSelected();
    void fileActionSelected();

public slots:


protected:
    void closeEvent(QCloseEvent* event);

signals:

    //void newThresh(int newThresh, bool trackDark);
    //void windowOpenState(bool);
    void windowClosed();
    void cameraSelected(int);
    void inputFileSelected(QString);


};



#endif // DIALOGS_H
