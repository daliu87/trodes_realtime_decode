/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "videoDecoder.h"
#include <limits.h>
#include <stdint.h>


H264_Decoder::H264_Decoder():
  fileIsOpen(false)
  ,codec(NULL)
  ,codec_context(NULL)
  ,parser(NULL)
  ,format_context(NULL)
  ,frame(0)
{
  avcodec_register_all();
  av_register_all();
}

H264_Decoder::~H264_Decoder() {

  if(parser) {
    av_parser_close(parser);
    parser = NULL;
  }

  if(codec_context) {
    avcodec_close(codec_context);
    av_free(codec_context);
    codec_context = NULL;
  }



  if(picture) {
    av_free(picture);
    picture = NULL;
  }

  // Close the video file
  if(format_context) {
     avformat_close_input(&format_context);
  }

  frame = 0;

}

bool H264_Decoder::load(QString filename, float fps) {
    //load the video file for decoding
    // Open video file


    if(avformat_open_input(&format_context, filename.toStdString().c_str(), NULL, NULL)!=0)
        return false; // Couldn't open file

    // Retrieve stream information
    if(avformat_find_stream_info(format_context,NULL)<0)
        return false; // Couldn't find stream information



    // Find the first video stream
    videoStream=-1;
    for(unsigned i=0; i<format_context->nb_streams; i++)
        if(format_context->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO)
        {
            videoStream=i;
            break;
        }
    if(videoStream==-1)
        return false; // Didn't find a video stream


    codec = avcodec_find_decoder(AV_CODEC_ID_H264);
    if(!codec) {
        printf("Error: cannot find the h264 codec: %s\n", filename.toStdString().c_str());
        return false;
    }

    codec_context = avcodec_alloc_context3(codec);

    if(codec->capabilities & CODEC_CAP_TRUNCATED) {
        codec_context->flags |= CODEC_FLAG_TRUNCATED;
    }

    if(avcodec_open2(codec_context, codec, NULL) < 0) {
        printf("Error: could not open codec.\n");
        return false;
    }


    fileIsOpen = true;

    //Allocate memory for picture
	#if LIBAVCODEC_VERSION_INT >= AV_VERSION_INT(55,28,1)

    	picture = av_frame_alloc();
	#else
		picture = avcodec_alloc_frame();
	#endif

    parser = av_parser_init(AV_CODEC_ID_H264);

    if(!parser) {
        printf("Error: cannot create H264 parser.\n");
        return false;
    }
    frame = -1; //The first frame has not been read yet
    qDebug() << "Video loaded";
    return true;
}

bool H264_Decoder::seekFrame(int64_t frameNum) {

    //Seek to the desired frame number    
    bool keyFrameBeforeTargetFound = false;

    //rewind file to the first frame

    //rewind file to the first data byte
#if LIBAVFORMAT_VERSION_MAJOR < 55
    if(avformat_seek_file(format_context,videoStream,format_context->data_offset,
                          format_context->data_offset,format_context->data_offset,
                          AVSEEK_FLAG_BYTE)<0) {
#else
    if(avformat_seek_file(format_context,videoStream,0,0,0,AVSEEK_FLAG_BYTE)<0) {
#endif
        qDebug() << "Rewind failed";
        return false;
    }
    frame = -1;

    /*
    if (frameNum == 0) {
        keyFrameBeforeTargetFound = true;
    }*/

    //find the last key frame (complete frame) before the desired frame
    while (frame < frameNum) {
        AVPacket pkt;
        av_init_packet(&pkt);

        // Read a frame
        if(av_read_frame(format_context, &pkt)<0) {
            av_free_packet(&pkt);
            qDebug() << "Can not read frame";
            return false;                             // Frame read failed (e.g. end of stream)
        }
        frame++;

        //Is this a key frame?
        if (pkt.flags & AV_PKT_FLAG_KEY) {
            //qDebug() << "key frame" << frame;
            if (frameNum-frame < 100) {
                //if we are within 100 frames of the desired frame, stop seeking.
                //We need to do full decoding the rest of the way
                keyFrameBeforeTargetFound = true;
                avcodec_flush_buffers(codec_context);

                decodeCurrentFrame(&pkt);
                av_free_packet(&pkt);

                break;
            }
        }


        av_free_packet(&pkt);      // Free the packet that was allocated by av_read_frame
    }

    if (!keyFrameBeforeTargetFound) {
        qDebug() << "Could not seek to desired frame";
        return 0;
    }


    if (frameNum == 0) {
        return true;
    }

    bool success = false;
    uint64_t lastSuccessfulDecode = 0;
    //For the rest of the way, we do full decoding
    while (frame < frameNum) {

        AVPacket pkt;
        av_init_packet(&pkt);

        // Read a frame from file
        if(av_read_frame(format_context, &pkt)<0) {
            av_free_packet(&pkt);
            if ((frameNum-frame)<5) {
                qDebug() << "Seek not axact, but within 5 frames.";
                return true;
            } else {
                qDebug() << "Frame read during seek failed.";
                return false;                             // Frame read failed (e.g. end of stream)
            }
        }

        //decode the frame
        if (!decodeCurrentFrame(&pkt)) {
            success = false;
            //qDebug() << "Frame decode during seek failed.";
            //return false; //decode failed

        } else {
            success = true;
            lastSuccessfulDecode = frame+1;
        }
        frame++;

        //free the memory
        av_free_packet(&pkt);

    }

    if (success) {
        return true;
    } else {
        qDebug() << lastSuccessfulDecode << frame;
        return false;
    }

}

bool H264_Decoder::decodeCurrentFrame(AVPacket* pkt) {

    int got_picture = 0;
    int len = 0;

    len = avcodec_decode_video2(codec_context, picture, &got_picture, pkt);
    if(len < 0) {       
        return false;
    }

    if(got_picture == 0) {       
        return false;
    }

    return true;

}

void H264_Decoder::createQImageFromFrame(QImage *&img) {
    //Public function to create a QImage from the current frame

    static SwsContext *m_swsCtx = NULL;


    //Here we create a new QImage.  This memory must be manually deallocated after it has been displayed and processed.

    QImage *tmpframe =  new QImage( picture->width, picture->height,
                             QImage::Format_RGB32 );
    m_swsCtx = sws_getCachedContext ( m_swsCtx, picture->width,
                                      picture->height, AV_PIX_FMT_YUV420P,
                                      picture->width, picture->height,
                                      AV_PIX_FMT_RGB32, SWS_BICUBIC,
                                      NULL, NULL, NULL );
    uint8_t *dstSlice[] = { tmpframe->bits() };
    int dstStride = tmpframe->width() * 4;
    sws_scale ( m_swsCtx, picture->data, picture->linesize,
                0, picture->height, dstSlice, &dstStride );
    img = tmpframe;

    lastFrame = tmpframe;

}

bool H264_Decoder::readFrame() {
    //public function to read in from file and decode next frame

    AVPacket pkt;
    av_init_packet(&pkt);


    // Read a frame
    if(av_read_frame(format_context, &pkt)<0) {
        qDebug() << "Frame: " << frame+1;
        return false;                             // Frame read failed (e.g. end of stream)
    }

    ++frame;

    if (!decodeCurrentFrame(&pkt)) {
        qDebug() << "Decode failed";
        return false;
    }

    av_free_packet(&pkt);      // Free the packet that was allocated by av_read_frame

    return true;
}

