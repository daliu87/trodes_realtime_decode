#ifndef LFPEXPORTHANDLER_H
#define LFPEXPORTHANDLER_H
#include "abstractexporthandler.h"
#include "iirFilter.h"


class DATExportHandler: public AbstractExportHandler
{
    Q_OBJECT

public:
    DATExportHandler(QStringList arguments);
    int processData();
    ~DATExportHandler();

protected:

    void parseArguments();
    void printHelpMenu();

    //void printCustomMenu();
    //void parseCustomArguments(int &argumentsProcessed);

private:
    QString sdFileName;
    QFile *sdFilePtr;
    QVector<char> sdFilePacketBuffer;
    QVector<uint32_t> sdSyncValues;
    QVector<uint32_t> sdTimeDuringSync;
    QVector<uint32_t> sdPacketDuringSync;

    QList<int> deviceListToUse;
    int sdFilePacketSize;
    int sdFilePacketStartDataLoc;
    int sdFilePacketFlagByteLoc;
    int sdFilePacketSyncByteLoc;
    int sdFilePacketTimeByteLoc;

    int trodesRFPacketByte;

    uint32_t currentSDTimeStamp;
    uint32_t lastSDTimeStamp;
    uint32_t currentRecSyncStamp;

    int64_t currentRecPacket = 0;
    int64_t currentSDPacket = 0;

    QString mergedConfFileName;

    int SDRecNumChan;

    QList<QDataStream*> neuroStreamPtrs;


    bool openSDInputFile();
    bool writeMergedPacket();
    bool readNextSDPacket();
    bool readNextRecPacket();



};

#endif // LFPEXPORTHANDLER_H
