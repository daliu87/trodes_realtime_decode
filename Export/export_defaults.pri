#-------------------------------------------------
#
# Project include file for all Trodes export utilities
#
#-------------------------------------------------

include (../build_defaults.pri)

QT       += core xml gui network widgets

CONFIG   += console
# Do not build an app bundle on Mac
macx:CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH  += ../../Trodes/src-main
INCLUDEPATH  += ../../Trodes/src-config
INCLUDEPATH  += ../../Trodes/src-threads
INCLUDEPATH  += ../


#unix: QMAKE_CXXFLAGS += -D__STDC_CONSTANT_MACROS
QMAKE_CFLAGS += -g -O3
# Requied for some C99 defines
DEFINES += __STDC_CONSTANT_MACROS



SOURCES += ../../Trodes/src-config/configuration.cpp \
           ../../Trodes/src-main/iirFilter.cpp \
           ../abstractexporthandler.cpp \
           ../../Trodes/src-threads/spikeDetectorThread.cpp \
           ../../Trodes/src-main/trodesSocket.cpp \
           ../../Trodes/src-main/trodesdatastructures.cpp \
           ../../Trodes/src-main/eventHandler.cpp

HEADERS  += ../../Trodes/src-config/configuration.h \
            ../../Trodes/src-main/iirFilter.h \
            ../abstractexporthandler.h \
            ../../Trodes/src-threads/spikeDetectorThread.h \
            ../../Trodes/src-main/trodesSocket.h \
           ../../Trodes/src-main/trodesdatastructures.h \
           ../../Trodes/src-main/eventHandler.h
