#include "datexporthandler.h"
#include <algorithm>
#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    maxGapSizeInterpolation = 0;
    invertSpikes = true;

    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    if ((maxGapSizeInterpolation > 0) || (maxGapSizeInterpolation < 0)) {
         qDebug() << "Error: interp must be 0.";
         argumentReadOk = false;
    }

    if (outputSamplingRate != -1) {
        qDebug() << "Error: outputrate must stay unchanged for spike processing.";
        qDebug() << outputSamplingRate;
        argumentReadOk = false;
    }

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to extract spike waveforms from a raw rec file and save to individual files for each nTrode. \n");
    printf("Usage:  exportspikes -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    printf("Defaults:\n -invert 1 -outputrate -1 (full sampling, can't be changed') \n -usespikefilters 1 \n -interp 0 \n -userefs 1 \n\n\n");
    printf("-invert <1 or 0> -- Whether or not to invert spikes to go upward\n");


    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    QString invertSpikes_string = "";
    int optionInd = 1;
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        } else if ((argumentList.at(optionInd).compare("-invert",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            invertSpikes_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }

    //Read the invert argument
    if (!invertSpikes_string.isEmpty()) {
        bool ok1;

        invertSpikes = invertSpikes_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Invert spikes argument could not be resolved into an integer. Use 0 (false) or 1 (true)";
            argumentReadOk = false;
            return;
        }
    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {


    qDebug() << "Exporting to spike waveform data...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    createFilters();

    if (!openInputFile()) {
        return -1;
    }


    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.baseName();
    } else {
        fileBaseName = outputFileName;
    }

    QString directory;
    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+".spikes"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating spikes directory.";
            return -1;
        }
    }

    //Pointers to the neuro files
    /*QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;*/



    QString infoLine;
    QString fieldLine;



    //Create an output file for the spike data
    //*****************************************


    //Create the output files (one for each nTrode) and write header sections
    for (int i=0; i<spikeConf->ntrodes.length(); i++) {
        neuroFilePtrs.push_back(new QFile);

        //Connect the spike detection event to a function that writes data to file
        connect(spikeDetectors[i],SIGNAL(spikeDetectionEvent(int,const QVector<int2d>*,const int*,uint32_t)),this,SLOT(writeSpikeData(int,const QVector<int2d>*,const int*,uint32_t)));
        //neuroFilePtrs.last()->setFileName(saveLocation+QString("spikes_nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));
        neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".spikes_nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));
        if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            return -1;
        }
        neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
        neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

        //Write the current settings to file

        neuroFilePtrs.last()->write("<Start settings>\n");
        infoLine = QString("Description: Spike waveforms for one nTrode\n");
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("Byte_order: little endian\n");
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("Original_file: ") + fi.fileName() + "\n";
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("nTrode_ID: %1\n").arg(spikeConf->ntrodes[i]->nTrodeId);
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("num_channels: %1\n").arg(spikeConf->ntrodes[i]->hw_chan.length());
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("Clockrate: %1\n").arg(hardwareConf->sourceSamplingRate);
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("Voltage_scaling: %1\n").arg(0.195);
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("Time_offset: %1\n").arg(startOffsetTime);
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("System_time_at_creation: %1\n").arg(globalConf->systemTimeAtCreation);
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("Timestamp_at_creation: %1\n").arg(globalConf->timestampAtCreation);
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("Threshold: %1\n").arg(spikeConf->ntrodes[i]->thresh[0]);
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

        if (invertSpikes) {
            infoLine = QString("Spike_invert: yes\n");
        } else {
            infoLine = QString("Spike_invert: no\n");
        }
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

        if (useRefs && nTrodeSettings[i].nTrodeRefOn) {
            infoLine = QString("Reference: on \nReferenceNTrode: %1\nReferenceChannel: %2\n").arg(spikeConf->ntrodes[nTrodeSettings[i].nTrodeRefNtrode]->nTrodeId).arg(nTrodeSettings[i].nTrodeRefNtrodeChannel+1);
        } else {
            infoLine = QString("Reference: off\n");
        }
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

        if (useSpikeFilters && nTrodeSettings[i].filterOn) {
            infoLine = QString("Filter: on \nlowPassFilter: %1\nhighPassFilter: %2\n").arg(nTrodeSettings[i].nTrodeLowPassFilter).arg(nTrodeSettings[i].nTrodeHighPassFilter);
        } else if ((filterLowPass != -1) || (filterHighPass != -1) ) {
            infoLine = QString("Filter: on \nlowPassFilter: %1\nhighPassFilter: %2\n").arg(filterLowPass).arg(filterHighPass);

        } else {
            infoLine = QString("Filter: off \n");
        }

        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        writeDefaultHeaderInfo(neuroFilePtrs.last());

        fieldLine.clear();
        fieldLine += "Fields: ";
        fieldLine += "<time uint32>";

        for (int ch=0;ch<spikeConf->ntrodes[i]->hw_chan.length();ch++) {
            fieldLine += "<waveformCh";
            fieldLine += QString("%1").arg(ch+1);
            fieldLine += " 40*int16>";
        }
        fieldLine += "\n";
        neuroFilePtrs.last()->write(fieldLine.toLocal8Bit());


        neuroFilePtrs.last()->write("<End settings>\n");
        neuroFilePtrs.last()->flush();
    }


    //************************************************
    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;


            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }



        }
        int pointsSinceLastSpikeCheck = 0;
        int spikeCheckInterval = 1000;
        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {
            //Read in a packet of data to make sure everything looks good
            if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
                //We have reached the end of the file
                break;
            }
            //Find the time stamp
            bufferPtr = buffer.data()+packetTimeLocation;
            tPtr = (uint32_t *)(bufferPtr);
            currentTimeStamp = *tPtr + startOffsetTime;

            //The number of expected data points between this time stamp and the last (1 if no data is missing)
            int numberOfPointsToProcess = (currentTimeStamp-lastTimeStamp);
            if (numberOfPointsToProcess < 1) {
                qDebug() << "Error with file: backwards timestamp at time " << currentTimeStamp << lastTimeStamp;
                if (abortOnBackwardsTimestamp) {
                    qDebug() << "Aborting";
                    return -1;
                } else {
                    for (int i=0; i < channelFilters.length(); i++) {
                        channelFilters[i]->resetHistory();
                    }

                    for (int i=0; i<spikeDetectors.length();i++){
                        spikeDetectors[i]->clearHistory();
                    }
                    numberOfPointsToProcess = 1;
                    pointsSinceLastLog = -1;
                }
            }

            if ((maxGapSizeInterpolation > -1) && (numberOfPointsToProcess > (maxGapSizeInterpolation+1))) {
                //The gap in data is too large to interpolate over, so we reset the filters (maxGapSizeInterpolation of -1 means the no gap is too large)
                for (int i=0; i < channelFilters.length(); i++) {
                    channelFilters[i]->resetHistory();
                }

                for (int i=0; i<spikeDetectors.length();i++){
                    spikeDetectors[i]->clearHistory();
                }
                numberOfPointsToProcess = 1;
                pointsSinceLastLog = -1;
            }
            /*
            if (numberOfPointsToProcess == 1) {
                //No missing data
                if (((pointsSinceLastLog+1)%decimation) == 0) {

                    //Write timestamp to file
                    //*timeStreamPtrs.at(0) << currentTimeStamp;
                }
            } else {
                //Some missing data, which we will interpolate over
                for (int p=1; p<numberOfPointsToProcess+1; p++) {
                    if (((pointsSinceLastLog+p)%decimation) == 0) {
                        //Write timestamp to file
                        //*timeStreamPtrs.at(0) << lastTimeStamp+p;
                    }
                }
            }*/
            for (int chInd=0; chInd < channelPacketLocations.length(); chInd++) {
                bufferPtr = buffer.data()+channelPacketLocations[chInd];
                tempDataPtr = (int16_t*)(bufferPtr);
                tempDataPoint = *tempDataPtr;

                if (tempDataPoint == -32768) {
                    //This is a NAN, replace with 0
                    tempDataPoint = 0;
                } else if (useRefs && refOn[chInd]) {
                    //Subtract the digital reference
                    bufferPtr = buffer.data() + refPacketLocations[chInd];
                    tempDataPtr = (int16_t*)(bufferPtr);
                    tempRefPoint = *tempDataPtr;
                    tempDataPoint -= tempRefPoint;
                }

                if (invertSpikes) {
                    //Extracellular recordings, so we reverse polarity to make spikes go upward
                    tempDataPoint *= -1;

                }



                if (numberOfPointsToProcess == 1) {
                    //No missing data
                    if (filterOn[chInd]) {
                        tempFilteredDataPoint = channelFilters.at(chInd)->addValue(tempDataPoint);
                    }

                    if (((pointsSinceLastLog+1)%decimation) == 0) {
                        if (filterOn[chInd]) {
                            //Add filtered point to spike detector
                            //qDebug() << "Writing one point for nTrode" << nTrodeForChannels[chInd] << " channel " << channelInNTrode[chInd];


                            spikeDetectors[nTrodeIndForChannels[chInd]]->newChannelData(channelInNTrode[chInd],tempFilteredDataPoint,currentTimeStamp);

                        } else {
                            spikeDetectors[nTrodeForChannels[chInd]]->newChannelData(channelInNTrode[chInd],tempDataPoint,currentTimeStamp);

                        }

                    }

                } else {
                    //We have missing data, so we need to interpolate.  TODO:  something here causing a crash condition

                    int16_t interpolatedDataPtInt;
                    double interpolatedDataPtFloat;
                    for (int mp = 1; mp < numberOfPointsToProcess+1; mp++) {
                        interpolatedDataPtFloat = (double)dataLastTimePoint[chInd] + ((double)(mp)/numberOfPointsToProcess)*(tempDataPoint-dataLastTimePoint[chInd]);
                        interpolatedDataPtInt = round(interpolatedDataPtFloat);
                        if (filterOn[chInd]) {
                            interpolatedDataPtInt = channelFilters.at(chInd)->addValue(interpolatedDataPtInt);
                        }

                        if (((pointsSinceLastLog+mp)%decimation) == 0) {

                            spikeDetectors[nTrodeForChannels[chInd]]->newChannelData(channelInNTrode[chInd],interpolatedDataPtInt,lastTimeStamp+mp);

                        }

                    }

                }

                dataLastTimePoint[chInd] = tempDataPoint; //remember the last unfiltered data point
            }

            //Print the progress to stdout
            printProgress();

            lastTimeStamp = currentTimeStamp;
            pointsSinceLastLog = (pointsSinceLastLog+numberOfPointsToProcess)%decimation;

            pointsSinceLastSpikeCheck = pointsSinceLastSpikeCheck+numberOfPointsToProcess;
            if (pointsSinceLastSpikeCheck > spikeCheckInterval) {
                for (int nTrodeInd = 0;nTrodeInd < spikeDetectors.length(); nTrodeInd++) {
                    spikeDetectors[nTrodeInd]->processNewData();
                }
                pointsSinceLastSpikeCheck = 0;
            }


        }
        filePtr->close();
        printf("\rDone\n");
        inputFileInd++;
    }



    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }



    return 0; //success
}

void DATExportHandler::writeSpikeData(int nTrodeNum, const QVector<int2d> *waveForm, const int *, uint32_t time){
    //A spike event was detected.
    //Write the spike waveforms plus the timestamps to the correct file


    //First, write the timestamp
    *neuroStreamPtrs[nTrodeNum] << time;

    //Then write the 40 waveform points for channel 0, then 40 points for channel 1, etc.
    for (int chInd=0; chInd < nTrodeSettings[nTrodeNum].numChannels; chInd++) {
        for (int sampleIndex = 0; sampleIndex < 40; sampleIndex++) {
            *neuroStreamPtrs[nTrodeNum] << waveForm[chInd][sampleIndex].y;
        }
    }

    /*
    for (int sampleIndex = 0; sampleIndex < 40; sampleIndex++) {
        for (int chInd=0; chInd < nTrodeSettings[nTrodeNum].numChannels; chInd++) {
            *neuroStreamPtrs[nTrodeNum] << waveForm[chInd][sampleIndex].y;
        }
    }*/

}
