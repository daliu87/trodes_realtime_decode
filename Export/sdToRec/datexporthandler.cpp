#include "datexporthandler.h"
#include <algorithm>
#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    setDefaultMenuEnabled(false); //the default options are turned off
    requireRecFile = false;
    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {


    printf("\nUsed to create a Rec file from SD file recording.  Output file is saved is same directory by default.\n");
    printf("Usage:  sdtorec -sd SDINPUTFILENAME -output OUTPUTFILENAME -numchan NUMBEROFCHANNELS -mergeconf FILEWORKSPACE\n\n");

    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    int optionInd = 1;

    QString sd_string = "";
    QString merge_workspace_string = "";
    QString numchan_string = "";


    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        }  else if ((argumentList.at(optionInd).compare("-mergeconf",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            merge_workspace_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-numchan",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            numchan_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-sd",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            sd_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }


    if (merge_workspace_string.isEmpty()) {
        qDebug() << "No merge workspace filename given.  Must include -mergeconf <filename> in arguments.";
        argumentReadOk = false;
        return;
    } else {
        //Make sure the first file exists
        mergedConfFileName = merge_workspace_string;

        QFileInfo fi(mergedConfFileName);

        if (!fi.exists()) {
            qDebug() << "File could not be found: " << mergedConfFileName;
            argumentReadOk = false;
        }

    }

    if (sd_string.isEmpty()) {
        qDebug() << "Error. Must include -sd SDINPUTFILENAME in arguments.";
        argumentReadOk = false;
        return;
    }  else {
        //Make sure the first file exists
        sdFileName = sd_string;

        QFileInfo fi(sdFileName);

        if (!fi.exists()) {
            qDebug() << "File could not be found: " << sdFileName;
            argumentReadOk = false;
        }

    }

    if (numchan_string.isEmpty()) {
        qDebug() << "Error. Must include -numchan NUMBEROFCHANNELS in arguments.";
        argumentReadOk = false;
        return;
    } else {
        bool ok;

        SDRecNumChan = numchan_string.toInt(&ok);
        if (!ok) {
            qDebug() << "Error. Must include -numchan NUMBEROFCHANNELS in arguments. NUMBEROFCHANNELS must be an integer multiple of 32.";
            argumentReadOk = false;
            return;
        }
        /*
        Below is the packet format for SD card recording.  Right now, we are hardcoding this format, but we may eventually want to use a config file.
        The total packet size is 14 bytes + 2*SDRecNumChan


        x55 (Sync byte)
        x68 (Data format byte - Note: MCU will replace this with Digital inputs)
        1 byte with valid flags (bit3 = acc_valid, bit2 = gyro_valid, bit1 = mag_valid, bit0 = rf_valid) (Note: at most one bit set)
        x00 (filler to make the packet byte count even)
        i2c_data_x[7:0]   (or rf_timestamp[7:0] if rf_valid)
        i2c_data_x[15:8]  (or rf_timestamp[15:8] if rf_valid)
        i2c_data_y[7:0]   (or rf_timestamp[23:16] if rf_valid)
        i2c_data_y[15:8]  (or rf_timestamp[31:24] if rf_valid)
        i2c_data_z[7:0]   (or x00 if rf_valid)
        i2c_data_z[15:8]  (or x00 if rf_valid)
        timestamp[7:0]
        timestamp[15:8]
        timestamp[23:16]
        timestamp[31:24]
        for (ch = 0, ch < 32, ch++) {
          sample0[7:0]
          sample0[15:8]
          sample1[7:0]
          sample1[15:8]
          sample2[7:0]
          sample2[15:8]
          sample3[7:0]
          sample3[15:8]
        }

        Note: i2c_data[x,y,z] = 0 if no valid flag set.
        */

        sdFilePacketSize = 2*SDRecNumChan + 14;
        sdFilePacketStartDataLoc = 14;
        sdFilePacketFlagByteLoc = 2;
        sdFilePacketSyncByteLoc = 4;
        sdFilePacketTimeByteLoc = 10;

    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {
    qDebug() << "\nCreating rec file from SD file...";



    if (!openSDInputFile()) {
        return -1;
    }




    QFileInfo fi(sdFileName);
    QString fileBaseName;

    if (outputFileName.isEmpty()) {
        fileBaseName = fi.baseName()+"_fromSD";
    } else {
        fileBaseName = outputFileName;
    }

    QString saveLocation;
    if (outputDirectory.isEmpty()) {
        saveLocation = fi.absolutePath()+QString(QDir::separator());
    } else {
        saveLocation = outputDirectory;
    }



    QList<QFile*> neuroFilePtrs;



    //Create an output file for the merged data
    //*****************************************

    neuroFilePtrs.push_back(new QFile);
    neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".rec"));
    if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
    neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    QFile finalConfFile;



    finalConfFile.setFileName(mergedConfFileName);
    if (!finalConfFile.open(QIODevice::ReadOnly)) {
        return -1;
    }

    QString configLine;
    bool foundEndOfConfig = false;
    int endPos = 0;
    int lastFCPos = 0;

    while (!finalConfFile.atEnd()) {
        configLine += finalConfFile.readLine();


        //neuroFilePtrs.last()->write()  write(configLine);
        //configContent += configLine;
        if (configLine.indexOf("</Configuration>") > -1) {
            foundEndOfConfig = true;
            endPos = finalConfFile.pos();
            //endPos = lastFCPos + configLine.indexOf("</Configuration>") + 16;
            break;
        } else {
            lastFCPos = finalConfFile.pos();
        }
        configLine = "";
    }

    if (!foundEndOfConfig) {
        qDebug() << "Error in reading mergeconf file.";
        return -1;
    }
    finalConfFile.seek(0);
    neuroFilePtrs.last()->write(finalConfFile.read(endPos+1));


    //*neuroStreamPtrs.last() << configLine;

    finalConfFile.close();

    neuroFilePtrs.last()->flush();
    //neuroFilePtrs.last()->close();


    //************************************************

    int inputFileInd = 0;

    progressMarker = sdFilePtr->size()/10;
    currentProgressMarker = 0;
    lastProgressMod = 100000;
    if (progressMarker == 0) {
        progressMarker = 1;
    }


        bool firstSyncMatchFound = false; //We don't start saving data until we find the first sync stamp in the rec file matching the first sync stamp in the SD file
        char *sdBufferPtr;
        uint32_t *sdTimePtr;
        int syncOffsetTime;
        int syncOffsetPackets;
        int currentSyncInd = 0; //used to keep track of which sync stamp we are on
        uint32_t packetsSinceLastRecSync = 0;

        //uint32_t currentSDSyncStamp = sdSyncValues[0];

        int packLengthAdjustmentofSD = 0;

        currentRecPacket = 0;
        currentSDPacket = 0;

        int numSyncsFound = 0;



        //Process the data and stream results to output files
        while(!sdFilePtr->atEnd()) {


            //readNextRecPacket();
            readNextSDPacket();

            //neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data(),10); //sync byte, extra aux byte, sensor data (8 bytes)


            neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data(),1); //sync byte
            neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+3,1); //aux byte moved from 4th byte to second
            neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+2,8); //sensor data (with repeat aux byte)


            *neuroStreamPtrs.at(0) << currentSDTimeStamp; //Write the timestamp from the trodes file
            //Write the neural data from the sd card
            neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+sdFilePacketStartDataLoc,(SDRecNumChan*2));


            if ((sdFilePtr->pos()%progressMarker) < lastProgressMod) {
                if (currentProgressMarker > 0) {
                    qDebug() << currentProgressMarker << "%";
                }
                currentProgressMarker = 100.0*((double)sdFilePtr->pos()/(double)sdFilePtr->size());
                //currentProgressMarker+=10;
            }
            lastProgressMod = (sdFilePtr->pos()%progressMarker);
            //printProgress();
            //pointsSinceLastLog = (pointsSinceLastLog+1)%decimation;







            currentSDPacket++;
            lastTimeStamp = currentTimeStamp;
            lastSDTimeStamp = currentSDTimeStamp;




        }

        printf("\rDone\n");
        sdFilePtr->close();
        inputFileInd++;


    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }



    return 0;
}

bool DATExportHandler::readNextRecPacket() {
    //Read in a packet of data to make sure everything looks good
    if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
        //We have reached the end of the rec file
        return false;
    }

    //Find the time stamp from rec file
    bufferPtr = buffer.data()+packetTimeLocation;
    tPtr = (uint32_t *)(bufferPtr);
    currentTimeStamp = *tPtr+startOffsetTime;

    bufferPtr = buffer.data() + trodesRFPacketByte;
    tPtr = (uint32_t *)(bufferPtr);
    currentRecSyncStamp = *tPtr;

}

bool DATExportHandler::readNextSDPacket() {

    if (!(sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketSize) == sdFilePacketSize)) {
        //We have reached the end of the sd file
        return false;
    }


    //Find the time stamp from sd file
    char *sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc;
    uint32_t *sdTimePtr = (uint32_t *)(sdBufferPtr);
    currentSDTimeStamp = *sdTimePtr;

}

bool DATExportHandler::writeMergedPacket() {

    neuroStreamPtrs.at(0)->writeRawData(buffer.data(),1); //We always have the the sync byte

    //Write data from approved devices
    for (int i=0; i < deviceListToUse.length(); i++) {
        neuroStreamPtrs.at(0)->writeRawData(buffer.data()+hardwareConf->devices[deviceListToUse[i]].byteOffset,hardwareConf->devices[deviceListToUse[i]].numBytes);

    }

    neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+2,8); //Sensor data


    *neuroStreamPtrs.at(0) << currentTimeStamp; //Write the timestamp from the trodes file


    //Write the neural data from the sd card
    neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+sdFilePacketStartDataLoc,(SDRecNumChan*2));




    return true;
}


bool DATExportHandler::openSDInputFile() {
    sdFilePtr = new QFile;
    sdFilePtr->setFileName(sdFileName);

    //open the raw data file
    if (!sdFilePtr->open(QIODevice::ReadOnly)) {
        delete sdFilePtr;
        qDebug() << "Error: could not open sd recording file for reading.";
        return false;
    }

    sdFilePacketBuffer.resize(sdFilePacketSize*2); //we make the input buffer the size of two packets
    //dataLastTimePoint.resize(channelPacketLocations.size()); //Stores the last data point.
    //dataLastTimePoint.fill(0);
    //pointsSinceLastLog=-1;

    //----------------------------------

    //skip past the config header
    /*
    if (!filePtr->seek(dataStartLocBytes)) {
        delete filePtr;
        qDebug() << "Error seeking in file";
        return false;
    }*/


    char *sdBufferPtr;
    uint32_t *sdTimePtr;


    //Read in a packet of data to make sure everything looks good
    if (!(sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketSize) == sdFilePacketSize)) {
        delete sdFilePtr;
        qDebug() << "Error: could not read from SD card recording file";
        return false;
    }

    sdFilePtr->seek(0);

    /*
    qDebug() << "Finding sync packets in SD file...";

    //Seek back to begining of data
    sdFilePtr->seek(0);

    uint32_t currentSDPacket = 0;
    //Find all of the sync stamps


    uint32_t ltime=0;
    uint32_t thistime=0;

    while (!sdFilePtr->atEnd()) {
        //Seek to flag location of data packet
        if (!sdFilePtr->seek((currentSDPacket*sdFilePacketSize))) {
            break;
        }
        if (sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketStartDataLoc) == sdFilePacketStartDataLoc) {

            //Report dropped packets

            sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc);
            thistime = *sdTimePtr;
            if (thistime < ltime) {
                qDebug() << "-" << ltime-thistime << "gap in data. Last time" << ltime << "Current time" << thistime << "Packet" << currentSDPacket;
            }
            else if (thistime-ltime > 1) {
                qDebug() << thistime-ltime << "gap in data at time. Last time" << ltime << "Current time" << thistime << "Packet" << currentSDPacket;
            }
            ltime = thistime;


            sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketFlagByteLoc;

            if (*sdFilePacketBuffer.data() != 0x55) {
                qDebug() << "Bad SD file packet found.";
            }
            if (*sdBufferPtr == 1) {
                //The 0th bit is set in the flag byte, we have a sync value




                sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketSyncByteLoc);
                uint32_t sVal = *sdTimePtr;
                sVal = sVal & 0xFFFFFF;
                //qDebug() << "SD sync: " << sVal;

                sdSyncValues.push_back(sVal);


                if (sdSyncValues.length() > 1) {
                    if (!(sdSyncValues.last() > sdSyncValues[sdSyncValues.length()-2])) {
                        qDebug() << "Error: sync values in SD file are not increasing.";
                        return false;
                    }
                }

                sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc);
                sdTimeDuringSync.push_back(*sdTimePtr);
                sdPacketDuringSync.push_back(currentSDPacket);
            }

        }
        currentSDPacket++;
        //sdFilePtr->

    }
    qDebug() << sdSyncValues.length() << "sync values found on SD file.";
    //qDebug() << sdPacketDuringSync;


    currentSDTimeStamp = 0;
    lastSDTimeStamp = 0;

    //Find first time stamp
    //sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc;
    //sdTimePtr = (uint32_t *)(sdBufferPtr);
    //currentSDTimeStamp = *sdTimePtr;
    //lastSDTimeStamp = currentSDTimeStamp-1;

    //Seek back to begining of data
    sdFilePtr->seek(0);

    */


    return true;

}

