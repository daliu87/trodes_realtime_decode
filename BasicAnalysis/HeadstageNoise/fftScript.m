directory = '/opt/data36/daliu/DL20/stimcalib/';
%filename = '20160620_noisetest_pearstone.rec';
filename = '20160620_noisetest_optical_run.rec';

%directory = '/opt/data36/daliu/trodes/test_20150717/';
%filename = '20150709_DL03_ephys2_newhs.rec';

%directory = '/opt/data36/daliu/trodes/DL03/';
%filename = '20150528.DL03.test1.rec';

%directory = '/opt/data36/daliu/trodes/DL03/';
%filename = '20150513.DL03.test3.rec';

%directory = '/opt/data36/daliu/trodes/test_20150305/'
%filename = '20150305.MS11.test.rec'

%directory = '/opt/data36/daliu/trodes/test_20150302/'
%filename = '20150302.KK68.test2.floating.rec'

channel = [1,1;2,1;5,1;8,1];

import_trodes = readTrodesFileContinuous(strcat(directory, filename),channel,0);

data = import_trodes.channelData;

timestamps = import_trodes.timestamps;

%% fft

% Set time range to calculate fft on

Fs = 30000;
T = 1/Fs;

offset_sec = 5;
offset = offset_sec * Fs;
sec = 50;


%plot(timestamps(1:10000),data(1:10000))
figure

for ii=1:size(data,2)
    

L = 30000*sec;
t = (0:L-1)*T;
NFFT = 2^nextpow2(L);
y = data(1+offset:L+offset, ii);
Y = fft(y,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);

% figure
% plot(timestamps(1+offset:offset+L),y);
% title(sprintf('Raw data %s, channel %d, for [%0.1f, %0.1f] seconds',filename, channel, offset_sec, offset_sec + sec));
% xlabel('time (s)')
% ylabel('uV')

subplot(2,2,ii)
semilogx(f,2*abs(Y(1:NFFT/2+1)));
ylim([0,40])
%title(sprintf('FFT of %s, channel %d, for [%0.1f, %0.1f] seconds',filename, channel, offset_sec, offset_sec + sec))
xlim([1,Fs/2])
%xlabel('Freq (Hz)')
end