

anim = 'REc';

directory = sprintf('/opt/databackup/shantanu/RippleInterruption/%s_direct/', anim);
figure(1)
clf;

day = 2;
epoch = 2;

multi = load(strcat(directory, sprintf('%smulti%02d.mat',anim,day)));
DIO = load(strcat(directory, sprintf('%sDIO%02d.mat',anim,day)));
stimtimes = DIO.DIO{day}{epoch}{16}.pulsetimes;

multi_tet = multi.multi{day}{epoch};


pos = load(strcat(directory, sprintf('%spos%02d.mat',anim,day)));
posdata = pos.pos{day}{epoch}.data;

tetinfo = load(strcat(directory, sprintf('%stetinfo.mat',anim)));

tetnum = find(cellfun(@(x)(is_riptet(x)),tetinfo.tetinfo{day}{epoch}));

ha = tight_subplot(length(tetnum)+1,1,[.005 .03],[.03 .01],[.03 .01]);

ripplefiltstruct = load('ripplefilter.mat');
rip_kernel = ripplefiltstruct.ripplefilter.kernel;

% old 100-400Hz filter
rip1_a = [1, -7.45735663369528, 28.7250866952453, -76.6576905574011, ...
    159.121219449125, -271.335670775639, 391.351587611873, -485.982406239585, ...
    525.205137459352, -496.514905259400, 411.014947844139, -297.145795944811, ...
    186.329835613820, -100.094136315397, 45.1637435607148, -16.5828393019837, ...
    4.68977600702455, -0.917188209533659, 0.0946362166342639];

rip1_b = [0.0232855416422778, -0.116923985094050, 0.267272526953982, -0.433074915275246, ...
    0.637527665723657, -0.793642910169906, 0.752702952156722, -0.600794056710678, ...
    0.383928961384643, 4.38452370247235e-15, -0.383928961384650, 0.600794056710682, ...
    -0.752702952156724, 0.793642910169908, -0.637527665723659, 0.433074915275249, ...
    -0.267272526953984, 0.116923985094051, -0.0232855416422780];

for ii=1:length(tetnum)
    axes(ha(ii));
    eeg = load(strcat(directory, sprintf('EEG/%seeg%02d-%d-%02d.mat',anim,day,epoch,tetnum(ii))));
    eegdata = eeg.eeg{day}{epoch}{tetnum(ii)}.data;
    eegripfilt = filter(rip_kernel, 1, eegdata);
    eegripenv = abs(hilbert(eegripfilt));
    eegrealtimeripfilt = filter(rip1_b, rip1_a, eegdata);

    eegstarttime = eeg.eeg{day}{epoch}{tetnum(ii)}.starttime;
    eegsamprate = eeg.eeg{day}{epoch}{tetnum(ii)}.samprate;
    eegtime = eegstarttime:1/eegsamprate:eegstarttime+(length(eegdata)-1)/eegsamprate;
    %plot(eegtime,eegdata,'b');
    %plot(eegtime(1:end-157),eegripfilt(158:end)*5,'r');
    %plot(eegtime(1:end-157),eegripenv(158:end),'r');
    plot(eegtime,eegrealtimeripfilt,'b');
    hold on;
    plot(stimtimes/10000, repmat([200],size(stimtimes)),'*r');
    hold off;
    ylim([-500,1000])
end
linkaxes(ha,'x');

pos_kernel = gaussian(1*30, ceil(4*1*30));


instvel = sqrt(diff(posdata(:,2)).^2 + diff(posdata(:,3)).^2)./diff(posdata(:,1));
axes(ha(end));
plot(posdata(1:end-1,1),smoothvect(instvel,pos_kernel),'r');
%plot(posdata(:,1),posdata(:,2),'r');
hold on;
%plot(posdata(:,1),posdata(:,3),'b');
hold off;
%%
days = [1,2,3,4];
epochs = [2,4];
plot_ii = 0;


figure;
clf;
ha = tight_subplot(length(days),length(epochs),[.04 .02],[.02 0.03],[.02 .01]);

for day = days
    for epoch = epochs
        plot_ii = plot_ii + 1;
        pos = load(strcat(directory, sprintf('%spos%02d.mat',anim,day)));
        posdata = pos.pos{day}{epoch}.data;
        
        multi = load(strcat(directory, sprintf('%smulti%02d.mat',anim,day)));
        DIO = load(strcat(directory, sprintf('%sDIO%02d.mat',anim,day)));
        stimtimes = DIO.DIO{day}{epoch}{15}.pulsetimes;
        
        tetinfo = load(strcat(directory, sprintf('%stetinfo.mat',anim)));        
        tetnum = find(cellfun(@(x)(is_riptet(x)),tetinfo.tetinfo{day}{epoch}));
        
        multi_tet = multi.multi{day}{epoch};
        
        stim_spktimes = [];
        win_range = [-300*10, 300*10];
        exclude_range = [0*10,10*10];
        for multi_ii = 1:length(tetnum)
            spktimes = multi_tet{tetnum(multi_ii)};
            for stim_ii = 1:size(stimtimes,1)
                stim_time = stimtimes(stim_ii,1);
                stim_spktimes = [stim_spktimes; spktimes(spktimes > stim_time + win_range(1) & spktimes < stim_time + exclude_range(1) | spktimes > stim_time + exclude_range(2) & spktimes < stim_time + win_range(2)) - stim_time];
            end
        end
        
        axes(ha(plot_ii))
        binsize = 2;
        [stim_mult_hist,stim_mult_hist_edges] = histcounts(stim_spktimes/10, [-300:binsize:300]);
        bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./size(stimtimes,1)/(binsize/1000),0.5,'FaceColor',[0.0,0.5,0.8],'EdgeColor',[0,0,0],'LineWidth',1);
        title(sprintf('%s: Day %d, Epoch %d, Multiunit stim trigger', anim, day, epoch));
        xlim([-300,300]);
    end
end

%%
anim_list = {'RCa','RCb','RCc','RCd'};
days = [1,2,3,4];
epochs = [2,4];

num_pulses_ctrl = zeros(length(anim_list) * length(days), length(epochs));
num_lockout_pulses_ctrl = zeros(length(anim_list) * length(days), length(epochs));
for anim_ii = 1:length(anim_list)
    anim = anim_list{anim_ii};
    %directory = sprintf('/mnt/hotswap/%s_direct/', anim);
    for day_ii = 1:length(days)
        day = days(day_ii);
        for epoch_ii = 1:length(epochs)
            epoch = epochs(epoch_ii);
            DIO = load(strcat(directory, sprintf('%sDIO%02d.mat',anim,day)));
            DIO_stim_times = DIO.DIO{day}{epoch}{16}.pulsetimes;
            num_pulses_ctrl((anim_ii-1)*length(days) + day_ii, epoch_ii) = size(DIO_stim_times,1);
            inter_stim_times = diff(DIO_stim_times(:,1))/10;
            num_lockout_pulses_ctrl((anim_ii-1)*length(days) + day_ii, epoch_ii) = length(find(inter_stim_times < 550));
        end
    end
end

%
anim_list = {'REc','REd', 'REe', 'REf', 'REg', 'REh'};

days = [1,2,3,4];
epochs = [2,4];

num_pulses_exp = zeros(length(anim_list) * length(days), length(epochs));
num_lockout_pulses_exp = zeros(length(anim_list) * length(days), length(epochs));

for anim_ii = 1:length(anim_list)
    anim = anim_list{anim_ii};
    directory = sprintf('/mnt/hotswap/%s_direct/', anim);
    for day_ii = 1:length(days)
        day = days(day_ii);
        for epoch_ii = 1:length(epochs)
            epoch = epochs(epoch_ii);
            DIO = load(strcat(directory, sprintf('%sDIO%02d.mat',anim,day)));
            DIO_stim_times = DIO.DIO{day}{epoch}{16}.pulsetimes;
            num_pulses_exp((anim_ii-1)*length(days) + day_ii, epoch_ii) = size(DIO_stim_times,1);
            inter_stim_times = diff(DIO_stim_times(:,1))/10;
            num_lockout_pulses_exp((anim_ii-1)*length(days) + day_ii, epoch_ii) = length(find(inter_stim_times < 540));
            
        end
    end
end