directory = '/opt/data36/daliu/DL10/wtrack/';
rec_filename = '20160327_7_sleep';
channel = [2,2;11,1;13,1;14,1;15,1;16,1;17,1;12,1];

% directory = '/mnt/hotswap/templeton/at_haight/';
% rec_filename = 'templeton_02-09-2016(6arm_r1)';
% channel = [2,2;11,1;13,1;14,1;15,1;16,1;17,1;12,4];



%directory = '/opt/data36/daliu/trodes/templeton/';
%rec_filename = 'NNF/20151208_NNF_r1';
%channel = [1,1;2,1;3,1;8,1;15,1;16,1;12,4];
%channel = [4,1;9,1;10,1;11,1;13,1;14,1;12,4];
%channel = [1,1;2,1;3,1;4,1;8,1;9,1;10,1;11,1;13,1;14,1;15,1;16,1;12,4];

tic;
import_trodes = readTrodesFileContinuous(strcat(directory, rec_filename, '.rec'),channel,0);
toc;

% calculate reference channels
cont_data_ref = (import_trodes.channelData(:,1:end-1) - ...
    repmat(import_trodes.channelData(:,end),[1,size(import_trodes.channelData,2)-1]));
timestamps = import_trodes.timestamps;

import_pos = readTrodesExtractedDataFile(strcat(directory, rec_filename, '.videoPositionTracking'));
pos_time = import_pos(1).data;
pos_x = double(import_pos(2).data + import_pos(4).data)/2;
pos_y = double(import_pos(3).data + import_pos(5).data)/2;
pos_timestep = diff(pos_time);
inst_vel = sqrt(double(((diff(pos_x)* 0.194) .^2) + ((diff(pos_y) * 0.194) .^ 2)))./pos_timestep;


%%
% select subsection and downsample
data_time_range = [20,800];
data_range = data_time_range * 30000;
cont_data_ref_sub = cont_data_ref(data_range(1):data_range(2),:);
timestamps_sub = timestamps(data_range(1):data_range(2));

% spike filteringThe envelope was smoothed with a Gaussian (4-ms s.d.). We initially identified SWR

cutoff_low = 600;
cutoff_high = 6000;
filt_order = 4;

fs = import_trodes.samplingRate;
% design filter
[b_an,a_an] = besself(filt_order,[2*pi*cutoff_low,2*pi*cutoff_high]);
[b,a] = bilinear(b_an,a_an,fs);

spike_filter_data_all = filter(b,a,cont_data_ref_sub);

samples_per_ms = 30;
win_size = 4;           %ms
win_center = 1;         %ms
threshold = 60;         %uV`
refractory = 1*samples_per_ms;
samples_behind = win_center*samples_per_ms;
samples_ahead = (win_size-win_center)*samples_per_ms;

all_waves = {};
all_spktime_ind = {};
disp('starting spike filter and thresholding');
tic;
for ii = 1:size(spike_filter_data_all,2)
    spike_filter_data = spike_filter_data_all(:,ii);
    spk_thresh = find(spike_filter_data > threshold);
    spk_thresh_refractory_ind = [1 (find(diff(spk_thresh) > refractory) + 1)'];

    spktime_ind = spk_thresh(spk_thresh_refractory_ind);

    spk_ind = cell2mat(arrayfun(@(x)(x-samples_behind:x+samples_ahead-1),spktime_ind,'UniformOutput',false));

    spk_ind = spk_ind(find(~max(spk_ind <= 0 | spk_ind > size(spike_filter_data,1),[],2)),:);

    spk_trans = spike_filter_data';
    waves = spk_trans(spk_ind);
    
    all_spktime_ind{ii} = spktime_ind;
    all_waves{ii} = waves;
end
toc;


lfp_fs = 1500;

lfp_data_ref_sub = downsample(cont_data_ref_sub, 30000/lfp_fs);
lfp_timestamps_sub = downsample(timestamps_sub, 30000/lfp_fs);


vel_above = inst_vel > 4;
vel_above_start_ind = find(diff(vel_above) == 1);
vel_above_end_ind = find(diff(vel_above) == -1);
vel_above_range = [pos_time(vel_above_start_ind(1:min(length(vel_above_start_ind)))), pos_time(vel_above_end_ind(1:min(length(vel_above_start_ind)))+1)];
lfp_above_vel_mask = lfp_timestamps_sub < vel_above_range(lookup(lfp_timestamps_sub,vel_above_range(:,1),-1),2);


%% Load nspike data instead
% directory = '/opt/data36/other/mkarlsso/Bon/old_mattias/EEG/';
% tetnums = [3,11,12,13,14,29];
% rec_filename = 'boneeg04-2-%02d.mat';
% lfp_fs = 1500;
% 
% lfp_start_times = zeros([length(tetnums),1]);
% for ii = 1:length(tetnums)
%     tmp_eeg = load(sprintf(strcat(directory, rec_filename),tetnums(ii)));
%     lfp_start_times(ii) = tmp_eeg.eeg{4}{2}{tetnums(ii)}.starttime;
%     tmp_all_eeg{ii} = tmp_eeg.eeg{4}{2}{tetnums(ii)}.data;
% end
% 
% % Align the start times
% lfp_max_start_time = max(lfp_start_times);
% lfp_start_offset = round((lfp_max_start_time - lfp_start_times) .* lfp_fs);
% for ii = 1:length(tmp_all_eeg)
%     tmp_all_eeg{ii} = tmp_all_eeg{ii}(lfp_start_offset(ii) + 1:end);
% end
% 
% % cut off trailing end times
% lfp_min_length = min(cellfun(@length, tmp_all_eeg));
% lfp_timestamps = (lfp_max_start_time:1/lfp_fs:lfp_max_start_time + (lfp_min_length-1)/lfp_fs)';
% lfp_data_ref = zeros(lfp_min_length, length(tetnums));
% for ii = 1:length(tmp_all_eeg)
%     lfp_data_ref(:,ii) = tmp_all_eeg{ii}(1:lfp_min_length);
% end
% 
% data_time_range = [20, 800];
% data_range = data_time_range * lfp_fs;
% lfp_data_ref_sub = lfp_data_ref(data_range(1):data_range(2),:);
% lfp_timestamps_sub = lfp_timestamps(data_range(1):data_range(2));
% 

%%

disp('ripple filtering');
tic;
%ripple_cutoff_low = 150;
%ripple_cutoff_high = 250;
%ripple_filt_order = 4;
%[rip_b_an, rip_a_an] = besself(ripple_filt_order,[2*pi*ripple_cutoff_low,2*pi*ripple_cutoff_high]);
%[rip_b,rip_a] = bilinear(rip_b_an,rip_a_an,fs);

%[rip_b, rip_a] = butter(4,[ripple_cutoff_low, ripple_cutoff_high]/(lfp_fs/2));

ripplefiltstruct = load('ripplefilter.mat');
rip_kernel = ripplefiltstruct.ripplefilter.kernel;
%rip_b = ripplefiltstruct.ripplefilter.tf.num;
%rip_a = ripplefiltstruct.ripplefilter.tf.den;


lfp_rip_filt_sub = filtfilt(rip_kernel, 1, lfp_data_ref_sub);
lfp_rip_hilbert_sub = hilbert(lfp_rip_filt_sub);
lfp_rip_env_sub = abs(lfp_rip_hilbert_sub);

smoothing_width = 0.004;

kernel = gaussian(smoothing_width*lfp_fs, ceil(8*smoothing_width*lfp_fs));
% change to half gaussian to smooth forward in time
%kernel(1:length(kernel)/2) = zeros(length(kernel)/2,1);
lfp_rip_env_smooth_sub = zeros(size(lfp_rip_env_sub));
for ii = 1:size(lfp_rip_env_sub,2)
    lfp_rip_env_smooth_sub(:,ii) = smoothvect(lfp_rip_env_sub(:,ii), kernel);
end

smooth_baseline = mean(lfp_rip_env_smooth_sub);
smooth_std = std(lfp_rip_env_smooth_sub);
smooth_thresh = smooth_baseline + 3 .* smooth_std;


for ii = 1:size(lfp_rip_env_smooth_sub,2)
    tmprip = extractevents(lfp_rip_env_smooth_sub(:,ii), smooth_thresh(ii), smooth_baseline(ii), int32(100*1.5), int32(50*1.5), 1)';
    tmp_smooth_rip_event_time = lfp_timestamps_sub(tmprip(:,1:2));
    tmp_smooth_rip_event_vel_thresh_ind = vel_above_range(lookup(tmp_smooth_rip_event_time(:,1), vel_above_range(:,1),-1),2) < tmp_smooth_rip_event_time(:,2);

    smooth_rip_events{ii} = tmprip(tmp_smooth_rip_event_vel_thresh_ind,1:2);
end

toc;

%% Kenny method
lfp_rip_smooth_kenny_sub = zeros(size(lfp_rip_filt_sub));
for lfp_ii = 1:size(lfp_rip_filt_sub,2)
    lfp_rip_smooth_kenny_sub(:,lfp_ii) = smoothvect(abs(lfp_rip_filt_sub(:,lfp_ii)), kernel);
end
lfp_rip_env_kenny_sub = sqrt(sum(lfp_rip_smooth_kenny_sub .^ 2, 2));
kenny_baseline = mean(lfp_rip_env_kenny_sub);
kenny_std = std(lfp_rip_env_kenny_sub);
kenny_thresh = kenny_baseline + 2 .* kenny_std;
kenny_rip_events = extractevents(lfp_rip_env_kenny_sub, kenny_thresh, kenny_baseline, 0, int32(15*1.5), 1)';

kenny_rip_events_time = lfp_timestamps_sub(kenny_rip_events(:,1:2));
kenny_rip_events_vel_thresh_ind = vel_above_range(lookup(kenny_rip_events_time(:,1), vel_above_range(:,1),-1),2) < kenny_rip_events_time(:,2);

kenny_rip_events = kenny_rip_events(kenny_rip_events_vel_thresh_ind,1:2);

%%

% old 100-400Hz filter
rip1_a = [1, -7.45735663369528, 28.7250866952453, -76.6576905574011, ...
    159.121219449125, -271.335670775639, 391.351587611873, -485.982406239585, ...
    525.205137459352, -496.514905259400, 411.014947844139, -297.145795944811, ...
    186.329835613820, -100.094136315397, 45.1637435607148, -16.5828393019837, ...
    4.68977600702455, -0.917188209533659, 0.0946362166342639];

rip1_b = [0.0232855416422778, -0.116923985094050, 0.267272526953982, -0.433074915275246, ...
    0.637527665723657, -0.793642910169906, 0.752702952156722, -0.600794056710678, ...
    0.383928961384643, 4.38452370247235e-15, -0.383928961384650, 0.600794056710682, ...
    -0.752702952156724, 0.793642910169908, -0.637527665723659, 0.433074915275249, ...
    -0.267272526953984, 0.116923985094051, -0.0232855416422780];

% new 150-250Hz filter
rip2_a = [1, -11.7211644621401, 69.4141606894030, -272.943693781472, ...
    793.733182246242, -1805.56956364536, 3320.66911787227, -5039.18721590951, ...
    6388.83865252807, -6813.09822646561, 6124.33733155433, -4630.48270472608, ...
    2924.84894521595, -1524.33752473424, 642.249494056762, -211.659943388859, ...
    51.5861946277428, -8.34803786957350, 0.682686766261136];

rip2_b = [0.00129180641792292, -0.0129686462053354, 0.0649860663276546, -0.213040690450758, ...
    0.505568917616276, -0.907525263464183, 1.24408910068877, -1.26054939315621, ...
    0.806575646754607, 0, -0.806575646754607, 1.26054939315621, ... 
    -1.24408910068877, 0.907525263464183, -0.505568917616276, 0.213040690450758, ...
    -0.0649860663276546, 0.0129686462053354, -0.00129180641792292];

realtime1_filt_data_sub = filter(rip1_b, rip1_a, lfp_data_ref_sub);
%realtime1_filt_data_sub = filtfilt(rip1_b, rip1_a, lfp_data_ref_sub);
realtime2_filt_data_sub = filter(rip2_b, rip2_a, lfp_data_ref_sub);
%realtime2_filt_data_sub = filtfilt(rip2_b, rip2_a, lfp_data_ref_sub);


%%
tic;

[thresh1, realtime1_env, m, s, lockout] = realtimeRippleAlgorithm(lfp_timestamps_sub,realtime1_filt_data_sub,3,lfp_above_vel_mask,20);
lockout_start_ind = find(diff(lockout(:,1)) == 1);
lockout_end_ind = find(diff(lockout(:,1)) == -1);
lockout_range_min = min(length(lockout_start_ind),length(lockout_end_ind));
lockout_range_ind = [lockout_start_ind(1:lockout_range_min), lockout_end_ind(1:lockout_range_min)];

[thresh2, realtime2_env, m, s, lockout2] = realtimeRippleAlgorithm(lfp_timestamps_sub,realtime2_filt_data_sub,3,lfp_above_vel_mask,20);
lockout2_start_ind = find(diff(lockout2(:,1)) == 1);
lockout2_end_ind = find(diff(lockout2(:,1)) == -1);
lockout2_range_min = min(length(lockout2_start_ind),length(lockout2_end_ind));
lockout2_range_ind = [lockout2_start_ind(1:lockout2_range_min), lockout2_end_ind(1:lockout2_range_min)];

toc;

%%
[new_thresh, new_realtime1_env, new_m, new_s, new_lockout] = newRealtimeRippleAlgorithm(lfp_timestamps_sub,realtime1_filt_data_sub,4.5,lfp_above_vel_mask);
new_lockout_start_ind = find(diff(new_lockout(:,1)) == 1);
new_lockout_end_ind = find(diff(new_lockout(:,1)) == -1);
new_lockout_range_min = min(length(new_lockout_start_ind),length(new_lockout_end_ind));
new_lockout_range_ind = [new_lockout_start_ind(1:new_lockout_range_min), new_lockout_end_ind(1:new_lockout_range_min)];


%%
figure(1);
clf;
ha = tight_subplot(size(lfp_data_ref_sub,2)+1,1,[.005 .03],[.03 .01],[.03 .01]);

for chan_ii = 1:size(lfp_data_ref_sub,2)
    axes(ha(chan_ii))
    plot(lfp_timestamps_sub, realtime1_filt_data_sub(:,chan_ii), 'Color', [0.8,0.8,0.8]);
    hold on;
    plot(lfp_timestamps_sub, realtime1_env(:,chan_ii), 'Color', [0.1,0.1,1], 'LineWidth', 1);
    plot(lfp_timestamps_sub, realtime2_env(:,chan_ii), 'Color', [0.5,0.5,1], 'LineWidth', 1);
%     plot(lfp_timestamps_sub, realtime2_env(:,chan_ii), '.', 'Color', [0.5,0.5,1], 'LineWidth', 2);
    %plot(lfp_timestamps_sub, lfp_rip_env_smooth_sub(:,chan_ii), 'Color', [1,0.1,0.1], 'LineWidth', 2);
    plot(lfp_timestamps_sub, lfp_rip_env_kenny_sub, 'Color', [0.1,1,0.1], 'LineWidth', 2);
    %plot(lfp_timestamps_sub, m(:,chan_ii), 'Color', [0.4,1,0.4], 'LineWidth', 2);
    %plot(lfp_timestamps_sub, s(:,chan_ii), 'Color', [.8,.8,0.1], 'LineWidth', 2);
    %plot(lfp_timestamps_sub(lockout(:,chan_ii) > 0), repmat([200],[length(find(lockout(:,chan_ii) > 0)),1]), 'b.');
    %plot(lfp_timestamps_sub(lockout2(:,chan_ii) > 0), repmat([210],[length(find(lockout2(:,chan_ii) > 0)),1]), '.', 'Color', [0.5,0.5,1]);

    %plot(lfp_timestamps_sub(new_lockout(:) > 0), repmat([220],[length(find(new_lockout(:) > 0)),1]), 'r.');

    %plot(lfp_timestamps_sub(smooth_rip_events{chan_ii})',repmat([190],size(smooth_rip_events{chan_ii}))','r-', 'LineWidth',3);
    plot(lfp_timestamps_sub(kenny_rip_events)',repmat([180],size(kenny_rip_events))','-', 'Color', [0,0,0], 'LineWidth',3);
    
    plot(timestamps_sub(all_spktime_ind{chan_ii}), repmat([150],size(all_spktime_ind{chan_ii})), 'g.');
    
    
    
    ylim([-100,250]);
    hold off;
end
axes(ha(end))
plot(pos_time(1:end-1), inst_vel);
linkaxes(ha);


%%
figure(2);
clf;
trace_range = [-200,400];
realtime_detect_delay = [];
for chan_ii = 1:size(lfp_data_ref_sub,2)
    rip_trace_time = trace_range(1):1/1.5:trace_range(2);
    realtime_rip_start = find(diff(lockout(:,chan_ii))>0);
    realtime_rip_trace = cell2mat(arrayfun(@(x)(lfp_rip_env_smooth_sub(int32(x+trace_range(1)*1.5):int32(x+trace_range(2)*1.5),1)),realtime_rip_start(2:end-2)','UniformOutput',false));
    smooth_rip_start = smooth_rip_events{chan_ii}(:,1);
    smooth_rip_trace = cell2mat(arrayfun(@(x)(lfp_rip_env_smooth_sub(int32(x+trace_range(1)*1.5):int32(x+trace_range(2)*1.5),1)),smooth_rip_start(2:end-3)','UniformOutput',false));

    subplot(size(lfp_data_ref_sub,2)+1,2,chan_ii*2-1);
    plot(rip_trace_time, mean(realtime_rip_trace,2), 'Color', [0.0,0.5,0.8])
    tmp_realtime_detect_delay = realtime_rip_start - smooth_rip_start(lookup(realtime_rip_start, smooth_rip_start, -1));
    realtime_detect_delay = [realtime_detect_delay; tmp_realtime_detect_delay];
    title(sprintf('tet %d: envelop triggered by fsdata reference (delay: %0.2f ms)', chan_ii, mean(realtime_detect_delay(realtime_detect_delay < 200*1.5 & realtime_detect_delay > 0))/1.5));
    grid on;
    
    subplot(size(lfp_data_ref_sub,2)+1,2,chan_ii*2);
    plot(rip_trace_time, mean(smooth_rip_trace,2), 'r')
    title(sprintf('tet %d: envelop triggered by offline 3std crossing', chan_ii));
    grid on;

end

kenny_rip_trace = cell2mat(arrayfun(@(x)(lfp_rip_env_smooth_sub(int32(x+trace_range(1)*1.5):int32(x+trace_range(2)*1.5),1)),kenny_rip_events(2:end-2,1)','UniformOutput',false));
subplot(size(lfp_data_ref_sub,2)+1,2,chan_ii*2+1);
plot(rip_trace_time, mean(kenny_rip_trace,2), 'Color', [0.0,0.5,0.8])
grid on;
kenny_detect_delay = smooth_rip_start(lookup(kenny_rip_events(:,1),smooth_rip_start,1)) - kenny_rip_events(:,1);
title(sprintf('envelop triggered by kenny 2std crossing (delay: %0.2f)',mean(kenny_detect_delay(kenny_detect_delay<200*1.5 & kenny_detect_delay > 0)/1.5)));
mean_delay_all = mean(realtime_detect_delay(realtime_detect_delay < 200*1.5 & realtime_detect_delay > 0))/1.5

%%
win_min = -500*30;
win_max = 500*30;
all_realtime_rip_spktime_ind = {};
all_realtime2_rip_spktime_ind = {};
all_smooth_rip_spktime_ind = {};

realtime_rip_start = {};
smooth_rip_start = {};
for ii=1:size(all_spktime_ind,2)
    disp(['starting stim window multiunit calculation for ', ii]);
    tic;
    %spktime_ind = all_spktime_ind{ii}(max(all_waves{ii},[],2) < 600);
    spktime_ind = all_spktime_ind{ii};

    realtime_rip_start{ii} = find(diff(lockout(:,ii))>0);
    smooth_rip_start{ii} = smooth_rip_events{ii}(:,1);

    tmp_realtime_rip_spktime_ind = [];
    for stim_jj = 1:length(realtime_rip_start{ii})
        tmp_realtime_rip_spktime_ind  = [ tmp_realtime_rip_spktime_ind ; (spktime_ind((spktime_ind > realtime_rip_start{ii}(stim_jj)*20 + win_min | ...
            spktime_ind < realtime_rip_start{ii}(stim_jj)*20 + win_max)) - realtime_rip_start{ii}(stim_jj)*20) ];
    end
    all_realtime_rip_spktime_ind{ii} = tmp_realtime_rip_spktime_ind;
    
    realtime2_rip_start{ii} = find(diff(lockout2(:,ii))>0);
    
    tmp_realtime2_rip_spktime_ind = [];
    for stim_jj = 1:length(realtime2_rip_start{ii})
        tmp_realtime2_rip_spktime_ind  = [ tmp_realtime2_rip_spktime_ind ; (spktime_ind((spktime_ind > realtime2_rip_start{ii}(stim_jj)*20 + win_min | ...
            spktime_ind < realtime2_rip_start{ii}(stim_jj)*20 + win_max)) - realtime2_rip_start{ii}(stim_jj)*20) ];
    end
    all_realtime2_rip_spktime_ind{ii} = tmp_realtime2_rip_spktime_ind;
    
    
    tmp_smooth_rip_spktime_ind = [];
    for stim_jj = 1:length(smooth_rip_start{ii})
         tmp_smooth_rip_spktime_ind = [ tmp_smooth_rip_spktime_ind; (spktime_ind((spktime_ind > smooth_rip_start{ii}(stim_jj)*20 + win_min | ...
            spktime_ind < smooth_rip_start{ii}(stim_jj)*20 + win_max)) - smooth_rip_start{ii}(stim_jj)*20) ];
    end
    all_smooth_rip_spktime_ind{ii} = tmp_smooth_rip_spktime_ind;
    toc;
end

kenny_rip_start = kenny_rip_events(:,1);
all_kenny_rip_spktime_ind = [];
for stim_jj = 1:length(kenny_rip_start)
    all_kenny_rip_spktime_ind = [ all_kenny_rip_spktime_ind; (spktime_ind((spktime_ind > kenny_rip_start(stim_jj)*20 + win_min | ...
        spktime_ind < kenny_rip_start(stim_jj)*20 + win_max)) - kenny_rip_start(stim_jj)*20) ];
end

all_new_realtime_rip_spktime_ind = [];
for stim_jj = 1:length(new_lockout_start_ind)
    all_new_realtime_rip_spktime_ind = [ all_new_realtime_rip_spktime_ind; (spktime_ind((spktime_ind > new_lockout_start_ind(stim_jj)*20 + win_min | ...
        spktime_ind < new_lockout_start_ind(stim_jj)*20 + win_max)) - new_lockout_start_ind(stim_jj)*20) ];
end

%%
figure(3)
for plot_ii = 1:length(all_realtime_rip_spktime_ind)
    subplot(length(all_realtime_rip_spktime_ind),2,plot_ii*2-1)
    hist_binsize = 2;
    hist_range = [-150, 250];
    %histogram(all_stim_spktime_ind/30, [-500:2:500]);
    [stim_mult_hist,stim_mult_hist_edges] = histcounts(all_realtime_rip_spktime_ind{plot_ii}/30, [hist_range(1):hist_binsize:hist_range(2)]);
    bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./length(realtime_rip_start{plot_ii})/(hist_binsize/1000),0.5,'FaceColor',[0.0,0.5,0.8],'EdgeColor',[0,0,0],'LineWidth',1);
    title(sprintf('tet %d: multiunit triggered by fsdata reference', plot_ii));
    xlim(hist_range);
    grid on;
    
    subplot(length(all_realtime_rip_spktime_ind),2,plot_ii*2)
    [stim_mult_hist,stim_mult_hist_edges] = histcounts(all_smooth_rip_spktime_ind{plot_ii}/30, [hist_range(1):hist_binsize:hist_range(2)]);
    bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./length(smooth_rip_start{plot_ii})/(hist_binsize/1000),0.5,'FaceColor',[1,0,0],'EdgeColor',[0,0,0],'LineWidth',1);
    title(sprintf('tet %d: multiunit triggered by offline 3std crossing', plot_ii));
    xlim(hist_range);
    grid on;
end


figure(4);
subplot(5,1,1);
hist_binsize = 2;
hist_range = [-150, 250];
%histogram(all_stim_spktime_ind/30, [-500:2:500]);
[stim_mult_hist,stim_mult_hist_edges] = histcounts(vertcat(all_realtime_rip_spktime_ind{:})/30, [hist_range(1):hist_binsize:hist_range(2)]);
bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./length(vertcat(realtime_rip_start{:}))/(hist_binsize/1000),0.5,'FaceColor',[0.1,0.1,1.0],'EdgeColor',[0,0,0],'LineWidth',1);
title(sprintf('tet %d: multiunit triggered by fsdata reference', plot_ii));
xlim(hist_range);
grid on;

subplot(5,1,2);
[stim_mult_hist,stim_mult_hist_edges] = histcounts(vertcat(all_realtime2_rip_spktime_ind{:})/30, [hist_range(1):hist_binsize:hist_range(2)]);
bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./length(vertcat(realtime2_rip_start{:}))/(hist_binsize/1000),0.5,'FaceColor',[0.5,0.5,1.0],'EdgeColor',[0,0,0],'LineWidth',1);
title(sprintf('tet %d: multiunit triggered by fsdata reference', plot_ii));
xlim(hist_range);
grid on;

subplot(5,1,3);
[stim_mult_hist,stim_mult_hist_edges] = histcounts(vertcat(all_new_realtime_rip_spktime_ind)/30, [hist_range(1):hist_binsize:hist_range(2)]);
bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./length(new_lockout_start_ind)/(hist_binsize/1000),0.5,'FaceColor',[1,0,0],'EdgeColor',[0,0,0],'LineWidth',1);
title(sprintf('tet %d: multiunit triggered by new fsdata reference', plot_ii));
xlim(hist_range);
grid on;

subplot(5,1,4);
[stim_mult_hist,stim_mult_hist_edges] = histcounts(vertcat(all_smooth_rip_spktime_ind{:})/30, [hist_range(1):hist_binsize:hist_range(2)]);
bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./length(vertcat(smooth_rip_start{:}))/(hist_binsize/1000),0.5,'FaceColor',[1,0,0],'EdgeColor',[0,0,0],'LineWidth',1);
title(sprintf('tet %d: multiunit triggered by offline 3std crossing', plot_ii));
xlim(hist_range);
grid on;

subplot(5,1,5);
[stim_mult_hist,stim_mult_hist_edges] = histcounts(vertcat(all_kenny_rip_spktime_ind)/30, [hist_range(1):hist_binsize:hist_range(2)]);
bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./length(kenny_rip_start)/(hist_binsize/1000),0.5,'FaceColor',[1,0,0],'EdgeColor',[0,0,0],'LineWidth',1);
title(sprintf('tet %d: multiunit triggered by offline kenny 2std crossing', plot_ii));
xlim(hist_range);
grid on;
    
    